







产品标签功能：<br>
orderby：rand随机，date时间排序，ID排序<Br>
rows:返回记录数<Br>
page:传入分页条数<Br>
@product($product,$page typeid=56 orderby=date rows=10 page=1)

 {{ $product->title }}**{{ $product->created_at }}<br>

@endproduct

{{ $page->links() }}
<hr>


案例<br>
orderby：rand随机，date时间排序，ID排序<Br>
rows:返回记录数<Br>
page:传入分页条数<Br>
@cases($cases typeid=76 orderby=date rows=8 page=1)

{{ $cases->title }}**{{ $cases->created_at }}<br>

@endcases

{{ $casespage->links() }}
<hr>

文章<Br>
orderby：rand随机，date时间排序，ID排序<Br>
rows:返回记录数<Br>
page:传入分页条数<Br>
@article($article typeid=19 rows=10 orderby=date page=1)

 {{ $article->title }} ** ** {{ $article->created_at}} <br>

@endarticle

{{ $articlepage->links() }}
<hr>


栏目标签：<Br>
@column($column group=0 rows=5 orderby=rand page=10)

 {{ $column->name }} ** {{ $column->created_at }}  <br>

@endcolumn

<hr>

banners标签：<br>
@banners($banner)
    <a href="{{ $banner->link }}">{{ $banner->title }}{{ $banner->thumb}}</a><Br>
@endbanners

<hr>

typeid:取广告位信息<Br>
group：取广告列表信息<Br>
orderby：rand随机，date时间排序，ID排序<Br>
rows:返回记录数<Br>

@adv($adv group=1 rows=10 orderby=rand page=10)

 {{ $adv->title }}  ** {{ $adv->description}} <br>

@endadv

<hr>