@extends('admin.base')
@section('content')
<form class="layui-form" id="uploadForm">
    <fieldset class="layui-elem-field layui-field-title" style="margin-top: 30px;">
      <legend>一次可上传多张图</legend>
    </fieldset>
    <div class="layui-upload">
      <button type="button" class="layui-btn" id="uploadfile">多图上传</button> 
      <blockquote class="layui-elem-quote layui-quote-nm" style="margin-top: 10px;">
        预览图：
        <div class="layui-upload-list" id="uploadview"></div>
     </blockquote>
    </div>
</form>
@endsection
@section('script')
<script>
layui.use(['form', 'formX','upload','element'], function () {
            var $ = layui.jquery;
            var form = layui.form;
            var formX = layui.formX;
            var upload = layui.upload;
            var element = layui.element;
            //多图片上传
              upload.render({
                elem: '#uploadfile'
                ,url:  '{{ route("admin.case.atlas.store",['id'=>$productid]) }}'
                ,multiple: true
                ,before: function(obj){
                  obj.preview(function(index, file, result){
                    $('#uploadview').append('<img src="'+ result +'" alt="'+ file.name +'" class="layui-upload-img" width="80px">')
                  });
                }
                ,done: function(res){
                        if (res.code == 0) {
                            layer.msg(res.msg, {icon: 1});
                        } else {
                            layer.msg(res.msg, {icon: 2});
                        }
                }
              });
            $('#closeFormBtn').click(function () {
                $(this).parent().parent().parent().remove();
            });


         
});
</script>
@endsection