<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use DB;

class LinksServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // link模板标签开始 参数：循环对象
        Blade::directive('links', function ($expression) {
            $expressions = explode(',',$expression);
            // 循环对象
            isset($expressions[0]) ? $obj = $expressions[0] : $obj = '';

            // 解析字符串
            $parseStr  = $parseStr   = '<?php ';
            // 读取page信息
			
			$parseStr .= '$__LINKS__ = cache()->remember("cache_links_key", '.webconfig("SYSCACHETIME").', function (){ ';

            $parseStr .= 'return \App\Models\Link::where(\'status\',1)->orderBy(\'sort\', \'asc\')->get();';
			
		    $parseStr .=  '}); ';
			
			
			/*
			$parseStr .='<?php  $count = count($__LINKS__); ?>';
			$parseStr .='<?php  $depth = 1; ?>';
			$parseStr .='<?php  $parent = 0; ?>';*/
            $parseStr .= ' foreach($__LINKS__ as $key=>'.$obj.') { ?>';
			/*
			$parseStr .='<?php  $index = $key; ?>';
			$parseStr .='<?php  $iteration = $key+1; ?>';
			$parseStr .='<?php  $key ==0?$first = true:$first=false; ?>';
			$parseStr .='<?php  $key == ($count-1)?$last = true:$last=false; ?>';
			$parseStr .='<?php  $remaining = $count-($key+1); ?>';
			$parseStr .='<?php  $obj = ["index"=>$index,"iteration"=>$iteration,"first"=>$first,"last"=>$last,"remaining"=>$remaining,"depth"=>$depth,"parent"=>$parent]; ?>';
			$parseStr .='<?php  $loop = (object)$obj; ?>';*/
            return $parseStr;
        });

        // link模板标签结束
        Blade::directive('endlinks', function () {
            return "<?php } ?>";
        });
     
    }
}
