<!DOCTYPE html>
<html class="" lang="en">
<head>
<meta charset="UTF-8">
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>@yield('title')@webname</title>
<meta name="keywords" content="@yield('keyword')">
<meta name="description" content="@yield('description')">
<script>
var logo_black="@logo";
var logo="@logo";
</script>
<link rel="stylesheet" type="text/css" href="/css/h5/swiper-3.4.1.min.css" />
<link rel="stylesheet" href="/css/h5/style.min.css">
<script type="text/javascript" src="/js/h5/flexble.js"></script>
<script type="text/javascript" src="/js/h5/jquery-2.1.0.js"></script>
<script type="text/javascript" src="/js/h5/swiper-3.4.1.min.js"></script>
@yield('css')
@yield('topjs')
</head>
<body class="@yield('body')" id="@yield('bodyid')">
@include('theme::layouts.m')
@yield('footerjs')
<script src="/js/h5/index.js"></script>
@foreach(webconfigGroup(15) as $item)
@if($item->val)
{!! $item->val !!}
@endif
@endforeach
</body>
</html>