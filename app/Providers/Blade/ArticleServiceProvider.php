<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\ArticleTagServices;

class ArticleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::directive('article', function ($expression) {

            $ArticleTagObject = new ArticleTagServices($expression);

			$ARTICLEOBJECT = (object)$ArticleTagObject->tag;
			
            $parseStr  = $parseStr   =  '<?php ';

			if(isset($ArticleTagObject->useVal)){
				 
				 $parseStr .= $ArticleTagObject->object .'Reslut = cache()->remember('.$ArticleTagObject->sysTagcachekey.', '.$ArticleTagObject->tagcachetime.', function () use('.$ArticleTagObject->useVal.'){ ';
			}else{
				 $parseStr .= $ArticleTagObject->object .'Reslut = cache()->remember("'.$ArticleTagObject->sysTagcachekey.'", '.$ArticleTagObject->tagcachetime.', function () use($column){ ';

			}

            $parseStr .=$ArticleTagObject->object .'ArticleModel = \Article::query();';

            $parseStr .=$ArticleTagObject->object .'ArticleModel = '.$ArticleTagObject->object .'ArticleModel->with("column")->condition();';

            if(isset($ARTICLEOBJECT->typeid))  $parseStr .=$ArticleTagObject->object .'ArticleModel  = '.$ArticleTagObject->object .'ArticleModel'.$ARTICLEOBJECT->typeid;
			
            if(isset($ARTICLEOBJECT->flag))    $parseStr .=$ArticleTagObject->object .'ArticleModel  = '.$ArticleTagObject->object .'ArticleModel'.$ARTICLEOBJECT->flag;
			
            if(isset($ARTICLEOBJECT->rows))    $parseStr .=$ArticleTagObject->object .'ArticleModel  = '.$ArticleTagObject->object .'ArticleModel'.$ARTICLEOBJECT->rows;

            if(isset($ARTICLEOBJECT->orderby)) $parseStr .=$ArticleTagObject->object .'ArticleModel  = '.$ArticleTagObject->object .'ArticleModel'.$ARTICLEOBJECT->orderby;

  
			$parseStr .='return '. $ArticleTagObject->object .'Reslut  = '.$ArticleTagObject->object .'ArticleModel->get();});'; 
		  
	 
			$parseStr .=$ArticleTagObject->object .'count = count('.$ArticleTagObject->object .'Reslut); ';
 
			$parseStr .=' foreach('.$ArticleTagObject->object .'Reslut  as '.$ArticleTagObject->object .'key=>$item) { ?>';
			
			if($ArticleTagObject->length > 0){
				$ArticleTagObject->adorn?
				$parseStr .='<?php $item->title=cutstr($item->title,'.$ArticleTagObject->length.',"'.$ArticleTagObject->adorn.'");?>':
				$parseStr .='<?php $item->title=cutstr($item->title,'.$ArticleTagObject->length.');?>';
			
			}
			if($ArticleTagObject->desclength > 0){
				$ArticleTagObject->adorn?
				$parseStr .='<?php $item->description=cutstr($item->description,'.$ArticleTagObject->desclength.',"'.$ArticleTagObject->adorn.'");?>':
				$parseStr .='<?php $item->description=cutstr($item->description,'.$ArticleTagObject->desclength.');?>';
			}
			$parseStr .='<?php '. $ArticleTagObject->object .'= $item; ?>';
			
			$parseStr .='<?php  '. $ArticleTagObject->object .'index = '. $ArticleTagObject->object .'key; ?>';
			$parseStr .='<?php  '. $ArticleTagObject->object .'iteration = '. $ArticleTagObject->object .'key+1; ?>';
			$parseStr .='<?php  '. $ArticleTagObject->object .'key ==0?'. $ArticleTagObject->object .'first = true:'. $ArticleTagObject->object .'first=false; ?>';
			$parseStr .='<?php  '. $ArticleTagObject->object .'key == ('.$ArticleTagObject->object .'count-1)?'. $ArticleTagObject->object .'last = true:'. $ArticleTagObject->object .'last=false; ?>';
			$parseStr .='<?php  '. $ArticleTagObject->object .'remaining = '.$ArticleTagObject->object .'count-('. $ArticleTagObject->object .'key+1); ?>';
			$parseStr .='<?php  
								$obj = [
									"index"=>		'. $ArticleTagObject->object .'index,
									"iteration"=>	'. $ArticleTagObject->object .'iteration,
									"first"=>		'. $ArticleTagObject->object .'first,
									"last"=>		'. $ArticleTagObject->object .'last,
									"remaining"=>	'. $ArticleTagObject->object .'remaining
								];?>';
			$parseStr .='<?php  '. $ArticleTagObject->object .'loop = (object)$obj; ?>'; 

            return $parseStr;
        });

        Blade::directive('endarticle', function () {
			
            return " <?php } ?>";
			
        });
 
    }
}
