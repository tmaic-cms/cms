@extends('theme::layouts.wap')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
<style>
.conBoxNone {
    width: 100%;
    margin: 0 auto;
    position: relative;
    background: #fff;
	height:150px;line-height:150px;
	text-align:center;
}


</style>
@endsection
@section('topjs')
  <script type="text/javascript">
    var heasid = "";


    heasid = '76,134';

    headinit(heasid);
  </script>
@endsection
@section('body', '')
@section('bodyid', '')
@section('main') 
 

  <section class="zzc"></section>
  <section class="mian InpageMain">
    <section class="InpageHeader clearfix">
      <a href="javascript:history.go(-1)" class="back"></a>
      <p>{{$column->name}}</p>
      <span></span>
    </section>
    <!--分类-->
    <section class="InpageNav clearfix newsListNav">
      <section class="InpageNav-slide">
        <div class="InpageNav-left">
          <div class="InpageNav-list">
            <ul class="clearfix">

 
				@channel($newlink group=15 rows=5)		 
					  <li sid="{{$newlink->id}}"><i></i><a href="{{$newlink->link}}">{{$newlink->name}}</a></li>
					  <li class="sideline"></li>
				@endchannel

            </ul>
          </div>
        </div>
      </section>
    </section>
    <!-- newsList -->
    <section class="newsList">
      <div class="top">
	    @adv($newAdv group=20 rows=1)
        <img src="{{$newAdv->wapthumb}}" title="{{$column->name}}" alt="{{$column->name}}" />
		@endadv
        <div class="top-title">
          <h3>{{$column->name}}</h3>
          <span></span>
        </div>
      </div>
      <div class="conBox">
	 
		@article($article typeid=$column->column_mid orderby=date page=8)
		<dl class="clearfix">
          <dt>
            <a href="{{$article->link}}" id="ctl00_newsimg">
              <img src="{{$article->thumb}}" title="{{$article->title}}" alt="{{$article->seo_title}}">
            </a>
          </dt>
          <dd>
            <h3><a href="{{$article->link}}">{{cutstr($article->title,20)}}</a></h3>
            <p><a href="{{$article->link}}">{{cutstr($article->description,50)}}</a></p>
            <span class="zan">{{$article->click}}</span>
            <span class="time">{{$article->created_at}}</span>
          </dd>
        </dl>
		@endarticle
 
        @empty($articleCount)
		<div class="conBoxNone">
            没有相关记录
        </div>
		@endempty
		

      </div>

	  {{$articlepage->links()}}

    </section>
  </section>
@endsection
@section('footerjs')
 
  <script type="text/javascript">
    var sid = '{{$column->id}}';
  </script>
 
@endsection










