<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="/favicon.ico" rel="icon">

    <title>{{webconfig('site_title')}}</title>
    <link rel="stylesheet" href="/assets/libs/layui/css/layui.css" />
    <link rel="stylesheet" href="/assets/module/admin.css?v=318" />
    <link rel="stylesheet" href="/assets/module/theme-blue.css?v=318" />
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<script>
var adminindex = '{{route("admin.index1")}}';
</script>
<style>
.theme-tmaic .layui-footer {left: 200px;}
#layout_index_drop li,#layout_index_user_drop li{line-height:40px;}

.menuData{font-weight:bold;font-size:18px;}
 
</style>

<body class="layui-layout-body theme-tmaic close-footer" data-theme="theme-tmaic">
    <div class="layui-layout layui-layout-admin">
        <!-- 头部 -->
        <div class="layui-header">
            <div class="layui-logo">
                <cite>{{webconfig('site_title')}}</cite>
            </div>
            <ul class="layui-nav layui-layout-left">
            <li class="layui-nav-item" lay-unselect>
                <a ew-event="flexible" title="侧边伸缩"><i class="layui-icon layui-icon-shrink-right"></i></a>
            </li>
            {{-- 小屏幕下变为下拉形式
            <li class="layui-nav-item layui-hide-xs layui-this menuData" data="" lay-unselect><a nav-bind="xt1">控制台</a></li>
            <li class="layui-nav-item layui-hide-sm layui-show-xs-inline-block" lay-unselect>
                <a>更多</a>
                <dl class="layui-nav-child">
                    <dd class="menuData" lay-unselect><a nav-bind="xt1">控制台</a></dd>
                    <dd class="menuData" lay-unselect><a nav-bind="xt2">栏目管理</a></dd>
                    <dd class="menuData" lay-unselect><a nav-bind="xt3">内容管理</a></dd>
                </dl>
            </li>
            --}}

            </ul>
            <ul class="layui-nav layui-layout-right">
                <li class="layui-nav-item" lay-unselect>
                    <a ew-event="refresh" title="刷新"><i class="layui-icon layui-icon-refresh-3"></i></a>
                </li>
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a href="/" target="_blank" title="前台">
                        <i class="layui-icon layui-icon-website"></i>
                    </a>
                </li>

                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a ew-event="fullScreen" title="全屏"><i class="layui-icon layui-icon-screen-full"></i></a>
                </li>
                <li class="layui-nav-item layui-hide-xs" lay-unselect>
                    <a ew-event="lockScreen" title="锁屏"><i class="layui-icon layui-icon-password"></i></a>
                </li>
                <li class="layui-nav-item dropdown-menu dropdown-hover" title="清除缓存" lay-filter="clearData">              
                    <a class="layui-nav-item">&nbsp;清除缓存 <i class="layui-icon layui-icon-drop"></i></a>
                    <ul class="dropdown-menu-nav" id="layout_index_drop">
                        <div class="dropdown-anchor"></div>
                        <li><a ><i class="layui-icon layui-icon-star-fill"></i><font style="color:#f80000">数据</font>缓存</a></li>
                        <li><a ><i class="layui-icon layui-icon-template-1"></i><font style="color:#f80000">模板</font>缓存</a></li>
                        <li><a class="clearData"><i class="layui-icon layui-icon-set-fill"></i><font style="color:#f80000">全部</font>缓存</a></li>
                    </ul>
               </li>
                <li class="layui-nav-item dropdown-menu dropdown-hover">              
                    <a class="layui-nav-item"><img src="/assets/images/head.jpg" class="layui-nav-img">
                         <cite>{{auth()->user()->nickname ?? auth()->user()->username}}</cite><i class="layui-icon layui-icon-drop"></i></a>
                    <ul class="dropdown-menu-nav" id="layout_index_user_drop">
                        <div class="dropdown-anchor"></div>
						 
                        <li lay-unselect><a ew-href="{{route('admin.admins.changePassword')}}">修改密码</a></li>
						<!--
                        <li lay-unselect><a ew-event="psw" data-url="{{route('admin.admins.changePassword')}}">修改密码</a></li>
						-->
                        <li lay-unselect><a ew-event="logout" data-url="{{route('admin.logout')}}">退出</a></li>
                    </ul>
               </li>

                <li class="layui-nav-item" lay-unselect>             
                    <a ew-event="theme" title="主题"><i class="layui-icon layui-icon-more-vertical"></i></a>
                </li>
            </ul>
        </div>
        <!-- 侧边栏 -->
        <div class="layui-side">
            <div class="layui-side-scroll">
            <ul class="layui-nav layui-nav-tree" nav-id="xt1" lay-filter="admin-side-nav" id="admin-side-nav" style="margin: 15px 0;">

                    <li class="layui-nav-item">
                        <a><i class="layui-icon layui-icon-home"></i>&emsp;<cite>控制台</cite></a>
                        <dl class="layui-nav-child">
                            <!-- <dd><a lay-href="{{route('admin.index')}}">工作台</a></dd> -->
                            <dd><a lay-href="{{route('admin.index1')}}">控制台</a></dd>
                           
                        </dl>
                    </li>

                        @foreach($menus as $menu)
                            @can($menu->name)
                        <li data-name="{{$menu->name}}" class="layui-nav-item">
                            <a href="javascript:;"  lay-direction="{{$loop->index}}">
                                <i class="layui-icon {{$menu->icon->class??''}}"></i>
                                <cite>{{$menu->display_name}}</cite>
                            </a>
                            @if($menu->childs->isNotEmpty())
                            <dl class="layui-nav-child">
                                @foreach($menu->childs as $subMenu)
                                  @can($subMenu->name)
                                    <dd data-name="{{$subMenu->name}}">
                                        <a lay-href="{{ route($subMenu->route) }}">{{$subMenu->display_name}}</a>
                                    </dd>
                                  @endcan
                                @endforeach
                            </dl>
                            @endif
                        </li>
                         @endcan
                        @endforeach                 
            </ul>
        </div>
    </div>
       
       
        <!-- 主体部分 -->
        <div class="layui-body"></div>
        <!-- 底部 -->
        <div class="layui-footer layui-text">
            copyright © 2020 <a href="http://www.qudoon.cn" target="_blank">QUDOON</a> all rights reserved.
            <span class="pull-right">Version 3.1.8</span>
        </div>
    </div>
 
<!-- js部分 -->
<script type="text/javascript" src="/assets/libs/layui/layui.js"></script>
<script type="text/javascript" src="/assets/js/common.js?v=318"></script>

<!-- 侧边栏渲染模板 -->
<script id="sideNav" type="text/html">
    @{{#  layui.each(d, function(index, item){ }}
    <li class="layui-nav-item">
        <a lay-href="@{{item.url}}"><i class="@{{item.icon}}"></i>&emsp;<cite>@{{ item.name }}</cite></a>
        @{{# if(item.subMenus&&item.subMenus.length>0){ getSubMenus(item.subMenus); } }}
    </li>
    @{{#  }); }}
    @{{# function getSubMenus(subMenus){ }}
    <dl class="layui-nav-child">
        @{{# layui.each(subMenus, function(index, subItem){ }}
        <dd>
            <a lay-href="@{{ subItem.url }}">@{{ subItem.name }}</a>
            @{{# if(subItem.subMenus&&subItem.subMenus.length>0){ getSubMenus(subItem.subMenus); } }}
        </dd>
        @{{# }); }}
    </dl>
    @{{# } }}
</script>


<script>
var module = '/assets/module/'
layui.use(['layer','admin', 'index', 'laytpl','dropdown', 'element'], function () {
	var $ = layui.jquery;
	var index = layui.index;
	var laytpl = layui.laytpl;
	var element = layui.element;
	var admin = layui.admin;
	index.loadHome({
		menuPath: adminindex,
		menuName: '<i class="layui-icon layui-icon-home"></i>'
	});
	$(".clearData").bind("click",function(){
		$.get("{{ route('admin.common.all') }}", {}, function (res) {
				layer.msg("清除成功!", {icon: 1})
		});
	});

 /***
$.post("{{ route('admin.permission.menuData') }}", function (res) {

	laytpl(sideNav.innerHTML).render(res.data, function (html) {
		$("#admin-side-nav").html(html);
		element.render('nav');
		admin.activeNav(adminindex);
	});

}, 'json');
$(".menuData").bind("click",function(){
var data = $(this).attr("data");  	   
	$.post("{{ route('admin.permission.menuData') }}?id="+data, function (res) {
		laytpl(sideNav.innerHTML).render(res.data, function (html) {
			$("#admin-side-nav").html(html);
			element.render('nav');

		});
	}, 'json');
});
***/
});
</script>
</body>
</html>