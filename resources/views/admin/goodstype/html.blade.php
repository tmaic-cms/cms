
<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('adv.advert.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('adv.advert.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>
<!-- 表格状态列 -->
<script type="text/html" id="tbState">
    <input type="checkbox" lay-filter="tbState" value="@{{d.id}}" lay-skin="switch" lay-text="正常|禁用" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'正常':'禁用'}}</p>
</script>

 
<script type="text/html" id="ismenu">
    <input type="checkbox" lay-filter="ismenu"  value="@{{d.id}}" lay-skin="switch" lay-text="菜单|栏目" @{{d.ismenu==0?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.ismenu==0?'正常':'锁定'}}</p>
</script>
 



<script type="text/html" id="thumb">
    @{{#  if(d.thumb){ }}
    <a href="@{{d.thumb}}" target="_blank" title="点击查看">
        <img src="@{{d.thumb}}" alt="" width="28" height="28">
    </a>
    @{{#  } }}
</script>