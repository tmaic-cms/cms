layui.use(['form', 'formX'], function () {
        var $ = layui.jquery;
        var form = layui.form;
        var formX = layui.formX;
        // 监听表单提交
        form.on('submit(SubmitData)', function (data) {
            layer.msg('表单验证通过', {icon: 1});
            return false;
        });
});