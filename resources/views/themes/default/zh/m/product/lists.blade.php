@extends('theme::layouts.h5')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'case')
@section('bodyid', 'top')
@section('main') 
<div class="box1">
    <ul>
	 @channel($productcolumn top=0 rows=10)
      <li class="transition @if($productcolumn->column_mid ==$column->column_mid) active @endif">
        <a href="{{$productcolumn->link}}"><h1>{{$productcolumn->name}}</h1></a>
      </li>
	 @endchannel
    </ul>
    <div class="caselist">
		<div class="casenr transition active">
			<ul>
			@list($product typeid=$column->column_mid orderby=date page=15)
			  <li>
				<a href="{{$product->link}}">
				 <img src="{{$product->thumb}}" title="{{$product->title}}" alt="{{$product->seo_title}}" />
				  <p class="transition">{{$product->title}}</p>
				</a>
			  </li>
			@endlist
			</ul>
        {{$productpage->links()}}
		</div>	  
    </div>
</div>
@endsection
@section('footerjs')
@endsection










