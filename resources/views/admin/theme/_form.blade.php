<input name="id" type="hidden" value=""/>	
<div class="layui-form-item">
	<label class="layui-form-label layui-form-required">主题名称:</label>
	<div class="layui-input-block">
		<input name="title" placeholder="请输入主题名称" class="layui-input" lay-verType="tips" lay-verify="required"  value="{{ $themes->title??'' }}"/>
	</div>
</div>

<div class="layui-form-item">
	<label class="layui-form-label layui-form-required">目录名:</label>
	<div class="layui-input-block">
		<input name="themename" placeholder="请输入目录名" class="layui-input" lay-verType="tips" lay-verify="required"  value="{{ $themes->themename??'' }}"/>
	</div>
</div>

<div class="layui-form-item">
	<label class="layui-form-label layui-required">终端属性:</label>
	<div class="layui-input-block">
		<input type="radio" name="type" value="1" title=" PC端主题" lay-verify="otherReq" lay-filter="type">
		<input type="radio" name="type" value="2" title=" H5主题" lay-verify="otherReq"  lay-filter="type">
	</div>
</div>

 
<div class="layui-form-item">
	<label for="" class="layui-form-label">主题封面</label>
	 <div class="layui-upload-drag uploadPic" id="uploadViewUpload">
	 @empty($themes->thumb)
	  <i class="layui-icon"></i>
	  <p>点击上传，或将文件拖拽到此处</p>
	 @else
	  <div class=" " id="uploadView">
		<img src="{{ $themes->thumb}}" alt="上传成功后渲染" style="max-width: 120px">
	  </div>
	  @endempty 
	  <input type="hidden" name="thumb" id="thumb" class="layui-upload-input" value="">
	</div>
</div>
  
<div class="layui-form-item">
	<label class="layui-form-label layui-form-required">主题介绍:</label>
	<div class="layui-input-block">
		<textarea name="desc" lay-verify="required" placeholder="主题描述..." lay-verType="tips" class="layui-textarea">{{ $themes->desc??'' }}</textarea>
	</div>
</div>

<div class="layui-form-item">
	<label class="layui-form-label layui-form-required">当前主题:</label>
	<div class="layui-input-block">
		<input type="radio" name="selected" value="1" title="设置为当前主题" lay-filter="setTheme">
		<input type="radio" name="selected" value="0" title="取消主题" lay-filter="setTheme">
	</div>
</div>
<div class="text-center" style="position: fixed;bottom: 0;left: 0;right: 0;background-color: #fff;box-shadow: 0 -1px 5px rgba(0,0,0,.15);padding: 15px;">
	<button class="layui-btn" lay-filter="SubmitForm" id="SubmitForm" lay-submit>保存</button>
	<button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
</div>

