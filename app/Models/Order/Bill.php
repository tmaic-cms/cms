<?php 
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/    
namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;
 
class Bill extends Model
{
    
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'order_bill';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = true;
    
    
    protected $fillable = ['start_date','end_date','order_totals','order_costtotals','shipping_totals','order_return_totals','store_cost_totals','result_totals','create_date','month','status','pay_date','pay_content','pay_img','store_id','store_name','store_points_offset_totals','platform_points_offset_totals','order_return_store_points_totals','order_return_platform_points_totals','orderid','order_no','trade_no'];
    
 

}
        
 