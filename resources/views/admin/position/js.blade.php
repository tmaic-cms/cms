@include('admin.position.html')
<script type="text/javascript" src="/assets/js/position.js"></script>
<script>
    var create = "{{ route('admin.position.create') }}";
    var edit = "{{ route('admin.position.edit') }}?id=";
    var update = "{{ route('admin.position.update') }}";
    var updateAjax = "{{ route('admin.case.updateAjax') }}";
    var destroy ="{{ route('admin.position.destroy') }}";
    var tableData = "{{ route('admin.position.data') }}?pid=";


    var AtlasData = "{{ route('admin.case.atlas.data') }}";
    var AtlasDestroy = "{{ route('admin.case.atlas.destroy') }}";
    var AtlasUpload= "{{ route('admin.case.atlas.upload') }}";
    var AtlasStore= "{{ route('admin.case.atlas.store') }}";


</script>