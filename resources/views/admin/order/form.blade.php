<div class="layui-card-body">
    <div class="layui-form-item">
        <label class="layui-form-label">*广告位</label>
        <div class="layui-input-block">
            <select name="position_id" lay-verify="required">
                <option value=""></option>
                @foreach($positions as $position)
                    <option value="{{ $position->id }}" {{ $position->selected??'' }} >{{$loop->iteration}}-{{ $position->title }}</option>
                @endforeach
            </select>

        </div>
    </div>


    <div class="layui-form-item">
        <label class="layui-form-label">*广告标题</label>
        <div class="layui-input-block">
            <input type="text" name="title" value="{{ $advert->title ?? old('name') }}" lay-verify="required" placeholder="请输入标题" class="layui-input" >
        </div>
    </div>
    <div class="layui-form-item">
        <label class="layui-form-label">副标题</label>
        <div class="layui-input-block">
            <input type="text" name="subtitle" value="{{ $advert->subtitle ?? old('subtitle') }}"  placeholder="请输入副标题" class="layui-input" >
        </div>
    </div>
{{--
    <div class="layui-form-item">
        <label class="layui-form-label">位置名称</label>
        <div class="layui-input-block">
            <input type="text" name="position_name" value="{{ $advert->position_name ?? old('position_name') }}" lay-verify="required" placeholder="请输入位置名称" class="layui-input" >
        </div>
    </div>
--}}


    <div class="layui-form-item">
        <label class="layui-form-label">缩略图</label>
        <div class="layui-input-block">
            <div class="layui-upload">
                        <button type="button" class="layui-btn" id="uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传</button>
                        <div class="layui-upload-list" >
                            <ul id="layui-upload-box" class="layui-clear">
                                @if(isset($advert->thumb))
                                    <li><img src="{{ $advert->thumb }}" /><p>上传成功</p></li>
                                @endif
                            </ul>
                            <input type="hidden" name="thumb" id="thumb" value="{{ $advert->thumb??'' }}" lay-verify="required" lay-reqText="缩略图不能为空!">
                        </div>
                    </div>
        </div>
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">排序</label>
        <div class="layui-input-block">
            <input type="number" name="sort" value="{{ $advert->sort ?? 0 }}" lay-verify="required|number" placeholder="请输入数字" class="layui-input" >
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label">链接</label>
        <div class="layui-input-block">
            <input type="text" name="link" value="{{ $advert->link ?? '' }}" placeholder="请输入链接地址" class="layui-input" >
            <div class="layui-form-mid"><span class="layui-word-aux">格式：http://xxxxx</span></div>
        </div>
        
    </div>



    <div class="layui-form-item">
        <label class="layui-form-label">描述</label>
        <div class="layui-input-block">
            <textarea name="description" placeholder="请输入描述" class="layui-textarea">{{$advert->description??old('description')}}</textarea>
        </div>
        
    </div>
</div>
<div class="form-group-bottom text-right">
    <button type="reset" class="layui-btn layui-btn-primary">&emsp;重置&emsp;</button>
    <button class="layui-btn" lay-filter="SubmitData" lay-submit>&emsp;提交&emsp;</button>
</div>
<style>
    #layui-upload-box li{
        width: 120px;
        height: 100px;
        float: left;
        position: relative;
        overflow: hidden;
        margin-right: 10px;
        border:1px solid #ddd;
    }
    #layui-upload-box li img{
        width: 100%;
    }
    #layui-upload-box li p{
        width: 100%;
        height: 22px;
        font-size: 12px;
        position: absolute;
        left: 0;
        bottom: 0;
        line-height: 22px;
        text-align: center;
        color: #fff;
        background-color: #333;
        opacity: 0.6;
    }
    #layui-upload-box li i{
        display: block;
        width: 20px;
        height:20px;
        position: absolute;
        text-align: center;
        top: 2px;
        right:2px;
        z-index:999;
        cursor: pointer;
    }
</style>
<script>
    layui.use(['upload'],function () {
        var upload = layui.upload
        //普通图片上传
        var uploadInst = upload.render({
            elem: '#uploadPic'
            ,url: '{{ route("uploadImg") }}'
            ,multiple: false
            ,data:{"_token":"{{ csrf_token() }}"}
            ,before: function(obj){
                //预读本地文件示例，不支持ie8
                /*obj.preview(function(index, file, result){
                 $('#layui-upload-box').append('<li><img src="'+result+'" /><p>待上传</p></li>')
                 });*/
                obj.preview(function(index, file, result){
                    $('#layui-upload-box').html('<li><img src="'+result+'" /><p>上传中</p></li>')
                });

            }
            ,done: function(res){
                //如果上传失败
                if(res.code == 0){
                    $("#thumb").val(res.url);
                    $('#layui-upload-box li p').text('上传成功');
                    return layer.msg(res.msg);
                }
                return layer.msg(res.msg);
            }
        });
    });
</script>

