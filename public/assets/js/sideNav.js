<!-- 侧边栏渲染模板 -->
<script id="sideNav" type="text/html">
    {{#  layui.each(d, function(index, item){ }}
    <li class="layui-nav-item">
        <a lay-href="{{item.url}}"><i class="{{item.icon}}"></i>&emsp;<cite>{{ item.name }}</cite></a>
        {{# if(item.subMenus&&item.subMenus.length>0){ getSubMenus(item.subMenus); } }}
    </li>
    {{#  }); }}
    {{# function getSubMenus(subMenus){ }}
    <dl class="layui-nav-child">
        {{# layui.each(subMenus, function(index, subItem){ }}
        <dd>
            <a lay-href="{{ subItem.url }}">{{ subItem.name }}</a>
            {{# if(subItem.subMenus&&subItem.subMenus.length>0){ getSubMenus(subItem.subMenus); } }}
        </dd>
        {{# }); }}
    </dl>
    {{# } }}
</script>