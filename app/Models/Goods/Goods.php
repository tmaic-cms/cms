<?php 
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/    
namespace App\Models\Goods;

use Illuminate\Database\Eloquent\Model;
 
class Goods extends Model
{
    
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'goods';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = false;
    
    
    protected $fillable = ['id','catid','GoodsName','goods_class','store_id','goods_image','goods_content','goods_state','price','market_price','cost_price','goods_stock','is_delete','unit','sort','sale_num','sku_id','goods_service_ids','virtual_sale','max_buy','min_buy','status','created_at','updated_at','delete_time'];
    
    

}
        
 