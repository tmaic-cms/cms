<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;


class ColumnTagServices
{
   
    public $expression;
    public $musterVal;
    public $sqlstr;
    public $object;
    public $columnid;
    public $rows;
    public $group;
    public $pageObj;
    public $orderby;
    public $page;
    public $flag;
    public $locale;
    public $tag;




    public function __construct($expression)
    {
       //出始化分页对象
	   $this->pageObj = NULL;
       $this->decompose($expression);

    }


    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		
		$this->musterVal = explode(',',current($this->expression));
		
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
			
		}else if(count($this->musterVal) === 2){
			
			//list($this->object,$this->pageObj) = explode(',',current($this->expression));
			// 循环对象
			$this->object = current($this->musterVal);  
			// 分页对象
			$this->pageObj = next($this->musterVal);  
			
		}
		


        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		


        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
        }

        if(array_key_exists("typeid",$this->tag)){
          $this->typeid($this->tag['typeid']);
        } 

        if(array_key_exists("flag",$this->tag)){
          $this->flag($this->tag['flag']);
        } 

        if(array_key_exists("group",$this->tag)){
          $this->group($this->tag['group']);
        } 


        if(array_key_exists("orderby",$this->tag)){
          $this->orderby($this->tag['orderby']);
        } 
        if(array_key_exists("rows",$this->tag)){
          $this->rows($this->tag['rows']);
        } 
        if(array_key_exists("page",$this->tag)){
          $this->page($this->tag['page']);
        }
    }

    
    public function typeid($columnid)
    {
        $this->columnid = $columnid; 

        $this->tag['typeid']= "->whereIn('column_mid',[".$this->columnid."]);";   

    }
    public function group($group = 10)
    {
        $this->group = $group;
        $this->tag['group']= "->whereIn('pid',[".$this->group."]);";   
        
    }

    public function rows($rows = 10)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows)";   
        
    }

    public function orderby($orderby = 'id')
    {
        $value = null;
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case 'date':
                $value = '->orderBy("created_at","desc");';
                break;

            default: $value = '->orderBy("id","desc");';break;
        }
        

      
        $this->tag['orderby']= $value;
        
    }

    public function page($page = 10)
    {
        $this->page = $page;
        $this->tag['page']= "->paginate($page);";   
        
    }


  
    public function flag($flag)
    {
        $this->flag = $flag;
        $this->tag['flag']= '->where("flag",'.$flag.')';    
       
    }
















}
