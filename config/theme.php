<?php
/**
 * theme全局配置
 * User: pangxianfei
 * Date: 2020/8/14
 * Time: 上午11:50
 */

return [
    
    'theme'   => env('WEB_THEME','default'),
    'skin'    => env('WEB_SKIN','default'),
 
    
    'skins' => [
        'default' => '默认',
        'dark' => '深色',
        'blue' => '深蓝',
        'green' => '青绿',
        'warm-red' => '暖红',
        'light' => '浅色'

    ],
  
];