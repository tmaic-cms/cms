@include('admin.ad.html')
<script>
    var create = "{{ route('admin.ad.create') }}";
    var createposition = "{{ route('admin.ad.position.create') }}";

    var edit = "{{ route('admin.ad.edit') }}?id=";
    var positionedit = "{{ route('admin.ad.position.edit') }}?id=";
    var update = "{{ route('admin.ad.update') }}";
    var updateAjax = "{{ route('admin.case.updateAjax') }}";
    var destroy ="{{ route('admin.ad.destroy') }}";
    var tableData = "{{ route('admin.ad.data') }}?pid=";
    var AtlasData = "{{ route('admin.case.atlas.data') }}";
    var AtlasDestroy = "{{ route('admin.case.atlas.destroy') }}";
    var AtlasUpload= "{{ route('admin.case.atlas.upload') }}";
    var AtlasStore= "{{ route('admin.case.atlas.store') }}";
</script>
<script type="text/javascript" src="/assets/js/ad.js"></script>