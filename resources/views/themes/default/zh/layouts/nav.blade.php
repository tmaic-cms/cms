@if($isMobile)
<header class="transition">
    <a class="logo" href="###">
      <img src="@logo" title="logo" alt="logo">
    </a>
    <div class="menu">
		<a href="{{ route('lang',['language'=>'zh']) }}" class="m-lang transition">中文</a>
		<a href="{{ route('lang',['language'=>'en']) }}" class="m-lang transition">EN</a>
		
	
      <a class="menu-btn">
        <i class="transition"></i>
        <i class="transition"></i>
        <i class="transition"></i>
      </a>
    </div>
    <div class="header-nav__cont transition">
      <a class="close-nav iconfont">&#xe61e;</a>
      <nav class="navigation">
        <ul class="nav-list">
			<li class="nav-item__sub ">
            <a href="{{ route('home') }}"class="nav-link">@index</a>
		   </li>
		@nav($nav group=0)
          <li class="nav-item__sub ">
            <a @if(!$navson->count()) href="{{$nav->link}}" @endif class="nav-link">
              {{$nav->name}}
              @if($navson->count())<i class="iconfont">&#xe602;</i>@endif
            </a>
			@if($navcount) 
            <div class="meganav-pane transition">
              <a href="###" class="iconfont back-nav">&#xe62b;</a>
              <a href="###" class="iconfont close-nav">&#xe61e;</a>
              <div class="meganav-pane__box">
                <section class="meganav-pane__inner">
                  <div class="meganav-subnav__content">
                    <ul>
					@nav($navson group=$nav->child)
                      <li>
                        <a href="{{$navson->link}}">{{$navson->name}}
                         @if($navsonloop->first) <span class="more-btn">More<i class="iconfont">&#xe602;</i></span>@endif
                        </a>
                      </li>
					@endnav
                    </ul>
                  </div>
                </section>
              </div>
            </div>
			@endif 
          </li>
		 @endnav 
        </ul>
      </nav>
	  
 
      </div>
    </div>
  </header>
@else
  
 <header>
    <div class="logo">
      <img src="@logo" title="@webname" alt="@webname">
    </div>
    <div class="link">
      <ul>
        <li>
          <a href="{{ route('home') }}">@index</a>
        </li>
	    @nav($nav group=0)
        <li>
          <a href="{{$nav->link}}">{{$nav->name}} @if($navson->count())<i class="iconfont">&#xe620;</i> @endif</a>
		  @if($navson->count())
          <ul class="transition">
	        @nav($navson group=$nav->child)   
            <li>
              <a href="{{$navson->link}}">{{$navson->name}}</a>
            </li>
			@endnav
          </ul>
		  @endif
        </li>
		@endnav
      </ul>
    </div>
    <div class="header-sub">
	  <a href="{{ route('lang',['language'=>'zh']) }}" class="hd-lang">中文</a>
	  <a href="{{ route('lang',['language'=>'en']) }}" class="hd-lang">EN</a>
    </div>
    <section class="search-full">
      <div class="search-head">
        <form action="" method="get" autocomplete="off">
          <div class="search-content">
            <input type="text" placeholder="Search..." name="keywords">
            <button type="button" class="iconfont search">&#xe648;</button>
          </div>
        </form>
      </div>
      <a class="iconfont close">&#xe61e;</a>
    </section>
    <div class="search-bg"></div>
  </header>
  
  
  
  
  
  
@endif