<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\CaseTagServices;




class CasesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
	public $loop;
	
    public function register() {
		
	}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::directive('cases', function ($expression) {

            $CASETAGOBJECT = new CaseTagServices($expression);
 
 			$CASEOBJECT = (object)$CASETAGOBJECT->tag;
 
            $parseStr  = $parseStr   =  '<?php ';
 
			if($CASETAGOBJECT->useVal){
				
				 $parseStr .= $CASETAGOBJECT->object .'Reslut = cache()->remember('.$CASETAGOBJECT->sysTagcachekey.', '.$CASETAGOBJECT->tagcachetime.', function () use('.$CASETAGOBJECT->useVal.'){ ';

			}else{ 
		         $parseStr .= $CASETAGOBJECT->object .'Reslut = cache()->remember("'.$CASETAGOBJECT->sysTagcachekey.'", '.$CASETAGOBJECT->tagcachetime.', function () use($column) { ';

			}

            $parseStr .=$CASETAGOBJECT->object .'CasesModel = \App\Models\Cases\CasesTranslation::query();';

            $parseStr .=$CASETAGOBJECT->object .'CasesModel = '.$CASETAGOBJECT->object.'CasesModel->with("column")->condition();';
 
            if(isset($CASEOBJECT->typeid))   $parseStr .=$CASETAGOBJECT->object .'CasesModel = '.$CASETAGOBJECT->object.'CasesModel'.$CASEOBJECT->typeid;
            if(isset($CASEOBJECT->rows))     $parseStr .=$CASETAGOBJECT->object .'CasesModel = '.$CASETAGOBJECT->object.'CasesModel'.$CASEOBJECT->rows;
            if(isset($CASEOBJECT->orderby))  $parseStr .=$CASETAGOBJECT->object .'CasesModel = '.$CASETAGOBJECT->object.'CasesModel'.$CASEOBJECT->orderby;
 
			$parseStr .='return '.$CASETAGOBJECT->object .'Reslut = '.$CASETAGOBJECT->object.'CasesModel->get();'; 
 
			$parseStr  .=  ' ;}); ';

			$parseStr .=$CASETAGOBJECT->object .'count = count('.$CASETAGOBJECT->object .'Reslut); ';
			
			$parseStr .='  $count = count('.$CASETAGOBJECT->object .'Reslut); ';
			$parseStr .=$CASETAGOBJECT->object .'count = count('.$CASETAGOBJECT->object .'Reslut); ';
 
			$parseStr .=' foreach('.$CASETAGOBJECT->object .'Reslut  as '.$CASETAGOBJECT->object .'key=>$item) { ?>';
			
			if($CASETAGOBJECT->length > 0){
				$CASETAGOBJECT->adorn?
				$parseStr .='<?php $item->title=cutstr($item->title,'.$CASETAGOBJECT->length.',"'.$CASETAGOBJECT->adorn.'");?>':
				$parseStr .='<?php $item->title=cutstr($item->title,'.$CASETAGOBJECT->length.');?>';
			}
			
			if($CASETAGOBJECT->desclength > 0){
				$CASETAGOBJECT->adorn?
				$parseStr .='<?php $item->description=cutstr($item->description,'.$CASETAGOBJECT->desclength.',"'.$CASETAGOBJECT->adorn.'");?>':
				$parseStr .='<?php $item->description=cutstr($item->description,'.$CASETAGOBJECT->desclength.');?>';
			}
			$parseStr .='<?php '. $CASETAGOBJECT->object .'= $item; ?>';
 
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'index = '.$CASETAGOBJECT->object .'key; ?>';
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'iteration = '.$CASETAGOBJECT->object .'key+1; ?>';
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'first ='.$CASETAGOBJECT->object .'key==0?$first = true:$first=false; ?>';
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'last = ('.$CASETAGOBJECT->object .'count-1)?$last = true:$last=false; ?>';
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'remaining = '.$CASETAGOBJECT->object .'count-('.$CASETAGOBJECT->object .'key+1); ?>';
			$parseStr .='<?php  $obj = [
							"index"=>		'. $CASETAGOBJECT->object .'index,
							"iteration"=>	'. $CASETAGOBJECT->object .'iteration,
							"first"=>		'. $CASETAGOBJECT->object .'first,
							"last"=>		'. $CASETAGOBJECT->object .'last,
							"remaining"=>	'. $CASETAGOBJECT->object .'remaining
				]; ?>';
			$parseStr .='<?php  '.$CASETAGOBJECT->object .'loop = (object)$obj; ?>';	
			 

            return $parseStr;
        });

        Blade::directive('endcases', function () {
            return " <?php } ?>";
        });
 
    }
}
