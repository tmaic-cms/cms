<form class="layui-form" action="" lay-filter="category_form">

<input name="sid" type="hidden" value="{{ $category->sid ?? old('sid') }}"/>
 
<style>
.layui-form-label.layui-required:after {
content: "*";
color: red;
position: absolute;
top: 10px;
left: 15px;
}
</style>
<div class="layui-form" style="">
	<div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
		<ul class="layui-tab-title tab_my">
			<li class="layui-this">基本信息</li>
			<li>高级选项</li>
			<li>栏目seo</li>
			<li>栏目内容</li>
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<div class="layui-form-item">
					<label class="layui-form-label layui-required">栏目分类</label>
					<div class="layui-input-inline">
						<select name="pid" lay-verify="required" lay-reqtext="栏目分类必填项，岂能为空？">
							<option value="0">顶级分类</option>
							@foreach($categories as $item)
								<option value="{{ $item->sid }}" @if(isset($category->pid)&&$category->pid==$category->id)selected @endif >{{ $item->name }}</option>
								@if(count($item->children))
									@foreach($item->children as $two)
										<option value="{{ $two->sid }}" @if(isset($category->pid)&&$category->pid==$two->sid) selected @endif >&nbsp;┗━{{ $two->name }}</option>
										@if(count($two->children))
											@foreach($two->children as $Three)
												<option value="{{ $Three->sid }}" @if(isset($category->pid)&&$category->pid==$Three->sid) selected @endif >&nbsp;&nbsp;┗━{{ $Three->name }}</option>
											@endforeach
										@endif
									@endforeach
								@endif
							@endforeach

						</select>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label layui-required">栏目名称</label>
					<div class="layui-input-block">
						<input type="text" name="name" id="name" lay-verify="required"
							   value="{{ $category->name ?? old('name') }}" autocomplete="off" placeholder="请输入栏目名称"
							   class="layui-input">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">副标题名称</label>
					<div class="layui-input-block">
						<input type="text" name="sub_name" id="sub_name" lay-verify="required"
							   value="{{ $category->sub_name ?? old('sub_name') }}" autocomplete="off" placeholder="请输入副标题名称"
							   class="layui-input">
					</div>
				</div>


				<div class="layui-form-item">
					<label class="layui-form-label layui-required">内容模型</label>
					<div class="layui-input-inline">
						<select name="model" lay-verify="required" lay-reqtext="内容模型必填项，岂能为空？">
							<option value="">请选择模型</option>
							<option value="Article" selected>文章模型</option>
							<option value="Product">产品模型</option>
							<option value="Cases">案例模型</option>
							<option value="About">关于模型</option> 
							<option value="Contact">联系模型</option>
						</select>
					</div>
					
				</div>



				<div class="layui-form-item">
					<label class="layui-form-label layui-required">栏目属性:</label>
					<div class="layui-input-block">
						<input type="radio" name="ispart" value="1" title=" 最终列表栏目" lay-verify="otherReq" 
							   @if(isset($category->ispart) && $category->ispart == 1) selected @endif lay-filter="ispart">
						<input type="radio" name="ispart" value="2" title=" 频道封面" lay-verify="otherReq" 
							   @if(isset($category->ispart) && $category->ispart == 2) selected @endif lay-filter="ispart">

						<input type="radio" name="ispart" value="3" title=" 外部连接" lay-verify="otherReq" 
							   @if(isset($category->ispart) && $category->ispart == 3) selected @endif lay-filter="ispart">
					</div>
				</div>


				<div class="layui-form-item" id="routeRuleUrl" @if(isset($category->ispart) && $category->ispart == 3 ) style="display:block" @else style="display:none" @endif>
					<label class="layui-form-label">验证链接</label>
					<div class="layui-input-block">
						<input type="text" name="urls" value="{{ $category->urls ?? old('urls') }}"  autocomplete="off" placeholder="请输入http链接" class="layui-input">
					<div class="layui-form-mid layui-word-aux">链接必须带有"http://"或者"https://"的前缀</div>
					</div>
					
				</div>


				<div class="layui-form-item" id="routeRule" @if(isset($category->ispart) && ($category->ispart == 1 || $category->ispart == 2)) style="display:block" @else style="display:block" @endif>
					<label class="layui-form-label">URL规则</label>
					<div class="layui-input-block">
						<input type="text" name="listrule" id="listrule"  value="{{ $category->listrule ?? old('listrule') }}" autocomplete="off"
							   placeholder="请输入URL规则" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label">路由名称</label>
					<div class="layui-input-block">
						<input type="text" name="routename" id="routename" value="{{ $category->routename ?? old('routename') }}" autocomplete="off"
							   placeholder="请输入路由名称" class="layui-input">
					</div>
				</div>
			</div>

			<div class="layui-tab-item">
				<div class="layui-form-item">
					<label class="layui-form-label">投稿:</label>
					<div class="layui-input-block">
						<input type="radio" name="issend" value="0" title="不支持"
							   @if(isset($category->issend) && $category->issend == 0) selected @endif>
						<input type="radio" name="issend" value="1" title="支持"
							   @if(isset($category->issend) && $category->issend == 0) selected @endif>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label layui-required">是否显示:</label>
					<div class="layui-input-block">
						<input type="radio" name="ismenu" value="0" title="菜单"
							   @if(isset($category->ismenu) && $category->ismenu == 1) selected @endif>
						<input type="radio" name="ismenu" value="1" title="栏目"
							   @if(isset($category->ismenu) && $category->ismenu == 0) selected @endif>
					</div>
				</div>
				<div class="layui-form-item">
					<label class="layui-form-label layui-required">是否菜单:</label>
					<div class="layui-input-block">
						<input type="radio" name="isdisplay" value="1" title="是"
							   @if(isset($category->isdisplay) && $category->isdisplay == 1) selected @endif>
						<input type="radio" name="isdisplay" value="0" title="否"
							   @if(isset($category->isdisplay) && $category->isdisplay == 0) selected @endif>
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label layui-required">排序:</label>
					<div class="layui-input-block">
						 <input type="text" name="sort"  value="{{ $category->sort ?? 255 }}" autocomplete="off"  placeholder="请输入排序" class="layui-input">
					</div>
				</div>
			  <div class="layui-form-item">
				<div class="layui-inline">
				  <label class="layui-form-label">网络图片</label>
				  <div class="layui-input-inline"  style="width:600px">
					<input type="url" name="downloadurl" id="downloadurl" autocomplete="off" class="layui-input">
				  </div>
				</div>
				<div class="layui-inline">
				  <button type="button" class="layui-btn downloadImage"><i class="layui-icon">&#xe67c;</i>上传</button>
				</div>
			  </div>

				<div class="layui-form-item">
					<label for="" class="layui-form-label">封面(PC端)</label>
					<div class="layui-input-block">
						<div class="layui-upload">
							<button type="button" class="layui-btn layui-btn-sm downloadurl uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
							</button>
							<div class="layui-upload-list">
								<ul class="layui-upload-box layui-clear" id="layui-upload-box">
									@if(isset($category->thumb))
										<li><img src="{{ $category->thumb }}" width="50"/><p>上传成功</p></li>
										@else
										<li><img src="" width="50"/><p></p></li>										
									@endif
								</ul>
								<input type="hidden" name="thumb" id="thumb" class="layui-upload-input"  value="{{ $category->thumb??'' }}">
							</div>
						</div>
					</div>
				</div>
				
			  <div class="layui-form-item">
				<div class="layui-inline">
				  <label class="layui-form-label">网络图片</label>
				  <div class="layui-input-inline"  style="width:600px">
					<input type="url" name="downloadurlwap" id="downloadurlwap" autocomplete="off" class="layui-input">
				  </div>
				</div>
				<div class="layui-inline">
				  <button type="button" class="layui-btn downloadImageWap"><i class="layui-icon">&#xe67c;</i>上传</button>
				</div>
			  </div>
			  
			  
				<div class="layui-form-item">
					<label for="" class="layui-form-label">封面(移动端)</label>
					<div class="layui-input-block">
						<div class="layui-upload">
							<button type="button" class="layui-btn layui-btn-sm downloadurlwap uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
							</button>
							<div class="layui-upload-list">
								<ul class="layui-upload-box layui-clear" id="wapView">
									@if(isset($category->wapthumb))
										<li><img src="{{ $category->wapthumb }}" width="50"/><p>上传成功</p></li>
										@else
										<li><img src="" width="50"/><p></p></li>
									@endif
								</ul>
								<input type="hidden" name="wapthumb" id="wapthumb" class="layui-upload-input"  value="{{ $category->wapthumb??'' }}">
							</div>
						</div>
					</div>
				</div>	
 
			</div>
			<div class="layui-tab-item">
				<div class="layui-form-item">
					<label class="layui-form-label">seo标题</label>
					<div class="layui-input-block">
						<input type="text" name="seo_title" id="seo_title" value="{{ $category->seo_title ?? old('seo_title') }}"
							   lay-verify="required" autocomplete="off" placeholder="请输入栏目名称" class="layui-input">
					</div>
				</div>

				<div class="layui-form-item">
					<label class="layui-form-label">seo关键字</label>
					<div class="layui-input-block">
						<input type="text" name="seo_key" id="seo_key" value="{{ $category->seo_key ?? old('seo_key') }}"
							   lay-verify="required" autocomplete="off" placeholder="请输入栏目名称" class="layui-input">
					</div>
				</div>
				<div class="layui-form-item layui-form-text">
					<label class="layui-form-label">栏目描述</label>
					<div class="layui-input-block">
						<textarea placeholder="请输入内容" name="description" id="description" class="layui-textarea"  lay-verify="required">{{ $category->description ?? old('description') }}</textarea>
					</div>
				</div>
			</div>
			<div class="layui-tab-item">
				<main>
					<div class="centered">
						<div class="document-editor">
							<div class="toolbar-container"></div>
							<div class="content-container">
								<div id="editor">
									{!! $category->body??old('body') !!}
								</div>
							</div>
						</div>
					</div>
				</main>
			</div>
		</div>
	</div>

	<div class="text-center" style="position: fixed;bottom: 0;left: 0;right: 0;background-color: #fff;box-shadow: 0 -1px 5px rgba(0,0,0,.15);padding: 15px;">
		<button class="layui-btn" lay-filter="SubmitForm" id="SubmitForm" lay-submit>保存</button>
	    <button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
	</div>
</div>
</form>