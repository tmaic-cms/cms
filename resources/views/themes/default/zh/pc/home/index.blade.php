@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'index')
@section('bodyid', 'top')
@section('main') 
  <div class="box1 lunbo1">
    <div class="swiper-container">
      <div class="swiper-wrapper">
	    @adv($banner group=1)
        <div class="swiper-slide">
          <img src="{{$banner->thumb}}" title="{{$banner->title}}" alt="@webname" />
        </div>
		@endadv 
      </div>
      <div class="swiper-pagination"></div>
      <div class="swiper-button-prev omg"></div>
      <div class="swiper-button-next omg"></div>
    </div>
  </div>
  <div class="box2">
    <div class="title">
      <h2>@fourcategories</h2>
      <p>@use_space_sub</p>
    </div>
    <div class="tab">
      <ul>
	  @channel($channelList group=20 rows=4)
        <li class="transition @if($channelListloop->first) active @endif">{{$channelList->name}}</li>
	  @endchannel	
      </ul>
      <div class="tabnr">
	    @channel($channelList group=20 rows=4)
        <div class="nr transition @if($channelListloop->first) active @endif">
		  @product($productList typeid=$channelList->column_mid rows=8)
          <a href="{{$productList->link}}">
            <img class="transition" src="{{$productList->thumb}}" title="{{$productList->title}}" alt="{{$productList->seo_title}}">
            <p>{{$productList->title}}</p>
          </a>
		  @endproduct
        </div>
		@endchannel	
      </div>
    </div>
  </div>
  <div class="box3">
    <div class="title">
      <h2>@products</h2>
      <p>@use_space_sub</p>
    </div>
    <div class="tab">
       <ul>
		@channel($productColumn group=26 rows=6)
        <li class="transition @if($productColumnloop->first) active @endif"> {{$productColumn->name}}</li>
		@endchannel	
      </ul>
      <div class="tabnr">
	  @channel($productColumn group=26 rows=6)
        <div class="nr transition @if($productColumnloop->first) active @endif">
		 @product($productList typeid=$productColumn->son rows=12)
          <a href="{{$productList->link}}">
            <img class="transition" src="{{$productList->thumb}}" title="{{$productList->title}}" alt="{{$productList->seo_title}}">
            <p>{{$productList->title}}</p>
          </a>
          @endproduct
        </div>
		@endchannel
      </div>
    </div>
  </div>
  <div class="box4">
    <div class="title">
      <h2>@cases_title</h2>
      <p>@cases_sub</p>
    </div>
    <ul class="icase">
	 @cases($caseslist orderby=date rows=6)
      <li>
        <img src="{{$caseslist->thumb}}" title="{{$caseslist->title}}" alt="{{$caseslist->title}}" />
        <p class="transition">{{$caseslist->title}}</p>
        <div class="xuanfuc transition">
          <div class="xuanfu">
            <a href="{{$caseslist->link}}">了解详情</a>
          </div>
        </div>
      </li>
	 @endcases 
    </ul>
  </div>
  <div class="box5">
    <div class="title">
      <h2>@use_space</h2>
      <p>@use_space_sub</p>
    </div>
    <ul>
	@adv($plan group=7 rows=6)
      <li>
        <a href="{{$plan->link}}">
          <img class="transition" src="{{$plan->thumb}}" title="{{$plan->title}}" alt="{{$plan->title}}">
        </a>
        <div class="font">
          <a href="{{$plan->link}}">{{$plan->title}}</a>
        </div>
      </li>
	@endadv  
    </ul>
  </div>
  
  
  
 <div class="box6">
    <div class="title">
	@adv($advantageTitle typeid=35 rows=1)
      <h2>{{$advantageTitle->title}}</h2>
      <p>{{$advantageTitle->firsttitle}}</p>
	@endadv   
    </div>
    <div class="nr">
	@adv($advantage group=35 rows=4)
      <div style="background: url({{$advantage->thumb}})">
        <h2>{{$advantage->title}}</h2>
        <p>{!! $advantage->description !!}</p>
        <a href="{{$advantage->link}}" class="more-btn">
          More<span class="iconfont">&#xe602;</span>
        </a>
      </div>
	  @endadv 

    </div>
  </div>

  @channel($about typeid=83)
  <div class="box7">
    <div class="title">
      <h2>{{$about->name}}</h2>
      <p>{{$about->sub_name}}</p>
    </div>
    <div class="lrbox">
      <div class="leftbox">
        <div class="img">
          <img class="transition" src="{{$about->thumb}}" title="{{$about->title}}" alt="{{$about->seo_title}}">
        </div>
      </div>
      <div class="rightbox">
		{!! $about->body !!}
        <a class="transition" href="{{$about->link}}">更多>></a>
      </div>
    </div>
  </div>
  @endchannel
  
  
  
  
 
  
  
  
  
  
  
  
<div class="box8">
    <div class="title">
      <h2>@news_title</h2>
      <p>@news_title_sub</p>
    </div>
    <div class="tab">
      <ul>
	  @channel($index_news group=5 rows=4)
        <li class="transition @if($index_newsloop->first) active @endif">
          <a>{{ $index_news->name }}</a>
        </li>
      @endchannel
      </ul>
      <div class="tabnr">
	    @channel($index_news_list group=5 rows=4)
		<div class="nr transition @if($index_news_listloop->first) active @endif">
		 @article($pushnew typeid=$index_news_list->son orderby=date rows=1)
          <div class="leftbox">
			<a href="{{$pushnew->link}}">
              <div class="img">
                <img class="transition" src="{{ $pushnew->thumb }}" title="{{$pushnew->title}}" alt="{{$pushnew->seo_title}}">
              </div>
              <div class="font">
                <em>
                  <h2>{{ $pushnew->created_at->format('d') }}</h2>
                  {{ $pushnew->created_at->format('Y-m') }}
                </em>
                <div class="text">
                  <h3>{{ $pushnew->title}}</h3>
                  <p>{{ $pushnew->description}}</p>
                </div>
              </div>
            </a>
          </div>
		  @endarticle
          <div class="rightbox">
            <ul>
			
			 @article($newlist typeid=$index_news_list->son orderby=date rows=4)
              <li>
                <a href="{{ $newlist->link }}">
                  <em>
                    <h2>{{ $newlist->created_at->format('d') }}</h2>
                    {{ $newlist->created_at->format('Y-m') }}
                  </em>
                  <h3>{{ $newlist->description}}</h3>
                </a>
              </li>
			  @endarticle 
			
               
            </ul>
          </div>
        </div>
		@endchannel
		
		 
        
      </div>
    </div>
  </div>
  
  
  
  
  
  
   

 
@endsection
@section('footerjs')
@endsection

