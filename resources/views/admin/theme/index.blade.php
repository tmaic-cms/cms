@extends('admin.base')
@section('content')
<link rel="stylesheet" href="/assets/css/store.css?v=318"/>
<style>
body{background:#FFF;margin-bottom:50px;}
.layui-upload-drag {padding: 5px;}
</style>
<div class="layui-fluid">
    <div class="layui-card">
        <div class="layui-tab layui-tab-brief">
            <ul class="layui-tab-title">
                <li class="layui-this">主题</li>
                <li>应用商店</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show" style="padding-top: 20px;">
                    <div class="layui-row layui-col-space30" id="demoCardList1"></div>
                </div>
                <div class="layui-tab-item" style="padding-top: 20px;">
				   
                    <div class="layui-row layui-col-space30" id="demoCardList2"></div>
				  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
@can('webconfig.defines')
@include('admin.theme.js')
 <script>
layui.use(['layer', 'dataGrid', 'element', 'dropdown','admin','upload'], function () {
	var $ = layui.jquery;
	var layer = layui.layer;
	var element = layui.element;
	var upload = layui.upload;
	var dataGrid = layui.dataGrid;
	var admin = layui.admin;
	var form = layui.form;


	//自定义验证规则
    form.verify({
		otherReq: function(value,item){
		  var verifyName=$(item).attr('name')
			,verifyType=$(item).attr('type')
			,formElem=$(item).parents('.layui-form')
			,verifyElem=formElem.find('input[name='+verifyName+']')
			,isTrue= verifyElem.is(':checked')//是否命中校验
			,focusElem = verifyElem.next().find('i.layui-icon');
			if(!isTrue || !value){
			//定位焦点
			focusElem.css(verifyType=='radio'?{"color":"#FF5722"}:{"border-color":"#FF5722"});
			//对非输入框设置焦点
			focusElem.first().attr("tabIndex","1").css("outline","0").blur(function() {
				focusElem.css(verifyType=='radio'?{"color":""}:{"border-color":""});
			 }).focus();
			  return '终端类型属性必填项,且不能为空';
			}
		}
    });

 
	// 项目
	$.get("{{ route('admin.theme.data')}}", function (res) {
		dataGrid.render({
			elem: '#demoCardList1',
			templet: '#demoCardItem1',
			data: res.data,
			page: {limit: 8, limits: [8, 16, 24, 32, 40]}
		});
	});

	dataGrid.on('item(demoCardList1)', function (obj) {
		//console.log(JSON.stringify(obj.data));
		//setTheme(obj.data);
		
		showEditModel(obj.data);
		
	});

	/** 应用 */
	$.get('/json/data-grid3.json', function (res) {
		dataGrid.render({
			elem: '#demoCardList2',
			templet: '#demoCardItem2',
			data: res.data,
			page: {limit: 8, limits: [8, 16, 24, 32, 40]}
		});
	});

	dataGrid.on('tool(demoCardList2)', function (obj) {
		if (obj.event === 'download') {
			layer.msg('点击了下载');
		} else if (obj.event === 'edit') {
			layer.msg('编辑');
		} else if (obj.event === 'share') {
			layer.msg('点击了分享');
		} else if (obj.event === 'item1') {
			layer.msg('点击了1st menu item');
		} else if (obj.event === 'item2') {
			layer.msg('点击了2nd menu item');
		} else if (obj.event === 'item3') {
			layer.msg('点击了3rd menu item');
		}
	});

	dataGrid.on('item(demoCardList2)', function (obj) {
		layer.msg('点击了第' + (obj.index + 1) + '个');
	});

   
 
	/* 显示表单弹窗 */
	function showEditModel(mData) {
			admin.open({
                type: 2,
                title: '编辑主题',
                content: "{{ route('admin.theme.edit') }}?id=" + mData.id,
                area: ['100%', '100%'],
                offset: '0px',
                scrollbar: false,
                yes: function (index, layero) {
                },
                end: function (index, layero) { 
                    return;
                }
            });
	}
      

 

});
</script>
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
@endcan
@endsection