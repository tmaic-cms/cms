<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\NavTagServices;




class NavServiceProvider extends ServiceProvider
{
/**
 * Register services.
 *
 * @return void
 */
public function register() {}

/**
 * Bootstrap services.
 *
 * @return void
 */
public function boot()
{
        

	Blade::directive('nav', function ($expression) {

		$NAVTAG = new NavTagServices($expression);

		$NAVOBJECT = (object)$NAVTAG->tag;
		
		$parseStr  = $parseStr   ='<?php ';

		if($NAVTAG->navPid){	
		//  $parseStr .= $NAVTAG->object .'COLUMN = cache()->remember("'.$NAVTAG->Tagcachekey.'".'.$NAVTAG->navPid.', '.$NAVTAG->tagcachetime.', function () use('.$NAVTAG->useVal.'){ ';
		
		}else{
		//  $parseStr .= $NAVTAG->object .'COLUMN = cache()->remember("'.$NAVTAG->Tagcachekey.'"'.$NAVTAG->navPid.', '.$NAVTAG->tagcachetime.', function (){ ';
		
		}

		$parseStr .= $NAVTAG->object.'NAVMODEL = \App\Models\ColumnTranslation::query();';
		
		$parseStr .=$NAVTAG->object.'NAVMODEL  = '.$NAVTAG->object.'NAVMODEL->field()->language()->condition();';
		
		$parseStr .=  $NAVTAG->object.'NAVMODEL= '.$NAVTAG->object.'NAVMODEL->with(["children"=>function($q){$q->field()->condition();}]);';

		if(isset($NAVOBJECT->typeid))  $parseStr .=$NAVTAG->object.'NAVMODEL = '.$NAVTAG->object.'NAVMODEL'.$NAVOBJECT->typeid;
		
		if(isset($NAVOBJECT->group))   $parseStr .=$NAVTAG->object.'NAVMODEL = '.$NAVTAG->object.'NAVMODEL'.$NAVOBJECT->group;
		
		if(isset($NAVOBJECT->rows))    $parseStr .=$NAVTAG->object.'NAVMODEL = '.$NAVTAG->object.'NAVMODEL'.$NAVOBJECT->rows;

		if(isset($NAVOBJECT->orderby)) $parseStr .=$NAVTAG->object.'NAVMODEL = '.$NAVTAG->object.'NAVMODEL'.$NAVOBJECT->orderby;

		$parseStr .=$NAVTAG->object.'COLUMN = '.$NAVTAG->object.'NAVMODEL->get();';

	 	//$parseStr  .=  '}); ';
		
		$parseStr .='  $count = count('.$NAVTAG->object.'COLUMN); ';
		
		$parseStr .=$NAVTAG->object.'count = count('.$NAVTAG->object.'COLUMN); ';
	

		$parseStr .= ' foreach('.$NAVTAG->object.'COLUMN as $key=>$item) { ?>';
		
		$parseStr .= '<?php '. $NAVTAG->object .'= $item; ?>';
		
		$parseStr .= '<?php '. $NAVTAG->navObj .'= $item->children; ?>';

		$parseStr .='<?php  $index = $key; ?>';
		$parseStr .='<?php  $iteration = $key+1; ?>';
		$parseStr .='<?php  $key ==0?$first = true:$first=0; ?>';
		$parseStr .='<?php  $key == ($count-1)?$last = true:$last=0; ?>';
		$parseStr .='<?php  $remaining = $count-($key+1); ?>';
		$parseStr .='<?php  $obj = [
			"index"=>$index,
			"iteration"=>$iteration,
			"first"=>$first,
			"last"=>$last,
			"remaining"=>$remaining
			 ];?>';
		$parseStr .='<?php  '. $NAVTAG->object .'loop = (object)$obj; ?>';

		return $parseStr;
		
	});

	
	Blade::directive('endnav', function () {
		return " <?php } ?>";
	});

}

}//end class
