<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-20
*/
namespace App\Services;
use Illuminate\Http\Request;
use App\Services\ChannelBaseServices;
use App\Models\ColumnTranslation;
use Cache;
class NextTagService
{
    public $tagName;
    public $model;
    public $typeid;
    public $expression;
    public $musterVal;
    public $object;
    public $cacheObject;
	//当前栏目
    public $column;
	//当前栏目ID
    public $columnid;
 
 
    //标题长度
    public $length;	
	//描述长度
    public $desclength;
    //字符串后缀	
	public $adorn;
 
	//当前语言
    public $locale;
	//组装标签
    public $tag;
	//标签对象
    public $objtag;
	//当前路由
    public $routeName;
	//缓存
    public $cache;
	//是否缓存
    public $iscache;
	//缓存key
    public $sysTagcachekey;
	//缓存时间
    public $tagcachetime;
	//传给模板，假如是闭包
    public $useVal;
    public $slug;
 

    public function __construct($expression)
    {
	   $request = new Request(); 
	   $this->tagName ="next";
	   $this->model = null;//"Article";
	   $this->columnid = 0;
 
	   $this->iscache = false;
	   //当前语言
	   $this->locale = lang();
	   //title 长度
	   $this->length = 0;
	   //描述长度
	   $this->desclength = 0;
	   //返回行数
	   $this->rows = 1;
	   //超出部分显示的字符串
	   $this->adorn = null;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //
       $this->slug = request()->route('slug');
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=0;
	   
	   //设置当前模型：有文章、产品、案例等模型
	   //$this->setModel();
	   //开始处理标签
       $this->decompose($expression);
	   //$this->setColumn();
	   //生成缓存key
	   $this->setSysTagcachekey();

    }
	

	
	
    //当前选中的栏目
    public function setColumn($column=null){
 
	   
        $Reslut = Cache::remember($this->routeName.'_'.$this->slug, 0, function() {
          return   $this->model::with('column')->where('slug',$this->slug)->lang()->first();
        });
		$Reslut = $this->model::with('column')->where('slug',$this->slug)->lang()->first();
		$this->column = $Reslut->column;
 
		//栏目id
		$this->columnid = $this->column?$this->column->column_mid:null; 
		//分类ID
		$this->pid =$this->column->pid?$this->column->pid:null;
		$this->sid =$this->column->sid?$this->column->sid:null;
 
	}
	
 

    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		//删除数组为空的下标
        foreach( $this->expression as $k=>$v){ if( !$v )unset( $this->expression[$k] ); } 
		
		$this->musterVal = explode(',',current($this->expression));
		 
		if(count($this->musterVal) === 1){
			// 对象
			$this->object = current($this->musterVal);  
		}

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		
 
        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			$this->objtag = (object)$this->tag;
        }
		
		if(isset($this->objtag->length)) $this->length=$this->objtag->length*2;
		if(isset($this->objtag->desclength)) $this->desclength=$this->objtag->desclength*2;
		if(isset($this->objtag->adorn)) $this->adorn="'{$this->objtag->adorn}'";
	   
	   
	    //有参数传入
        if(isset($this->objtag->model) && $this->objtag->model){
			switch ($this->objtag->model) {
				case "a": $value = "Article"; break;
				case "c": $value = "Cases"; break;
				case "p": $value = "Product"; break;
				default:  $value = "Article";
			}
			$this->model=$value; 
        }
		//无参数传入 默认文章模型			
		$this->setColumn();
		$this->setModel();
		
		
		
		
		
		
		
		
		//设置缓存时间
		if(isset($this->objtag->cachetime)){
			$this->tagcachetime=(int)$this->objtag->cachetime;
		}

		//设置缓存 如果传入 统一转成小写
		if(isset($this->objtag->cache)) $this->objtag->cache = strtolower($this->objtag->cache);

		if(isset($this->objtag->cache) && ($this->objtag->cache == true || $this->objtag->cache == 1)){
			  $this->iscache = true;
			  
		}else if(isset($this->objtag->cache) && ($this->objtag->cache == false || $this->objtag->cache == 0)){
			 $this->iscache = false;
			 $this->tagcachetime = 0;
		}

		$this->cache = $this->object.'cache';  
		$this->cacheObject = $this->object.'obj';  
		 
    }
	
    //传参数，组装字符串
    public function setModel()
    {
		$this->tag['model']= "\\".$this->model."::query();"; 
    }
	
    //缓存key
	public function setSysTagcachekey(){
		    $this->sysTagcachekey = "request()->route()->getName().'_next_'.request()->route('slug')"; 
	}
 






}
