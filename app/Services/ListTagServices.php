<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;
use App\Services\ChannelBaseServices;
use App\Models\ColumnTranslation;
use Cache;
class ListTagServices
{
    public $tagName;
    public $model;
    public $typeid;
    public $expression;
    public $musterVal;
    public $object;
    public $cacheObject;
	//当前栏目
    public $column;
	//当前栏目ID
    public $columnid;
	//当前页码
    public $page;
	//排序
    public $orderby;
    //标题长度
    public $length;	
	//描述长度
    public $desclength;
    //字符串后缀	
	public $adorn;
	//推荐属性
    public $flag;
	//当前语言
    public $locale;
	//组装标签
    public $tag;
	//标签对象
    public $objtag;
	//当前路由
    public $routeName;
	//缓存
    public $cache;
	//是否缓存
    public $iscache;
	//缓存key
    public $sysTagcachekey;
	//缓存时间
    public $tagcachetime;
	//传给模板，假如是闭包
    public $useVal;
	//是否为顶级分类
	public $isPid;
	//分类ID
	public $pid;
	//栏目记录ID
	public $sid;
 

    public function __construct($expression)
    {
		$this->tagName ="list";
       //出始化变量
	   $this->pageObj = NULL;
	   $this->model = "Article";
	   $this->isPid = false;
	   $this->pid = -1;
	   $this->columnid = 0;
	   //闭包变量
	   $this->useVal = NULL;
	   $this->iscache = false;
	   //当前语言
	   $this->locale = lang();
	   //title 长度
	   $this->length = 0;
	   //描述长度
	   $this->desclength = 0;
	   //返回行数
	   $this->rows = 1;
	   //超出部分显示的字符串
	   $this->adorn = NULL;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=0;
	   $this->setColumn();
	   //设置当前模型：有文章、产品、案例等模型
	   $this->setModel();
	   //开始处理标签
       $this->decompose($expression);
	   //生成缓存key
	   $this->setSysTagcachekey();

    }
 
	
	
    //当前选中的栏目
    public function setColumn($column=null){
	     
	    //缓存栏目	
        $this->column = Cache::remember($this->routeName.'_'.$this->locale, webconfig('SYSCACHETIME') , function() {
          return  ColumnTranslation::with(['children','class'])->where('routename',$this->routeName)->lang()->first();
        });
		//$this->column = ColumnTranslation::with(['children','class'])->where('routename',$this->routeName)->lang()->first();
		 
		//当前模型
		$this->model = $this->column->model;
		
		
		//栏目id
		$this->columnid = $this->column?$this->column->column_mid:null; 
		//分类ID
		$this->pid =$this->column->pid?$this->column->pid:null;
		$this->sid =$this->column->sid?$this->column->sid:null;

		if($this->column->pid == 0){
			$this->isPid = true;
		}
		
	}
	
 

    public function decompose($expression = null){
        
        $this->expression = explode(' ',$expression);
		//删除数组为空的下标
        foreach( $this->expression as $k=>$v){ if( !$v )unset( $this->expression[$k] ); } 
		
		$this->musterVal = explode(',',current($this->expression));
		 
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
			
		}else if(count($this->musterVal) === 2){
			
			// 循环对象
			$this->object = current($this->musterVal);  
			// 分页对象
			$this->pageObj = next($this->musterVal);  
		}

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		
 
        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			$this->objtag = (object)$this->tag;
        }
		

		
		
		if(isset($this->objtag->length)) $this->length=$this->objtag->length*2;
		if(isset($this->objtag->desclength)) $this->desclength=$this->objtag->desclength*2;
		if(isset($this->objtag->adorn)) $this->adorn="'{$this->objtag->adorn}'";
	   
		if($this->isPid && $this->isPid === true){
			 
		 	$this->group(); 
			
 
		}else{
			 
			if(isset($this->objtag->typeid) && substr($this->objtag->typeid,0,1)=="$"){
			   //闭包变量
			   $this->useVal = substr($this->objtag->typeid,0,strrpos($this->objtag->typeid,"->"));
			   $this->typeid($this->objtag->typeid);
			}else{
			   $this->useVal = '$column';
			   $this->typeid('$column->son');
			}				
			
		}
 
 
      
		//推荐属性
        if(isset($this->objtag->flag)){
          $this->flag($this->objtag->flag);
		   
        } 
	
		//设置排序
        if(isset($this->objtag->orderby)){
          $this->orderby($this->objtag->orderby);
		   
        }else{
		  $this->orderby('id');
	 
		}			
		
        //分页
        if(isset($this->objtag->page) && $this->objtag->page > 0){
          $this->page($this->objtag->page);
        }else{
		  $this->page();
		}
		
		//设置缓存时间
		if(isset($this->objtag->cachetime)){
			$this->tagcachetime=(int)$this->objtag->cachetime;
		}

		//设置缓存 如果传入 统一转成小写
		if(isset($this->objtag->cache)) $this->objtag->cache = strtolower($this->objtag->cache);

		if(isset($this->objtag->cache) && ($this->objtag->cache == true || $this->objtag->cache == 1)){
			  $this->iscache = true;
			  
		}else if(isset($this->objtag->cache) && ($this->objtag->cache == false || $this->objtag->cache == 0)){
			 $this->iscache = false;
			 $this->tagcachetime = 0;
		}

		$this->cache = $this->object.'cache';  
		$this->cacheObject = $this->object.'obj';  
		if(empty($this->pageObj)) $this->pageObj = $this->object.'page'; 
    }
	
    //传参数，组装字符串
    public function setModel()
    {
		$this->tag['model']= "\\".$this->model."::query();"; 
    }
	
 
 
	
    public function page($page = 10)
    {
        $this->page = $page;
        $this->tag['page']= "->paginate($page);";     
    }

    //传参数，组装字符串
    public function typeid($columnid)
    {
        $this->typeid = $columnid; 
		$this->tag['typeid'] = '->whereIn("column_id",p(\Cache::get("column_translations"),$column->sid));';
    }
	
    //传参数，组装字符串
    public function group()
    {
		$this->tag['typeid'] = '->whereIn("column_id",p(\Cache::get("column_translations"),$column->sid));';
    }
 
  
    //并接排序
    public function orderby($orderby)
    {
        $this->orderby = $orderby;
		 
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case "date":
                $value = '->orderBy("created_at","desc");';
                break;

            default: $value = '->orderBy("created_at","desc");';break;
        }
		
        $this->tag['orderby']= $value;
        
    }

 
    //并接查询条件 推荐类属性
    public function flag($flag)
    {
        $this->flag = $flag;
        $this->tag['flag']= '->where("flag",'.$flag.')';    
       
    }
    //缓存key
	public function setSysTagcachekey(){
		$this->sysTagcachekey = '$column';
	}
 






}
