<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/

namespace App\Services;

use zgldh\QiniuStorage\QiniuStorage;
use Ramsey\Uuid\Uuid;

class QiniuServices
{


//删除云端图片资源
public function delete($filename=null)
{
	if(empty($filename)) return false;
 
	$filename = (array)$filename;
	
	foreach($filename as $item){
		
		$basename = pathinfo($item)['basename'];
		
		$disk     = QiniuStorage::disk('qiniu');

		$disk->delete($basename);

	}
	return true;
}

//删除云端图片资源
//传入产品、案例、广告的数据库结集
public function batchDelete($filenameArray)
{
	if(empty($filenameArray)) return 0;
	foreach($filenameArray as $item){
      if(isset($item->path))     $this->del($item->path);	
      if(isset($item->thumb))    $this->del($item->thumb);	
      if(isset($item->wapthumb)) $this->del($item->wapthumb);	
      if(isset($item->body)){
		  $bodyimageArray = getContentPicture($item->body);
		  if(count($bodyimageArray)){
			  foreach($bodyimageArray as $img){
				$this->del($img);  
			  }
		  }
	  }
	  
      if(isset($item->value)){
		  $valueimageArray = getContentPicture($item->value);
		  if(count($valueimageArray)){
			  foreach($valueimageArray as $img){
				$this->del($img);  
			  }
		  }
	  }
	  
	  
	  if(isset($item->description)){
		  $descriptionimageArray = getContentPicture($item->description);
		  if(count($descriptionimageArray)){
			  foreach($descriptionimageArray as $img){
				$this->del($img);  
			  }
		  }
	  }
	}
	return 1;
}



//删除云端图片资源
//传入数据对象
public function objFromDelete($item)
{
	  if(empty($item)) return 0;
      if(isset($item->val))      $this->del($item->val);//配置表
      if(isset($item->path))     $this->del($item->path);	
      if(isset($item->thumb))    $this->del($item->thumb);	
      if(isset($item->wapthumb)) $this->del($item->wapthumb);	
      if(isset($item->body)){
		  $bodyimageArray = getContentPicture($item->body);
		  if($bodyimageArray && count($bodyimageArray)){
			  foreach($bodyimageArray as $img){
				$this->del($img);  
			  }
		  }
	  }
	  if(isset($item->description)){
		  $descriptionimageArray = getContentPicture($item->description);
		  if($descriptionimageArray && count($descriptionimageArray)){
			  foreach($descriptionimageArray as $img){
				$this->del($img);  
			  }
		  }
	  }
	return 1;
}









private function del($filename=null){
	if(empty($filename)) return false;
	$disk     = QiniuStorage::disk('qiniu');
	$basename = pathinfo($filename)['basename'];
	$disk->delete($basename);
}











}
