	
	layui.use(['form', 'admin', 'laytpl', 'table', 'util', 'laydate', 'element', 'layer', 'layedit', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
    var layer = layui.layer;
    var layedit = layui.layedit;
    var upload = layui.upload;

    var langUrl = location.hash.replace(/^#tablist=/);
    element.tabChange('tablist', langUrl);
    element.on('tab(tablist)', function (elem) {
        location.hash = 'tablist=' + $(this).attr('lay-id');
        lang = $(this).attr('lay-data');
        insTb.reload({where: {'lang': lang}, page: {curr: 1}});
    });

        var insTb = table.render({
            elem: '#tableData',
            skin: 'line',
            size: 'lg',
            url: definesData,
            page: true,
            toolbar: ['<p>',
                '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
                '<button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
                '</p>'].join(''),
            cellMinWidth: 100,
            cols: [[
                {type: 'checkbox'},
                {type: 'numbers'},
                {field: 'group_id', title: '配置组名称', sort: true, width: 150},
                {field: 'name', title: '配置名称', sort: true, width: 200},
                {field: 'keys', title: '键key', sort: true, width: 200},
                {field: 'value', title: '配置值', sort: true, width: 500},
                {field: 'langType', title: '所属语言', sort: true, width: 120},
                {
                    field: 'created_at', title: '创建时间', templet: function (d) {
                        return util.toDateString(d.created_at);
                    }, sort: true, width: 160
                },
                {field: 'status', title: '状态', templet: '#TbState', sort: true, width: 120},
                {title: '操作', toolbar: '#options', align: 'left', fixed: "right"}
            ]]
        });


        /* 表格搜索 */
        form.on('submit(TbSearch)', function (data) {
            insTb.reload({where: data.field, page: {curr: 1}});
            return false;
        });

        form.on('select(groupname)', function(data){
            var formData = form.val('groupNameForm');
            console.log(formData);
            insTb.reload({where: formData, page: {curr: 1}});

        });
        
        /* 表格工具条点击事件 */
        table.on('tool(tableData)', function (obj) {
            if (obj.event === 'edit') { // 修改
                showEditModel(obj.data);
            } else if (obj.event === 'del') { // 删除
                doDel({ids: obj.data.id});
            } else if (obj.event === 'reset') { // 重置密码
                resetPsw(obj);
            }
        });

        /* 表格头工具栏点击事件 */
        table.on('toolbar(tableData)', function (obj) {
            if (obj.event === 'add') { // 添加
                showEditModel();
            } else if (obj.event === 'del') {
                var checkRows = table.checkStatus('tableData');
                if (checkRows.data.length === 0) {
                    layer.msg('请选择要删除的数据', {icon: 2});
                    return;
                }
                var ids = checkRows.data.map(function (d) {
                    return d.id;
                });
                doDel({ids: ids});
            }
        });

        /* 显示表单弹窗 */
        function showEditModel(mData) {
            admin.open({
                type: 1,
                area: ['700px', '450px'],
                title: (mData ? '修改' : '添加') + '配置',
                content: $('#EditDialog').html(),
                success: function (layero, dIndex) {
                    // 回显表单数据
                    form.val('EditForm', mData);
                    // 表单提交事件
                    form.on('submit(EditSubmit)', function (data) {
                       
                        var loadIndex = layer.load(2);
                        $.ajax({
                            type: "POST",
                            url: mData ? definesupdate : definesstore,
                            data: $(this).parents('form').serialize(),
                            dataType: 'json',
                            success: function (res) {
                                layer.close(loadIndex);
                                if (res.code == 0) {
                                    layer.close(dIndex);
                                    layer.msg(res.msg, {icon: 1});
                                    insTb.reload({page: {curr: 1}});
                                } else {
                                    layer.msg(res.msg, {icon: 2});
	
                                }

                            },
                            error: function (msg) {
                                layer.close(loadIndex);
                                if (msg.status == 422) {
                                    var json = JSON.parse(msg.responseText);
                                    json = json.errors;
                                    for (var item in json) {
                                        for (var i = 0; i < json[item].length; i++) {
                                            layer.msg(json[item][i], {icon: 2});
                                            return;
                                        }
                                    }
                                } else {
                                    layer.msg('服务器连接失败', {icon: 2});
                                    return;
                                }
                            }
                        });
                        return false;
                    });
                     
                    // 禁止弹窗出现滚动条
                    $(layero).children('.layui-layer-content').css('overflow', 'visible');
                }
            });
        }

        /* 删除 */
        function doDel(obj) {
            layer.confirm('确定要删除选中数据吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);
                $.post(definesdestroy, {
                    _method: 'delete',
                    id: obj.data ? obj.data.id : '',
                    ids: obj.ids
                }, function (res) {
                    layer.close(loadIndex);
                    if (res.code === 0) {
                        layer.msg(res.msg, {icon: 1});
                        insTb.reload({page: {curr: 1}});
                    } else {
                        layer.msg(res.msg, {icon: 2});
                    }
                }, 'json');
            });
        }

        /* 修改状态 */
        form.on('switch(TbStateCk)', function (obj) {
            var loadIndex = layer.load(2);
            $.post(definesupdateajax, {
                id: obj.elem.value,
                state: obj.elem.checked ? 0 : 1
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                } else {
                    layer.msg(res.msg, {icon: 2});
                    $(obj.elem).prop('checked', !obj.elem.checked);
                    form.render('checkbox');
                }
            }, 'json');
        });


});
