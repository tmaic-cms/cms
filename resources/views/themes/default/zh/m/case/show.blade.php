@extends('theme::layouts.h5')
@section('title'){{$showData->seo_title}}@endsection
@section('keyword'){{$showData->keywords}}@endsection
@section('description'){{$showData->description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'casedetail')
@section('bodyid', 'top')
@section('main') 




@extends('theme::layouts.h5')
@section('title'){{$showData->seo_title}}@endsection
@section('keyword'){{$showData->keywords}}@endsection
@section('description'){{$showData->description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'casedetail')
@section('bodyid', 'top')
@section('main') 
  <div class="box1">
    <div class="nt">
      <h1>{{$showData->seo_title}}</h1>
      <p>{{ $showData->created_at->format('Y-m-d') }}</p>
    </div>
    <div class="nr">
	  {!! $showData->body !!}
    </div>
    <div class="near">
      <div class="list-toggle__full">
		<div>
		  @if($prev_data)
          <a href="{{$prev_data->link}}" class="list-prev">
            <span>上一篇：</span>
            <span>{{$prev_data->title}}</span>
          </a>
			@else
            <a class="list-prev">
              <span>上一篇：</span>
              <span>没有了</span>
            </a>
			@endif
        </div>
        <div>
		   @if($next_data)
			<a href="{{$next_data->link}}" class="list-next">
			  <span>下一篇：</span>
			  <span>{{$next_data->title}}</span>
			</a>
			@else
			<a class="list-prev">
			  <span>下一篇：</span>
			  <span>没有了</span>
			</a>
			@endif
        </div>
        <div>
          <a href="{{$column->link}}" class="back-list">返回列表<i class="iconfont">&#xe602;</i></a>
        </div>
      </div>
 
      <div class="nd-guide__box">
        <span>其他新闻</span>
		<ul class="nd-guide__list">
			@foreach($articlelist as $item)
            <li>
              <a href="{{$item->link}}">
                <div class="img">
                  <img class="transition" src="{{$item->thumb}}" alt="{{$item->seo_title}}">
                </div>
                <article>
                  <time>{{$item->created_at->format('Y-m-d') }}</time>
                  <h2>{{$item->title}}</h2>
                </article>
              </a>
            </li>
			@endforeach
        </ul>
      </div>
    </div>
    </div>
  </div>
@endsection





















<!--
  <div class="box1">
    <div class="nt">
      <h1>{{$showData->seo_title}}</h1>
      <p>{{ $showData->created_at->format('Y-m-d') }}</p>
    </div>
    <div class="nr">
	  {!! $showData->body !!}
    </div>
    <div class="near">
      <div class="list-toggle__full">
		<div>
		  @if($prev_data)
          <a href="{{$prev_data->link}}" class="list-prev">
            <span>上一篇：</span>
            <span>{{$prev_data->title}}</span>
          </a>
			@else
            <a class="list-prev">
              <span>上一篇：</span>
              <span>没有了</span>
            </a>
			@endif
        </div>
        <div>
		   @if($next_data)
			<a href="{{$next_data->link}}" class="list-next">
			  <span>下一篇：</span>
			  <span>{{$next_data->title}}</span>
			</a>
			@else
			<a class="list-prev">
			  <span>下一篇：</span>
			  <span>没有了</span>
			</a>
			@endif
        </div>
        <div>
          <a href="{{$column->link}}" class="back-list">返回列表<i class="iconfont">&#xe602;</i></a>
        </div>
      </div>
      <div class="case-guide__box">
        <span>其他案例</span>
       <ul class="case-guide__list">
		@foreach($casesList as $item)
 
          <li>
           <a href="{{$item->link}}">
              <img src="{{$item->thumb}}" title="{{$item->title}}" alt="{{$item->title}}" />
              <p class="transition">{{$item->title}}</p>
            </a>
          </li>
		  
		@endforeach
        </ul>
      </div>
      <div class="nd-guide__box">
        <span>其他新闻</span>
		<ul class="nd-guide__list">
			@foreach($articlelist as $item)
            <li>
              <a href="{{$item->link}}">
                <div class="img">
                  <img class="transition" src="{{$item->thumb}}" alt="{{$item->seo_title}}">
                </div>
                <article>
                  <time>{{$item->created_at->format('Y-m-d') }}</time>
                  <h2>{{$item->title}}</h2>
                </article>
              </a>
            </li>
			@endforeach
		   
        </ul>
      </div>
    </div>
    </div>
  </div>
  -->
@endsection