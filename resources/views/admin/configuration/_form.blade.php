 {{csrf_field()}}
<div class="layui-card-body">
	
	<div class="layui-form-item">
		<label for="" class="layui-form-label">配置组</label>
		<div class="layui-input-inline">
			<select name="group_id" lay-verify="required">
				<option value=""></option>
				@foreach($groups as $group)
				<option value="{{$group->id}}">{{$group->name}}</option>
				@endforeach
				
				@foreach($groups as $first)
				 <option value="{{ $first->id }}"
				  @if($configuration->group_id==$first->id) selected @endif>{{ $first->name }}</option>
				@endforeach
			</select>
			
		</div>
	</div>
	<div class="layui-form-item">
	<label for="" class="layui-form-label">类型</label>
	<div class="layui-input-inline">
		<select name="type" >
			<option value="input" @if($configuration->type=="input") selected @endif>输入框</option>
			<option value="textarea" @if($configuration->type=="textarea") selected @endif>文本域</option>
			<option value="radio" @if($configuration->type=="radio") selected @endif>单选</option>
			<option value="select" @if($configuration->type=="select") selected @endif>下拉</option>
			<option value="image" @if($configuration->type=="image") selected @endif>图片</option>
		</select>
	</div>
	</div>
	<div class="layui-form-item">
		<label for="" class="layui-form-label">配置名称</label>
		<div class="layui-input-inline">
			<input type="text" name="label" value="{{ $configuration->label??old('label') }}" lay-verify="required" placeholder="请输入名称" class="layui-input" >
		</div>
	</div>
	<div class="layui-form-item">
		<label for="" class="layui-form-label">配置字段</label>
		<div class="layui-input-inline">
			<input type="text" name="key" value="{{ $configuration->key??old('key') }}" lay-verify="required" placeholder="请输入,如name" class="layui-input" >
		</div>
	</div>

	
	<div class="layui-form-item">
		<label for="" class="layui-form-label">输入提示</label>
		<div class="layui-input-inline">
			<input type="text" name="tips" value="{{ $configuration->tips??old('tips') }}" placeholder="请输入" class="layui-input" >
		</div>
	</div>
	
	
	<div class="layui-form-item">
		<label for="" class="layui-form-label">排序</label>
		<div class="layui-input-inline">
			<input type="number" name="sort" value="{{ $configuration->sort??10 }}" placeholder="请输入" class="layui-input" >
		</div>
	</div>
	
	
	<div class="layui-form-item">
		<div class="layui-input-block">
			<button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
			<a  class="layui-btn" href="{{route('admin.configuration.list')}}" >返 回</a>
		</div>
	</div>
	
	
	
	
	
	
	
</div>