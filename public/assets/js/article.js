layui.use(['form', 'admin', 'laytpl', 'table', 'util', 'laydate', 'element', 'layer', 'layedit', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
    var layer = layui.layer;
    var layedit = layui.layedit;
    var upload = layui.upload;

    var langUrl = location.hash.replace(/^#tablist=/);
    element.tabChange('tablist', langUrl);
    element.on('tab(tablist)', function (elem) {
        location.hash = 'tablist=' + $(this).attr('lay-id');
        lang = $(this).attr('lay-data');
        insTb.reload({where: {'lang': lang}, page: {curr: 1}});
    });
    // 渲染表格
    var insTb = table.render({
        elem: '#articleData',
        skin: 'line',
        size: 'lg',
        url: articleData,
        page: true,
        toolbar: ['<p>',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
            '<button lay-event="delete" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
            '</p>'].join(''),
        cellMinWidth: 100,
        cols: [[
            {type: 'numbers', title: '序号'},
            {type: 'checkbox'},
			{templet: '<div>{{d.column.name}}</div>', title: '栏目名称', width: 150},
            {field: "title", title: '标题', width: 350},
            {field: 'thumb', title: '缩略图', toolbar: '#thumb', width: 80},
            {field: "keywords", title: 'seo关键字', width: 150},
            {field: "description", title: '描述', width: 400},
            {field: 'locale', title: '语言', width: 80},
            //{align: 'center', toolbar: '#eDialogTbBar', title: '详情', width: 120},
            {field: 'sort', title: '排序', width: 80},
            {field: 'click', title: '浏览量', width: 80},
            {field: 'status', title: '状态', templet: '#tbState', sort: true, width: 120},
            {field: 'updated_at', title: '更新时间', width: 180},
            {align: 'left', toolbar: '#options', title: '操作',width: 120, fixed: "right"}
        ]]
    });
    laydate.render({
        elem: '#created_at'
        , type: 'datetime'
        , range: true
    });
    // 列点击事件
    table.on('tool(articleData)', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (obj.event === 'edit') {
            admin.open({
                type: 2,
                title: '编辑文章',
                content: '/admin/article/edit?id=' + obj.data.id + "&lang=" + obj.data.locale,
                area: ['100%', '100%'],
                offset: '0px',
                scrollbar: false,
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    insTb.reload();layer.close(index);
                    return;
                }
            });
        } else if (obj.event === 'del') {
            doDel({ids: obj.data.id,mid:obj.data.article_mid});
        } else if (obj.event === 'checkList') {
            showComments(data.id);
        } else if (obj.event === 'comments') {
            showComments(data.id);
        }
    });
    // 列点击事件 end
 

 
     
    /* 表头工具栏点击事件 */
    table.on('toolbar(articleData)', function (obj) {
        if (obj.event === 'add') { // 添加
            admin.open({
                type: 2,
                title: '添加文章', 
                content: articlecreate,
                area: ['100%', '100%'],
                offset: '0px',
                fixed: true,
                data: "",
                end: function () {
                    insTb.reload();layer.close(index);
                    return false;
                },
                cancel: function (index, layero) {
                    insTb.reload();
                    layer.close(index);
                    return false;
                }
            });
        } else if (obj.event === 'delete') {
            var checkRows = table.checkStatus('articleData');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
			
            var article_mid = checkRows.data.map(function (d) {
                return d.article_mid;
            });
			
			
            doDel({ids: ids,mid:article_mid});
        }
    });
    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要删除选中数据吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            $.post(articledestroy, {
                _method: 'delete',
                ids: obj.ids,
                mid: obj.mid
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    }
    /* 搜索 */
    form.on('submit(btnSearch)', function (data) {
        insTb.reload({where: data.field, page: {curr: 1}});
        return false;
    });

    form.on('submit(btnClearSearch)', function (data) {
        $("#Search_from")[0].reset();
        form.render();
        insTb.reload({where: '', page: {curr: 1}});
        return false;
    });
    /* 修改状态 */
    form.on('switch(tbState)', function (obj) {  
      
         var loadIndex = layer.load(2);
        $.post(updateAjax, {id: obj.elem.value, status: obj.elem.checked ? 1 : 0}, function (res) {
            layer.close(loadIndex);
            if (res.code == 0) {
                layer.msg(res.msg, {icon: 1});
            } else {
                layer.msg(res.msg, {icon: 2});
                $(obj.elem).prop('checked', !obj.elem.checked);
                form.render('checkbox');
            }
        }, 'json');

     });











});