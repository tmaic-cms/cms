@extends('admin.base')
@section('content')
<div class="layui-card">
    <div class="layui-card-body">
        <form class="layui-form toolbar">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label left">搜索:</label>
                    <div class="layui-input-inline">
                        <input name="name" class="layui-input" placeholder="输入品牌名称" />
                    </div>
                </div>
                <div class="layui-inline">&emsp;
                    <button class="layui-btn icon-btn" lay-filter="Search" type="button" id="Search" lay-submit>
                        <i class="layui-icon">&#xe615;</i>搜索
                    </button>
                    <button class="layui-btn icon-btn layui-btn-danger" lay-filter="clearSearch" type="button" id="Search" lay-submit>
                        <i class="layui-icon">&#xe615;</i>清除
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="layui-card-body">
    <table id="dataTable" lay-filter="dataTable"></table>
</div>
<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('goods.brand.edit')
        <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('goods.brand.destroy')
        <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="del">删除</a>
        @endcan
    </div>
</script>
@endsection
@section('script')
@can('goods.brand')
<script type="text/javascript" src="/assets/js/brand.js"></script>
<script>
    var create = "{{ route('admin.brand.create') }}";
    var destroy = "{{ route('admin.brand.destroy') }}";
    var datalist = "{{ route('admin.brand.data') }}";

    layui.use(['layer', 'table', 'form'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var dataTable = table.render({
            elem: '#dataTable',
            url: datalist,
            page: true,
            toolbar: ['<p>',
                '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
                '<button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
                '</p>'
            ].join(''),
            cols: [
                [ //表头
                    {
                        checkbox: true,
                        fixed: true
                    }, {
                        field: 'id',
                        title: 'ID',
                        sort: true,
                        width: 80
                    }, {
                        field: 'name',
                        title: '品牌'
                    }, {
                        field: 'thumb',
                        title: 'LOGO'
                    }, {
                        field: 'status',
                        title: '状态'
                    }, {
                        field: 'sort',
                        title: '排序'
                    }, {
                        field: 'created_at',
                        title: '创建时间'
                    }, {
                        field: 'updated_at',
                        title: '更新时间'
                    }, {
                        fixed: 'right',
                        width: 220,
                        align: 'center',
                        toolbar: '#options'
                    }
                ]
            ]
        });
        /* 表格头工具栏点击事件 */
        table.on('toolbar(dataTable)', function (obj) {
            if (obj.event === 'add') {

                layer.open({
                    type: 2,
                    title: '添加品牌',
                    content: create,
                    area: ['40%', '60%'],
                    scrollbar: false,
                    yes: function (index, layero) {},
                    end: function (index, layero) {
                        table.reload('dataTable');
                        layer.close(index);
                        return;
                    }
                });

            } else if (obj.event === 'del') {
                var checkRows = table.checkStatus('dataTable');
                if (checkRows.data.length === 0) {
                    layer.msg('请选择要删除的数据', {
                        icon: 2
                    });
                    return;
                }
                var ids = checkRows.data.map(function (d) {
                    return d.id;
                });
                doDel({ ids: ids});
            }
        });
        //监听工具条
        table.on('tool(dataTable)', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;
            var ids={ids: data.id};
            if (layEvent === 'del') {
                doDel({ids:ids});
            } else if (layEvent === 'edit') {
            
                layer.open({
                    type: 2,
                    title: '编辑品牌',
                    content: '/admin/brand/'+data.id+'/edit',
                    area: ['40%', '60%'],
                    scrollbar: false,
                    yes: function (index, layero) {},
                    end: function (index, layero) {
                        table.reload('dataTable');
                        layer.close(index);
                        return;
                    }
                });
            }
        });
        /* 表格搜索 */
        form.on('submit(Search)', function (data) {
            console.log(data)
            dataTable.reload({where: data.field, page: {curr: 1}});
            return false;
        });
        /* 清除搜索 */
        form.on('submit(clearSearch)', function (data) {
            $(".layui-input").val('');
            dataTable.reload({where: data.field, page: {curr: 1}});
            return false;
        });
          /* 删除 */
        function doDel(obj) {
            layer.confirm('确定要删除选中数据吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);
                $.post('{{ route("admin.brand.destroy") }}', {
                    _method: 'delete',
                    ids: obj.ids
                }, function (res) {
                    layer.close(loadIndex);
                    if (res.code == 0) {
                        layer.msg(res.msg, {icon: 1});
                        dataTable.reload({page: {curr: 1}});
                    } else {
                        dataTable.msg(res.msg, {icon: 2});
                    }
                }, 'json');
            });
        }

    });
</script>
@endcan
@endsection