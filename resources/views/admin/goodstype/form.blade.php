<form class="layui-form" id="goodsCat_form" lay-filter="goodsCat_form">
<style>
.layui-form-label.layui-required:after {
content: "*";
color: red;
position: absolute;
top: 10px;
left: 15px;
}
body{ background:#fff;}
.layui-fluid {
    padding: 0px;
}
.layui-card-body {
    position: relative;
    padding: 0;
    line-height: 24px;
}

</style>

      <input type="hidden" name="id"  value="{{ $goodsCat->id??'' }}"> 
      <input type="hidden" name="locale"  value="{{ $goodsCat->locale }}"> 

    <div class="layui-form" style="">
        <div class="layui-tab layui-tab-brief" lay-filter="docDemoTabBrief">
            <ul class="layui-tab-title tab_my">
                <li class="layui-this">基本信息</li>
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                        <div class="layui-form-item">
                            <label class="layui-form-label layui-required">产品分类</label>
                            <div class="layui-input-inline">
                                <select name="pid" lay-verify="required" lay-reqtext="产品分类必填项，岂能为空？">
                                    <option value="0">-顶级分类-</option>
                                    @foreach($goodstype as $item)
                                        <option value="{{ $item->sid }}"   @if(isset($goodsCat->pid) && $goodsCat->pid==$item->sid) selected @endif >{{ $item->title }}</option>

                                            @if(isset($item['children']))
                                                @foreach($item['children'] as $second)
                                                    <option value="{{$second->sid}}" 
                                                            {{ isset($goodsCat->pid) && $goodsCat->pid==$second->sid ? 'selected' : '' }}>┗━{{$second->title}}</option>
                                                @endforeach
                                            @endif

                                    @endforeach
    
                                </select>
                            </div>
                        </div>

                        <div class="layui-form-item">
                            <label class="layui-form-label layui-required">分类名称</label>
                            <div class="layui-input-block">
                                <input type="text" name="title" lay-verify="required"
                                       value="{{ $goodsCat->title ?? old('title') }}" autocomplete="off" placeholder="请输入分类名称"
                                       class="layui-input">
                            </div>
                        </div>


                        <div class="layui-form-item">
                            <label for="" class="layui-form-label">描述</label>
                            <div class="layui-input-block">
                                <input type="text" name="description" value="{{ $goodsCat->description ?? old('description') }}" placeholder="请输入描述" class="layui-input" >
                            </div>
                        </div>




                       <div class="layui-form-item">
                        <label class="layui-form-label layui-required">是否禁用:</label>
                        <div class="layui-input-block">
                            <input type="radio" name="status" value="1" title="是" lay-verify="otherReq"
                                   @if(isset($goodsCat->status) && $goodsCat->status == 0) selected @endif>
                            <input type="radio" name="status" value="0" title="否" lay-verify="otherReq"
                                   @if(isset($goodsCat->status) && $goodsCat->status == 1) selected @endif>
                        </div>
                      </div>


                    <div class="layui-form-item">
                        <label for="" class="layui-form-label">排序</label>
                        <div class="layui-input-inline">
                            <input type="number" name="sort" value="{{ $goodsTypeId->sort ?? 255 }}" lay-verify="required|number" placeholder="请输入数字" class="layui-input" >
                        </div>
                    </div>


                     <div class="layui-form-item">
                        <label for="" class="layui-form-label">封面</label>
                        <div class="layui-input-block">
                            <div class="layui-upload">
                                <button type="button" class="layui-btn layui-btn-sm uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
                                </button>
                                <div class="layui-upload-list">
                                    <ul class="layui-upload-box layui-clear">
                                        @if(isset($goodsCat->thumb))
                                            <li><img src="{{ $goodsCat->thumb }}" width="100"/>
                                                <p>上传成功</p></li>
                                        @endif
                                    </ul>
                                    <input type="hidden" name="thumb" class="layui-upload-input" value="{{ $goodsCat->thumb??'' }}">
                                </div>
                            </div>
                        </div>
                    </div>
           </div>
        </div>
        <div class="text-center"
             style="position: fixed;bottom: 0;left: 0;right: 0;background-color: #fff;box-shadow: 0 -1px 5px rgba(0,0,0,.15);padding: 15px;">
            <!-- <button class="layui-btn layui-btn-primary" type="reset">重置表单</button> -->
            <button class="layui-btn" lay-filter="SubmitForm" lay-submit>保存</button>
        </div>
    </div>
</form>