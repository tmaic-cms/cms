@extends('theme::layouts.app')
@section('title'){{$showData->seo_title}}@endsection
@section('keyword'){{$showData->keywords}}@endsection
@section('description'){{$showData->description}}@endsection
@section('css')


@endsection
@section('topjs')

@endsection
@section('body', 'casedetail')
@section('bodyid', 'top')
@section('main') 

<div class="box1">
    <div class="nt">
      <h1>{{$showData->title}}</h1>
      <p>{{$showData->created_at->format('Y-m-d') }}</p>
    </div>
    <div class="lrbox">
      <div class="leftbox">
         {!! $showData->body !!}
 
      </div>
      <div class="rightbox">
        <div class="case-guide__box">
          <span>其他案例</span>
          <ul class="case-guide__list">
		    @cases($casesList rows=6 orderby=rand)
            <li>
              <img src="{{$casesList->thumb}}" title="{{$casesList->title}}" alt="{{$casesList->seo_title}}" />
              <p class="transition">{{$casesList->title}}</p>
              <div class="xuanfuc transition">
                <div class="xuanfu">
                  <a href="{{$casesList->link}}">了解详情</a>
                  <a>立即预约</a>
                </div>
              </div>
            </li>
			@endcases
			
			 
			
          </ul>
        </div>
        <div class="nd-guide__box">
          <span>其他新闻</span>
          <ul class="nd-guide__list">
		    @article($articlelist rows=6 orderby=rand)
            <li>
              <a href="{{$articlelist->link}}">
                <div class="img">
                  <img class="transition" src="{{$articlelist->thumb}}" alt="{{$articlelist->seo_title}}">
                </div>
                <article>
                  <time>{{$articlelist->created_at->format('Y-m-d') }}</time>
                  <h2>{{$articlelist->title}}</h2>
                </article>
              </a>
            </li>
			@endarticle
			 
          </ul>
        </div>
        <div class="list-toggle__full">
          <div>
		@prev($prev_data model=c)
          @if($prev_data)
          <div>
            <a href="{{$prev_data->link}}" class="list-prev">
              <span>上一篇：</span>
              <span>{{$prev_data->title}}</span>
            </a>
          </div>
		  @else
          <div>
            <a class="list-prev">
              <span>上一篇：</span>
              <span>没有了</span>
            </a>
          </div>
		  @endif
		@endprev
		@next($next_data model=c) 
		 
          @if($next_data)
          <div>
            <a href="{{$next_data->link}}" class="list-next">
              <span>下一篇：</span>
              <span>{{$next_data->title}}</span>
            </a>
          </div>
		  @else
          <div>
            <a class="list-next">
              <span>下一篇：</span>
              <span>没有了</span>
            </a>
          </div> 
		  @endif
		@endnext 
		  
          <div>
            <a href="{{$column->link}}" class="back-list">返回列表<i class="iconfont">&#xe602;</i></a>
          </div>
        </div>
      </div>
    </div>
	<!-- end rightbox-->
  </div>
 </div>  
@endsection