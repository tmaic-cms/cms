<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use App\Models\Product\Atlas;
use Illuminate\Support\Facades\DB;
use App\Services\UploadServices;


class AtlasController extends AdminController
{
 

    /**
     * 数据接口
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {

        $productid = $request->get('id');
     
        $model = Atlas::query();
        if ($productid) {
            $model = $model->where('product_id', $productid);
        }
    
        $res = $model->where('status',1)->orderBy('id', 'desc')->paginate($request->get('limit', 10));
        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res->total(),
            'data' => $res->items(),
        ];
        return Response::json($data);
    }

  


 
    public function upload(Request $request)
    {
       $productid= $request->get('id');
 
        return View::make('admin.product.upload',compact('productid'));
    }

   
    /**
     * 添加
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request,UploadServices $atlas)
    {

        $productid=(int)$request->get('id');
        if(!$productid) return Response::json(['code' => 401, 'msg' => '上传失败,productid is not null', 'data' => '上传失败']);

         $fileData = $atlas->uploadImg($request);

        if($fileData['code'] !=200){
           return Response::json(['code' => 401, 'msg' => '上传失败!', 'data' => '上传失败']);
        }
        DB::beginTransaction();
         try {
            $Atlas = new Atlas();
            $Atlas->product_id = $productid;
            $Atlas->name = $fileData['name'];
            $Atlas->path = $fileData['url'];
            $Atlas->status = 1;
  
            if($Atlas->save()){
                 DB::commit();
                return Response::json(['code' => 0, 'msg' => '保存成功', 'data' => '保存成功']);
            }

        } catch (\Exception $exception) {
            DB::rollback();

        }
        return Response::json(['code' => 0, 'msg' => '保存失败', 'data' => '保存失败']);
    }



    public function updateAjax(Request $request)
    {
        $id = (int)$request->get('id');
        $article = Atlas::where('id', $id)->first();
        $data = $request->all();
        DB::beginTransaction();
        try {
            $article->update($data);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '状态更新成功', 'data' => '状态更新成功']);
        } catch (\Exception $exception) {
            DB::rollback();

        }
        return Response::json(['code' => 1, 'msg' => '状态更新失败', 'data' => $exception->getMessage()]);
    }

    public function destroy(Request $request,UploadServices $atlas)
    {
        $id = (int)$request->get('ids');

        if (empty($id)) {
            return Response::json(['code' => 1, 'msg' => '请选择删除项']);
        }
        DB::beginTransaction();
        try {
           
             $Atlas = Atlas::where('id', $id)->first();
                
             if($Atlas && $atlas->delete($Atlas->name)){
                $Atlas = Atlas::where('id', $id)->delete();
                DB::commit();
                return Response::json(['code' => 0, 'msg' => '删除成功']);
             }
            
            
        } catch (\Exception $exception) {
            DB::rollback();
            return Response::json(['code' => 1, 'msg' => '删除失败', 'data' => $exception->getMessage()]);
        }


    }


}
