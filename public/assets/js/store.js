layui.use(['treeTable', 'form', 'admin', 'laytpl', 'table', 'util', 'laydate', 'element'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
    // 渲染表格
    var insTb = table.render({
        elem: '#tableData',
        skin: 'line',
        size: 'lg',
        url: postData,
        page: true,
        toolbar: ['<p>',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
            '<button lay-event="delete" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
            '</p>'].join(''),
        cellMinWidth: 100,
        cols: [[
            {type: 'numbers', title: '序号'},
            {type: 'checkbox'},
            //{templet: '<div>{{d.name||d.title}}</div>', title: '广告位名称',width:350},
            {field: "store_name", title: '商家名称', width: 250},
            {field: 'username', title: '管理员', width: 80},
            {field: 'store_image', title: '缩略图', toolbar: '#store_image', width: 80},
            {field: 'telphone', title: '联系电话', width: 200},
            {field: 'address', title: '地址', width: 200},
            //{field: 'market_price', title: '市场价', width: 80},
            //{field: 'unit', title: '单位', width: 80},
            {field: "order_money", title: '交易额', width: 150},
            {field: 'order_num', title: '订单数量', width: 80},
            //{field: 'pay_time_format', title: '支付时间'},
            //{toolbar: '#status', sort: true, title: '发布状态', width: 95},
            //{align: 'center', toolbar: '#eDialogTbBar', title: '详情', width: 160},
            {field: 'uid', title: 'UID'},
            {field: 'status', title: '状态', templet: '#goodstatus', sort: true, width: 100},
            {field: 'updated_at', title: '更新时间', width: 180},
            {align: 'left', toolbar: '#options', title: '操作', width: 200, fixed: "right"},
        ]]
    });
    laydate.render({
        elem: '#created_at'
        , type: 'datetime'
        , range: true
    });
    // 列点击事件
    table.on('tool(tableData)', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (obj.event === 'edit') {
            admin.open({
                type: 2,
                title: '编辑商品信息',
                content: '/admin/goods/edit?id=' + obj.data.id,
                area: ['100%', '100%'],
                offset: '0px',
                scrollbar: true,
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    treeTable.reload('advertTree');
                    layer.close(index);
                    return;
                }
            });
        } else if (obj.event === 'del') {
            doDel({ids: obj.data.id});
        } else if (obj.event === 'checkList') {
            showComments(data.id);
        } else if (obj.event === 'comments') {
            showComments(data.id);
        } else if (obj.event === 'examine') {
            layer.confirm('确定要审核通过吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);
                /*
                $.post(examine, {id: obj.data.id, status: 1,}, function (res) {
                    layer.close(loadIndex);
                    if (res.code === 0) {
                        layer.msg(res.msg, {icon: 1});
                        insTb.reload({page: {curr: 1}});
                    } else {
                        layer.msg(res.msg, {icon: 2});
                    }
                }, 'json');*/

                $.ajax({
                    type: "POST",
                    url: examine,
                    data: {id: obj.data.id, status: 1,},
                    dataType: 'json',
                    success: function (res) {layer.close(loadIndex);
                        if (res.code === 0) {  layer.msg(res.msg, {icon: 1}); insTb.reload({page: {curr: 1}});
                        } else { layer.msg(res.msg, {icon: 2}); }
                    },
                    error: function (msg) {
                        if (msg.status == 422) {
                            var json = JSON.parse(msg.responseText);
                            json = json.errors;
                            for (var item in json) {
                                for (var i = 0; i < json[item].length; i++) {
                                    layer.msg(json[item][i], {icon: 2});  return;
                                }
                            }
                          layer.close(loadIndex);
                        } else {
                            layer.msg('服务器连接失败', {icon: 2});
                            layer.close(loadIndex);
                            return;
                        }
                    }
                });


            });
        }

    });
    // 列点击事件 end
    // 审核记录弹窗
    function openCheckList(d) {
        laytpl(eDialogCheckDialog.innerHTML).render(d, function (html) {
            admin.open({
                type: 1,
                title: '审核详情',
                content: html
            });
        });
    };
    // 审核记录弹窗 end

    // 查看评论工具条点击事件
    table.on('tool(eDialogCommentTable)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { // 删除
            layer.msg('删除成功', {icon: 1});
        }
    });

    //添加备注
    $('#eDialogCommentBtnAdd').click(function () {
        layer.prompt({
            title: '添加评论',
            shade: .1,
            offset: '165px',
            skin: 'layui-layer-admin layui-layer-prompt',
            formType: 2
        }, function (value, index, elem) {
            layer.close(index);
            layer.msg('备注成功', {icon: 1});
        });
    });


    // 查看订单清单
    function showComments(id) {
        admin.open({
            type: 1,
            area: '900px',
            offset: '65px',
            title: 'SKU清单',
            content: $('#eDialogCommentDialog').html(),
            success: function (layero) {
                // 渲染表格
                var insTbCom = table.render({
                    elem: '#eDialogCommentTable',
                    url: skuData + id,
                    page: true,

                    height: 400,
                    cellMinWidth: 100,
                    cols: [[
                        {type: 'numbers', title: '序号'},
//                        {type: 'checkbox'},
                        {templet: '<div>{{d.goods.GoodsName}}</div>', title: '商品名称'},
                        {field: 'attr', title: '属性', toolbar: '#skulist', width: 250},
                        {field: 'amount', sort: true, title: '价格', width: 80},
                        {field: 'stock', sort: true, title: '库存', width: 80},
/*
                        {
                            title: '创建时间', sort: true, templet: function (d) {
                                return util.toDateString(d.created_at);
                            }, width: 160
                        },
*/
                        {align: 'center', toolbar: '#eDialogCommentTbBar', title: '操作', width: 60}
                    ]],
                    done: function () {
                        $(layero).find('.layui-table-view').css('margin', '0');
                    }
                });

                // 工具条点击事件
                table.on('tool(eDialogCommentTable)', function (obj) {
                    var data = obj.data;
                    var layEvent = obj.event;
                    if (layEvent === 'del') { // 删除
                        layer.msg('删除成功', {icon: 1});
                    }
                });

                // 添加备注
                $('#eDialogCommentBtnAdd').click(function () {
                    layer.prompt({
                        title: '添加备注',
                        shade: .1,
                        offset: '165px',
                        skin: 'layui-layer-admin layui-layer-prompt',
                        formType: 2
                    }, function (value, index, elem) {
                        layer.close(index);
                        layer.msg('备注成功', {icon: 1});
                    });
                });

            }
        });
    };
    /* 表头工具栏点击事件 */
    table.on('toolbar(tableData)', function (obj) {
        if (obj.event === 'add') { // 添加
            admin.open({
                type: 2,
                title: '添加订单', fixed: true,
                content: advcreate,
                area: ['900px', '650px'],
                fixed: true,
                data: {},
                end: function () {
                    layer.close(index);
                    insTb.reload();
                    return false;
                },
                cancel: function (index, layero) {
                    layer.close(index);
                    insTb.reload();
                    return false;
                }
            });
        } else if (obj.event === 'delete') {
            var checkRows = table.checkStatus('tableData');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
            doDel({ids: ids});
        }
    });

    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要下线商家吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            /*
            $.post(destroy, {
                _method: 'delete',
                id: obj.ids
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');*/
            $.ajax({
                type: "POST",
                url: destroy,
                data: {_method: 'delete',id: obj.ids, status: 4,},
                dataType: 'json',
                success: function (res) {
                    layer.close(loadIndex);
                    if (res.code === 0) {  layer.msg(res.msg, {icon: 1}); insTb.reload({page: {curr: 1}});
                    } else { layer.msg(res.msg, {icon: 2}); }
                },
                error: function (msg) {
                    if (msg.status == 422) {
                        var json = JSON.parse(msg.responseText);
                        json = json.errors;
                        for (var item in json) {
                            for (var i = 0; i < json[item].length; i++) {
                                layer.msg(json[item][i], {icon: 2});  return;
                            }
                        }
                      layer.close(loadIndex);
                    } else {
                        layer.msg('服务器连接失败', {icon: 2});
                        layer.close(loadIndex);
                        return;
                    }
                }
            });





        });
    }

    /* 搜索 */
    form.on('submit(btnSearch)', function (data) {
        insTb.reload({where: data.field, page: {curr: 1}});
        return false;
    });

    var orderurl = location.hash.replace(/^#datalist=/, '');
    console.log(orderurl);
    element.tabChange('datalist', orderurl);
    element.on('tab(datalist)', function (elem) {
        location.hash = 'datalist=' + $(this).attr('lay-id');
        var field = $(this).attr('lay-data');
        insTb.reload({where: {'status': field}, page: {curr: 1}});
    });

});