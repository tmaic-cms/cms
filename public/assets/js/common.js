var module ='/assets/module/'
layui.config({  // common.js是配置layui扩展模块的目录，每个页面都需要引入
    version: '318',   // 更新组件缓存，设为true不缓存，也可以设一个固定值
    base: getProjectUrl() + 'assets/module/'
}).extend({
    steps: 'steps/steps',
    notice: 'notice/notice',
    cascader: 'cascader/cascader',
    dropdown: 'dropdown/dropdown',
    fileChoose: 'fileChoose/fileChoose',
    Split: 'Split/Split',
    Cropper: 'Cropper/Cropper',
    tagsInput: 'tagsInput/tagsInput',
    citypicker: 'city-picker/city-picker',
    introJs: 'introJs/introJs',
    zTree: 'zTree/zTree'
}).use(['layer', 'admin','index','upload'], function () {
    var $ = layui.jquery;
    var layer = layui.layer;
    var admin = layui.admin;
    var index = layui.index;
    var upload = layui.upload;
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
	
	
	
	
	
	
layui.$('.downloadImage').click(function () {
	var url=layui.$("#downloadurl").val()
	if(!url) return layer.msg("网络图片资源地址不能为空!");
	layui.$.ajax({
		type: "POST",
		url: downloadimage,
		data: {url:url},
		dataType: 'json',
		success: function (res) {
			if (res.code == 0) {
				layui.$("#downloadurl").val('')
				layui.$('#layui-upload-box').find('img').attr('src', res.url);
				layui.$("#thumb").val(res.url);
				layui.$('#layui-upload-box li p').text('上传成功');
				layer.msg(res.msg, {icon: 1, time: 2000});
			} else {
				layer.msg(res.msg, {icon: 2});
			}
		},
		error: function (msg) {
			if (msg.status == 422) {
				var json = JSON.parse(msg.responseText);
				json = json.errors;
				for (var item in json) {
					for (var i = 0; i < json[item].length; i++) {
						layer.msg(json[item][i], {icon: 2});
						return;
					}
				}
			} else {
				layer.msg('服务器连接失败', {icon: 2});
				return;
			}
		}
	});
});


/** 移动端上传**/
layui.$('.downloadImageWap').click(function () {
	var url=layui.$("#downloadurlwap").val()
	if(!url) return layer.msg("网络图片资源地址不能为空!");
	layui.$.ajax({
		type: "POST",
		url: downloadimage,
		data: {url:url},
		dataType: 'json',
		success: function (res) {
			if (res.code == 0) {
				layui.$("#downloadurlwap").val('')
				layui.$('#uploadView-upload-box').find('img').attr('src', res.url);
				layui.$("#wapthumb").val(res.url);
				layui.$('#uploadView-upload-box li p').text('上传成功');
				layer.msg(res.msg, {icon: 1, time: 2000});
			} else {
				layer.msg(res.msg, {icon: 2});
			}
		},
		error: function (msg) {
			if (msg.status == 422) {
				var json = JSON.parse(msg.responseText);
				json = json.errors;
				for (var item in json) {
					for (var i = 0; i < json[item].length; i++) {
						layer.msg(json[item][i], {icon: 2});
						return;
					}
				}
			} else {
				layer.msg('服务器连接失败', {icon: 2});
				return;
			}
		}
	});
});











	
/******普通图片上传 start*******/	
layui.$(".uploadPic").each(function (index,elem) {
	upload.render({
		elem: $(elem)
		,url: uploadImg+'?span=1'
		,multiple: false
		,data:{"_token":$('meta[name="csrf-token"]').attr('content')}
		,done: function(res){
			if(res.code == 0){
				$(elem).parent('.layui-upload').find('.layui-upload-box').html('<li><img src="'+res.url+'"  width="80"/><p>上传成功</p></li>');
				layui.$('#thumb').val(res.url);
				//主题封面
				layui.$('#uploadView').removeClass('layui-hide').find('img').attr('src', res.url);
				$(elem).parent('.layui-upload').find('.layui-upload-input').val(res.url);
				layer.msg(res.msg,{icon:1})
			}else {
				layer.msg(res.msg,{icon:2})
			}
		}
	});
});
/******普通图片上传 end*******/	
});

 
 
 
/***********/

/** 获取当前项目的根路径，通过获取layui.js全路径截取assets之前的地址 */
function getProjectUrl() {
    var layuiDir = layui.cache.dir;
    if (!layuiDir) {
        var js = document.scripts, last = js.length - 1, src;
        for (var i = last; i > 0; i--) {
            if (js[i].readyState === 'interactive') {
                src = js[i].src;
                break;
            }
        }
        var jsPath = src || js[last].src;
        layuiDir = jsPath.substring(0, jsPath.lastIndexOf('/') + 1);
    }
    return layuiDir.substring(0, layuiDir.indexOf('assets'));
}
