<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\AdvTagServices;
 
class BannersServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
         
    
        // banner模板标签开始 参数：循环对象，广告位置名称
        Blade::directive('banners', function ($expression) {

            $tag = new AdvTagServices($expression);
          
            $parseStr  = $parseStr   =  '<?php ';
			
			$parseStr .= '$__ADVCOLUMN__ = cache()->remember("cache_index_banner_key", '.webconfig("SYSCACHETIME").', function (){ ';
			
            $parseStr .='$__ADVMODEL__ = \App\Models\PositionTranslation::query();';
			
            $parseStr .='$__ADVMODEL__ = $__ADVMODEL__->lang();';
			
			$parseStr .='$__ADVMODEL__->whereIn("pid",[1]);'; 
           
            $parseStr .='return $__ADVMODEL__->get();';

            $parseStr .=  '}); '; 
			 
            $parseStr .= ' foreach($__ADVCOLUMN__ as $key=>$item) { ?>';
            $parseStr .= '<?php '. $tag->object .'= $item; ?>';
	 
            

            return $parseStr;
        });

        // banner模板标签结束
        Blade::directive('endbanners', function () {
            return "<?php } ?>";
        });

 






 
    }
}
