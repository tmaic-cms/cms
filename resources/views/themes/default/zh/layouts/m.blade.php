@include('theme::layouts.nav')
@if($routename != "home")
@include('theme::layouts.crumb')	
@endif
@yield('main')
@include('theme::layouts.footer')

