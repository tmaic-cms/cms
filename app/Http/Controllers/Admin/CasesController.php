<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use App\Models\Cases\MainCases;
use App\Models\Cases\CasesTranslation;
use App\Models\Cases\Atlas;
use App\Services\QiniuServices;
use Illuminate\Support\Facades\DB;



class CasesController extends AdminController
{
	private $model='Cases';

    /**
     * 案例列表
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
         return View::make('admin.case.index');
    }

    /**
     * 数据接口
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {

        $created_at = $request->get('created_at') ?? null;
        $lang = $request->get('lang', 'zh');
        $keyword = $request->get('title');

        $model = CasesTranslation::query();
        if ($lang) {
            $model = $model->where('locale', $lang);
        }

        if (!empty($keyword)) {
            $model = $model->where('title', 'like', "%{$keyword}%")->orWhere('description', 'like', "%{$keyword}%")->orWhere('body','like', "%{$keyword}%");
        }

        if (!empty($created_at)) {
            $startime = substr($created_at, 0, 20);
            $endtime = substr($created_at, -20, 20);
            $model = $model->whereBetween('created_at', [$startime, $endtime]);
        }
        $res = $model->with('column')->isdelete()->orderBy('id', 'desc')->paginate($request->get('limit', 10));
        $data =['code' => 0,'msg' => '正在请求中...', 'count' => $res->total(),'data' => $res->items(),
        ];
        return Response::json($data);
    }

    public function create()
    {
         
        $GoodsType = $this->getColumnClass($this->model);
        return View::make('admin.case.create', compact('GoodsType'));
    }

    /**
     * 添加
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token', 'file']);
		$request = (object)$data;
        DB::beginTransaction();
        try {
            $Cases = new MainCases();
            $Cases->name = $request->title;
            $Cases->column_id = $request->column_id;
            $Cases->online = 1;         
            foreach (config('translatable.locales') as $locale => $item) {
                $Cases = $this->createItem($Cases,$request,$locale);
            }
            if($Cases->save()){
                DB::commit();
                return Response::json(['code' => 0, 'msg' => '添加成功', 'data' => '添加成功']);
            }
         } catch (\Exception $exception) {
            DB::rollback();
         }
        return Response::json(['code' => 1, 'msg' => '添加失败', 'data' => '添加失败']);
    }


    /**
     * 更新
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Request $request)
    {
 
        $slug = $request->get('slug');
        $locale = $request->get('locale');
        $case = MainCases::whereTranslation('slug', $slug)->firstOrFail();
        $case = $case->translate($locale);
        //栏目
        $GoodsType = $this->getColumnClass($this->model);

        return View::make('admin.case.edit', compact('case', 'GoodsType'));
    }

    /**
     * 更新
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $id = (int)$request->get('id');
        $locale = $request->get('lang');
        if (empty($locale)) return Response::json(['code' => 0, 'msg' => '更新失败-lang', 'data' => '更新失败']);
        $MainCases = MainCases::whereTranslation('id', $id)->firstOrFail();
        $Cases = CasesTranslation::where('id', $id)->firstOrFail();
        $data = (array)$request->except(['file', '_token']);
        $request = (object)$data;

        DB::beginTransaction();
        try {
			$Cases->update($data); 
            
            foreach (config('translatable.locales') as $locale => $item) {
				
				if(!$MainCases->hasTranslation($locale)){
					$MainCases = $this->createItem($MainCases,$request,$locale);
					
					
				}
				$MainCases->translateOrNew($locale)->column_id = $request->column_id;	
            }
			$MainCases->save();
			
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '更新成功', 'data' => '更新成功']);
        } catch (\Exception $exception) {
            DB::rollback();
        }
        return Response::json(['code' => 0, 'msg' => '更新失败', 'data' => '更新失败']);
    }

    public function updateAjax(Request $request)
    {
        $id = (int)$request->get('id');
        $model = CasesTranslation::where('id', $id)->first();
        $data = $request->all();
        DB::beginTransaction();
        try {
            $model->update($data);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '状态更新成功', 'data' => '状态更新成功']);
        } catch (\Exception $exception) {
            DB::rollback();

        }
        return Response::json(['code' => 1, 'msg' => '状态更新失败', 'data' => $exception->getMessage()]);
    }

    public function destroy(Request $request,QiniuServices $qiniu)
    {
        $ids = (array)$request->get('ids');
        $mids = (array)$request->get('mid');
        if (!is_array($ids) || empty($ids)) {
            return Response::json(['code' => 1, 'msg' => '请选择删除项']);
        }
        DB::beginTransaction();
        try {
            foreach ($ids as $id){

			    $case = CasesTranslation::where('id', $id)->first();	
				$mainCsese = MainCases::where('mid', $case->cases_mid)->firstOrFail();
				
				foreach($mainCsese->translations as $item){
				//删除产品资源
				 $qiniu->objFromDelete($item);
				 
				//删除产品资源
				$qiniu->objFromDelete($item);
				//删除图集
				$Atlas = Atlas::where('cases_id',$item->id)->get();
				$qiniu->batchDelete($Atlas);
				
				$case->delete();
				}

				$mainCsese->deleteTranslations();
            }
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '删除成功']);
        } catch (\Exception $exception) {
            DB::rollback();
            return Response::json(['code' => 1, 'msg' => '删除失败', 'data' => $exception->getMessage()]);
        }
    }
	
	
	private function createItem($Cases,$request,$locale){
		
			$Cases->translateOrNew($locale)->column_id = $request->column_id;
			$Cases->translateOrNew($locale)->title = $request->title.'_请翻译成：'.$locale;
			$Cases->translateOrNew($locale)->seo_title = $request->seo_title;
			$Cases->translateOrNew($locale)->build_area = $request->build_area??'';
			$Cases->translateOrNew($locale)->build_style = $request->build_style??'';
			$Cases->translateOrNew($locale)->keywords = $request->keywords;
			$Cases->translateOrNew($locale)->description = $request->description;
			$Cases->translateOrNew($locale)->thumb = $request->thumb;
			$Cases->translateOrNew($locale)->body = $request->body;
			$Cases->translateOrNew($locale)->updated_at = now();
			$Cases->translateOrNew($locale)->created_at = now();
			return $Cases;
		
	}

	
}
