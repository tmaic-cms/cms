<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;

class LanguageController extends Controller
{
    public function switchLang(Request $request, $language)
    {

        $request->session()->put('language', $language);
		
		$lang['APP_LOCALE']=$language;
		sysEnv($lang);
		 
		\Artisan::call('cache:clear');
		\Artisan::call('config:clear');
		//\Artisan::call('route:clear');
		//\Artisan::call('view:clear');
		//\Artisan::call('clear-compiled');
		\Cache::flush();
		 
		// echo session()->get('language');
        return redirect()->back();

    }
}
