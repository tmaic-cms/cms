<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\ListTagServices;




class ListServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::directive('list', function ($expression) {

		$listObject = new ListTagServices($expression);

		$ListTag = (object)$listObject->tag;
		
		$parseStr   =  '<?php ';
 
        if(isset($listObject->useVal)){
			
			$parseStr .= $listObject->object .'Reslut = cache()->remember(setCacheKey('.$listObject->sysTagcachekey.'), '.$listObject->tagcachetime.', function () use('.$listObject->useVal.'){ ';

		}else{
			$parseStr .= $listObject->object .'Reslut = cache()->remember(setCacheKey('.$listObject->sysTagcachekey.'), '.$listObject->tagcachetime.', function () use($column) { ';

		}
 
		$parseStr .= $listObject->object .'ListModel = '.$ListTag->model;

		$parseStr .= $listObject->object .'ListModel = '.$listObject->object .'ListModel->with("column")->lang();';

		if($listObject->pid && $listObject->pid == true){
			
			$parseStr .=$listObject->object .'ListModel = '.$listObject->object .'ListModel'.$ListTag->typeid;
		}else{
			
			$parseStr .=$listObject->object .'ListModel = '.$listObject->object .'ListModel'.$ListTag->typeid;
		}
		

		 
		$parseStr .= $listObject->object .'ListModel = '.$listObject->object .'ListModel'.$ListTag->orderby;
	 
 
 
		$parseStr .='return '. $listObject->object .'Reslut = '.$listObject->object .'ListModel'.$ListTag->page;
		
		$parseStr  .=  ' ;}); ';
		$parseStr .= $listObject->pageObj.'= '.$listObject->object .'Reslut;';

		
 
		$parseStr .=' $count = count('.$listObject->object .'Reslut); ';
		
		$parseStr .=$listObject->object .'Count = count('.$listObject->object .'Reslut); ';
 
		$parseStr .=' foreach('.$listObject->object .'Reslut  as '.$listObject->object .'key=>$item) { ?>';
		
		if($listObject->length > 0) {
 
			$listObject->adorn?
			$parseStr .='<?php $item->title=cutstr($item->title,'.$listObject->length.',"'.$ListTag->adorn.'");?>':
			$parseStr .='<?php $item->title=cutstr($item->title,'.$listObject->length.');?>';
			
		}
		
		
		
		
		if($listObject->desclength > 0){
			$listObject->adorn?
			$parseStr .='<?php $item->description=cutstr($item->description,'.$listObject->desclength.',"'.$ListTag->adorn.'");?>':
			$parseStr .='<?php $item->description=cutstr($item->description,'.$listObject->desclength.');?>';
		}
		$parseStr .='<?php '. $listObject->object .'= $item; ?>';
 
		 
		$parseStr .='<?php  $index = '.$listObject->object .'key; ?>';
		$parseStr .='<?php  $iteration = '.$listObject->object .'key+1; ?>';
		$parseStr .='<?php  '.$listObject->object .'key ==0?$first = true:$first=false; ?>';
		$parseStr .='<?php  '.$listObject->object .'key == ('.$listObject->object .'Count-1)?$last = true:$last=false; ?>';
		$parseStr .='<?php  $remaining = '.$listObject->object .'Count-('.$listObject->object .'key+1); ?>';
		$parseStr .='<?php  $obj = [
								"index"=>$index,
								"iteration"=>$iteration,
								"first"=>$first,
								"last"=>$last,
								"remaining"=>$remaining
								]; ?>';
		$parseStr .='<?php  '. $listObject->object .'loop = (object)$obj; ?>';			



 
 
		return $parseStr;
        });

        Blade::directive('endlist', function () {
            return " <?php } ?>";
        });
 
    }
}
