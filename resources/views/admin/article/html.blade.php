<!-- 表格操作列 -->
<script type="text/html" id="userTbBar">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="reset">重置密码</a>
</script>



<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('zixun.article.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('zixun.article.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>



<script type="text/html" id="thumb">
    @{{#  if(d.thumb){ }}
    <a href="@{{d.thumb}}" target="_blank" title="点击查看">
        <img src="@{{d.thumb}}" alt="" width="28" height="28">
    </a>
    @{{#  } }}
</script>
 


<!-- 表格操作列 -->
<script type="text/html" id="eDialogTbBar">
    <!-- <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="view"><i class="layui-icon">&#xe6b2;</i>查看</a> -->
    <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="comments"><i class="layui-icon">&#xe6b2;</i></i>查看</a>
</script>

<!-- 查看评论弹窗 -->
<script type="text/html" id="eDialogCommentDialog">
    <table id="eDialogCommentTable" lay-filter="eDialogCommentTable" class="layui-hide"></table>
    <div class="btn-circle" id="eDialogCommentBtnAdd" style="position: absolute; bottom: 60px;" title="发表评论">
        <i class="layui-icon layui-icon-edit"></i>
    </div>
</script>



 <!-- 查看表格操作列 -->
<script type="text/html" id="eDialogCommentTbBar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>



  


<!-- 表格状态列 -->
<script type="text/html" id="tbState">
    <input type="checkbox" lay-filter="tbState" value="@{{d.id}}" lay-skin="switch" lay-text="正常|禁用" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'正常':'锁定'}}</p>
</script>

<!-- 表格状态列 -->
<script type="text/html" id="userTbState">
    <input type="checkbox"  value="@{{d.id}}" lay-skin="switch"
           lay-text="正常|锁定" @{{d.order_status==0?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.state==0?'正常':'锁定'}}</p>
</script>