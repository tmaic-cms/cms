@extends('theme::layouts.wap')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
 
@endsection
@section('topjs')
  <!-- 导航高亮不能去掉 -->
  <script type="text/javascript">
    var heasid = "";
    heasid = '76,138,141';
    headinit(heasid);
  </script>
@endsection
@section('body', '')
@section('bodyid', '')
@section('main') 

  <section class="zzc"></section>
  <section class="mian InpageMain">
    <section class="InpageHeader clearfix">
      <a href="javascript:history.go(-1)" class="back"></a>
      <p>联系我们</p>
      <span></span>
    </section>
    <!--分类-->
	 {{--
    <section class="InpageNav clearfix">
      <section class="InpageNav-slide">
        <div class="InpageNav-left">
          <div class="InpageNav-list">
            <ul class="clearfix">

              <li sid="139">
                <i></i>
                <a href="/help/gsjj.html" sid="139">公司简介</a>
              </li>

              <li sid="140">
                <i></i>
                <a href="/help/scjd.html" sid="140">生产基地</a>
              </li>

              <li sid="170">
                <i></i>
                <a href="/help/tbzzgr.html" sid="170">投标资质</a>
              </li>

              <li sid="141">
                <i></i>
                <a href="/help/lxyfm.html" sid="141">联系我们</a>
              </li>

              <li class="sideline"></li>
            </ul>
          </div>
        </div>
      </section>
	 
      <section class="more">更多</section>
      <section class="classfiy">
        <span class="classfiy_close"><img src="/picture/nav_close.png" title="关闭" alt="关闭"></span>
        <div class="conBox">
          <dl>
            <dt>
              <!--<a href="#" class="on" sid="141">更多分类</a>-->
            </dt>
            <dd>

              <a href="/help/gsjj.html" sid="139">
                公司简介
              </a>

              <a href="/help/scjd.html" sid="140">
                生产基地
              </a>

              <a href="/help/tbzzgr.html" sid="170">
                投标资质
              </a>

              <a href="/help/lxyfm.html" class="on" sid="141">
                联系我们
              </a>

            </dd>
          </dl>
        </div>
      </section>
    </section>
	--}}
 
@adv($adv group=22 row=1)
  <div><img src="{{ $adv->thumb}}" width="100%" /> </div>
@endadv

    <!-- 一篇文章-->
    <section class="about">
      <div class="mainCon">
        <div class="con conlxyfm">

          <p style="text-align: center; margin-top: 5px;"><strong>
		  <span  style="font-size: .500rem;">联系{{ webconfig('abbreviation') }}</span><br /></strong></p>
          
		  <p style="text-align: center; line-height: 1.75em;">
		  @channel($contact typeid=20 rows=1)
		  {!! $contact->body !!} 
		  @endchannel 
          </p>
		  
		  
          <p><br /></p>
          <div style="white-space: normal; overflow-x: scroll;">
            <table align="center">
              <tbody>
                <tr class="firstRow">
                  <td valign="middle" rowspan="1" colspan="2"
                    style="word-break: break-all;padding-left: 50px;padding-bottom: 10px;padding-top: 14px;" width="364"
                    align="left"><strong><span style="font-size: .480rem;">{{ webconfig('abbreviation') }}</span></strong></td>
                </tr>
                <tr style="border-bottom: 1px solid #f4f4f4;">
                  <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                    align="right">
                    <p><img src="/picture/a364f2b489b6a14c.jpg" title="热线" alt="热线" /></p>
                  </td>
                  <td valign="middle" align="left" style="word-break: break-all;" width="313">服务热线：@400</td>
                </tr>
                <tr style="border-bottom: 1px solid #f4f4f4;">
                  <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                    align="right">
                    <p><img src="/picture/9552e5c2093e1070.jpg" title="电话" alt="电话" /></p>
                  </td>
                  <td valign="middle" align="left" style="word-break: break-all;" width="313">联系电话：@phone</td>
                </tr>
                <tr style="border-bottom: 1px solid #f4f4f4;">
                  <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                    align="right"><img src="/picture/6a3debd401e2bd8c.jpg" title="邮箱" alt="邮箱" /></td>
                  <td valign="middle" align="left" style="word-break: break-all;" width="313">电子邮箱：@email
                  </td>
                </tr>
              
                <tr>
                  <td valign="middle" style="word-break: break-all;padding-bottom: 10px;padding: 10px 10px 10px 10px;"
                    width="30" align="right">
                    <p style="text-align:center"><img src="/picture/221b71e5ea5a95c6.jpg" title="地址" alt="地址" />
                    </p>
                  </td>
                  <td valign="middle" align="left" style="word-break: break-all;padding-bottom: 10px;" width="313">
                    <p>地址： @address</p>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
          <p><br /></p>
          <p
            style="white-space: normal; overflow-x: scroll; text-align: center; margin-top: 5px; margin-bottom: 5px; line-height: 1.75em;">
            <span style="font-size: .450rem;"><strong>公司地图</strong></span></p>
          <p style="text-align: center;">
            <div id="container"></div>
          </p>
           
        </div>
      </div>
    </section>
  </section>
@endsection
@section('footerjs')

<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=8325164e247e15eea68b59e89200988b"></script>
<script type="text/javascript">    
var map = new AMap.Map("container", {
    resizeEnable: true,
    center: [113.669498, 22.943401],
    zoom: 16
});
addMarker();

var title = '{{ webconfig("corporateName") }}',  content = [];
content.push("<img src='http://image.zhuomulao.com/1597126240_cc723e4a-c6fc-4614-9c5e-4bd4983a089f.jpg' width='100'>地址：{{ webconfig('address')}}");
content.push("联系人：@Contacts");
content.push("电话：@phone");
content.push("<a href='http://{{ webconfig("abbreviation") }}'>详细信息</a>");
var infoWindow = new AMap.InfoWindow({
    isCustom: true,
    content: createInfoWindow(title, content.join("<br/>")),
    offset: new AMap.Pixel(16, -45)
});



function createInfoWindow(title, content) {
    var info = document.createElement("div");
    info.className = "custom-info input-card content-window-card";
    var top = document.createElement("div");
    var titleD = document.createElement("div");
    var closeX = document.createElement("img");
    top.className = "info-top";
    titleD.innerHTML = title;
    closeX.src = "https://webapi.amap.com/images/close2.gif";
    closeX.onclick = closeInfoWindow;

    top.appendChild(titleD);
    top.appendChild(closeX);
    info.appendChild(top);



    var middle = document.createElement("div");
    middle.className = "info-middle";
    middle.style.backgroundColor = 'white';
    middle.innerHTML = content;
    info.appendChild(middle);


    var bottom = document.createElement("div");
    bottom.className = "info-bottom";
    bottom.style.position = 'relative';
    bottom.style.top = '0px';
    bottom.style.margin = '0 auto';
    var sharp = document.createElement("img");
    sharp.src = "https://webapi.amap.com/images/sharp.png";
    bottom.appendChild(sharp);
    info.appendChild(bottom);
    return info;
}


function closeInfoWindow() {
    map.clearInfoWindow();
}
 
function addMarker() {
    map.clearMap();
    var marker = new AMap.Marker({
        map: map,
        position: [113.669498, 22.943401]
    });
     
    AMap.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker.getPosition());
    });
}


</script>
@endsection