@extends('admin.base')
@section('content')
<form class="layui-form" id="ArticleForm">
@include('admin.product.form')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','admin'], function () {
	var $ = layui.jquery;
	var form = layui.form;
	var formX = layui.formX;
	var upload = layui.upload;
	var element = layui.element;
	var admin = layui.admin;

	form.on('submit(SubmitData)', function (data) {
	$(this).parent().hide();	
	  var  editorData = editor.getData();
	  var formData=data.field;
	  formData.body=editorData;
	  
	  console.log($(this).parents('form').serialize());
		$.ajax({
			type: "POST",
			url: '{{ route("admin.product.store")}}',
			data: formData,
			dataType: 'json',
			success: function (res) {
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				$(this).parent().show();	
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
		return false;
	});



	var orderurl = location.hash.replace(/^#tablist=/, '');
		element.tabChange('tablist', orderurl);
		element.on('tab(tablist)', function(elem){
	location.hash = 'tablist='+ $(this).attr('lay-id');
		var field = $(this).attr('lay-data');
	});
});
</script>
@endsection