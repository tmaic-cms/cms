layui.use(['treeTable', 'form', 'admin', 'laytpl', 'table', 'util', 'laydate', 'element'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
    // 渲染表格
    var insTb = table.render({
        elem: '#orderTree',
        skin: 'line',
        size: 'lg',
        url: postData,
        page: true,
        toolbar: ['<p>',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
            '<button lay-event="delete" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
            '</p>'].join(''),
        cellMinWidth: 100,
        cols: [[
            {type: 'numbers', title: '序号'},
            {type: 'checkbox'},
            //{templet: '<div>{{d.name||d.title}}</div>', title: '广告位名称',width:350},
            {field: "order_no", title: '订单号', width: 200},
            {field: "trade_no", title: '交易号', width: 200},
            {templet: '<div>{{d.store_name||d.goods_name}}</div>', title: '商家名称', width: 150},
            {field: 'order_totals', title: '交易金额', width: 90},
            {field: 'order_return_totals', title: '退款金额', width: 90},
            {field: 'store_points_offset_totals', title: '商家积分抵现', width: 100},
            {field: 'platform_points_offset_totals', title: '平台积分抵现', width: 100},
            {field: 'order_totals', title: '应结金额', width: 100},
            {
                field: 'pay_date', title: '创建时间', templet: function (d) {
                    return util.toDateString(d.createTime);
                }, align: 'center', sort: true
            },
            //{toolbar: '#status', sort: true, title: '状态', width: 120},
            {align: 'center', toolbar: '#status', title: '详情', width: 130},
            //{field: 'state', title: '状态', templet: '#userTbState', sort: true, width: 100},
            {field: 'updated_at', title: '申请时间', width: 160},
            {align: 'l2eft', toolbar: '#options', title: '操作', width: 120, fixed: "right"},
        ]]
    });
    laydate.render({
        elem: '#created_at'
        , type: 'datetime'
        , range: true
    });
    // 列点击事件
    table.on('tool(orderTree)', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (obj.event === 'edit') {
            admin.open({
                type: 2,
                title: '编辑订单信息',
                content: '/admin/order/edit?id=' + obj.data.id,
                area: ['40%', '60%'],
                scrollbar: false,
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    treeTable.reload('advertTree');
                    layer.close(index);
                    return;
                }
            });
        } else if (obj.event === 'del') {
            doDel({ids: obj.data.id});
        } else if (obj.event === 'checkList') {
            showComments(data.orderid);
        } else if (obj.event === 'comments') {
            showComments(data.id);
        } else if (obj.event === 'settlement') {
 
            admin.open({
                type: 2,
                title: '编辑订单信息',
                content: '/admin/cashout/edit?id=' + obj.data.id,
                area: ['70%', '90%'],
                scrollbar: false,
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    treeTable.reload('advertTree');
                    layer.close(index);
                    return;
                }
            });

        } else if (obj.event === 'examine') {
            layer.confirm('确定要审核通过吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);
                $.post(examine_a, {
                    _method: 'delete',
                    id: obj.data.id
                }, function (res) {
                    layer.close(loadIndex);
                    if (res.code === 0) {
                        layer.msg(res.msg, {icon: 1});
                        insTb.reload({page: {curr: 1}});
                    } else {
                        layer.msg(res.msg, {icon: 2});
                    }
                }, 'json');
            });
        }

    });
    // 列点击事件 end
    // 审核记录弹窗
    function openCheckList(d) {
        laytpl(eDialogCheckDialog.innerHTML).render(d, function (html) {
            admin.open({
                type: 1,
                title: '审核详情',
                content: html
            });
        });
    };
    // 审核记录弹窗 end

    // 查看评论工具条点击事件
    table.on('tool(eDialogCommentTable)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { // 删除
            layer.msg('删除成功', {icon: 1});
        }
    });

    //添加备注
    $('#eDialogCommentBtnAdd').click(function () {
        layer.prompt({
            title: '添加评论',
            shade: .1,
            offset: '165px',
            skin: 'layui-layer-admin layui-layer-prompt',
            formType: 2
        }, function (value, index, elem) {
            layer.close(index);
            layer.msg('备注成功', {icon: 1});
        });
    });


    // 查看订单清单
    function showComments(id) {
        admin.open({
            type: 1,
            area: '900px',
            offset: '65px',
            title: '订单清单',
            content: $('#eDialogCommentDialog').html(),
            success: function (layero) {
                // 渲染表格
                var insTbCom = table.render({
                    elem: '#eDialogCommentTable',
                    url: goodsData + id,
                    page: true,

                    height: 400,
                    cellMinWidth: 100,
                    cols: [[
                        {type: 'numbers', title: '序号'},
                        {type: 'checkbox'},
                        {field: 'order_no', title: '订单号'},
                        {field: 'goods_name', sort: true, title: '商品名称'},
                        {field: 'price', sort: true, title: '交易金额', width: 80},
                        {
                            title: '创建时间', sort: true, templet: function (d) {
                                return util.toDateString(d.created_at);
                            }, width: 160
                        },
                      //  {align: 'center', toolbar: '#eDialogCommentTbBar', title: '操作', width: 60}
                    ]],
                   
                });

                // 工具条点击事件
                table.on('tool(eDialogCommentTable)', function (obj) {
                    var data = obj.data;
                    var layEvent = obj.event;
                    if (layEvent === 'del') { // 删除
                        layer.msg('删除成功', {icon: 1});
                    }
                });

                // 添加备注
                $('#eDialogCommentBtnAdd').click(function () {
                    layer.prompt({
                        title: '添加备注',
                        shade: .1,
                        offset: '165px',
                        skin: 'layui-layer-admin layui-layer-prompt',
                        formType: 2
                    }, function (value, index, elem) {
                        layer.close(index);
                        layer.msg('备注成功', {icon: 1});
                    });
                });

            }
        });
    };
    /* 表头工具栏点击事件 */
    table.on('toolbar(orderTree)', function (obj) {
        if (obj.event === 'add') { // 添加
            admin.open({
                type: 2,
                title: '添加订单', fixed: true,
                content: advcreate,
                area: ['900px', '650px'],
                fixed: true,
                data: {},
                end: function () {
                    layer.close(index);
                    insTb.reload();
                    return false;
                },
                cancel: function (index, layero) {
                    layer.close(index);
                    insTb.reload();
                    return false;
                }
            });
        } else if (obj.event === 'delete') {
            var checkRows = table.checkStatus('orderTree');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
            doDel({ids: ids});
        }
    });

    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要退回？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            $.post(examine_b, {
                _method: 'delete',
                ids: obj.ids
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    }

    /* 搜索 */
    form.on('submit(btnSearch)', function (data) {
        insTb.reload({where: data.field, page: {curr: 1}});
        return false;
    });
    var orderurl = location.hash.replace(/^#orderlist=/, '');
    element.tabChange('orderlist', orderurl);
    element.on('tab(orderlist)', function (elem) {
        location.hash = 'orderlist=' + $(this).attr('lay-id');
        var field = $(this).attr('lay-data');
        insTb.reload({where: {'status': field}, page: {curr: 1}});
    });


});