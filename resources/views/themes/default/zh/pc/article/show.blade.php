@extends('theme::layouts.app')
@section('title'){{$showData->seo_title??$showData->title}}@endsection
@section('keyword'){{$showData->keyword}}@endsection
@section('description'){{$showData->description}}@endsection
@section('css')


@endsection
@section('topjs')


@endsection
@section('body', 'newsdetail')
@section('bodyid', 'top')
@section('main') 

  <div class="box1">
    <div class="nt">
      <h1>{{$showData->title}}</h1>
      <p>{{$showData->created_at->format('Y-m-d') }}</p>
    </div>
    <div class="lrbox">
      <div class="leftbox">
	  {!! $showData->body !!}
      </div>
      <div class="rightbox">
        <div class="nd-guide__box">
          <span>其他新闻</span>
          <ul class="nd-guide__list">
		    @article($articleList rows=6  cachetime=2800)
            <li>
              <a href="{{$articleList->link}}">
                <div class="img">
                  <img class="transition" src="{{$articleList->thumb}}" alt="{{$articleList->title}}">
                </div>
                <article>
                  <time>{{$articleList->created_at->format('Y-m-d') }}</time>
                  <h2>{{$articleList->title}}</h2>
                </article>
              </a>
            </li>
			@endarticle
          </ul>
        </div>
        <div class="list-toggle__full">

         @prev($prev_data model=a)
          @if($prev_data)
          <div>
            <a href="{{$prev_data->link}}" class="list-prev">
              <span>上一篇：</span>
              <span>{{$prev_data->title}}</span>
            </a>
          </div>
		  @else
          <div>
            <a href="#" class="list-prev">
              <span>上一篇：</span>
              <span>没有了</span>
            </a>
          </div>
		  @endif
		@endprev
		
		@next($next_data model=a) 
		 
          @if($next_data)
          <div>
            <a href="{{$next_data->link}}" class="list-next">
              <span>下一篇：</span>
              <span>{{$next_data->title}}</span>
            </a>
          </div>
		  @else
          <div>
            <a class="list-next">
              <span>下一篇：</span>
              <span>没有了</span>
            </a>
          </div> 
		  @endif
		@endnext 
		  
		  
          <div>
            <a href="{{$column->link}}" class="back-list">返回列表<i class="iconfont">&#xe602;</i></a>
          </div>
        </div>
      </div>
    </div>
  </div>
   

 

 

@endsection
@section('footerjs')
  
@endsection










