@extends('admin.base')
@section('content')
<!-- 正文开始 -->
@include('admin.category.form')
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','element','upload','pinyin','admin'], function () {
	var $ = layui.jquery;
	var form = layui.form;
	var formX = layui.formX;
	var upload = layui.upload;
	var element = layui.element;
	var pinyin = layui.pinyin;
	var admin = layui.admin;
	form.val('category_form', {
	  "pid": "{{$category->pid}}"
	  ,"model": "{{$category->model}}"
	  ,"ispart": "{{$category->ispart}}"
	  ,"ismenu": "{{$category->ismenu}}"
	  ,"isdisplay": "{{$category->isdisplay}}"
	  ,"issend": "{{$category->issend}}"
	});

	form.on('radio(ispart)', function(data){
	  $isRoute = data.value;
	  if($isRoute == 1 || $isRoute == 2){
		$("#routeRuleUrl").css('display','none'); 
		$("#routeRule").css('display','block'); 

	  }else{
		$("#routeRuleUrl").css('display','block'); 
		$("#routeRule").css('display','none'); 
	  }
	}); 

  $("#name").on("input",function(e){
	$("#listrule").val(pinyin.ConvertPinyin(e.delegateTarget.value)); 
	//$("#seo_title").val(e.delegateTarget.value); 
	//$("#seo_key").val(e.delegateTarget.value); 
	//$("#description").val(e.delegateTarget.value); 
	//$("#description").val(e.delegateTarget.value); 
	$("#routename").val(pinyin.ConvertPinyin(e.delegateTarget.value)); 
	
  });


	form.on('submit(SubmitForm)', function (data) {
	 $(this).parent().hide();	
	  var editorData = editor.getData();
	  var formData=data.field;
	  formData.body=editorData;
		$.ajax({
			type: "POST",
			url: '{{ route("admin.columns.update",['id'=>$category->id]) }}',
			data: formData,
			dataType: 'json',
			success: function (res) {
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				$(this).parent().show();
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
		return false;
	});
	 

	//普通图片上传
	$(".uploadPic").each(function (index,elem) {
		upload.render({
			elem: $(elem)
			,url: '{{ route("uploadImg") }}'
			,multiple: false
			,data:{"_token":"{{ csrf_token() }}"}
			,done: function(res){
				//如果上传失败
				if(res.code == 0){
					$(elem).parent('.layui-upload').find('.layui-upload-box').html('<li><img src="'+res.url+'"  width="100"/><p>上传成功</p></li>');
					$(elem).parent('.layui-upload').find('.layui-upload-input').val(res.url);
					layer.msg(res.msg,{icon:1})
				}else {
					layer.msg(res.msg,{icon:2})
				}
			}
		});
	});



 });
</script>
@endsection