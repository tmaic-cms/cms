<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/     
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
 
class BaseModel extends Model
{
	 
    protected static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            if (!$model->slug)  $model->slug = randno();
        });
    }
 
	
	
    //过虑语言
    public function scopeLang($query)
    {

        return $query->where('locale','=',lang());
    }
	
	
    public function scopeLanguage($query)
    {
        return $query->where('locale','=',lang());
    }
	

	
	
	
	
	
    public function scopeStatus($query)
    {
        return $query->where('status',1);
    }
	
	//排序
    public function scopeBy($query)
    {
        return $query->orderBy('created_at','desc');
    }

	
	
    public function getLangTypeAttribute($query)
    {  
        switch ($this->locale) {
            case "zh":  $value = "中文";  break;
            case "en":  $value = "英语";  break;
            case "jp":  $value = "日语";  break;
            case "ru":  $value = "俄语";  break;
            case "de":  $value = "德语";  break;
            case "tu":  $value = "韩语";  break;
        }
        return $value;
    }
 
} 
 