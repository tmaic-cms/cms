<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use zgldh\QiniuStorage\QiniuStorage;
use Ramsey\Uuid\Uuid;
use App\Services\InfoService;
use DB;
class PublicController extends AdminController
{
   

    //图片上传处理
    public function uploadImg(Request $request, $isValid = false)
    {
       //return $request->file('file');
        //上传文件最大大小,单位M
        $maxSize =2;//(int) webconfig('UploadMaxSize');
		 
        //支持的上传图片类型
        $allowed_extensions = ["png", "jpg", "gif","svg"];
        //返回信息json
        $data = ['code' => 200, 'msg' => '上传失败', 'data' => ''];
        $file = $request->file('file')??$request->file('upload');
		 
        //dd(time() . '_' . Uuid::uuid4()->toString());
        //检查文件是否上传完成
        if ($file->isValid()) {
            //检测图片类型
            $ext = $file->getClientOriginalExtension();
            if (!in_array(strtolower($ext), $allowed_extensions)) {
                $data['msg'] = "请上传" . implode(",", $allowed_extensions) . "格式的图片";
                return response()->json($data);
            }
			  
            //检测图片大小 $file->getClientSize()
            if (filesize($file) > $maxSize * 1024 * 1024) {
                $data['msg'] = "图片大小限制" . $maxSize . "M";
                return response()->json($data);
            }
			 
			
        } else {
            $data['msg'] = $file->getErrorMessage();
            return response()->json($data);
        }

        $uniqid  = time() . '_' . Uuid::uuid4()->toString();
        $newFile = $uniqid . "." . $file->getClientOriginalExtension();
        $disk    = QiniuStorage::disk(webconfig('OSS_OPEN'));
	 
        $res     = $disk->put($newFile, file_get_contents($file->getRealPath()));
        if ($res) {
            $data = [
                'code' => 0,
                'uploaded'=>"true",
                'msg' => '上传成功',
                'data' => [
                    'src' => $disk->downloadUrl($newFile)
                ],
                'url' => $disk->downloadUrl($newFile),
                'newFile' => $newFile,
				 'files'=>[
				   'file'=> $disk->downloadUrl($newFile),
				 
				 ],
            ];

        } else {
            $data['data'] = $file->getErrorMessage();
        }

        //内部调用返回数组
        if ($isValid) return $data;

        return response()->json($data);
    }

     

    
    public function downloadImage(Request $request)
    {
		
		$url = (string) $request->url;
	    if(empty($url)) return null;
		$extension = @pathinfo($url, PATHINFO_EXTENSION);
		$uniqid  = time() . '_' . Uuid::uuid4()->toString();
		$newFile = $uniqid.'.'.$extension;
		$themfile = file_get_contents($url);
        $disk    = QiniuStorage::disk(webconfig('OSS_OPEN'));
		 
        $res     = $disk->put($newFile,$themfile);
	   
        if ($res) {
            $data = [
                'code' => 0,
                'uploaded'=>"true",
                'msg' => '上传成功',
				//正常接回返回格式
                'data' => [
                    'src' => $disk->downloadUrl($newFile)
                ],
				//单图上传返回格式
                'url' => $disk->downloadUrl($newFile),
                'newFile' => $newFile,
				 //多图片上传返回格式
				 'files'=>[
				   'file'=> $disk->downloadUrl($newFile),
				 
				 ],
            ];
        } else {
            $data['data'] = $file->getErrorMessage();
        }
        
		return $data;
    }
    
 
 
    
    
    
    
    
    
    
    
    
    
    
}