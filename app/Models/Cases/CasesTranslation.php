<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/     
namespace App\Models\Cases;

use App\Models\BaseModel;
 
class CasesTranslation extends BaseModel
{
 
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'case_translations';
    protected $fillable = ['id','cases_mid','seo_title','title','keywords','description','build_area','build_style','locale','thumb','body','slug','sort','isdelete','status','click'];
    protected $appends = ['link','langType'];
    
    


    //生成url
    public function getLinkAttribute($query)
    {
        return route('case.info',['slug'=>$this->slug]);
    }

 
  
	//过虑删除
    public function scopeIsdelete($query,$isdelete=0)
    {
        return $query->where('isdelete', $isdelete);
    }

    //前台使用 isdelete：1：已删除
    public function scopeCondition($query)
    {
        return $query->lang()->status()->where('isdelete',0);
    }
	//排序
    public function scopeBy($query)
    {
        return $query->orderBy('created_at','desc');
    }
	
    //案例
    public function class()
    {
        return $this->hasMany('App\Models\Cases\ColumnTranslation','column_id','column_id')->where('locale',lang());;
    }
    //关联栏目
    public function column()
    {
        return $this->hasOne('App\Models\ColumnTranslation','column_mid','column_id')->lang();
    }

    //关联栏目
    public function image()
    {
        return $this->hasMany('App\Models\Cases\Atlas','cases_id','id');
    }

 




}
        
 