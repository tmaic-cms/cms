layui.use(['treeTable','form', 'admin', 'laytpl','element'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var admin = layui.admin;
    var treeTable = layui.treeTable;
    var element = layui.element;
    var lang='zh';

    var langUrl = location.hash.replace(/^#tablist=/);
    element.tabChange('tablist', langUrl);
    element.on('tab(tablist)', function(elem){

        location.hash = 'tablist='+ $(this).attr('lay-id');
        lang = $(this).attr('lay-data');
        insTb.reload({'id':'25'});
    });

    // 渲染表格
    var insTb = treeTable.render({
        elem: '#permissionTree',
        skin: 'line',
        page: true,
        tree: {
            iconIndex: 3,
            isPidData: true,
            arrowType: 'arrow2',
            getIcon: 'ew-tree-icon-style2'
        },
        toolbar: ['<p>',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
            '</p>'].join(''),
        cols: [[
            {type: 'numbers'},
            {type: 'checkbox'},
            {field: 'id', title: 'ID', width: 80},
            {field: 'display_name', title: '权限名称', width: 220},
            {field: 'name', title: '标识', width: 220},
            {field: 'route', title: '路由', width: 220},


           // {field: 'locale', title: '语言', width: 80},
            //{field: 'thumb', title: '缩略图', toolbar: '#thumb', width: 80},
            //{field: 'seo_title', title: 'seo标题', width: 250},
            //{field: 'description', title: '描述'},
            {field: 'sort', title: '排序', edit: 'text'},
            //{templet: '#ismenu', title: '作为导航', width: 95},
            {templet: '#tbState', title: '状态', width: 95},
            // {field: 'created_at', title: '创建时间', width: 160},
            {field: 'updated_at', title: '更新时间', width: 180},
            {align: 'left', toolbar: '#options', title: '操作', fixed: "right"}
        ]],
        reqData: function (data, callback) {
            setTimeout(function (data) {

                $.post(columnsData,{'lang':lang}, function (res) {
                    callback(res.data);
                });
            }, 100);
        },
        style: 'margin-top:0;'
    });
    // 列点击事件
    treeTable.on('tool(permissionTree)', function (obj) {
        var event = obj.event;
        if (event === 'del') {
            if (obj.data.children && obj.data.children.length > 0) return layer.msg('有子级不能删除');
            console.log({obj: obj.data.id})
            doDel({ids: obj.data.id})
            //layer.msg('删除成功');
        } else if (event === 'edit') {
           
            admin.open({
                type: 2,
                title: '编辑权限 ' + obj.data.id,
                content: edit + obj.data.id,
                 area: ['800px', '600px'],
                scrollbar: false,
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    treeTable.reload('advertTree');
                    layer.close(index);
                    return;
                }
            });
            return;

        } 

    });
    // 搜索
    $('#btnSearch').click(function () {
        var keywords = $('#catName').val();
        if (keywords) {
            insTb.filterData(keywords);
        } else {
            insTb.clearFilter();
        }
    });
    $('#btnClearSearch').click(function () {
        $('#catName').val('');
        insTb.clearFilter();
    });

    treeTable.on('toolbar(permissionTree)', function (obj) {
        switch (obj.event) {
            case 'add':
                admin.open({
                    type: 2,
                    title: '添加栏目', fixed: true,
                    content: create,
                    area: ['800px', '600px'],
                    fixed: true,
                    data: "",
                    end: function () {
                        layer.close(index);
                        insTb.reload();
                        return false;
                    },
                    cancel: function (index, layero) {
                        layer.close(index);
                        insTb.reload();
                        return false;
                    }
                });
                break;
            case 'delete':
                var ids = insTb.checkStatus().map(function (d) {
                    return d.id;
                });
                doDel({ids: ids});
                break;
        }
    });

    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要删除选中数据吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            $.post(destroy, {
                _method: 'delete',
                ids: obj.ids
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload();
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    };

    /* 修改状态 */
    form.on('switch(tbState)', function (obj) {  
         var loadIndex = layer.load(2);
        $.post(updateAjax, {id: obj.elem.value, status: obj.elem.checked ? 1 : 0}, function (res) {
            layer.close(loadIndex);
            if (res.code == 0) {
                layer.msg(res.msg, {icon: 1});
            } else {
                layer.msg(res.msg, {icon: 2});
                $(obj.elem).prop('checked', !obj.elem.checked);
                form.render('checkbox');
            }
        }, 'json');

     });
     

 


});