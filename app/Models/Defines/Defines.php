<?php     
namespace App\Models\Defines;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;
class Defines extends Model
{
    use Translatable;
	
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'defines';
    protected $primaryKey = 'mid';
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = true;
    public $translatedAttributes = ['name','slug'];
    
    protected $fillable = ['title','slug'];
    
    

}
        
 