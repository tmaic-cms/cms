@extends('admin.base')
@section('content')
<style>
body{background:#FFF;margin-bottom:50px;}
.layui-upload-drag {padding: 5px;}
</style>
<form class="layui-form" action="" lay-filter="theme_form">
	@include('admin.theme.form')
</form>
<script>
layui.use(['form', 'formX','element','upload','admin'], function () {
	var $ = layui.jquery;
	var form = layui.form;
	var formX = layui.formX;
	var upload = layui.upload;
	var element = layui.element;
	var admin = layui.admin;
	form.val('theme_form', {
	  "id": "{{$themes->id}}"
	  ,"title": "{{$themes->title}}"
	  ,"type": "{{$themes->type}}"
	  ,"themename": "{{$themes->themename}}"
	  ,"thumb": "{{$themes->thumb}}"
	  ,"desc": "{{$themes->desc}}"
	  ,"selected": "{{$themes->selected}}"
	});

	form.on('submit(SubmitForm)', function (data) {
	  $(this).parent().hide();
	  var formData=data.field;
		$.ajax({
			type: "POST",
			url: '{{ route("admin.theme.update",['id'=>$themes->id]) }}',
			data: formData,
			dataType: 'json',
			success: function (res) {
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				$(this).parent().show();
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
		return false;
	});
	 

	//普通图片上传
	/*
	$("#uploadViewUpload").each(function (index,elem) {
		upload.render({
		elem: $(elem)
		,url: '{{ route("uploadImg") }}'
		,multiple: false
		,data:{"_token":"{{ csrf_token() }}"}
		,done: function(res){
			//如果上传失败
			if(res.code == 0){
				layui.$('#uploadView').removeClass('layui-hide').find('img').attr('src', res.url);
				
				layui.$('#thumb').val(res.url);
				layer.msg(res.url,{icon:1})
				layer.msg(res.msg,{icon:1})
			}else {
				layer.msg(res.msg,{icon:2})
			}
		}
		});
	});
	
	*/

	form.on('radio(setTheme)', function(data){
		$.ajax({
			type: "POST",
			url: '{{ route("admin.theme.updateajax",['id'=>$themes->id]) }}',
			data: {selected:data.value},
			dataType: 'json',
			success: function (res) {
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 2000});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				$(this).parent().show();
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
	});  


});
</script>	
@endsection


