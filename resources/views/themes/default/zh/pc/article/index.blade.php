@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
<link href="/css/tinpage-1.0.css" rel="stylesheet" type="text/css">
<link href="/css/fancybox.css" rel="stylesheet" type="text/css" />
<link href="/css/inpage.css" rel="stylesheet" type="text/css">
@endsection
@section('topjs')
<script type="text/javascript" charset="utf-8" src="/js/inpage.js"></script>
<script type="text/javascript">
var sid = '1,3';
headinit(sid);
</script>
@endsection
@section('body', '')
@section('bodyid', '')
@section('main') 
@adv($adv group=20 row=1)
  <div class="inpage-banner" style="background:url({{ $adv->thumb}});" title="{{ $adv->title }}"></div>
@endadv
@include('theme::layouts.crumb')

  <div class="y-wrapper clearfix">
    <div class="newsListMenu">
      <ul class="clearfix">
		@channel($newlink group=15 rows=5)		 
        <li><a href="{{$newlink->link}}">{{$newlink->name}}</a></li>
		@endchannel
      </ul>
    </div>

@channel($newlink group=15 rows=2)		 
    <div class="newsCenterPress" id="newsCenterPress" style="margin-top:0px;">
      <div class="container">
        <div class="mub01Title">
          <a href="{{ $newlink->link }}">
            <h3><i class="xianL"></i>{!! $newlink->sub_name !!}<i class="xianR"></i></h3>
          </a>
        </div>
        <div class="mainCon">
          <ul class="clearfix">
			@article($article typeid=$newlink->column_mid rows=3 orderby=date)
            <li>
              <a href="{{ $article->link }}" class="Pic" target="_blank">
                <img src="{{ $article->thumb }}" title="{{ $article->title }}" alt="{{ $article->title }}">
              </a>
              <h3>
                <a href="{{ $article->link }}" target="_blank">{{ $article->title }}</a>
              </h3>
              <p class="p1">
                <span class="time">{{ $article->created_at }}</span>
                <span class="readNum">阅读量：{{ $article->click }}</span>
              </p>
              <p class="p2">{{ $article->description }} </p>
            </li>
			@endarticle
          </ul>
        </div>
      </div>
    </div>
@endchannel
@channel($newlink typeid=66 rows=1)
    <div class="newsCenterPress2" id="newsCenterPress2">
      <div class="container">
        <div class="mub01Title">
          <a href="###">
            <h3><i class="xianL"></i>常见<em>问题</em><i class="xianR"></i></h3>
          </a>
        </div>
        <div class="mainCon">
        @article($article typeid=$newlink->column_mid rows=3 orderby=date)
          <dl class="clearfix">
            <dt>
              <a href="{{$article->link}}" target="_blank">
                <img src="{{$article->thumb}}" title="{{$article->title}}" alt="{{$article->seo_title}}">
              </a>
            </dt>
            <dd>
              <h3><a href="{{$article->link}}" target="_blank">{{$article->title}}</a>
              </h3>
              <p>{{$article->description}}</p>
              <span class="label">

              </span>
              <span class="readDate">{{$article->created_at}}</span>
              <span class="readNum">{{$article->click}}</span>
              <a href="{{$article->link}}" target="_blank" class="more"></a>
            </dd>
          </dl>
        @endarticle

 

        </div>
      </div>
    </div>
	
@endchannel
    <script>
      var h = $('#newsCenterPress2 dl img').height();
      if (h > 50) {
        $('#newsCenterPress2 dl dd').height(h);
      }
    </script>
  </div>


@endsection
@section('footerjs')
@endsection










