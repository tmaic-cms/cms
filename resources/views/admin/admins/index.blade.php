@extends('admin.base')
@section('content')
   <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form toolbar">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label left">账&emsp;号:</label>
                        <div class="layui-input-inline">
                            <input name="username" class="layui-input" placeholder="输入账号"/>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">用户名:</label>
                        <div class="layui-input-inline">
                            <input name="nickName" class="layui-input" placeholder="输入用户名"/>
                        </div>
                    </div>
         
                    <div class="layui-inline">&emsp;
                        <button class="layui-btn icon-btn" lay-filter="userTbSearch" lay-submit>
                            <i class="layui-icon">&#xe615;</i>搜索
                        </button>
                    </div>
                    <!-- 下拉按钮 -->
                    <div class="layui-inline dropdown-menu dropdown-hover">

                        <button class="layui-btn layui-btn-normal icon-btn" type="button" lay-filter="dropdownExp" data-dropdown="#dropdownExp">&nbsp;更多
                            <i class="layui-icon layui-icon-drop"></i></button>
                    </div>

                </div>
            </form>
        </div>    
     </div>


      <div class="layui-card-body">
       <table id="userTable" lay-filter="userTable"></table>
     </div>
 </div>
@endsection
@section('script')
    @can('system.admins')
        @include('admin.admins.js')
    @endcan
@endsection