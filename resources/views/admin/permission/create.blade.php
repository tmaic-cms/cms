@extends('admin.base')
@section('content')
@include('admin.editor')
<!-- 正文开始 -->
@include('admin.permission.form')
@endsection
@section('script')
<script>
layui.use(['form', 'formX','element','upload'], function () {
	var $ = layui.jquery;
	var form = layui.form;
	var formX = layui.formX;
	var upload = layui.upload;
	form.val('permission_form', { "status":1});
	form.on('submit(SubmitForm)', function (data) {
		$.ajax({
			type: "POST",
			url: '{{ route("admin.permission.store") }}',
			data: $(this).parents('form').serialize(),
			dataType: 'json',
			success: function (res) {
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1});
					treeTable.reload();
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
		return false;
	});

});
</script>
@endsection