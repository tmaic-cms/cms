<div class="crumbs">
    <div class="crumbsnr">
      <a href="{{route('home')}}">首页</a>@if($crumb) <span class="iconfont">&#xe602;</span> @endif
		@if($crumb)
		  @foreach($crumb as $item)
		 <a href="{{ $item->link }}">{{ $item->name }}</a>@if(!$loop->last) <span class="iconfont">&#xe602;</span> @endif
		  @endforeach
		@else
		<a href="{{ $column->link }}">{{ $column->name }}</a>
		@endif
		@isset($showData)
			<span class="iconfont">&#xe602;</span><a href="{{ $showData->link }}">{{ $showData->title }}</a>
		@endisset
    </div>
</div>
