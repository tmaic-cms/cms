<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8"/>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>{{webconfig('site_title')}}</title>
{{-- js部分 --}}
<script type="text/javascript" src="/assets/libs/layui/layui.js"></script>
<script type="text/javascript" src="/assets/js/common.js?v=318"></script>
<script type="text/javascript" src="/assets/ckeditor5/ckeditor.js"></script>
<link rel="stylesheet" href="/assets/libs/layui/css/layui.css"/>
<link rel="stylesheet" href="/assets/module/admin.css?v=318"/>
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<style>
.numberInfoSubTitle{color: rgba(0, 0, 0, .45);font-size: 14px;height: 22px;line-height: 22px;overflow: hidden;text-overflow: ellipsis;white-space: nowrap;word-break: break-all;}
.numberInfoValue{color: rgba(0, 0, 0, .85);font-size: 24px;margin-top: 10px;height: 32px;line-height: 32px;}
.numberInfoSuffix{color: rgba(0, 0, 0, .65);font-size: 16px;font-style: normal;margin-left: 4px;line-height: 32px;}
.form-footer{width: 100%;background:#FFF;position: fixed;bottom: 0;left: 0;margin-bottom:0;text-align: center;height: 50px;line-height: 50px;}
#formAdvForm{background:#FFF}
#formAdvForm .layui-form-item{margin-top: 20px;margin-bottom: 0;}
#formAdvForm .layui-form-item .layui-inline{margin-bottom: 25px;margin-right: 0;}
.form-group-bottom{position: fixed;left: 0;right: 0;bottom: 0;padding: 10px 20px;background-color: #fff;box-shadow: 0 -1px 2px 0 rgba(0, 0, 0, .05);}
.layui-tab {margin:0px 0px;text-align:left!important}
.layui-table, .layui-table-view {  margin: 0px 0;}
.layui-form-label.layui-required:after {content: "*"; color: red; position: absolute; top: 10px;left: 15px; }
</style>
<script>
var adminindex = '{{route("admin.index")}}';
var uploadImg = "{{ route('uploadImg') }}";
var downloadimage="{{ route('downloadimage') }}";
</script>
<body class="layui-layout-body theme-tmaic" data-theme="theme-tmaic">
<div class="layui-fluid">
@yield('content')
</div>
@yield('script')
<script>
layui.use(['element','layer','upload'],function () {
var layer = layui.layer;
/*错误提示*/
@if(count($errors)>0)
	@foreach($errors->all() as $error)
		layer.msg("{{$error}}",{icon:5});
		@break
	@endforeach
@endif
/*信息提示*/
@if(session('status'))
	layer.msg("{{session('status')}}",{icon:6});
@endif
});
</script>
</body>
</html>



