@extends('theme::layouts.h5')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'index')
@section('bodyid', '')
@section('main') 
  <div class="box1 lunbo1">
    <div class="swiper-container">
      <div class="swiper-wrapper">
		@adv($banner group=1)
        <div class="swiper-slide">
          <img src="{{$banner->wapthumb}}" title="{{$banner->title}}" alt="{{$banner->title}}" />
        </div>
		@endadv	
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
  <div class="box2">
    <div class="title">
      <h2>致力于城市综合体设计</h2>
      <p>城市生长 设计赋能</p>
    </div>
    <div class="tab">
      <ul>
	  @channel($channelList group=20 rows=4)
        <li class="transition @if($channelListloop->first) active @endif"><a>{{$channelList->name}}</a></li>
	  @endchannel
      </ul>
      <div class="tabnr">
	    @channel($channelList group=20 rows=4)
        <div class="nr transition @if($channelListloop->first) active @endif">
		  @product($productList typeid=$channelList->son rows=4)
           <a href="{{$productList->link}}">
            <img class="transition" src="{{$productList->thumb}}" title="{{$productList->title}}" alt="{{$productList->seo_title}}">
            <p>{{$productList->title}}</p>
          </a>
		  @endproduct
        </div>
		@endchannel	
      </div>
    </div>
  </div>
  <div class="box3">
    <div class="title">
      <h2>致力于城市综合体设计</h2>
      <p>城市生长 设计赋能</p>
    </div>
    <div class="tab">
      <ul>
		@channel($productColumn group=26 rows=6)
        <li class="transition @if($productColumnloop->first) active @endif"><a>{{$productColumn->name}}</a></li>
		@endchannel
      </ul>
      <div class="tabnr">
	    @channel($productColumn group=26 rows=6)
        <div class="nr transition @if($productColumnloop->first) active @endif">
		@product($productList typeid=$productColumn->son rows=6)
          <a href="{{$productList->link}}">
            <img class="transition" src="{{$productList->thumb}}" title="{{$productList->title}}" alt="{{$productList->seo_title}}">
            <p>{{$productList->title}}</p>
          </a>
         @endproduct 
        </div>
		@endchannel
       
      </div>
    </div>
  </div>
  <div class="box4">
    <div class="title">
      <h2>案例展示</h2>
      <p>创意驱动 成就价值</p>
    </div>
    <ul class="icase">
	@cases($caseslist orderby=date rows=3)
      <li>
        <a href="{{$caseslist->link}}">
           <img src="{{$caseslist->thumb}}" title="{{$caseslist->title}}" alt="{{$caseslist->title}}" />
          <p class="transition">{{$caseslist->title}}</p>
        </a>
      </li>
      @endcases 
    </ul>
	@channel($caseMore group=85 rows=1)
    <a href="{{$caseMore->link}}" class="more-btn">
      More<span class="iconfont">&#xe602;</span>
    </a>
	@endchannel
  </div>
  
 
 
  

  <div class="box5 lunbo2">
    <div class="title">
	 @adv($advantage typeid=7 rows=1)
      <h2>{{$advantage->title}}</h2>
      <p>{{$advantage->firsttitle}}</p>
	 @endadv 
    </div>
    <div class="swiper-container">
      <div class="swiper-wrapper">
	  @adv($plan group=7 rows=6)
        <div class="swiper-slide">
          <a href="{{$plan->link}}">
            <img class="transition" src="{{$plan->thumb}}" title="{{$plan->title}}" alt="{{$plan->title}}">
          </a>
          <div class="font">
            <a href="{{$plan->link}}">{{$plan->title}}</a>
          </div>
        </div>
        @endadv 
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
 

  
<div class="box5 lunbo2">
    <div class="title">
	 @adv($advantage typeid=35 rows=1)
      <h2>{{$advantage->title}}</h2>
      <p>{{$advantage->firsttitle}}</p>
	 @endadv 
    </div>
    <div class="swiper-container">
      <div class="swiper-wrapper">
	  
	   @adv($advantage group=35 rows=4)
        <div class="swiper-slide">
          <a href="{{$advantage->link}}">
            <img class="transition" src="{{$advantage->thumb}}" title="{{$advantage->title}}" alt="{{$advantage->title}}">
          </a>
          <div class="font">
            <a href="{{$advantage->link}}">{{$advantage->title}}</a>
          </div>
        </div>
		@endadv
		
		{{--
        <div class="swiper-slide">
          <a href="###">
            <img class="transition" src="picture/a29.jpg" title="建筑设计" alt="建筑设计">
          </a>
          <div class="font">
            <a href="###">建筑设计</a>
          </div>
        </div>
        <div class="swiper-slide">
          <a href="###">
            <img class="transition" src="picture/a30.jpg" title="室内设计" alt="室内设计">
          </a>
          <div class="font">
            <a href="###">室内设计</a>
          </div>
        </div>
        <div class="swiper-slide">
          <a href="###">
            <img class="transition" src="picture/a31.jpg" title="机电设计" alt="机电设计">
          </a>
          <div class="font">
            <a href="###">机电设计</a>
          </div>
        </div>
        <div class="swiper-slide">
          <a href="###">
            <img class="transition" src="picture/a32.jpg" title="" alt="">
          </a>
          <div class="font">
            <a href="/service/18.html">陈设设计</a>
          </div>
        </div>
        <div class="swiper-slide">
          <a href="###">
            <img class="transition" src="picture/a33.jpg" title="" alt="">
          </a>
          <div class="font">
            <a href="/service/19.html">导向标识设计</a>
          </div>
        </div>
		--}}
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
  
  
  
@channel($about typeid=83)  
  <div class="box7">
    <div class="title">
      <h2>{{$about->name}}</h2>
      <p>{{$about->sub_name}}</p>
    </div>
    <div class="img">
      <img class="transition" src="{{$about->thumb}}" title="{{$about->name}}" alt="{{$about->seo_title}}">
    </div>
    <div class="text">
	
		{!! $about->body !!}
      <a href="{{$about->link}}" class="more-btn">
        More<span class="iconfont">&#xe602;</span>
      </a>
    </div>
  </div>
@endchannel




 

<div class="box8 lunbo4">
    <div class="title">
      <h2>资讯</h2>
      <p>城市生长 设计赋能</p>
    </div>
    <div class="swiper-container">
      <div class="swiper-wrapper">
	  @channel($index_news_list group=5 rows=4)
        <div class="swiper-slide">
          <h2>{{$index_news_list->name}}</h2>
          <ul>
		  @article($newlist typeid=$index_news_list->son orderby=date rows=4)
            <li>
              <a href="{{ $newlist->link }}">
                  <em>
                    <h2>{{ $newlist->created_at->format('d') }}</h2>
                    {{ $newlist->created_at->format('Y-m') }}
                  </em>
                <h3>{{ $newlist->description}}</h3>
              </a>
            </li>
			@endarticle 
          </ul>
        </div>
		@endchannel
		
		
		{{--
        <div class="swiper-slide">
          <h2>家具</h2>
          <ul>
            <li>
              <a href="###">
                <em>
                  <h2>04</h2>
                  2020-03
                </em>
                <h3>广州办公家具定制厂家说说保养不当的危广州办公家具定制厂家说说保养不当的危</h3>
              </a>
            </li>
            
          </ul>
        </div>
        <div class="swiper-slide">
          <h2>门</h2>
          <ul>
            <li>
              <a href="###">
                <em>
                  <h2>04</h2>
                  2020-03
                </em>
                <h3>广州办公家具定制厂家说说保养不当的危广州办公家具定制厂家说说保养不当的危</h3>
              </a>
            </li>
             
          </ul>
        </div>
        <div class="swiper-slide">
          <h2>装饰板</h2>
          <ul>
            <li>
              <a href="###">
                <em>
                  <h2>04</h2>
                  2020-03
                </em>
                <h3>广州办公家具定制厂家说说保养不当的危广州办公家具定制厂家说说保养不当的危</h3>
              </a>
            </li>
            
          </ul>
        </div>
		--}}
      </div>
      <div class="swiper-pagination"></div>
    </div>
  </div>
  
  
  
  
  
  
  
  
  
  
  
  
@endsection


















