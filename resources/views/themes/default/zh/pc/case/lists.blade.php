@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'case')
@section('bodyid', 'top')
@section('main') 
  <div class="box1">
    <ul>
		@channel($topColum top=0 rows=10)
		<li class="transition @if($topColum->column_mid ==$column->column_mid) active @endif">
		<a href="{{$topColum->link}}"><h1>{{$topColum->name}}</h1></a>
		</li>
		@endchannel
    </ul>
    <div class="casenr active">
      <ul>
	   @list($listData length=20  desclength=100 page=12)
        <li>
          <img src="{{$listData->thumb}}" title="{{$listData->title}}" alt="{{$listData->seo_title}}" />
          <p class="transition">{{$listData->title}}</p>
          <div class="xuanfuc transition">
            <div class="xuanfu">
              <a href="{{$listData->link}}">了解详情</a>
            </div>
          </div>
        </li>
		@endlist
      </ul>
	  {{$listDatapage->links()}}
    </div>
  </div>
   
@endsection
@section('footerjs')
@endsection


 





