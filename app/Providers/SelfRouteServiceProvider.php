<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-19
*/
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Http\Request;
use App\Models\Routes\Routes;
use Cache;
use Route;


class SelfRouteServiceProvider extends ServiceProvider
{
    private $namespaces = 'App\Http\Controllers\Home';
    public function boot()
    {
		Route::group(['namespace' => $this->namespaces,'middleware' => 'nav'], function () {
			$routes = Cache::remember('routes', webconfig('SYSCACHETIME') , function() {
			 return Routes::get();
			}); 
			foreach($routes as $router) {
				app()->make('router')
				->namespace($this->namespaces)
				->get($router->path, $router->controller)
				->name($router->routename);
				
			}
		});
    }
}
