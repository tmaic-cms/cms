

<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('product.fabric.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('product.fabric.create')
            @{{# if(d.refund_status==0){ }}
              <a class="layui-btn layui-btn-warm layui-btn-sm" lay-event="DisableRefund">禁退</a>
            @{{# }else if(d.refund_status==1){ }}
             <a class="layui-btn layui-btn-sm" lay-event="refund">退款</a>
            @{{# } }}
        @endcan

        @can('product.fabric.create')
            @{{# if(d.is_lock==0){ }}
              <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="unlock">锁定</a>
            @{{# }else if(d.is_lock==1){ }}
             <a class="layui-btn layui-btn-sm" lay-event="locking">解锁</a>
            @{{# } }}
        @endcan


        @can('product.fabric.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>

<!-- 表格操作列 -->
<script type="text/html" id="eDialogTbBar">
   <!-- <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="view"><i class="layui-icon">&#xe6b2;</i>查看</a>-->
    <a class="layui-btn layui-btn-sm" lay-event="comments"><i class="layui-icon">&#xe63a;</i>图集</a>
</script>
<script type="text/html" id="thumb">
    @{{#  if(d.thumb){ }}
    <a href="@{{d.thumb}}" target="_blank" title="点击查看">
        <img src="@{{d.thumb}}" alt="" width="28" height="28">
    </a>
    @{{#  } }}
</script>




<!-- 上传图集 -->
<script type="text/html" id="eDialogCommentDialog">
    <table id="eDialogCommentTable" lay-filter="eDialogCommentTable" class="layui-hide"></table>
    <div class="btn-circle" id="eDialogCommentBtnAdd" style="position: absolute; bottom: 60px;" title="上传图集">
        <i class="layui-icon"></i>
    </div>
</script>

<!-- 上传图集操作列 -->
<script type="text/html" id="eDialogCommentTbBar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<!-- 上传图集操作列 -->
<script type="text/html" id="paththumb">
    @{{#  if(d.path){ }}
    <a href="@{{d.path}}" target="_blank" title="点击查看">
        <img src="@{{d.path}}" alt="" width="28" height="28">
    </a>
    @{{#  } }}
</script>







<!-- 表格状态列 -->
<script type="text/html" id="tbState">
    <input type="checkbox" lay-filter="tbState" value="@{{d.id}}" lay-skin="switch" lay-text="正常|禁用" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'正常':'锁定'}}</p>
</script>

 