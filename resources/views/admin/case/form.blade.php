<div class="layui-card-body">
   <input type="hidden" name="id"  value="{{ $case->id??'' }}"> 
   <input type="hidden" name="lang"  value="{{ $case->locale??'' }}"> 
   <div class="layui-tab layui-tab-brief" lay-filter="tablist">
      <ul class="layui-tab-title">
        <li class="layui-this" lay-id="base" >基本信息</li>
        <li lay-id="body">详情内容</li>
      </ul>
      <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <div class="layui-form-item">
                <label class="layui-form-label">栏目分类</label>
                <div class="layui-input-block" style="width:500px">
                    <select name="column_id" lay-verify="required" lay-reqtext="栏目必填项，岂能为空？">
                        <option value="">顶级分类</option>
                    @foreach($GoodsType as $item)
                        <option value="{{ $item->column_mid }}" @if(isset($case->column_id) && $case->column_id==$item->column_mid) selected @endif>{{ $item->name }}</option>
        
                        @if(isset($item->children))
                            @foreach($item->children as $second)
                                <option value="{{$second->column_mid}}" {{ isset($case->column_id) && $case->column_id==$second->column_mid ? 'selected' : '' }}>&nbsp;┗━━{{$second->name}}</option>
                            @endforeach
                        @endif
                    @endforeach
        
                    </select>
                </div>
            </div>
         
 

            <div class="layui-form-item">
                <label class="layui-form-label layui-required">项目名称</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{ $case->title ?? old('title') }}" lay-verify="required" placeholder="请输入标题" class="layui-input" >
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label layui-required">seo标题</label>
                <div class="layui-input-block">
                    <input type="text" name="seo_title" value="{{ $case->seo_title ?? old('seo_title') }}" lay-verify="required" placeholder="请输入seo标题" class="layui-input" >
                </div>
            </div>
 
            <div class="layui-form-item">
                <label class="layui-form-label">seo关键字</label>
                <div class="layui-input-block">
                    <input name="keywords" placeholder="请输入seo关键字" value="{{$case->keywords??old('keywords')}}" class="layui-input" />
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">描述</label>
                <div class="layui-input-block">
                    <input name="description" placeholder="请输入描述" value="{{$case->description??old('description')}}" class="layui-input" />
                </div>
            </div>
 
			  <div class="layui-form-item">
				<div class="layui-inline">
				  <label class="layui-form-label">网络图片</label>
				  <div class="layui-input-inline"  style="width:600px">
					<input type="url" name="downloadurl" id="downloadurl" autocomplete="off" class="layui-input">
				  </div>
				</div>
				<div class="layui-inline">
				  <button type="button" class="layui-btn downloadImage"><i class="layui-icon">&#xe67c;</i>上传</button>
				</div>
			  </div>


            <div class="layui-form-item">
                <label class="layui-form-label layui-required">主图</label>
                <div class="layui-input-block">
                    <div class="layui-upload">
						<button type="button" class="layui-btn uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传</button>
						<div class="layui-upload-list" >
							<ul id="layui-upload-box" class="layui-clear layui-upload-box">
								@if(isset($case->thumb))
									<li><img src="{{ $case->thumb??'' }}" width="50"/><p></p></li>
								@else
								<li><img src="" width="50"/><p></p></li>
								@endif
							</ul>
							<input type="hidden" name="thumb" id="thumb" class="layui-upload-input" value="{{ $case->thumb??'' }}"  lay-reqText="主图不能为空!">
						</div>
					</div>
                </div>
            </div>
        
     

        </div>

        <div class="layui-tab-item">
         <div class="layui-form-item layui-form-text">
                <main>
                	<div class="centered">
                		<div class="document-editor">
                			<div class="toolbar-container"></div>
                			<div class="content-container">
                				<div id="editor">
                					{!! $case->body??old('body') !!} 
                				</div>
                			</div>
                		</div>
                	</div>
                </main>
        </div>

      </div>
    </div>
</div>
<div class="form-group-bottom text-center">
    <button class="layui-btn" lay-filter="SubmitData" id="SubmitData" lay-submit>&emsp;提交&emsp;</button>
	<button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
</div>

