<div class="layui-card-body">
      <input type="hidden" name="id"  value="{{ $position->id??'' }}"> 
   <div class="layui-tab layui-tab-brief" lay-filter="tablist">
      <ul class="layui-tab-title">
        <li class="layui-this" lay-id="base" >基本信息</li>
        <li lay-id="body">详情内容</li>
      </ul>
      <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <div class="layui-form-item" style="display:none">
                <label class="layui-form-label">广告位</label>
                <div class="layui-input-block" style="width:500px">
                    <select name="pid" lay-verify="required" lay-reqtext="产品分类必填项，岂能为空？">
                        <option value="0" selected>-选择广告位-</option>
                    @foreach($positions as $item)
                        <option value="{{ $item->sid }}" @if(isset($positions->pid)&&$positions->pid==$item->sid) selected @endif>{{ $item->title }}</option>
                       
                    @endforeach
        
                    </select>
                </div>
            </div>
        
            <div class="layui-form-item">
                <label class="layui-form-label">广告标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{ $position->title ?? old('title') }}" lay-verify="required" placeholder="请输入广告标题" class="layui-input" >
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">副标题</label>
                <div class="layui-input-block">
                    <input type="text" name="firsttitle" value="{{ $position->firsttitle ?? old('firsttitle') }}" placeholder="请输入副标题" class="layui-input" >
                </div>
            </div>

	 
            <div class="layui-form-item">
                <label class="layui-form-label">seo关键字</label>
                <div class="layui-input-block">
                    <input name="keywords" placeholder="请输入seo关键字" value="{{$position->keywords??old('keywords')}}" class="layui-input" />
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">描述</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea layui-hide" name="description" id="description" placeholder="请输入描述">{!! $position->description??'' !!}</textarea>
                </div>
            </div>
 

 
        </div>




        <div class="layui-tab-item">
         <div class="layui-form-item layui-form-text">
                <main>
                	<div class="centered">
                		<div class="document-editor">
                			<div class="toolbar-container"></div>
                			<div class="content-container">
                				<div id="editor">
                					{!! $position->body??old('body') !!} 
                				</div>
                			</div>
                		</div>
                	</div>
                </main>
        </div>



      </div>
    </div>
</div>
<div class="form-group-bottom text-center">
    <button class="layui-btn" lay-filter="SubmitData" lay-submit>&emsp;提交&emsp;</button>
</div>

