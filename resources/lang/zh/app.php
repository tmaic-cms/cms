<?php

return [
    'website'     => '雨林木业',
    'website_title'     => 'My Cool Website With Cool Blog Posts',
    'login'             => '登陆',
    'register'          => '注册',
    'logout'            => '退出',
    'about_page_title'  => '关于',
    'about_page_text'   => 'This website is a Multilingual Laravel application example',
    'view_more'         => 'View More',
];
