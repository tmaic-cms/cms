@extends('admin.base')
@section('content')
<style>
.layui-card {margin-bottom: 0px;border-radius: 2px;line-height: 20px;}
.layui-tab-content {padding: 0px;}
.layui-card-body {padding: 5px 0px;
.layui-form.toolbar .layui-form-item .layui-inline {margin-bottom: 1px;}
.layui-fluid {padding: 10px 1px 0 1px;
</style>
<div class="layui-card">
	<div class="layui-card-body">
		<form class="layui-form toolbar" lay-filter="groupNameForm">
			<div class="layui-form-item">
				<div class="layui-inline">
					<label class="layui-form-label left">配置组:</label>
					<div class="layui-input-inline">
					
						 <select name="group_id" id="group_id" lay-filter="groupname">
								<option value="">请选择配置组</option>
								@foreach($Group as $groupname)
									<option value="{{ $groupname->id }}">{{ $groupname->name }}</option>
								@endforeach
							</select>
					</div>
				</div>
			
				<div class="layui-inline">
					<label class="layui-form-label left">配置名称:</label>
					<div class="layui-input-inline">
						<input name="name" class="layui-input" placeholder="输入配置名称"/>
					</div>
				</div>
				<div class="layui-inline">
					<label class="layui-form-label">模糊查找:</label>
					<div class="layui-input-inline">
						<input name="keyword" class="layui-input" placeholder="输入关键字查找"/>
					</div>
				</div>
	 
				<div class="layui-inline">&emsp;
					<button class="layui-btn icon-btn" lay-filter="TbSearch" lay-submit>
						<i class="layui-icon">&#xe615;</i>搜索
					</button>
				</div>
				<!-- 下拉按钮 -->
				<div class="layui-inline dropdown-menu dropdown-hover">

					<button class="layui-btn layui-btn-normal icon-btn" type="button" lay-filter="dropdownExp" data-dropdown="#dropdownExp">&nbsp;更多
						<i class="layui-icon layui-icon-drop"></i></button>
				</div>

			</div>
		</form>
	</div>
</div>
<div class="layui-card-body">
	<div class="layui-tab layui-tab-brief" lay-filter="tablist">
		<ul class="layui-tab-title">
			@foreach(config('translatable.locales') as $locale=>$item)
				@if($loop->first)
					<li class="layui-this" lay-id="{{$locale}}" lay-data="{{$locale}}">{{$item}}</li>
				@else
					<li lay-id="{{$locale}}" lay-data="{{$locale}}">{{$item}}</li>
				@endif
			@endforeach
		</ul>
		<div class="layui-tab-content">
			<div class="layui-tab-item layui-show">
				<table id="tableData" lay-filter="tableData"></table>
			</div>
		</div>
	</div>
</div>
@endsection
@section('script')
    @can('webconfig.defines')
        @include('admin.defines.js')
    @endcan
@endsection