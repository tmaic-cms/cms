<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Home;


use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Mail;
use App\Models\Consult;

class SubscribeController extends Controller
{

     

    public function sendPhone(Request $request)
    {
		
		 
        $conntent = $request->conntent ?? '';
        $username = $request->username;
        $phone = $request->phone;
        $address = $request->address;
        $email = $request->email;
        $qqwx = $request->qqwx;
        $type = $request->type??1;
		

        if ( !preg_match ('/^1([1-9]{9})/', $phone)) {

            return ['msg' => '您的手机号码不正确!'];
        }
        $this->html_email ($request);
        $phoneData = DB::table ("consult")->where("phone", $phone)->where('type',$type)->whereDay ('created_at', Carbon::today ())->count ();
        if ($phoneData) {
            return ['msg' => '您已经提交过了,稍候有专业人员联系您，请留意电话!'];
        }
        $data['code'] = 0;
        $data['msg'] = '您已经提交过了,稍候有专业人员联系您，请留意电话!';

        if (empty($phoneData)) {

            DB::beginTransaction ();
            try {

                Consult::create ([
                    'username' => $username,
                    'phone' => $phone,
                    'address' => $address,
                    'email' => $email,
                    'qq' => $qqwx,
                    'conntent' => $conntent,
					'type'=>$type,
                    'status' => 1
                ]);
                DB::commit ();

                $data['code'] = 200;
                $data['msg'] = "提交成功,稍候有专业人员联系您，请留意电话!";

            } catch (\Exception $exception) {
                DB::rollBack ();
                $data['code'] = 0;
            }
        }

        return $data;
    }


    public function html_email($request)
    {
        $data = array(
            'name' => $request->username,
            'phone' => $request->phone,
            'comments' => $request->conntent,
            'email' => $request->email,
            'qqwx' => $request->qqwx,
            'address' => $request->address,
			'type' => $request->type??1,
        );
		
		
		
		
		
        if (webconfig ('email')) {
            Mail::send ('mail', $data, function ($message) use($request) {
                $mail = webconfig ('email');
                $mail="421339244@qq.com";
				$title= $request->type ==2?"来自".webconfig('seo_title')."官网用户投诉与建议":"来自".webconfig('seo_title')."官网用户咨询问题";
                $message->to ($mail, 'admin')->subject ($title);
                $message->from ('421339244@qq.com', 'admin Author');
            });
            return true;
        }

        return false;

    }


}
