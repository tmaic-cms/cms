<!-- 表格操作列 -->
<script type="text/html" id="userTbBar">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="reset">重置密码</a>
</script>
<!-- 表格状态列 -->
<script type="text/html" id="userTbState">
    <input type="checkbox" lay-filter="userTbStateCk" value="@{{d.id}}" lay-skin="switch"
           lay-text="正常|锁定" @{{d.state==0?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.state==0?'正常':'锁定'}}</p>
</script>

<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('system.admins.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('system.admins.permission')
            <a class="layui-btn layui-btn-sm" lay-event="permission">权限</a>
        @endcan
        @can('system.admins.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>
<!-- 下拉菜单 -->
<ul class="dropdown-menu-nav dropdown-bottom-right layui-hide" id="dropdownExp">
    <div class="dropdown-anchor"></div>
    <li class="title">HEADER</li>
    <li><a><i class="layui-icon layui-icon-star-fill"></i>1st menu item</a></li>
    <li class="disabled">
        <a><i class="layui-icon layui-icon-template-1"></i>2nd menu item</a></li>
    <hr>
    <li class="title">HEADER</li>
    <li><a><i class="layui-icon layui-icon-set-fill"></i>3rd menu item</a></li>
</ul>
<!-- 表单弹窗 -->
<script type="text/html" id="userEditDialog">
    <form id="userEditForm" lay-filter="userEditForm" class="layui-form model-form">
        <input name="id" type="hidden" value=""/>
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">账号:</label>
            <div class="layui-input-block">
                <input name="username" placeholder="请输入账号" class="layui-input" lay-verType="tips" lay-verify="required"
                       value=""/>
            </div>
        </div>
 
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">昵称:</label>
            <div class="layui-input-block">
                <input name="nickName" placeholder="请输入昵称" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>
 
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">密码:</label>
            <div class="layui-input-block">
                <input name="password" placeholder="请输入密码" class="layui-input" lay-verType="tips" value=""/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">重复密码:</label>
            <div class="layui-input-block">
                <input name="password_confirmation" placeholder="请输入密码" class="layui-input" lay-verType="tips" value=""/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">角色:</label>
            <div class="layui-input-block">
                <div id="userRole"></div>
            </div>
        </div>
        <div class="layui-form-item text-right">
            <button class="layui-btn" lay-filter="userEditSubmit" lay-submit>保存</button>
            <button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
        </div>
    </form>
</script>

<script>
var userupdate='{{ route("admin.admins.update") }}';
var userstore ='{{ route("admin.admins.store") }}';
var userData ='{{ route("admin.data") }}';
var userdestroy = '{{ route("admin.admins.destroy") }}';
var userupdateajax = "";
</script>

<script type="text/javascript" src="/assets/js/admins.js"></script>