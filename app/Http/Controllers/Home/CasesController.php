<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Home;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\ColumnTranslation;
use Product;
use Cases;
use App\Models\Cases\MianCases;
use Article;
use Cache;

class CasesController extends BaseController
{
	private $pageCount;
	private $slug;
    public function __construct()
    {
       parent::__construct();
       //$this->pageCount = $this->isMobile?webconfig('CasePageCountWap'):webconfig('CasePageCountPC');
    }
    public function index(Request $request)
    {
 
        return $this->view();
    }






    public function lists(Request $request)
    {
        return $this->view();
    }
	
    public function show(Request $request)
    {
        $this->slug = $request->slug;
		Cases::where('slug',$this->slug)->increment('click');
		$showData = Cache::remember($this->routeName.'_'.$request->slug, webconfig('SYSCACHETIME'), function (){
			return Cases::with('image')->where('slug', $this->slug)->firstOrFail();
		});
		$this->columnInitData($showData->column_id);
       return $this->view(compact('showData'));
    }
}
