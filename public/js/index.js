$(function () {
  // 首页轮播
  var mySwiper1 = new Swiper('.lunbo1 .swiper-container', {
    observer: true, //修改swiper自己或子元素时，自动初始化swiper
    observeParents: true, //修改swiper的父元素时，自动初始化swiper
    pagination: '.swiper-pagination',
    paginationClickable: true,
    autoplay: 2000,
    prevButton: '.swiper-button-prev',
    nextButton: '.swiper-button-next',
    loop: true,
    autoplayDisableOnInteraction: false,
    effect: "fade",
  });

  // //导航
  // //进来页面判断
  // if(document.documentElement.scrollTop > 0){
  //   $('header').addClass('on');
  //   $('header .logo img').attr('src','images/logo.svg');
  // } else {
  //   $('header').removeClass('on');
  //   $('header .logo img').attr('src','images/logo-black.svg');
  // };
  // //指向判断
  // $('header').hover(function(){
  //   $(this).addClass('on');
  //   $('header .logo img').attr('src','images/logo.svg');
  // },function(){
  //   if(document.documentElement.scrollTop > 0){
  //     $('header').addClass('on');
  //     $('header .logo img').attr('src','images/logo.svg');
  //   } else {
  //     $('header').removeClass('on');
  //     $('header .logo img').attr('src','images/logo-black.svg');
  //   };
  // });
  // //滚动判断
  // $(document).scroll(function() {
  //   var scroH = $(document).scrollTop();  //滚动高度
  //   console.log(scroH);

  //   if(scroH > 0){
  //     $('header').addClass('on');
  //     $('header .logo img').attr('src','images/logo.svg');
  //   } else {
  //     $('header').removeClass('on');
  //     $('header .logo img').attr('src','images/logo-black.svg');
  //   };
  // });

  //导航指向二级出现动画
  $('header .link>ul>li').hover(function(){
    $(this).find('ul').addClass('mask');
  },function(){
    $(this).find('ul').removeClass('mask');
  })

  // 导航搜索
  $('header .header-sub .search-btn').click(function(){
    $('header .search-full').addClass("show");
    $('header .search-bg').addClass("show");
    $('html').css('overflow','hidden');
  })
  $('header .close').click(function(){
    $('header .search-full').removeClass("show");
    $('header .search-bg').removeClass("show");
    $('html').css('overflow','visible');
  })
  //首页四大品类选项卡切换
  $('.index .box2 .tab>ul>li').click(function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    let tabnum = $(this).index();
    $('.index .box2 .tabnr .nr').removeClass('active');
    $('.index .box2 .tabnr .nr:eq(' + tabnum + ')').addClass('active');
  });

  //首页产品展示选项卡切换
  $('.index .box3 .tab>ul>li').click(function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    let tabnum = $(this).index();
    $('.index .box3 .tabnr .nr').removeClass('active');
    $('.index .box3 .tabnr .nr:eq(' + tabnum + ')').addClass('active');
  });

  //首页资讯选项卡切换
  $('.index .box8 .tab>ul>li').click(function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    let tabnum = $(this).index();
    $('.index .box8 .tabnr .nr').removeClass('active');
    $('.index .box8 .tabnr .nr:eq(' + tabnum + ')').addClass('active');
  });

  //右侧菜单栏显示
  if(document.documentElement.scrollTop > 0){
    $('.foot-sidebar__plug').addClass('fs-show')
  } else {
    $('.foot-sidebar__plug').removeClass('fs-show')
  };

  //新闻tab
  $('.news .box1 .tab>ul>li').click(function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    let tabnum = $(this).index();
    $('.news .box1 .tab .tabnr .nr').removeClass('active');
    $('.news .box1 .tab .tabnr .nr:eq(' + tabnum + ')').addClass('active');
  })
  //新闻页年份选择
  $('.news .box1 .tab .filtare-year__full .filtare-year__cont .fy-select').click(function(){
    if($(this).hasClass('active')){
      $(this).removeClass('active');
      $(this).siblings('.filtare-year__list').removeClass('listshow');
    } else {
      $(this).addClass('active');
      $(this).siblings('.filtare-year__list').addClass('listshow');
    }
  })

  // 案例页tab
  $('.case .box1>ul>li').click(function(){
    $(this).siblings().removeClass('active');
    $(this).addClass('active');
    let tabnum = $(this).index();
    $('.case .box1 .casenr').removeClass('active');
    $('.case .box1 .casenr:eq(' + tabnum + ')').addClass('active');
  })
});
//屏幕发生滚动时
$(document).scroll(function() {
  //右侧菜单栏显示
  var scroH = $(document).scrollTop();  //滚动高度
  if(scroH > 0){
    $('.foot-sidebar__plug').addClass('fs-show')
  } else {
    $('.foot-sidebar__plug').removeClass('fs-show')
  };
});