<!-- 表单弹窗 -->
<script type="text/html" id="EditDialog">
    <form id="EditForm" lay-filter="EditForm" class="layui-form model-form">
        <input name="defines_mid" type="hidden" value=""/>
        <input name="id" type="hidden" value=""/>
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">主题名称:</label>
            <div class="layui-input-block">
                <input name="title" placeholder="请输入主题名称" class="layui-input" lay-verType="tips" lay-verify="required"  value=""/>
            </div>
        </div>
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">目录名:</label>
            <div class="layui-input-block">
                <input name="themename" placeholder="请输入目录名" class="layui-input" lay-verType="tips" lay-verify="required"  value=""/>
            </div>
        </div>
		
		<div class="layui-form-item">
			<label class="layui-form-label layui-required">终端属性:</label>
			<div class="layui-input-block">
				<input type="radio" name="type" value="1" title=" PC端主题" lay-verify="otherReq" lay-filter="type">
				<input type="radio" name="type" value="2" title=" H5主题" lay-verify="otherReq"  lay-filter="type">
			</div>
		</div>
		
  
		<div class="layui-form-item">
			<label for="" class="layui-form-label">主题封面</label>
			 <div class="layui-upload-drag" id="uploadViewUpload">
			  <i class="layui-icon"></i>
			  <p>点击上传，或将文件拖拽到此处</p>
			  <div class="layui-hide" id="uploadView">
				<img src="" alt="上传成功后渲染" style="max-width: 347px">
			  </div>
			  <input type="hidden" name="thumb" id="thumb" class="layui-upload-input" value=""  lay-reqText="主题封面不能为空!">
			</div>
		</div>
	 
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">主题介绍:</label>
            <div class="layui-input-block">
				<textarea name="desc" lay-verify="required" placeholder="主题描述..." lay-verType="tips" class="layui-textarea"></textarea>
            </div>
        </div>
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">状态:</label>
            <div class="layui-input-block">
				<input type="radio" name="status" value="1" title="启用" lay-filter="status">
				<input type="radio" name="status" value="0" title="禁用" lay-filter="status">
            </div>
        </div>
		
		
        <div class="layui-form-item text-center">
            <button class="layui-btn" lay-filter="EditSubmit" lay-submit>保存</button>
            <button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
        </div>
    </form>
</script>


<!-- 项目模板 -->
<script type="text/html" id="demoCardItem1">
    <div class="layui-col-md3" ids="@{{d.id}}">
        <div class="project-list-item">
            <img class="project-list-item-cover" src="@{{d.thumb}}"/>
            <div class="project-list-item-body">
                <h2>@{{d.title}}</h2>
                <div class="project-list-item-text layui-text">@{{d.desc}}</div>
                <div class="project-list-item-desc">
                    <span class="time">@{{d.time }}</span>
                    <div class="ew-head-list">
                        <img class="ew-head-list-item" lay-tips="曲丽丽" lay-offset="0,-5px"
                             src="https://gw.alipayobjects.com/zos/rmsportal/ZiESqWwCXBRQoaPONSJe.png"/>
                         
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>






<!-- 应用模板 -->
<script type="text/html" id="demoCardItem2">
    <div class="layui-col-md3">
        <div class="application-list-item">
            <div class="application-list-item-header">
                <img class="head" src="@{{d.head}}"/>
                <h2>@{{d.name}}</h2>
            </div>
            <div class="application-list-item-body">
                <div class="text-num-item">
                    <div class="text-num-item-title">活跃用户</div>
                    <div class="text-num-item-text">
                        @{{d.activeNum}}
                        <small>万</small>
                    </div>
                </div>
                <div class="text-num-item">
                    <div class="text-num-item-title">新增用户</div>
                    <div class="text-num-item-text">@{{d.newNum}}</div>
                </div>
            </div>
            <div class="application-list-item-tool">
                <span class="application-list-item-tool-item" lay-event="download">
                    <i class="layui-icon layui-icon-download-circle" lay-tips="下载" lay-offset="0,-8px"></i>
                </span>
                <span class="application-list-item-tool-item" lay-event="edit">
                    <i class="layui-icon layui-icon-edit" lay-tips="编辑" lay-offset="0,-8px"></i>
                </span>
                <span class="application-list-item-tool-item" lay-event="share">
                    <i class="layui-icon layui-icon-share" lay-tips="分享" lay-offset="0,-8px"></i>
                </span>
                <span class="application-list-item-tool-item" lay-event="more">
                    <div class="dropdown-menu dropdown-hover">
                        <i class="layui-icon layui-icon-more"></i>
                        <ul class="dropdown-menu-nav dropdown-bottom-center">
                            <div class="dropdown-anchor"></div>
                            <li><a lay-event="item1">1st menu item</a></li>
                            <li><a lay-event="item2">2nd menu item</a></li>
                            <li><a lay-event="item3">3rd menu item</a></li>
                        </ul>
                    </div>
                </span>
            </div>
        </div>
    </div>
</script>



 
 














