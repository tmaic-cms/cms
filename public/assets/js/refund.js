layui.use(['treeTable','form','admin', 'laytpl','table','util','laydate','element'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
        // 渲染表格
        var insTb = table.render({
            elem: '#orderTree',
            skin: 'line',
            size: 'lg',
            url: postData,
            page: true,
            toolbar: ['<p>',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
            '<button lay-event="delete" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
            '</p>'].join(''),
            cellMinWidth: 100,
            cols: [[
            {type: 'numbers',title: '序号'},
            {type: 'checkbox'},
            {field:"rid", title: 'ID',width: 90},
            {field:"refund_no", title: '退款单号',width: 200},
            {field:"out_trade_no", title: '交易号',width: 200},
            {templet: '<div>{{d.order.order_name}}</div>', title: '订单名称', width: 150,hide:true},
            {field: 'refund_fee', title: '退款金额', width: 150},
            {field: 'total_money', title:'交易金额', width: 150},
            {templet: '<div>{{d.user.username}}</div>', title: '用户名',width: 110},
            //{align: 'center', toolbar: '#eDialogTbBar', title: '详情', width: 95},

            {field: 'refund_status_name', title:'详情', width: 150},
            {field: 'refund_status', title: '状态', templet: '#refund_status', sort: true, width: 100},
            {field: 'updated_at', title: '更新时间', width: 180},
            {align: 'left', toolbar: '#options', title: '操作',fixed:"right"},
            ]]
            ,done: function (res, curr, count) {
                //res为表格的所有数据，curr为当前页码，count为数据总条数
                //console.info(res.data);
                //遍历本页所有表格数据的值，依次判断是否为空闲中状态，若不是则通过data-index改变该行样式
                //这里item和index分别对应每行的数据和data-index索引值
                res.data.forEach(function (item, index) {
                    //这里的entity_status_id 为表格数据中的一列数据，用于判断特定行
                    if (item.refund_status == 1) {
                        //禁用复选框，设置不可选中标识，将该行设置为阴影色
                        var tr = $(".layui-table tr[data-index=" + index + "]");
                        tr.find("input[type='checkbox']").prop('disabled', true);
                        tr.find("input[type='checkbox']").next().addClass('layui-btn-disabled');
                        //tr.css("background", "rgba(0, 0, 0, 0.35)");
                       // console.info(tr.find("a[lay-event='edit']"));
                        //禁用该行工具栏的按钮特效
                        tr.find("a[lay-event='edit']").prop('disabled', true);
                        tr.find("a[lay-event='edit']").removeClass("layui-btn-normal").addClass("layui-btn-disabled");
                        tr.find("a[lay-event='del']").prop('disabled', true);
                        tr.find("a[lay-event='del']").removeClass("layui-btn-normal").addClass("layui-btn-disabled");
                    }

                })
            },

        });
       // 列点击事件
        table.on('tool(orderTree)', function (obj) {
            var data = obj.data;

            var event = obj.event;
            if (obj.event === 'edit') {
                admin.open({
                    type: 2,
                    title: '编辑订单信息',
                    content: '/admin/order/edit?id='+obj.data.id,
                    area: ['40%', '60%'],
                    scrollbar: false,
                    yes: function (index, layero) {},
                    end: function (index, layero) {
                        treeTable.reload('advertTree');
                        layer.close(index);
                        return;
                    }
                });
            } else if (obj.event === 'del') {
                doDel({ids: obj.data.id});
            }else if (obj.event === 'checkList') {
                 showComments(data.id);
            } else if (obj.event === 'comments') {
                 showComments(data.id);
            } else if (obj.event === 'refund') {
 

            layer.confirm('确定要执行退款吗？', {
                    skin: 'layui-layer-admin',
                    shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);


                $.post(executeRefund, {
                    _method: 'delete',
                    ids: data.rid
                },
                function (res) {
                    layer.close(loadIndex);
                    if (res.status == 200) {
                        layer.msg(res.msg, {icon: 1});
                        insTb.reload({page: {curr: 1}});
                    } else {
                        layer.msg(res.msg, {icon: 2});
                        layer.close(loadIndex);
                    }
                }, 'json');


                });
           return;
          }
        });
    // 列点击事件 end
    // 审核记录弹窗
    function openCheckList(d) {
        laytpl(eDialogCheckDialog.innerHTML).render(d, function (html) {
            admin.open({
                type: 1,
                title: '审核详情',
                content: html
            });
        });
    };
    // 审核记录弹窗 end

    // 查看评论工具条点击事件
    table.on('tool(eDialogCommentTable)', function (obj) {
        var data = obj.data;
        var layEvent = obj.event;
        if (layEvent === 'del') { // 删除
            layer.msg('删除成功', {icon: 1});
        }
    });

    //添加备注
    $('#eDialogCommentBtnAdd').click(function () {
        layer.prompt({
            title: '添加评论',
            shade: .1,
            offset: '165px',
            skin: 'layui-layer-admin layui-layer-prompt',
            formType: 2
        }, function (value, index, elem) {
            layer.close(index);
            layer.msg('备注成功', {icon: 1});
        });
    });


   // 查看订单清单
    function showComments(id) {
        admin.open({
            type: 1,
            area: '900px',
            offset: '65px',
            title: '订单清单',
            content: $('#eDialogCommentDialog').html(),
            success: function (layero) {
                // 渲染表格
                var insTbCom = table.render({
                    elem: '#eDialogCommentTable',
                    url: goodsData + id,
                    page: true,

                    height: 400,
                    cellMinWidth: 100,
                    cols: [[
                        {type: 'numbers',title: '序号'},
                        {type: 'checkbox'},
                        {field: 'order_no', title: '订单号'},
                        {field: 'goods_name', sort: true, title: '商品名称'},
                        {field: 'price', sort: true, title: '价格',width: 80},
                        {title: '创建时间', sort: true, templet: function (d) {   return util.toDateString(d.created_at);  },width: 160  },
                        {align: 'center', toolbar: '#eDialogCommentTbBar', title: '操作', width: 60}
                    ]],
                    done: function () {
                        $(layero).find('.layui-table-view').css('margin', '0');
                    }
                });

                // 工具条点击事件
                table.on('tool(eDialogCommentTable)', function (obj) {
                    var data = obj.data;
                    var layEvent = obj.event;
                    if (layEvent === 'del') { // 删除
                        layer.msg('删除成功', {icon: 1});
                    }
                });

                // 添加备注
                $('#eDialogCommentBtnAdd').click(function () {
                    layer.prompt({
                        title: '添加备注',
                        shade: .1,
                        offset: '165px',
                        skin: 'layui-layer-admin layui-layer-prompt',
                        formType: 2
                    }, function (value, index, elem) {
                        layer.close(index);
                        layer.msg('备注成功', {icon: 1});
                    });
                });

            }
        });
    };
    /* 表头工具栏点击事件 */
    table.on('toolbar(orderTree)', function (obj) {
        if (obj.event === 'add') { // 添加
            admin.open({
                type: 2,
                title: '添加订单',fixed: true,
                content: advcreate,
                area: ['900px', '650px'],
                fixed:true,
                data: {}, 
                end: function() {
                    layer.close(index);insTb.reload(); return false;
                },
                cancel: function(index, layero){  
                    layer.close(index); insTb.reload(); return false;                  
                }    
            });
        } else if (obj.event === 'delete') {
            var checkRows = table.checkStatus('orderTree');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
            doDel({ids: ids});
        }
    });
 
    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要删除选中数据吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
           // var loadIndex = layer.load(2);
/*
            $.post(destroy, {
                _method: 'delete',
                ids: obj.ids
            }, function (res) {
                layer.close(loadIndex);
                if (res.code == 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });*/
    }



    /* 搜索 */
    form.on('submit(btnSearch)', function (data) {
        insTb.reload({where: data.field, page: {curr: 1}});
        return false;
    });
    var orderurl = location.hash.replace(/^#orderlist=/, '');
        element.tabChange('orderlist', orderurl);
        element.on('tab(orderlist)', function(elem){
        location.hash = 'orderlist='+ $(this).attr('lay-id');
        var field = $(this).attr('lay-data');
        insTb.reload({where:{'status':field}, page: {curr: 1}});
    });
 

});