<!DOCTYPE html>
<html class="" lang="en">
<head>
<meta charset="UTF-8" />
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta name="renderer" content="webkit">
<meta name="force-rendering" content="webkit" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta property="og:image" content="http://www.ep-sf.com/images/Index/logo.png" />
<title>@yield('title')@webname</title>
<meta name="keywords" content="@yield('keyword')">
<meta name="description" content="@yield('description')">
<link rel="stylesheet" type="text/css" href="css/swiper-3.4.1.min.css" />
<link rel="stylesheet" href="/css/style.min.css">
<script type="text/javascript" src="/js/jquery-2.1.0.js"></script>
<script type="text/javascript" src="/js/swiper-3.4.1.min.js"></script>
<script src="/js/index.js"></script>
@yield('css')
@yield('topjs')
</head>
<body class="@yield('body')" id="@yield('bodyid')">
@include('theme::layouts.pc')
@yield('footerjs')
@foreach(webconfigGroup(15) as $item)
@if($item->val)
{!! $item->val !!}
@endif
@endforeach
</body>
</html>