<!-- 表格操作列 -->
<script type="text/html" id="userTbBar">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="reset">重置密码</a>
</script>



<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('order.order.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('order.order.create')
            @{{# if(d.refund_status==0){ }}
              <a class="layui-btn layui-btn-warm layui-btn-sm" lay-event="DisableRefund">禁退</a>
            @{{# }else if(d.refund_status==1){ }}
             <a class="layui-btn layui-btn-sm" lay-event="refund">退款</a>
            @{{# } }}
        @endcan

        @can('order.order.create')
            @{{# if(d.is_lock==0){ }}
              <a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="unlock">锁定</a>
            @{{# }else if(d.is_lock==1){ }}
             <a class="layui-btn layui-btn-sm" lay-event="locking">解锁</a>
            @{{# } }}
        @endcan


        @can('order.order.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>



<script type="text/html" id="thumb">
    @{{#  if(d.thumb){ }}
    <a href="@{{d.thumb}}" target="_blank" title="点击查看">
        <img src="@{{d.thumb}}" alt="" width="28" height="28">
    </a>
    @{{#  } }}
</script>
<script type="text/html" id="wapthumb">
    @{{#  if(d.wapthumb){ }}
    <a href="@{{d.wapthumb}}" target="_blank" title="点击查看">
        <img src="@{{d.wapthumb}}"  width="28" height="28">
    </a>
    @{{#  } }}
</script>


<!-- 表格操作列 -->
<script type="text/html" id="eDialogTbBar">
    <!-- <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="view"><i class="layui-icon">&#xe6b2;</i>查看</a> -->
    <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="comments"><i class="layui-icon">&#xe6b2;</i></i>查看</a>
</script>

<!-- 查看评论弹窗 -->
<script type="text/html" id="eDialogCommentDialog">
    <table id="eDialogCommentTable" lay-filter="eDialogCommentTable" class="layui-hide"></table>
    <div class="btn-circle" id="eDialogCommentBtnAdd" style="position: absolute; bottom: 60px;" title="发表评论">
        <i class="layui-icon layui-icon-edit"></i>
    </div>
</script>



 <!-- 查看表格操作列 -->
<script type="text/html" id="eDialogCommentTbBar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>





<!-- 表格状态列 -->
<script type="text/html" id="payState">
    @{{# if(d.pay_status==0){ }}
    <span style="color: orange;cursor: default;" lay-tips="待支付">待支付</span>
    @{{# }else if(d.pay_status==1){ }}
    <span class="icon-text green" lay-event="checkList">
        已支付<i class="layui-icon layui-icon-tips"></i>
    </span>
    @{{# } }}
</script>


<!-- 查看支付状态 -->
<script id="eDialogCheckDialog" type="text/html">
    <div style="padding: 25px 0 0 30px;">
        <ul class="layui-timeline">
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">曲丽丽</h3>&emsp;2019-06-03 13:29
                    </div>
                    <p>提交了外出申请</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            林东东&nbsp;<span class="layui-badge layui-bg-green">组长</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请（速去速回！）</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            周星星&nbsp;<span class="layui-badge layui-bg-blue">经理</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            付小小&nbsp;<span class="layui-badge layui-bg-orange">人事</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请</p>
                </div>
            </li>
        </ul>
    </div>
</script>


<!-- 表格状态列 -->
<script type="text/html" id="tbState">
    <input type="checkbox" lay-filter="ckState" value="@{{d.id}}" lay-skin="switch" lay-text="正常|禁用" @{{d.status==0?'checked':''}}/>
</script>

<!-- 表格状态列 -->
<script type="text/html" id="userTbState">
    <input type="checkbox"  value="@{{d.id}}" lay-skin="switch"
           lay-text="正常|锁定" @{{d.order_status==0?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.state==0?'正常':'锁定'}}</p>
</script>