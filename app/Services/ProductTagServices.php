<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;
use App\Services\ChannelBaseServices;
use App\Models\ColumnTranslation;
use Cache;
class ProductTagServices extends ChannelBaseServices
{
   
    public $tagName;
    public $expression;
    public $musterVal;
    public $object;
    public $cacheObject;
    public $column;
    public $columnid;
    public $rows;
    public $orderby;
    public $length;	
    public $desclength;	
	public $adorn;
    public $flag;
    public $locale;
    public $tag;
	//标签对象
    public $objtag;
    public $routeName;
    public $cache;
    public $iscache;
	//缓存key
    public $sysTagcachekey;
    public $tagcachetime;
    public $useVal;
	//是否为顶级分类
	public $isPid;
	//分类ID
	public $pid;
	//栏目记录ID
	public $sid;
	//栏目记录ID
	public $columnArray;

    public function __construct($expression)
    {
	   $this->tagName ="product";
       //出始化变量
	   $this->pageObj = NULL;
	   $this->isPid = false;
	   $this->pid = -1;
	   $this->columnid = 0;
	   $this->columnArray = [];
	   //闭包变量
	   $this->useVal = NULL;
	   $this->iscache = false;
	   //当前语言
	   $this->locale = lang();
	   //title 长度
	   $this->length = 0;
	   //描述长度
	   $this->desclength = 0;
	   //返回行数
	   $this->rows = 1;
	   //超出部分显示的字符串
	   $this->adorn = null;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=0;
	   $this->setColumn();
	   
	   //开始处理标签
       $this->decompose($expression);
	   //生成缓存key
	   $this->setSysTagcachekey();
       

    }
    //当前选中的栏目
    public function setColumn($column=null){
	    
	    //自动栏目	
        $this->column = Cache::remember($this->routeName.'_'.$this->locale, webconfig('SYSCACHETIME') , function() {
          return  ColumnTranslation::with(['children','class'])->where('routename',$this->routeName)->lang()->first();
        });
 
		if($this->column){
		
			//栏目id
			$this->columnid = $this->column?$this->column->column_mid:null; 
			//分类ID
			$this->pid =$this->column->pid?$this->column->pid:null;
			$this->sid =$this->column->sid?$this->column->sid:null;
			 
		 
			$this->columnid = $this->column->column_mid;

			if($this->column->pid == 0){
				$this->isPid = true;
			}
		
		}
	}
	
 

    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		//删除数组为空的下标
        foreach( $this->expression as $k=>$v){ if( !$v )unset( $this->expression[$k] ); } 
		
		$this->musterVal = explode(',',current($this->expression));
		 
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
 
		}
		

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		
 
        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			 
			$this->objtag = (object)$this->tag;
			 
        }
		


       if(isset($this->objtag->length)) $this->length=$this->objtag->length*2;
       if(isset($this->objtag->desclength)) $this->desclength=$this->objtag->desclength*2;
       if(isset($this->objtag->adorn)) $this->adorn="'{$this->objtag->adorn}'";
	   
	   
	
	   
		//栏目 column
		//模板与传入变量，只有模板才能识别变量后台只能将变量传回模板才能处理
        if(isset($this->objtag->typeid) && substr($this->objtag->typeid,0,1)=="$"){
		   //闭包变量
		   $this->useVal = substr($this->objtag->typeid,0,strrpos($this->objtag->typeid,"->"));
		   $this->typeid($this->objtag->typeid);
        } 
		//模板传入栏目ID
        if(isset($this->objtag->typeid) && $this->objtag->typeid > 0 && is_numeric($this->objtag->typeid)){
		   $this->typeid($this->objtag->typeid);  
		    
        } 
		
	 
 

        if(isset($this->objtag->flag)){
          $this->flag($this->objtag->flag);
		   
        } 
		
	
		//设置排序
        if(isset($this->objtag->orderby)){
          $this->orderby($this->objtag->orderby);
		   
        }else{
		  $this->orderby('id');
	 
		}			
		

        //设置返回条数
         
        if(isset($this->objtag->rows)){
          $this->rows($this->objtag->rows);
		   
        }
 
	 
		//设置缓存时间
		if(isset($this->objtag->cachetime)){
			$this->tagcachetime=(int)$this->objtag->cachetime;
		}
		
		
        //设置缓存
		//如果传入 统一转成小写
		if(isset($this->objtag->cache)) $this->objtag->cache = strtolower($this->objtag->cache);

		if(isset($this->objtag->cache) && ($this->objtag->cache == true || $this->objtag->cache == 1)){
			  $this->iscache = true;
			  
		}else if(isset($this->objtag->cache) && ($this->objtag->cache == false || $this->objtag->cache == 0)){
			 $this->iscache = false;
			 $this->tagcachetime = 0;
		}

		$this->cache = $this->object.'cache';  
		$this->cacheObject = $this->object.'obj';  
	    
    }
	
	
	
	
	

    //传参数，组装字符串
    public function typeid($columnid)
    {
        $this->typeid = $columnid; 
		$this->tag['typeid']= "->whereIn('column_id',[".$columnid."]);"; 
    }
	
    //传参数，组装字符串
    public function group($columnid)
    {
		$this->tag['typeid'] = '->whereIn("column_id",p(\Cache::get("column_translations",'.$columnid.')));';
		//$this->tag['typeid']= "->whereIn('column_id',[".$columnid."]);"; 
    }
    //并接limit
    public function rows($rows = 1)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows);";   
      
    }
    //并接排序
    public function orderby($orderby)
    {
        $this->orderby = $orderby;
		 
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case "date":
                $value = '->orderBy("created_at","desc");';
                break;

            default: $value = '->orderBy("created_at","desc");';break;
        }
		
        $this->tag['orderby']= $value;
        
    }

 
    //并接查询条件 推荐类属性
    public function flag($flag)
    {
        $this->flag = $flag;
        $this->tag['flag']= '->where("flag",'.$flag.')';    
       
    }
    //TODO:
	public function setSysTagcachekey(){
		if($this->useVal){
			$this->sysTagcachekey ="\""; 
		}
		$this->sysTagcachekey .=$this->routeName."_"; 
		$this->sysTagcachekey .=$this->tagName."_"; 
		$this->sysTagcachekey .=substr($this->object,1)."_"; 

		foreach($this->objtag as $k=>$v){ 
		  $this->sysTagcachekey .= (substr($v,0,1)=="$"?substr($v,1):(string)$v).'_';
		} 

		foreach(request()->all() as $k=>$v){ 
		  $this->sysTagcachekey .=(string)$v.'_';
		} 

		if($this->useVal){
			$useVal = $this->useVal."->son";
			$this->sysTagcachekey .= "\".".$useVal;
		}
	}
 


}
