<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models;
use App\Models\BaseModel;
 

class PositionTranslation extends BaseModel
{
 
    public $timestamps = true;
    protected $fillable = ['title','sid','pid','firsttitle','secondtitle','thirdtitle','thumb','wapthumb','link','position_mid','keywords','description','sort','status','body','remarks'];
    protected $appends = ['type_name','langType'];
 
 
	
    //广告类型
    public function getTypeNameAttribute($query)
    {
		$value='';
        switch ($this->type) {
            case 1:   $value = "图型广告";  break;
            case 2:   $value = "文字广告";  break;
        }
        return $value;
    }


    //广告所在的位置信息
    public function position()
    {
        return $this->belongsTo('App\Models\Position');
    }

    //广告位置名称
    public function advname()
    {
        return $this->hasOne('App\Models\PositionTranslation','sid','pid');
    }

	
	

}
