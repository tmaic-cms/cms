@extends('theme::layouts.h5')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('body', 'news')
@section('bodyid', 'top')
@section('main') 
<div class="box1">
    <div class="title">
      <h1>新闻媒体</h1>
      <p>了解前沿设计探索 获取最新企业动态</p>
    </div>
    <div class="tab">
      <ul>
		@channel($articlecolumn group=5 rows=5)
        <li class="transition @if($articlecolumn->column_mid == $column->column_mid ) active @endif">
          <a href="{{$articlecolumn->link}}">{{$articlecolumn->name}}</a>
        </li>
        @endchannel
      </ul>
      <div class="tabnr">
        <div class="nr transition active">
          <ul>
		  @list($article orderby=date page=10)
            <li>
              <a href="{{$article->link}}">
                <div class="img">
                  <img class="transition" src="{{$article->thumb}}" title="{{$article->title}}" alt="{{$article->seo_title}}">
                </div>
                <article>
                  <div class="nl-box">
                    <h2>{{$article->title}}</h2>
                    <div class="nl-txt">
                      <p>{{$article->description}}</p>
                    </div>
                    <span class="more-btn">
                      More<span class="iconfont">&#xe602;</span>
                    </span>
                  </div>
                </article>
              </a>
            </li>
            @endlist
			{{$articlepage->links()}} 
          </ul>
        </div>
      </div>
    </div>
  </div>
@endsection









