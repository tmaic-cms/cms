@extends('admin.base')
@section('content')
<form class="layui-form" id="positionForm" lay-filter="positionForm">
@include('admin.position.form')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','layedit'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    var upload = layui.upload;
    var element = layui.element;
    var layedit = layui.layedit;
    var admin = layui.admin;
    form.val('positionForm', {
      "pid": "{{$position->pid}}",
      "title": "{{$position->title}}",
      "type": "{{$position->type}}"
       
    });

    var editIndex = layedit.build('description', {
      uploadImage: {url: '{{ route("uploadImg") }}', type: 'post'},
      height:100
    }); 
 

    //普通图片上传
    var uploadInst = upload.render({
        elem: '#uploadPic'
        ,url: '{{ route("uploadImg") }}'
        ,multiple: false
        ,data:{"_token":"{{ csrf_token() }}"}
        ,before: function(obj){
            obj.preview(function(index, file, result){
                $('#layui-upload-box').html('<li><img src="'+result+'" width=50/><p>上传中</p></li>')
            });
        }
        ,done: function(res){
            //如果上传失败
            if(res.code == 0){
                $("#thumb").val(res.url);
                $('#layui-upload-box li p').text('上传成功');
                return layer.msg(res.msg);
            }
            return layer.msg(res.msg);
        }
    });


    //自定义广告类型验证规则
    form.verify({
    otherReq: function(value,item){
      var verifyName=$(item).attr('name')
        ,verifyType=$(item).attr('type')
        ,formElem=$(item).parents('.layui-form')
        ,verifyElem=formElem.find('input[name='+verifyName+']')
        ,isTrue= verifyElem.is(':checked')//是否命中校验
        ,focusElem = verifyElem.next().find('i.layui-icon');
        if(!isTrue || !value){
        //定位焦点
        focusElem.css(verifyType=='radio'?{"color":"#FF5722"}:{"border-color":"#FF5722"});
        //对非输入框设置焦点
        focusElem.first().attr("tabIndex","1").css("outline","0").blur(function() {
            focusElem.css(verifyType=='radio'?{"color":""}:{"border-color":""});
         }).focus();
          return '广告类型属性必填项,且不能为空';
        }
    }
    });

    form.on('submit(SubmitData)', function (data) {
	  //$(this).parent().hide();	
      var  editorData = editor.getData();
      var formData=data.field;
      formData.body=editorData;
       
        $.ajax({
            type: "POST",
            url: '{{ route("admin.position.update")}}',
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.code == 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						//admin.closeDialog();
					});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {
                $(this).parent().show();	
                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
    });
   

       
});
</script>
@endsection