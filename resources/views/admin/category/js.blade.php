@include('admin.category.html')
<script type="text/javascript" src="/assets/js/category.js"></script>
<script>
    var categorycreate = "{{ route('admin.columns.create') }}";
    var categoryedit = "{{ route('admin.columns.edit') }}?id=";
    var categoryupdate = "{{ route('admin.columns.update') }}";
    var categoryupdateAjax = "{{ route('admin.columns.updateAjax') }}";
    var categorydestroy ="{{ route('admin.columns.destroy') }}";
    var categoryData = "{{ route('admin.columns.data') }}?pid=";
    var columnsData = "{{ route('admin.columns.data') }}";


</script>