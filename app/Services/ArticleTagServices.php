<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2017-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2017-11-13
*/

namespace App\Services;
use App\Services\ChannelBaseServices;
use App\Models\ColumnTranslation;
use Cache;
class ArticleTagServices extends ChannelBaseServices
{
   
    public $tagName;
    public $expression;
    public $musterVal;
    public $sqlstr;
    public $object;
    public $cacheObject;
    public $column;
    public $columnid;
    public $rows;
    public $orderby;
    public $length;	
    public $desclength;	
	public $adorn;
    public $flag;
    public $locale;
    public $tag;
    public $objtag;
    public $routeName;
    public $cache;
    public $iscache;
	//缓存key
    public $sysTagcachekey;
    public $tagcachetime;
	//闭包变量
    public $useVal;




    public function __construct($expression)
    {
		$this->tagName ="article";
       //出始化变量
	   $this->pageObj = NULL;
	   //闭包变量
	   $this->useVal = NULL;
	   $this->iscache = false;

	   //当前语言
	   $this->locale = lang();
	   //title 长度
	   $this->length = 0;
	   //描述长度
	   $this->desclength = 0;
	   //返回行数
	   $this->rows = 1;
	   //超出部分显示的字符串
	   $this->adorn = null;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=0;
	   $this->setColumn();
	   //开始处理标签
       $this->decompose($expression);
	   //生成缓存key
	   $this->setSysTagcachekey();

    }
    //出始化栏目首次执行 并且缓存，下次有相同的从缓存取数据
    public function setColumn(){
	    
       $this->column = Cache::remember($this->routeName.'_'.$this->locale, webconfig('SYSCACHETIME') , function() {
          return  ColumnTranslation::with(['children','class'])->where('routename',$this->routeName)->lang()->first();
        });
		
		$this->columnid = $this->column->column_mid??null; 
		
		 
	}

    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		//删除数组为空的下标
        foreach( $this->expression as $k=>$v){ if( !$v )unset( $this->expression[$k] ); } 
		
		$this->musterVal = explode(',',current($this->expression));
		 
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
		}
		

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
 
        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			$this->objtag = (object)$this->tag;
        }
		
       //长度限制
       if(isset($this->objtag->length)) $this->length=$this->objtag->length*2;
       if(isset($this->objtag->desclength)) $this->desclength=$this->objtag->desclength*2;
       if(isset($this->objtag->adorn)) $this->adorn="'{$this->objtag->adorn}'";
	   
		//设置栏目
        if(isset($this->objtag->typeid) && substr($this->objtag->typeid,0,1)=="$"){
			$this->useVal = substr($this->objtag->typeid,0,strrpos($this->objtag->typeid,"->"));
			$this->columnid = $this->objtag->typeid; 
			$this->typeid($this->objtag->typeid);
        }elseif(isset($this->objtag->typeid)){
		 
          $this->columnid = $this->objtag->typeid; 
          $this->typeid($this->objtag->typeid);
		  
        } 
		
        //推荐文章属性
        if(isset($this->objtag->flag)){
          $this->flag($this->objtag->flag);
        }
		
		

		//设置排序
        if(isset($this->objtag->orderby)){
          $this->orderby($this->objtag->orderby);
        }else{
		  $this->orderby('id');
		}			
		

        //设置返回条数
        if(isset($this->objtag->rows)){
          $this->rows($this->objtag->rows);
        }else{
			 $this->rows(1);
		}
	 
	 
		//设置缓存时间
		if(isset($this->objtag->cachetime)){
			$this->tagcachetime=(int)$this->objtag->cachetime;
		}
 
		
        //设置缓存
		//如果传入 统一转成小写
		if(isset($this->objtag->cache)) $this->objtag->cache = strtolower($this->objtag->cache);

		if(isset($this->objtag->cache) && ($this->objtag->cache == true || $this->objtag->cache == 1)){
			  $this->iscache = true;
			  
		}else if(isset($this->objtag->cache) && ($this->objtag->cache == false || $this->objtag->cache == 0)){
			 $this->iscache = false;
			 $this->tagcachetime = 0;
		}
		$this->cache = $this->object.'cache';  
		$this->cacheObject = $this->object.'obj';
    }
	
	

    //栏目拼接
    public function typeid($columnid)
    {
	    
        $this->tag['typeid']= "->whereIn('column_id',[".$this->columnid."]);";   

    }

	//返回行数拼接
    public function rows($rows = 1)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows);";   
        
    }
    
	//排序拼接
    public function orderby($orderby = 'id')
    {
        
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case 'date':
                $value = '->orderBy("created_at","desc");';
                break;
            default: $value = '->orderBy("id","desc");';break;
        }
   
        $this->tag['orderby']= $value;
        
    }

    //分页拼接
    public function pageNum($pageNum = null)
    {
        
        $this->pageNum = $pageNum;
        $this->tag['pageNum']= "->paginate($pageNum);";   

    }


    //推荐文章属性
    public function flag($flag=0)
    {
        $this->flag = $flag;

        switch ($this->flag) {
            case 1:  $value = '->flag1();';  break;
            case 2:  $value = '->flag2();';  break;
            case 3:  $value = '->flag3();';  break;
            case 4:  $value = '->flag4();';  break;
            case 5:  $value = '->flag5();';  break;
            case 6:  $value = '->flag6();';  break;
			default: $value = '->flag();';   break;
        }
		$this->tag['flag']= $value;   
    }


    //TODO:
	public function setSysTagcachekey(){
		
		if($this->useVal){
			$this->sysTagcachekey ="\""; 
		}
		$this->sysTagcachekey .=$this->routeName."_"; 
		$this->sysTagcachekey .=$this->tagName."_"; 
		$this->sysTagcachekey .=substr($this->object,1)."_"; 

		foreach($this->objtag as $k=>$v){ 
		  $this->sysTagcachekey .= (substr($v,0,1)=="$"?substr($v,1):(string)$v).'_';
		} 
		
		foreach(request()->all() as $k=>$v){ 
		  $this->sysTagcachekey .=(string)$v.'_';
		} 
	    

		if($this->useVal){
			$useVal = $this->useVal."->son";
			$this->sysTagcachekey .= "\".".$useVal;
		}
 
	}












}
