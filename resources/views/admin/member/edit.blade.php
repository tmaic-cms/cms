@extends('admin.base')

@section('content')
    <div class="layui-card">
 
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('admin.member.update',['member'=>$member->id])}}" method="post">
                <input type="hidden" name="id" value="{{$member->id}}">
                
                @include('admin.member._form')
            </form>
        </div>
    </div>
@endsection

@section('script')
    @include('admin.member._js')
@endsection
