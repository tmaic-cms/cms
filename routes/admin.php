<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/

$prefix="admin";
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| 后台公共路由部分
|
*/
Route::group(['namespace' => 'App\Http\Controllers\Admin','prefix' =>$prefix], function () {
    //登录、注销
    Route::get('login', 'LoginController@showLoginForm')->name('admin.loginForm');
    Route::post('login', 'LoginController@login')->name('login');
    Route::post('auth/login', 'LoginController@authenticate')->name('admin.authenticate');
    Route::get('logout', 'LoginController@logout')->name('admin.logout');
     
});

Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => 'auth'], function () {
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| 后台需要授权的路由 admins
|
*/
   //文件上传接口，前后台共用
    Route::post('uploadImg', 'PublicController@uploadImg')->name('uploadImg');
    Route::delete('included', 'PublicController@included')->name('included');
    //上传并保存DB
    Route::post('uploadImg/picture', 'PublicController@picture')->name('goods.img.upload');
    //删除远程资源
    Route::delete('uploadImg/delete', 'PublicController@imaggDelete')->name('uploadImg.del');
    //网络下载图片资源
    Route::post('downloadimage', 'PublicController@downloadImage')->name('downloadimage');
 
 
    //后台布局
    Route::get('/', 'IndexController@layout')->name('admin.layout');
    //后台首页
    Route::get('/index', 'IndexController@index')->name('admin.index');
    Route::get('/index1', 'StatisticsController@index')->name('admin.index1');
    Route::get('/index2', 'IndexController@index2')->name('admin.index2');
    Route::get('/index3', 'IndexController@index2')->name('admin.test3');

    Route::get('/test', 'TestController@index')->name('admin.test');
    Route::get('test/create', 'TestController@create')->name('admin.test.create');

    Route::get('/test2', 'TestController@index2')->name('admin.test2');
 

    //图标
    Route::get('icons', 'IndexController@icons')->name('admin.icons');

    //更改密码
    Route::get('change_my_password_form', 'AdminsController@changeMyPasswordForm')->name('admin.admins.changePassword')->middleware('auth');
    Route::post('change_my_password', 'AdminsController@changeMyPassword')->name('admin.admins.changeMyPassword')->middleware('auth');

 });



Route::group(['namespace' => 'App\Http\Controllers\Admin','prefix' =>$prefix, 'middleware' => ['auth','menu','permission:order.manage']], function () {
	Route::get('order', 'OrderController@index')->name('admin.order');
	Route::get('order/data', 'OrderController@data')->name('admin.order.data');
	Route::get('order/goodsdata', 'OrderController@goodsData')->name('admin.order.goodsData');
	Route::get('order/create', 'OrderController@create')->name('admin.order.create')->middleware('permission:order.order.create');
	Route::post('order/store', 'OrderController@store')->name('admin.order.store')->middleware('permission:order.order.create');
	Route::post('order/edit', 'OrderController@edit')->name('admin.order.edit')->middleware('permission:order.order.edit');
	Route::post('order/update', 'OrderController@update')->name('admin.order.update')->middleware('permission:order.order.edit');
	Route::delete('order/destroy', 'OrderController@destroy')->name('admin.order.destroy')->middleware('permission:order.order.destroy');
 
});




/*
|--------------------------------------------------------------------------
| 财务管理
|--------------------------------------------------------------------------
*/
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:affairs.manage']], function () {
//统计
    Route::group(['middleware' => 'permission:affairs.statistics'], function () {
        Route::get('statistics', 'StatisticsController@index')->name('admin.statistics');
    });
//提现审核
    Route::group(['middleware' => 'permission:affairs.cashout'], function () {
        Route::get('order.cashout', 'CashoutController@index')->name('admin.cashout');
    });
//退款
    Route::group(['middleware' => 'permission:affairs.refund'], function () {
        Route::get('refund', 'RefundController@index')->name('admin.refund')->middleware('permission:affairs.refund');
        Route::get('refund.data', 'RefundController@data')->name('admin.refund.data')->middleware('permission:affairs.refund');
        Route::get('refund.create', 'RefundController@index')->name('admin.refund.create')->middleware('permission:affairs.refund.create');
        Route::delete('execute.refund', 'RefundController@execute')->name('admin.execute.refund')->middleware('permission:affairs.refund.execute');
        Route::delete('refund.destroy', 'RefundController@destroy')->name('admin.refund.destroy')->middleware('permission:affairs.refund.create');


    }); 

});
//商品管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:goods.manage']], function () {
    Route::get('goods', 'GoodsController@index')->name('admin.goods');
    Route::get('goods/data', 'GoodsController@data')->name('admin.goods.data');
    Route::get('goods/create', 'GoodsController@create')->name('admin.goods.create')->middleware('permission:goods.goods.edit');
    Route::post('goods/store', 'GoodsController@store')->name('admin.goods.store')->middleware('permission:goods.goods.edit');
    Route::get('goods/edit', 'GoodsController@edit')->name('admin.goods.edit')->middleware('permission:goods.goods.edit');
    Route::post('goods/update', 'GoodsController@update')->name('admin.goods.update')->middleware('permission:goods.goods.edit');
    Route::delete('goods/destroy', 'GoodsController@destroy')->name('admin.goods.destroy')->middleware('permission:goods.brand.destroy');
    Route::get('goods/data.sku', 'GoodsController@dataSku')->name('admin.goods.sku');
 
});

//系统管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:system.manage']], function () {
    //数据表格接口
    Route::get('data', 'IndexController@data')->name('admin.data')->middleware('permission:system.role|system.admins|system.permission');
    //用户管理
    Route::group(['middleware' => ['permission:system.admins']], function () {

        Route::get('admins', 'AdminsController@index')->name('admin.admins');
        //添加
        Route::get('admins/create', 'AdminsController@create')->name('admin.admins.create')->middleware('permission:system.admins.create');
        Route::post('admins/store', 'AdminsController@store')->name('admin.admins.store')->middleware('permission:system.admins.create');
        //编辑
        Route::get('admins/{id}/edit', 'AdminsController@edit')->name('admin.admins.edit')->middleware('permission:system.admins.edit');
        Route::post('admins/update', 'AdminsController@update')->name('admin.admins.update')->middleware('permission:system.admins.edit');


        //删除
        Route::delete('admins/destroy', 'AdminsController@destroy')->name('admin.admins.destroy')->middleware('permission:system.admins.destroy');
        //分配角色
        Route::get('admins/{id}/role', 'AdminsController@role')->name('admin.admins.role')->middleware('permission:system.admins.role');
        Route::put('admins/{id}/assignRole', 'AdminsController@assignRole')->name('admin.admins.assignRole')->middleware('permission:system.admins.role');
        //分配权限
        Route::get('admins/{id}/permission', 'AdminsController@permission')->name('admin.admins.permission')->middleware('permission:system.admins.permission');
        Route::put('admins/{id}/assignPermission', 'AdminsController@assignPermission')->name('admin.admins.assignPermission')->middleware('permission:system.admins.permission');
    });
    //角色管理
    Route::group(['middleware' => 'permission:system.role'], function () {
        Route::get('role', 'RoleController@index')->name('admin.role');
        //添加
        Route::get('role/create', 'RoleController@create')->name('admin.role.create');
        Route::post('role/store', 'RoleController@store')->name('admin.role.store');
        //编辑
        Route::get('role/{id}/edit', 'RoleController@edit')->name('admin.role.edit');
        Route::put('role/{id}/update', 'RoleController@update')->name('admin.role.update');
        //删除
        Route::delete('role/destroy', 'RoleController@destroy')->name('admin.role.destroy');
        //分配权限
        Route::get('role/{id}/permission', 'RoleController@permission')->name('admin.role.permission');
        Route::post('role/assignPermission', 'RoleController@assignPermission')->name('admin.role.assignPermission');
    });

    //权限管理
    Route::group(['middleware' => 'permission:system.permission'], function () {
        Route::get('permission', 'PermissionController@index')->name('admin.permission');
        Route::Any('permission/data', 'PermissionController@data')->name('admin.permission.data');
        Route::Any('permission/menu.data', 'PermissionController@menuData')->name('admin.permission.menuData');
        //添加
        Route::get('permission/create', 'PermissionController@create')->name('admin.permission.create')->middleware('permission:system.permission.create');
        Route::post('permission/store', 'PermissionController@store')->name('admin.permission.store')->middleware('permission:system.permission.create');
        //编辑
        Route::get('permission/edit', 'PermissionController@edit')->name('admin.permission.edit')->middleware('permission:system.permission.edit');
        Route::post('permission/update', 'PermissionController@update')->name('admin.permission.update')->middleware('permission:system.permission.edit');
        Route::post('permission/updateAjax', 'PermissionController@updateAjax')->name('admin.permission.updateAjax')->middleware('permission:system.permission.edit');
        //删除
        Route::delete('permission/destroy', 'PermissionController@destroy')->name('admin.permission.destroy')->middleware('permission:system.permission.destroy');
    });
    //配置组
    Route::group(['middleware' => 'permission:system.configgroup'], function () {
        Route::get('configgroup', 'ConfigGroupController@index')->name('admin.configgroup');
        Route::get('configgroup/data', 'ConfigGroupController@data')->name('admin.configgroup.data');
        //添加
        Route::get('configgroup/create', 'ConfigGroupController@create')->name('admin.configgroup.create')->middleware('permission:system.configgroup.create');
        Route::post('configgroup/store', 'ConfigGroupController@store')->name('admin.configgroup.store')->middleware('permission:system.configgroup.create');
        //编辑
        Route::get('configgroup/{id}/edit', 'ConfigGroupController@edit')->name('admin.configgroup.edit')->middleware('permission:system.configgroup.edit');
        Route::put('configgroup/{id}/update', 'ConfigGroupController@update')->name('admin.configgroup.update')->middleware('permission:system.configgroup.edit');
        //删除
        Route::delete('configgroup/destroy', 'ConfigGroupController@destroy')->name('admin.configgroup.destroy')->middleware('permission:system.configgroup.destroy');
    });
    //系统配置
    Route::group(['middleware' => 'permission:system.sys'], function () {
        Route::get('sys', 'SysController@index')->name('admin.sys');
        Route::get('sys/data', 'SysController@data')->name('admin.sys.data');
        //添加
        Route::get('sys/create', 'SysController@create')->name('admin.sys.create')->middleware('permission:system.sys.create');
        Route::post('sys/store', 'SysController@store')->name('admin.sys.store')->middleware('permission:system.sys.create');
        //编辑
        Route::get('sys/{id}/edit', 'SysController@edit')->name('admin.sys.edit')->middleware('permission:system.sys.edit');
        Route::put('sys/update', 'SysController@update')->name('admin.sys.update')->middleware('permission:system.sys.edit');
        //删除
        Route::delete('sys/destroy', 'SysController@destroy')->name('admin.sys.destroy')->middleware('permission:system.sys.destroy');
    });	
	
});



//品牌管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:brand.manage']], function () {
    Route::get('brand', 'BrandController@index')->name('admin.brand');
    Route::get('brand/data', 'BrandController@data')->name('admin.brand.data');
    Route::get('brand/create', 'BrandController@create')->name('admin.brand.create')->middleware('permission:goods.brand.create');
    Route::post('brand/store', 'BrandController@store')->name('admin.brand.store')->middleware('permission:goods.brand.create');
    Route::get('brand/{id}/edit', 'BrandController@edit')->name('admin.brand.edit')->middleware('permission:goods.brand.edit');
    Route::post('brand/update', 'BrandController@update')->name('admin.brand.update')->middleware('permission:goods.brand.edit');
    Route::delete('brand/destroy', 'BrandController@destroy')->name('admin.brand.destroy')->middleware('permission:goods.brand.destroy');
 
});
/**
 * 产品类型
 */
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:product.goodstype']], function () {
  
        Route::get('goodstype', 'GoodsTypeController@index')->name('admin.goodstype');
        Route::Any('goodstype/data', 'GoodsTypeController@data')->name('admin.goodstype.data');
        Route::get('goodstype/create', 'GoodsTypeController@create')->name('admin.goodstype.create')->middleware('permission:product.goodstype.create');
        Route::post('goodstype/store', 'GoodsTypeController@store')->name('admin.goodstype.store')->middleware('permission:product.goodstype.create');
        Route::get('goodstype/edit', 'GoodsTypeController@edit')->name('admin.goodstype.edit')->middleware('permission:product.goodstype.edit');
        Route::post('goodstype/update', 'GoodsTypeController@update')->name('admin.goodstype.update')->middleware('permission:product.goodstype.edit');
        Route::delete('goodstype/destroy', 'GoodsTypeController@destroy')->name('admin.goodstype.destroy')->middleware('permission:product.goodstype.destroy');;
 
});
//产品管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:product.manage']], function () {
  
    Route::group(['middleware' => 'permission:product.fabric'], function () {
        Route::get('product', 'FabricController@index')->name('admin.fabric');
        Route::get('product/data', 'FabricController@data')->name('admin.product.data');
        Route::get('product/create', 'FabricController@create')->name('admin.product.create')->middleware('permission:product.fabric.create');
        Route::post('product/store', 'FabricController@store')->name('admin.product.store')->middleware('permission:product.fabric.create');
        Route::get('product/edit', 'FabricController@edit')->name('admin.product.edit')->middleware('permission:product.fabric.edit');
        Route::post('product/update', 'FabricController@update')->name('admin.product.update')->middleware('permission:product.fabric.edit');
        Route::post('product/updateAjax', 'FabricController@updateAjax')->name('admin.product.updateAjax')->middleware('permission:product.fabric.edit'); 
        Route::delete('product/destroy', 'FabricController@destroy')->name('admin.product.destroy')->middleware('permission:product.fabric.destroy');
        Route::get('Atlas/data', 'AtlasController@data')->name('admin.Atlas.data');
        Route::get('Atlas/upload', 'AtlasController@upload')->name('admin.Atlas.upload');
        Route::post('Atlas/store', 'AtlasController@store')->name('admin.Atlas.store');
        Route::delete('Atlas/destroy', 'AtlasController@destroy')->name('admin.Atlas.destroy');
    });

    Route::group(['middleware' => 'permission:product.case'], function () {
        Route::get('case', 'CasesController@index')->name('admin.case');
        Route::get('case/data', 'CasesController@data')->name('admin.case.data');
        Route::get('case/create', 'CasesController@create')->name('admin.case.create')->middleware('permission:product.case.create');
        Route::post('case/store', 'CasesController@store')->name('admin.case.store')->middleware('permission:product.case.create');
        Route::get('case/edit', 'CasesController@edit')->name('admin.case.edit')->middleware('permission:product.case.edit');
        Route::post('case/update', 'CasesController@update')->name('admin.case.update')->middleware('permission:product.case.edit');
        Route::post('case/updateAjax', 'CasesController@updateAjax')->name('admin.case.updateAjax')->middleware('permission:product.case.edit'); 
        Route::delete('case/destroy', 'CasesController@destroy')->name('admin.case.destroy')->middleware('permission:product.case.destroy');

        Route::get('case/atlas/data', 'CaseAtlasController@data')->name('admin.case.atlas.data');
        Route::get('case/atlas/upload', 'CaseAtlasController@upload')->name('admin.case.atlas.upload');
        Route::post('case/atlas/store', 'CaseAtlasController@store')->name('admin.case.atlas.store');
        Route::delete('case/atlas/destroy', 'CaseAtlasController@destroy')->name('admin.case.atlas.destroy');

    });


});



//案例分类
/*
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:product.casetype']], function () {
        Route::get('casetype', 'CaseTypeController@index')->name('admin.casetype');
        Route::get('casetype/data', 'CaseTypeController@data')->name('admin.casetype.data');
        Route::get('casetype/create', 'CaseTypeController@create')->name('admin.casetype.create')->middleware('permission:product.casetype.create');
        Route::Any('casetype/store', 'CaseTypeController@store')->name('admin.casetype.store')->middleware('permission:product.casetype.create');
        Route::get('casetype/{id}/edit', 'CaseTypeController@edit')->name('admin.casetype.edit')->middleware('permission:product.casetype.edit');
        Route::put('casetype/{id}/update', 'CaseTypeController@update')->name('admin.casetype.update')->middleware('permission:product.casetype.edit');
        Route::delete('casetype/destroy', 'CaseTypeController@destroy')->name('admin.casetype.destroy')->middleware('permission:product.casetype.destroy');
});
*/

//栏目管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:columns.manage']], function () {
    
    
       
    Route::get('columns', 'ColumnsController@index')->name('admin.columns');
    Route::get('columns/edit', 'ColumnsController@edit')->name('admin.columns.edit')->middleware('permission:columns.edit');
    Route::post('columns/update', 'ColumnsController@update')->name('admin.columns.update')->middleware('permission:columns.edit');
	Route::post('columns/updateajax', 'ColumnsController@updateAjax')->name('admin.columns.updateAjax')->middleware('permission:columns.edit');
    Route::Any('columns/data', 'ColumnsController@data')->name('admin.columns.data');
    Route::get('columns/create', 'ColumnsController@create')->name('admin.columns.create');
    Route::post('columns/store', 'ColumnsController@store')->name('admin.columns.store')->middleware('permission:columns.create');
    Route::delete('columns/destroy', 'ColumnsController@destroy')->name('admin.columns.destroy')->middleware('permission:columns.destroy');

});

//资讯管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:zixun.manage']], function () {
  
    //文章管理
    Route::group(['middleware' => 'permission:zixun.article'], function () {
        Route::get('article/data', 'ArticleController@data')->name('admin.article.data');
        Route::get('article', 'ArticleController@index')->name('admin.article');
        //添加
        Route::get('article/create', 'ArticleController@create')->name('admin.article.create')->middleware('permission:zixun.article.create');
        Route::post('article/store', 'ArticleController@store')->name('admin.article.store')->middleware('permission:zixun.article.create');
        //编辑
        Route::get('article/edit', 'ArticleController@edit')->name('admin.article.edit')->middleware('permission:zixun.article.edit');
        Route::post('article/update', 'ArticleController@update')->name('admin.article.update')->middleware('permission:zixun.article.edit');
        Route::post('article/updateAjax', 'ArticleController@updateAjax')->name('admin.article.updateAjax')->middleware('permission:zixun.article.edit');
        //删除
        Route::delete('article/destroy', 'ArticleController@destroy')->name('admin.article.destroy')->middleware('permission:zixun.article.destroy');
    });

    //标签管理
    Route::group(['middleware' => 'permission:zixun.tag'], function () {
        Route::get('tag/data', 'TagController@data')->name('admin.tag.data');
        Route::get('tag', 'TagController@index')->name('admin.tag');
        //添加
        Route::get('tag/create', 'TagController@create')->name('admin.tag.create')->middleware('permission:zixun.tag.create');
        Route::post('tag/store', 'TagController@store')->name('admin.tag.store')->middleware('permission:zixun.tag.create');
        //编辑
        Route::get('tag/{id}/edit', 'TagController@edit')->name('admin.tag.edit')->middleware('permission:zixun.tag.edit');
        Route::put('tag/{id}/update', 'TagController@update')->name('admin.tag.update')->middleware('permission:zixun.tag.edit');
        //删除
        Route::delete('tag/destroy', 'TagController@destroy')->name('admin.tag.destroy')->middleware('permission:zixun.tag.destroy');
    });
});
//web站点配置
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:webconfig.manage']], function () {
	
    Route::group(['middleware' => 'permission:webconfig.configuration'], function () {
        Route::get('configuration', 'ConfigurationController@index')->name('admin.configuration');
        Route::get('configuration/data', 'ConfigurationController@data')->name('admin.configuration.data');
        Route::get('configuration/list', 'ConfigurationController@lists')->name('admin.configuration.list');
        //添加
        Route::get('configuration/create', 'ConfigurationController@create')->name('admin.configuration.create')->middleware('permission:webconfig.configuration.create');
        Route::post('configuration/store', 'ConfigurationController@store')->name('admin.configuration.store')->middleware('permission:webconfig.configuration.create');
        //编辑
        Route::get('configuration/{id}/edit', 'ConfigurationController@edit')->name('admin.configuration.edit')->middleware('permission:webconfig.configuration.edit');
        Route::put('configuration/update', 'ConfigurationController@update')->name('admin.configuration.update')->middleware('permission:webconfig.configuration.edit');
        Route::put('configuration/{id}/updateone', 'ConfigurationController@updateOne')->name('admin.configuration.updateOne')->middleware('permission:webconfig.configuration.edit');
        //删除
        Route::delete('configuration/destroy', 'ConfigurationController@destroy')->name('admin.configuration.destroy')->middleware('permission:webconfig.configuration.destroy');
    });
	
	//自定义配置
    Route::group(['middleware' => 'permission:webconfig.defines'], function () {
        Route::get('defines', 'DefinesController@index')->name('admin.defines');
        Route::get('defines/data', 'DefinesController@data')->name('admin.defines.data');
        Route::get('defines/list', 'DefinesController@lists')->name('admin.defines.list');
        //添加
        Route::get('defines/create', 'DefinesController@create')->name('admin.defines.create')->middleware('permission:webconfig.defines.create');
        Route::post('defines/store', 'DefinesController@store')->name('admin.defines.store')->middleware('permission:webconfig.defines.create');
        //编辑
        Route::get('defines/{id}/edit', 'DefinesController@edit')->name('admin.defines.edit')->middleware('permission:webconfig.defines.edit');
        Route::post('defines/update', 'DefinesController@update')->name('admin.defines.update')->middleware('permission:webconfig.defines.edit');
        Route::post('defines/updateajax', 'DefinesController@updateajax')->name('admin.defines.updateajax')->middleware('permission:webconfig.defines.edit');
        //删除
        Route::delete('defines/destroy', 'DefinesController@destroy')->name('admin.defines.destroy')->middleware('permission:webconfig.defines.destroy');
    });
	
	//主题配置
    Route::group(['middleware' => 'permission:webconfig.theme'], function () {
        Route::get('theme', 'ThemeController@index')->name('admin.theme');
        Route::get('theme/data', 'ThemeController@data')->name('admin.theme.data');
        Route::get('theme/list', 'ThemeController@lists')->name('admin.theme.list');
        //添加
        Route::get('theme/create', 'ThemeController@create')->name('admin.theme.create')->middleware('permission:webconfig.theme.create');
        Route::post('theme/store', 'ThemeController@store')->name('admin.theme.store')->middleware('permission:webconfig.theme.create');
        //编辑
        Route::get('theme/edit', 'ThemeController@edit')->name('admin.theme.edit')->middleware('permission:webconfig.theme.edit');
        Route::post('theme/update', 'ThemeController@update')->name('admin.theme.update')->middleware('permission:webconfig.theme.edit');
        Route::post('theme/updateajax', 'ThemeController@updateajax')->name('admin.theme.updateajax')->middleware('permission:webconfig.theme.edit');
        //删除
        Route::delete('theme/destroy', 'ThemeController@destroy')->name('admin.theme.destroy')->middleware('permission:webconfig.theme.destroy');
    });
	
	
	
    //友情链接
    Route::group(['middleware' => 'permission:webconfig.link'], function () {
        Route::get('link/data', 'LinkController@data')->name('admin.link.data');
        Route::get('link', 'LinkController@index')->name('admin.link');
        //添加
        Route::get('link/create', 'LinkController@create')->name('admin.link.create')->middleware('permission:webconfig.link.create');
        Route::post('link/store', 'LinkController@store')->name('admin.link.store')->middleware('permission:webconfig.link.create');
        //编辑
        Route::put('link/update', 'LinkController@update')->name('admin.link.update')->middleware('permission:webconfig.link.edit');
        Route::get('link/{id}/edit', 'LinkController@edit')->name('admin.link.edit')->middleware('permission:webconfig.link.edit');
        //删除
        Route::delete('link/destroy', 'LinkController@destroy')->name('admin.link.destroy')->middleware('permission:webconfig.link.destroy');
    });
    //导航设置
    Route::group(['middleware' => 'permission:webconfig.nav'], function () {
        Route::get('nav', 'NavigationController@index')->name('admin.nav');
        Route::get('nav/data', 'NavigationController@data')->name('admin.nav.data');
        //添加
        Route::get('nav/create', 'NavigationController@create')->name('admin.nav.create')->middleware('permission:webconfig.nav.create');
        Route::post('nav/store', 'NavigationController@store')->name('admin.nav.store')->middleware('permission:webconfig.nav.create');
        //编辑
        Route::put('nav/update', 'NavigationController@update')->name('admin.nav.update')->middleware('permission:webconfig.nav.edit');
        Route::get('nav/{id}/edit', 'NavigationController@edit')->name('admin.nav.edit')->middleware('permission:webconfig.nav.edit');
        //删除
        Route::delete('nav/destroy', 'NavigationController@destroy')->name('admin.nav.destroy')->middleware('permission:webconfig.nav.destroy');
    });
});
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:adv.manage']], function () {
    //广告位
    Route::group(['middleware' => 'permission:adv.position'], function () {
        Route::get('position/data', 'PositionController@data')->name('admin.position.data');
        Route::get('position', 'PositionController@index')->name('admin.position');
        //添加
        Route::get('position/create', 'PositionController@create')->name('admin.position.create')->middleware('permission:adv.position.create');
        Route::post('position/store', 'PositionController@store')->name('admin.position.store')->middleware('permission:adv.position.create');
        //编辑
        Route::get('position/edit', 'PositionController@edit')->name('admin.position.edit')->middleware('permission:adv.position.edit');
        Route::post('position/update', 'PositionController@update')->name('admin.position.update')->middleware('permission:adv.position.edit');
        //删除
        Route::delete('position/destroy', 'PositionController@destroy')->name('admin.position.destroy')->middleware('permission:adv.position.destroy');
        
    });

    //广告位新版 图型
    Route::group(['middleware' => 'permission:adv.ad'], function () {
        Route::get('ad/data', 'AdvController@data')->name('admin.ad.data');
        Route::get('ad', 'AdvController@index')->name('admin.ad');
        //添加
        Route::get('ad/create', 'AdvController@create')->name('admin.ad.create')->middleware('permission:adv.ad.create');
        Route::post('ad/store', 'AdvController@store')->name('admin.ad.store')->middleware('permission:adv.ad.create');
        //编辑
        Route::get('ad/edit', 'AdvController@edit')->name('admin.ad.edit')->middleware('permission:adv.ad.edit');
        Route::post('ad/update', 'AdvController@update')->name('admin.ad.update')->middleware('permission:adv.ad.edit');
        //删除
        Route::delete('ad/destroy', 'AdvController@destroy')->name('admin.ad.destroy')->middleware('permission:adv.ad.destroy');
        //添加广告位
        Route::get('ad/position.create', 'AdvController@createPosition')->name('admin.ad.position.create')->middleware('permission:adv.ad.create');
        Route::get('ad/position.edit', 'AdvController@editPosition')->name('admin.ad.position.edit')->middleware('permission:adv.ad.create');
    });

    //广告位新版 文字
    Route::group(['middleware' => 'permission:adv.ads'], function () {
        Route::get('ads/data', 'AdsController@data')->name('admin.ads.data');
        Route::get('ads', 'AdsController@index')->name('admin.ads');
        //添加
        Route::get('ads/create', 'AdsController@create')->name('admin.ads.create')->middleware('permission:adv.ads.create');
        Route::post('ads/store', 'AdsController@store')->name('admin.ads.store')->middleware('permission:adv.ads.create');
        //编辑
        Route::get('ads/edit', 'AdsController@edit')->name('admin.ads.edit')->middleware('permission:adv.ads.edit');
        Route::post('ads/update', 'AdsController@update')->name('admin.ads.update')->middleware('permission:adv.ads.edit');
        //删除
        Route::delete('ads/destroy', 'AdsController@destroy')->name('admin.ads.destroy')->middleware('permission:adv.ads.destroy');
        //添加广告位
        Route::get('ads/position.create', 'AdsController@createPosition')->name('admin.ads.position.create')->middleware('permission:adv.ads.create');
        Route::get('ads/position.edit', 'AdsController@editPosition')->name('admin.ads.position.edit')->middleware('permission:adv.ads.create');
    });




 
});
//会员管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:member.manage']], function () {
    //账号管理
    Route::group(['middleware' => 'permission:member.member'], function () {
        Route::get('member/data', 'MemberController@data')->name('admin.member.data');
        Route::get('member', 'MemberController@index')->name('admin.member');
        //添加
        Route::get('member/create', 'MemberController@create')->name('admin.member.create')->middleware('permission:member.member.create');
        Route::post('member/store', 'MemberController@store')->name('admin.member.store')->middleware('permission:member.member.create');
        //编辑
        Route::get('member/{id}/edit', 'MemberController@edit')->name('admin.member.edit')->middleware('permission:member.member.edit');
        Route::post('member/update', 'MemberController@update')->name('admin.member.update')->middleware('permission:member.member.edit');
        Route::Any('member/updateAjax', 'MemberController@updateAjax')->name('admin.member.updateajax')->middleware('permission:member.member.edit');
        //删除
        Route::delete('member/destroy', 'MemberController@destroy')->name('admin.member.destroy')->middleware('permission:member.member.destroy');
    });
});
//清缓存
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:member.manage']], function () {
    Route::get('/clear/clearall', 'CommonController@clearAll')->name('admin.common.all');
});
/*
//消息管理
Route::group(['namespace' => 'App\Http\Controllers\Admin', 'prefix' =>$prefix, 'middleware' => ['auth','menu', 'permission:message.manage']], function () {
    //消息管理
    Route::group(['middleware' => 'permission:message.message'], function () {
        Route::get('message/data', 'MessageController@data')->name('admin.message.data');
        Route::get('message/getUser', 'MessageController@getUser')->name('admin.message.getUser');
        Route::get('message', 'MessageController@index')->name('admin.message');
        //添加
        Route::get('message/create', 'MessageController@create')->name('admin.message.create')->middleware('permission:message.message.create');
        Route::post('message/store', 'MessageController@store')->name('admin.message.store')->middleware('permission:message.message.create');
        //删除
        Route::delete('message/destroy', 'MessageController@destroy')->name('admin.message.destroy')->middleware('permission:message.message.destroy');
        //我的消息
        Route::get('mine/message', 'MessageController@mine')->name('admin.message.mine')->middleware('permission:message.message.mine');
        Route::post('message/{id}/read', 'MessageController@read')->name('admin.message.read')->middleware('permission:message.message.mine');
        Route::get('message/count', 'MessageController@getMessageCount')->name('admin.message.get_count');
    });
});

*/