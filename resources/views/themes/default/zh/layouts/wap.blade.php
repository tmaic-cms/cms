<!DOCTYPE html>
<html class="" lang="en">
<head>
<meta name="csrf-token" content="{{ csrf_token() }}">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0" />
<meta content="yes" name="apple-mobile-web-app-capable" />
<meta content="black" name="apple-mobile-web-app-status-bar-style" />
<meta content="telephone=no" name="format-detection" />
<meta property="og:image" content="http://www.ep-sf.com/images/Index/logo.png" />
<title>@yield('title')-@webname</title>
<meta name="keywords" content="@yield('keyword')">
<meta name="description" content="@yield('description')">
<link href="/css/h5/common.css" rel="stylesheet" type="text/css">
<link href="/css/h5/style.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="/css/h5/index.css" />
<script type="text/javascript" charset="utf-8" src="/js/h5/jquery-3.3.1.min.js"></script>
<script src="/js/h5/lodash-4.17.4.min.js" type="text/javascript"></script>
<script src="/js/h5/stlclient.js" type="text/javascript"></script>
<script src="/js/h5/common.js" charset="utf-8" type="text/javascript"></script>
<style>
.empty{
	height:200px;
	width:100%!important;
	line-height:200px;
	text-align: center;
}
</style> 
@yield('css')
@yield('topjs')
</head>
<body class="@yield('body')" id="@yield('bodyid')">

 
  <div class="nav_menu">
    <div class="top clearfix">
      <a href="{{route('home')}}" class="Navlog">
        <img src="/picture/36015c4fe663a34c.png" title="@webname" alt="@webname" />
      </a>
      <span class="btn"><img src="/picture/nav_close.png" title="@webname" alt="@webname" /></span>
    </div>
    <ul id="navBox">
      <li>
        <p><a href="{{route('home')}}">首页</a></p>
      </li>
      @nav($item,$second group=0)
      <li mark='5' px='2'>
        <p>
          <a href="{{$item->link}}">{{$item->name}}</a>
        </p>
      </li>
	  @endnav 
    </ul>
  </div>




@yield('main')



@include('theme::layouts.footer')
@yield('footerjs')
<script type="text/javascript" charset="utf-8" src="/js/h5/inpage.js"></script>
@foreach(webconfigGroup(15) as $item)
@if($item->val)
{!! $item->val !!}
@endif
@endforeach
</body>
</html>