<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;

//use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;
use App\Models\Configuration;
use App\Models\LoginLog;
class LoginController extends AdminController
{
   // use AuthenticatesUsers;

    /**
     * 登录表单
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('admin.login_register.login');
    }

    /**
     * 用于登录的字段
     * @return string
     */
    public function username()
    {
        return 'username';
    }

    /**
     * 登录成功后的跳转地址
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function redirectTo()
    {
        return route('admin.layout');
    }

    /**
     * 处理登录认证
     *
     * @return \Illuminate\Http\JsonResponse|Response
     * @translator laravelacademy.org
     */
    public function authenticate(Request $request)
    {
        $username = $request->get('username');
        $password = $request->get('password');

        if (Auth::attempt(['username' => $username, 'password' => $password])) {
            $user = Auth::user();


                LoginLog::create([
                    'username' => $user->username,
                    'ip' => $request->ip(),
                    'method' => $request->method(),
                    'user_agent' => $request->header('User-Agent'),
                    'remark' => '登录成功',
                ]);

            return Response::json(['code' => 0,'msg' => '登陆成功','data' => '登陆成功','url'=>route('admin.layout')]);

        }



            LoginLog::create([
                'username' => $username,
                'ip' => $request->ip(),
                'method' => $request->method(),
                'user_agent' => $request->header('User-Agent'),
                'remark' => '登陆失败',
            ]);

        return Response::json(['code' => 1,'msg' => '登陆失败','data' => '登陆失败','url'=>route('admin.login')]);

    }






    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
		
		 
        $this->guard()->logout();

        $request->session()->invalidate();

        return redirect(route('login'));
    }

    /**
     * Get the guard to be used during authentication.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard();
    }

}
