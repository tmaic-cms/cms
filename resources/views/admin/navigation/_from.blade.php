{{csrf_field()}}

<div class="layui-form-item">
    <label for="" class="layui-form-label">父级</label>
    <div class="layui-input-inline">
        <select name="parent_id">
            <option value="0">顶级名称</option>
            @forelse($Navigations as $p1)
                <option value="{{$p1->id}}" {{ isset($Navigation->id) && $p1->id == $Navigation->parent_id ? 'selected' : '' }} >{{$p1->display_name}}</option>
                @if($p1->childs->isNotEmpty())
                    @foreach($p1->childs as $p2)
                        <option value="{{$p2->id}}" {{ isset($Navigation->id) && $p2->id == $Navigation->parent_id ? 'selected' : '' }} >&nbsp;&nbsp;&nbsp;┗━━{{$p2->display_name}}</option>
                        @if($p2->childs->isNotEmpty())
                            @foreach($p2->childs as $p3)
                                <option value="{{$p3->id}}" {{ isset($Navigation->id) && $p3->id == $Navigation->parent_id ? 'selected' : '' }} >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;┗━━{{$p3->display_name}}</option>
                            @endforeach
                        @endif
                    @endforeach
                @endif
            @empty
            @endforelse
        </select>
    </div>
</div>



<div class="layui-form-item">
    <label for="" class="layui-form-label">显示名称</label>
    <div class="layui-input-inline">
        <input type="text" name="display_name" value="{{$Navigation->display_name??old('display_name')}}" lay-verify="required" class="layui-input" placeholder="如：系统管理">
    </div>
</div>
{{--
<div class="layui-form-item">
    <label for="" class="layui-form-label">显示英文名</label>
    <div class="layui-input-inline">
        <input type="text" name="display_name_en" value="{{$Navigation->display_name_en??old('display_name_en')}}"  class="layui-input" placeholder="如：INDEX">
    </div>
</div>

--}}
 
  <div class="layui-form-item">
    <label class="layui-form-label">位置</label>
    <div class="layui-input-block">
	
	@for($i=1;$i<6;$i++)
      <input type="radio" name="position" value="{{$i}}" title="@if($i==1) 顶部 @elseif($i==2) 主导航 @elseif($i==3) 左侧导航 @elseif($i==4) 右侧导航 @else 底部 @endif" 
     @if(isset($Navigation->position) && $i==$Navigation->position ) checked @endif  >      
	 @endfor 

    </div>
  </div>
 <div class="layui-form-item">
    <label for="" class="layui-form-label">seo标题</label>
    <div class="layui-input-block">
        <input type="text" name="seo_title" value="{{ $Navigation->seo_title ?? old('seo_title') }}" placeholder="请输入seo标题" class="layui-input" >
    </div>
</div>

<div class="layui-form-item">
    <label for="" class="layui-form-label">seo关键字</label>
    <div class="layui-input-block">
        <input type="text" name="seo_key" value="{{ $Navigation->seo_key ?? old('seo_key') }}" placeholder="请输入分类关键字" class="layui-input" >
    </div>
</div>



<div class="layui-form-item">
    <label for="" class="layui-form-label">seo描述</label>
    <div class="layui-input-block">
        <input type="text" name="description" value="{{ $Navigation->description ?? old('description') }}" placeholder="请输入seo描述" class="layui-input" >
    </div>
</div>

{{--
<div class="layui-form-item">
    <label for="" class="layui-form-label">路由</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="text" name="route" value="{{$Navigation->route??old('route')}}" placeholder="如：admin.member" >
    </div>
</div>
--}}

<div class="layui-form-item">
    <label for="" class="layui-form-label">URL地址</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="text" name="nav_url" value="{{$Navigation->nav_url??old('nav_url')}}" placeholder="如：https:/www.aa.com" >
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">参数</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="text" name="param" value="{{$Navigation->param??old('param')}}" placeholder="参数，如：id=1&cid=2" >
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">spm</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="text" name="nav_url" value="{{$Navigation->spm??old('spm')}}" disabled placeholder="系统生成，无需设置" >
    </div>
</div>
{{--

<div class="layui-form-item">
    <label for="" class="layui-form-label">类型</label>
    <div class="layui-input-inline">
        <input type="radio" name="type" value="1" title="按钮" @if(isset($Navigation) && $Navigation->type==1) checked @endif >
        <input type="radio" name="type" value="2" title="菜单" @if(!isset($Navigation) || (isset($Navigation) && $Navigation->type==2)) checked @endif>
    </div>
</div>
--}}




<div class="layui-form-item">
    <label for="" class="layui-form-label">图标</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="hidden" name="icon" value="{{$Navigation->icon??''}}" >
    </div>
    <div class="layui-form-mid layui-word-aux" id="icon_box">
        <i class="layui-icon {{$Navigation->icon??''}}"></i>
    </div>
    <div class="layui-form-mid layui-word-aux">
        <button type="button" class="layui-btn layui-btn-xs" onclick="showIconsBox()">选择图标</button>
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">排序</label>
    <div class="layui-input-inline">
        <input class="layui-input" type="number" name="sort" value="{{$Navigation->sort??0}}" placeholder="" >
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">状态</label>
    <div class="layui-input-inline">
        <input type="radio" name="status" value="1" title="启用" @if(isset($Navigation) && $Navigation->status==1) checked @endif >
        <input type="radio" name="status" value="0" title="禁用" @if(!isset($Navigation) || (isset($Navigation) && $Navigation->status==0)) checked @endif>
    </div>
</div>


<div class="layui-form-item">
    <div class="layui-input-block">
        <button type="submit" class="layui-btn" lay-submit="" >确 认</button>
        <a href="{{route('admin.nav')}}" class="layui-btn"  >返 回</a>
    </div>
</div>

