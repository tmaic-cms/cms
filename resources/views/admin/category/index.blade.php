@extends('admin.base')
@section('content')
    <style>
        .layui-card {
            margin-bottom: 0px;
            border-radius: 2px;
            line-height: 20px;
        }

        .layui-tab-content {
            padding: 0px;
        }

        .layui-card-body {
            padding: 5px 0px;
        }

        .layui-form.toolbar .layui-form-item .layui-inline {
            margin-bottom: 1px;
        }

        .layui-fluid {
            padding: 10px 1px 0 1px;
        }
    </style>
    <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form toolbar">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label left">关键字:</label>
                        <div class="layui-input-inline">
                            <input name="catName" class="layui-input" id="catName" placeholder="输入关键字"/>
                        </div>
                    </div>
                    <div class="layui-inline">&emsp;
                        <button class="layui-btn icon-btn" type="button" lay-filter="btnSearch" id="btnSearch"
                                lay-submit>
                            <i class="layui-icon">&#xe615;</i>搜索
                        </button>

                        <button id="btnClearSearch" class="layui-btn layui-btn-sm layui-btn-primary" type="button">
                            <i class="layui-icon">&#x1006;</i>清除搜索
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="layui-card-body">
        <div class="layui-tab layui-tab-brief" lay-filter="tablist">
            <ul class="layui-tab-title">
                @foreach(config('translatable.locales') as $locale=>$item)
                    @if($loop->first)
                        <li class="layui-this" lay-id="{{$locale}}" lay-data="{{$locale}}">{{$item}}</li>
                    @else
                        <li lay-id="{{$locale}}" lay-data="{{$locale}}">{{$item}}</li>
                    @endif
                @endforeach
            </ul>
            <div class="layui-tab-content">
                <div class="layui-tab-item layui-show">
                    <table id="categoryTree" lay-filter="categoryTree"></table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection
@section('script')
    @can('zixun.category')
        @include('admin.category.js')
    @endcan
@endsection