<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Home;

use App\Http\Controllers\BaseController;
use Illuminate\Http\Request;
use App\Models\Column;
use App\Models\ColumnTranslation;
class AboutController extends BaseController
{
    public function __construct()
    {
       parent::__construct();
 
    }


    public function index(Request $request)
    {
 
       $about="";

        return $this->view(compact('about'));
    }

    public function lists(Request $request)
    {
    

       return $this->view(compact('request'));
    }

    public function show(Request $request)
    {

       return $this->view(compact('request'));
    }












}
