<?php 
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-04
*/    
namespace App\Models\Defines;

use App\Models\BaseModel;
 


class DefinesTranslation extends BaseModel
{
 
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'defines_translations';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = true;
    
    
    protected $fillable = ['defines_id','group_id','locale','name','keys','value','tips','sort','slug','body','status'];
    
    protected $appends = ['langType'];
 
    
    //0:禁用：1启用
    public function scopeStatus($query)
    {
        return $query->where('status', 1);
    }
    //过虑语言 前台使用
    public function scopeLanguage($query)
    {
        return $query->where('locale', language());
    }
	
	
	
    //过虑语言
    public function scopeLang($query)
    {
        return $query->where('locale', lang());
    }
	
	
	
	
	
	
   
    //过虑

    public function scopeCondition($query)
    {
        return $query->lang()->status();
    }
	
	
	
	
 
}
        
 