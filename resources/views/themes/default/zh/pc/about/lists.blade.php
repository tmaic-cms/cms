@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')


@endsection
@section('topjs')


@endsection
@section('body', 'aboutus')
@section('bodyid', 'top')
@section('main') 
  
  
  
<div class="box1">
@adv($aboutadv group=31 rows=1)
    <div class="as-bgPic" style="background:url({{ $aboutadv->thumb }});"></div>
@endadv    
  </div>
 @channel($about typeid=83 rows=1)
  <div class="box2">
    <div class="title">
      <h2>{{$about->name}}</h2>
      <p>{{$about->description}}</p>
    </div>
    <div class="lrbox">
      <div class="leftbox">
        <div class="img">
          <img class="transition" src="{{ $about->thumb }}" title="{{ $about->name }}" alt="{{ $about->seo_title }}">
        </div>
      </div>
      <div class="rightbox">
	    {!! $about->body !!}
        <a class="transition" href="{{$about->link}}">更多>></a>
      </div>
    </div>
  </div>
 @endchannel 
  
  <div class="box3">
    <div class="title">
	  @adv($adv typeid=14)
      <h2>{{$adv->title}}</h2>
      <p>{{$adv->description}}</p>
	  @endadv
    </div>
    <div>
      <span>合作企业</span>
      <div class="logobox">
	    @adv($adv group=14)
        <div class="item-box">
          <div class="pic">
            <img src="{{$adv->thumb}}" title="{{$adv->title}}" alt="{{$adv->title}}">
          </div>
        </div>
		@endadv
		{{--
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo2.jpg" title="2万科集团" alt="2万科集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo3.jpg" title="3保利置业" alt="3保利置业">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo4.jpg" title="4招商蛇口" alt="4招商蛇口">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo5.jpg" title="4恒大集团" alt="4恒大集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo6.jpg" title="5中海地产" alt="5中海地产">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo7.jpg" title="6龙湖地产" alt="6龙湖地产">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo8.jpg" title="7金地集团" alt="7金地集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo9.jpg" title="8绿地集团" alt="8绿地集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo10.jpg" title="9卓越集团" alt="9卓越集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo11.jpg" title="10深圳地铁" alt="10深圳地铁">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo12.jpg" title="11星河集团" alt="11星河集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo13.jpg" title="12绿城集团" alt="12绿城集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo14.jpg" title="13世茂集团" alt="13世茂集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo15.jpg" title="14中信泰富" alt="14中信泰富">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo16.jpg" title="15新城控股" alt="15新城控股">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo17.jpg" title="16正荣集团" alt="16正荣集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo18.jpg" title="17复地集团" alt="17复地集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo20.jpg" title="19中国金茂" alt="19中国金茂">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo21.jpg" title="20奥园地产" alt="20奥园地产">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo22.jpg" title="21鲁能集团" alt="21鲁能集团">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo23.jpg" title="22碧桂园" alt="22碧桂园">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo24.jpg" title="23龙光地产" alt="23龙光地产">
          </div>
        </div>
        <div class="item-box">
          <div class="pic">
            <img src="picture/logo25.jpg" title="24鸿荣源" alt="24鸿荣源">
          </div>
        </div>
		--}}
      </div>
    </div>
  </div>
  
  
  
  
  
@endsection
@section('footerjs') 


@endsection













