<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = ['name','sort'];

    //与文章多对多关联
    public function articles()
    {
        return $this->belongsToMany('App\Models\Article','article_tag','tag_id','article_id');
    }
	
    //与案例多对多关联
    public function cases()
    {
        return $this->belongsToMany('App\Models\Casees','case_tag','tag_id','case_id');
    }
    //与产品多对多关联
    public function goods()
    {
        return $this->belongsToMany('App\Models\Goods','goods_tag','tag_id','goods_id');
    }
	

}
