layui.use(['form', 'admin', 'laytpl', 'table', 'util', 'laydate', 'element', 'layer', 'layedit', 'upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var treeTable = layui.treeTable;
    var table = layui.table;
    var util = layui.util;
    var admin = layui.admin;
    var laytpl = layui.laytpl;
    var laydate = layui.laydate;
    var element = layui.element;
    var layer = layui.layer;
    var layedit = layui.layedit;
    var upload = layui.upload;

    var langUrl = location.hash.replace(/^#tablist=/);
    element.tabChange('tablist', langUrl);
    element.on('tab(tablist)', function (elem) {
        location.hash = 'tablist=' + $(this).attr('lay-id');
        lang = $(this).attr('lay-data');
        insTb.reload({where: {'lang': lang}, page: {curr: 1}});
    });
    // 渲染表格
    var insTb = table.render({
        elem: '#tableData',
        skin: 'line',
        size: 'lg',
        url: tableData,
        page: true,
		where:{type:1},
        toolbar: ['<p>',
            '<button lay-event="position" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>图形广告位</button>&nbsp;',
            '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>图形广告</button>&nbsp;',
            '<button lay-event="delete" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
            '</p>'].join(''),
        cellMinWidth: 100,
        cols: [[
            {type: 'numbers', title: '序号'},
            {type: 'checkbox'},
            {field: "id", title: 'ID', width: 80,hide:true},
            {field: "sid", title: '广告ID', width: 80,hide:true},
            {field: "pid", title: '广告组ID', width: 100,hide:false},


           // {field: "slug", title: 'tag', width: 120},
			
			 
			//{field: 'slug', title: 'tag', toolbar: '#slug', width: 120},
 
           // {field: "classname", title: '广告位置名称', width: 250},
            {templet: '<div>{{d.advname.title}}</div>', title: '广告位', width: 150},
            {field: "title", title: '标题',sort: true},
            {field: "remarks", title: '备注'},
            {field: 'thumb', title: '主图', toolbar: '#thumb', width: 80},
            {field: 'wapthumb', title: '移动端图', toolbar: '#wapthumb', width: 80},
            {field: 'link', title: '链接地址', toolbar: '#link', width: 100},
            //{field: "keywords", title: 'seo关键字', width: 150},
            //{field: "description", title: '描述', width: 250},
            {field: 'locale', title: '语言', width: 80},
            //{align: 'center', toolbar: '#eDialogTbBar', title: '详情', width: 120},
            //{field: 'sort', title: '排序', width: 80},
            //{field: 'click', title: '浏览量', width: 80},
            {field: 'status', title: '状态', templet: '#tbState', sort: true, width: 120},
            //{align: 'left', toolbar: '#eDialogTbBar',width: 250, title: '操作'},
            //{field: 'updated_at', title: '更新时间', width: 180},
            {align: 'left', toolbar: '#options', title: '操作',width:120, fixed: "right"}
        ]]
    });
    laydate.render({
        elem: '#created_at'
        , type: 'datetime'
        , range: true
    });
    // 列点击事件
    table.on('tool(tableData)', function (obj) {
        var data = obj.data;
        var event = obj.event;
        if (obj.event === 'edit') {
            admin.open({
                type: 2,
                title: '编辑图形广告',
                content: '/admin/ad/edit?id=' + obj.data.id + "&locale=" + obj.data.locale +"&slug=" +obj.data.slug,
                area: ['100%', '100%'],
                offset: '0px',
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    insTb.reload();
                    return;
                }
            });
        }else if (obj.event === 'positionedit') {
           
             var url ='/admin/ad/position.edit?id=' + obj.data.id + "&locale=" + obj.data.locale +"&slug=" +obj.data.slug;
            admin.open({
                type: 2,
                title: '编辑广告位',
                content: url,
                area: ['600px', '400px'],
                offset: '30px',
                yes: function (index, layero) {
                },
                end: function (index, layero) {
                    insTb.reload();
                    return;
                }
            });


        }else if (obj.event === 'del') {
            doDel({ids: obj.data.id});
        } else if (obj.event === 'checkList') {
            showComments(data.id);
        } else if (obj.event === 'comments') {
            showComments(data.id);
        }
    });
    // 列点击事件 end
 // 查看评论
        function showComments(id) {
            admin.open({
                type: 1,
                area: '850px',
                offset: '65px',
                title: '案例图集',
                content: $('#eDialogCommentDialog').html(),
                success: function (layero) {
                    // 渲染表格
                    var insTbCom = table.render({
                        elem: '#eDialogCommentTable',
                        url: AtlasData +'?id=' + id,
                        page: true,
                        height: 400,
                        cellMinWidth: 100,
                        cols: [[
                            {type: 'numbers', title: '#'},
                            {field: 'path', title: '缩略图', toolbar: '#paththumb', width: 80},
                            {field: 'name', sort: true, title: '图片名称', width: 300},
                            //{field: 'path', sort: true, title: 'url地址'},
                            {
                                title: '上传时间', sort: true, templet: function (d) {
                                    return util.toDateString(d.creted_at);
                                }
                            },
                            {align: 'center', toolbar: '#eDialogCommentTbBar', title: '操作', minWidth: 80, width: 80}
                        ]],
                        done: function () {
                            $(layero).find('.layui-table-view').css('margin', '0');
                        }
                    });

                    // 查看评论工具条点击事件
                    table.on('tool(eDialogCommentTable)', function (obj) {
                        var data = obj.data;
                         console.log(data);
                        var layEvent = obj.event;
                        if (layEvent === 'del') { // 删除
                            layer.confirm('确定要删除选中数据吗？', {
                                skin: 'layui-layer-admin',
                                shade: .1
                            }, function (i) {
                                layer.close(i);
                                var loadIndex = layer.load(2);
                                $.post(AtlasDestroy, {
                                    _method: 'delete',
                                    ids: data.id
                                }, function (res) {
                                    layer.close(loadIndex);
                                    if (res.code == 0) {
                                        layer.msg(res.msg, {icon: 1});
                                        insTbCom.reload({page: {curr: 1}});
                                    } else {
                                        layer.msg(res.msg, {icon: 2});
                                    }
                                }, 'json');
                            });
                        }
                    });

               
                    $('#eDialogCommentBtnAdd').click(function () {
                        admin.open({
                            type: 2,
                            title: '上传案例图集', fixed: true,
                            content: AtlasUpload +"?id=" +id,
                            area: ['650px','400px'],
                            fixed: true,
                            data: {id:id},
                            end: function () {
                                layer.close(index);
                                insTbCom.reload();
                                return false;
                            },
                            cancel: function (index, layero) {
                                layer.close(index);
                                insTbCom.reload();
                                return false;
                            }
                        });


                    });

                }
            });
        };

  
    /* 表头工具栏点击事件 */
    table.on('toolbar(tableData)', function (obj) {
        if (obj.event === 'add') { // 添加
            admin.open({
                type: 2,
                title: '添加广告', fixed: true,
                content: create,
                area: ['100%', '100%'],
                offset: '0px',
                data: "",
                end: function () {
                    insTb.reload();
					layer.close(index);
                    return false;
                },
                cancel: function (index, layero) {
                    insTb.reload();
                    layer.close(index);
                    return false;
                }
            });
        }else if (obj.event === 'position'){

            admin.open({
                type: 2,
                title: '添加广告位', fixed: true,
                content: createposition,
                area: ['100%', '100%'],
                offset: '0px',
                data: "",
                end: function () {
                    insTb.reload();
                    layer.close(index);
                    return false;
                },
                cancel: function (index, layero) {
                    insTb.reload();
                    layer.close(index);
                    return false;
                }
            });
        }else if (obj.event === 'delete') {
            var checkRows = table.checkStatus('tableData');
            if (checkRows.data.length === 0) {
                layer.msg('请选择要删除的数据', {icon: 2});
                return;
            }
            var ids = checkRows.data.map(function (d) {
                return d.id;
            });
            doDel({ids: ids});
        }
    });
    /* 删除 */
    function doDel(obj) {
        layer.confirm('确定要删除选中数据吗？', {
            skin: 'layui-layer-admin',
            shade: .1
        }, function (i) {
            layer.close(i);
            var loadIndex = layer.load(2);
            $.post(destroy, {
                _method: 'delete',
                ids: obj.ids
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            }, 'json');
        });
    }
    /* 搜索 */
    form.on('submit(btnSearch)', function (data) {
        insTb.reload({where: data.field, page: {curr: 1}});
        return false;
    });

    form.on('submit(btnClearSearch)', function (data) {
        $("#Search_from")[0].reset();
        form.render();
        insTb.reload({where: '', page: {curr: 1}});
        return false;
    });
    /* 修改状态 */
    form.on('switch(tbState)', function (obj) {  
      
         var loadIndex = layer.load(2);
        $.post(updateAjax, {id: obj.elem.value, status: obj.elem.checked ? 0 : 1}, function (res) {
            layer.close(loadIndex);
            if (res.code == 0) {
                layer.msg(res.msg, {icon: 1});
            } else {
                layer.msg(res.msg, {icon: 2});
                $(obj.elem).prop('checked', !obj.elem.checked);
                form.render('checkbox');
            }
        }, 'json');

     });

});