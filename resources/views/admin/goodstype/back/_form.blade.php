{{csrf_field()}}
<div class="layui-form-item">
    <label for="" class="layui-form-label">上级分类</label>
    <div class="layui-input-inline">
        <select name="parent_id" lay-search  lay-filter="parent_id">
            <option value="0">一级分类</option>
            @foreach($goodstype as $first)
                <option value="{{ $first->id }}" @if(isset($goodsTypeId->parent_id) && $goodsTypeId->parent_id==$first->id) selected @endif>{{ $first->name }}</option>
                @if($first->childs->isNotEmpty())
                    @foreach($first->childs as $second)
                        <option value="{{ $second->id }}" {{ isset($goodsTypeId->id) && $goodsTypeId->parent_id==$second->id ? 'selected' : '' }}>┗━━{{$second->name}}</option>
                    @endforeach
                @endif
            @endforeach
        </select>
    </div>
</div>
<div class="layui-form-item">
    <label for="" class="layui-form-label">名称</label>
    <div class="layui-input-inline">
        <input type="text" name="name" value="{{ $goodsTypeId->name ?? old('name') }}" lay-verify="required" placeholder="请输入名称" class="layui-input" >
    </div>
</div>
{{--
<div class="layui-form-item">
    <label for="" class="layui-form-label">英文名称</label>
    <div class="layui-input-inline">
        <input type="text" name="en_name" value="{{ $goodsTypeId->en_name ?? old('en_name') }}"  placeholder="请输入英文名称" class="layui-input" >
    </div>
</div>
--}}
<div class="layui-form-item">
    <label for="" class="layui-form-label">seo标题</label>
    <div class="layui-input-block">
        <input type="text" name="seo_title" value="{{ $goodsTypeId->seo_title ?? old('seo_title') }}" placeholder="请输入seo标题" class="layui-input" >
    </div>
</div>

<div class="layui-form-item">
    <label for="" class="layui-form-label">seo关键字</label>
    <div class="layui-input-block">
        <input type="text" name="seo_key" value="{{ $goodsTypeId->seo_key ?? old('seo_key') }}" placeholder="请输入分类关键字" class="layui-input" >
    </div>
</div>

<div class="layui-form-item">
    <label for="" class="layui-form-label">seo描述</label>
    <div class="layui-input-block">
        <input type="text" name="description" value="{{ $goodsTypeId->description ?? old('description') }}" placeholder="请输入描述" class="layui-input" >
    </div>
</div>

<div class="layui-form-item">
    <label for="" class="layui-form-label">排序</label>
    <div class="layui-input-inline">
        <input type="number" name="sort" value="{{ $goodsTypeId->sort ?? 10 }}" lay-verify="required|number" placeholder="请输入数字" class="layui-input" >
    </div>
</div>

<div class="layui-form-item">
    <div class="layui-input-block">
        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
        <a  class="layui-btn" href="{{route('admin.goodstype')}}" >返 回</a>
    </div>
</div>
