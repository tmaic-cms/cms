<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
//语言切换
Route::get('/lang/{language}', ['as' => 'lang','uses' => 'App\Http\Controllers\LanguageController@switchLang']);
Route::get('/post', ['as' => 'post','uses' => 'App\Http\Controllers\PostController@index']);

Route::group(['namespace' => 'App\Http\Controllers\Home',  'middleware' => 'nav'], function () {
    Route::get('/', 'HomeController@index')->name('home')->middleware('cache.response');
    Route::get('/news/{slug}.html', 'ArticleController@show')->name('article.info');
    Route::get('/case/{slug}.html', 'CasesController@show')->name('case.info');
    Route::get('/product/{slug}.html', 'ProductController@show')->name('product.info'); 
	Route::post('/subscribe.phone.html', 'SubscribeController@sendPhone')->name('subscribe');
	Route::get('/tag.html', 'TagController@index')->name('tag');
 	//专题
    Route::get('/services.html', 'AboutController@services')->name('services')->middleware('cache.response');
    Route::get('/quality.html', 'AboutController@quality')->name('quality')->middleware('cache.response');	
});

























