<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;

use App\Http\Requests\MemberCreateRequest;
use App\Http\Requests\MemberUpdateRequest;
use App\Models\Member;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;

class MemberController extends AdminController
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return view('admin.member.index');
    }
    public function data(Request $request)
    {
 
        $model = Member::query();
        if ($request->get('username')){
            $model = $model->where('username','like','%'.$request->get('username').'%')->orWhere('nickname','like','%'.$request->get('username').'%');
        }
        if ($request->get('phone')){
            $model = $model->where('moblie','like','%'.$request->get('phone').'%');
        }
        $res = $model->orderBy('created_at','desc')->paginate($request->get('limit',30))->toArray();
        $data = [
            'code' => 0,
            'msg'   => '正在请求中...',
            'count' => $res['total'],
            'data'  => $res['data']
        ];
        return response()->json($data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.member.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MemberCreateRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
 
        if (Member::create($data)){
			return response()->json(['code'=>0,'msg'=>'添加账号成功']);
             
        }
		return response()->json(['code'=>1,'msg'=>'系统错误']);
        
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $member = Member::findOrFail($id);
        return view('admin.member.edit',compact('member'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(MemberUpdateRequest $request)
    {
		$id = (int)$request->id;
        $member = Member::where('id',$id)->firstOrFail();

		$data = $request->except(['_token', 'file','password','password_confirmation']);
		
		
        if ($request->get('password')){
            $data['password'] = bcrypt($request->get('password'));
        }
 
        if ($member->update($data)){
            
			return response()->json(['code'=>0,'msg'=>'更新用户成功']);
        }
        return response()->json(['code'=>1,'msg'=>'更新用户失败']);
    }

    /**
     * Update ajax.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateAjax(Request $request)
    {
        $id = $request->get('id');
        $status = $request->status;
        $Member = Member::find($id);
 
        if ($Member->update(['status'=>$status])) {
            return response()->json(['code' => 0, 'msg' => '状态更新成功']);
        }

        return response()->json(['code' => 1, 'msg' => '状态更新失败']);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = (array)$request->get('ids');
        if (empty($ids)){
            return response()->json(['code'=>1,'msg'=>'请选择删除项']);
        }
        if (Member::destroy($ids)){
            return response()->json(['code'=>0,'msg'=>'删除成功']);
        }
        return response()->json(['code'=>1,'msg'=>'删除失败']);
    }
}
