<!-- 表格状态列 -->
<script type="text/html" id="TbState">
    <input type="checkbox" lay-filter="TbStateCk" value="@{{d.id}}" lay-skin="switch"
           lay-text="启用|禁用" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'启用':'禁用'}}</p>
</script>
<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('webconfig.defines.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('webconfig.defines.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>


<!-- 表单弹窗 -->
<script type="text/html" id="EditDialog">
    <form id="EditForm" lay-filter="EditForm" class="layui-form model-form">
		<input name="defines_mid" type="hidden" value=""/>
        <input name="id" type="hidden" value=""/>
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">配置名称:</label>
            <div class="layui-input-block">
                <select name="group_id" id="group_id" class="layui-input" lay-verType="tips">
                    <option value="">请选择配置组</option>
                    @foreach($Group as $groupname)
                        <option value="{{ $groupname->id }}">{{ $groupname->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
		
		
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">配置名称:</label>
            <div class="layui-input-block">
                <input name="name" placeholder="请输入配置名称" class="layui-input" lay-verType="tips" lay-verify="required"  value=""/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">键key:</label>
            <div class="layui-input-block">
                <input name="keys" placeholder="请输入键key" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">配置值:</label>
            <div class="layui-input-block">
				<textarea name="value" lay-verify="required" placeholder="请输入配置值" lay-verType="tips" class="layui-textarea"></textarea>
            </div>
        </div>
        <div class="layui-form-item text-right">
            <button class="layui-btn" lay-filter="EditSubmit" lay-submit>保存</button>
            <button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
        </div>
    </form>
</script>

<script>
var definesupdate='{{ route("admin.defines.update") }}';
var definesstore ='{{ route("admin.defines.store") }}';
var definesData ='{{ route("admin.defines.data") }}';
var definesdestroy = '{{ route("admin.defines.destroy") }}';
var definesupdateajax = "{{ route('admin.defines.updateajax') }}";
</script>

<script type="text/javascript" src="/assets/js/defines.js"></script>