@extends('admin.base')
@section('content')
<form class="layui-form" id="adForm">
@include('admin.ad.positionform')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','layedit'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    var upload = layui.upload;
    var element = layui.element;
    var layedit = layui.layedit;
    form.on('submit(SubmitData)', function (data) {

 
      var formData=data.field;
 

        $.ajax({
            type: "POST",
            url: '{{ route("admin.ad.update")}}?id='+"{{$id}}",
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {

                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
    });
    $('#closeFormBtn').click(function () {
        $(this).parent().parent().parent().remove();
    });
});
</script>
@endsection