<style>
body{ background:#fff;}
.layui-fluid {
    padding: 0px;
}
.layui-card-body {
    position: relative;
    padding: 0;
    line-height: 24px;
}
.centered {
    margin: 0 auto;
    padding: 0 var(--ck-sample-base-spacing);
}
main .document-editor {
    border: 0px solid  #DFE4E6;

    border-right-color: rgb(223, 228, 230);
    border-bottom-color: rgb(223, 228, 230);

    border-bottom-color:#cdd0d2;
    
    border-right-color:#cdd0d2;
 
    border-radius: 2px;
    
    max-height: 680px;
    
    display: flex;
    
    flex-flow: column nowrap;
    
    box-shadow: 2px 2px 2px rgba(0,0,0,.1);

}
.ck.ck-toolbar {

    background: var(--ck-color-toolbar-background);
    padding: 0 var(--ck-spacing-small);
    border: 0px solid var(--ck-color-toolbar-border);

}
main .toolbar-container {
    z-index: 1;
    position: relative;
    box-shadow: 2px 2px 1px rgba(0,0,0,.05);
}
main .content-container {

    padding: var(--ck-sample-base-spacing);
    background:  #eee;
    overflow-y: scroll;

}
main .content-container #editor {
    border-top-left-radius: 0;
    border-top-right-radius: 0;
    width: 15.8cm;
    min-height: 600px;
    padding: 2px 1cm 2cm;
    margin: 0 auto;
    box-shadow: 2px 2px 1px rgba(0,0,0,.05);

}
main #editor { background: #FFF;}
</style>
<script>
layui.use(['form','element'], function () {
    var $ = layui.jquery;
    var data;
    var editor= DecoupledEditor
        .create(document.querySelector( '#editor' ), {
            ckfinder: {
              uploadUrl: '{{ route("uploadImg") }}'+"?_token={{ csrf_token() }}"
            },
        })
    .then( editor => {
        const toolbarContainer = document.querySelector( 'main .toolbar-container' );
        toolbarContainer.prepend( editor.ui.view.toolbar.element );
        window.editor = editor;
    })
    .catch( err => {
        console.error( err.stack );
    });
});
</script>