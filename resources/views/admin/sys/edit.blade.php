@extends('admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>更新配置</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('admin.configuration.updateOne',['id'=>$configuration->id])}}" method="post">
                {{ method_field('put') }}
                @include('admin.configuration._form')
            </form>
        </div>
    </div>
@endsection

@section('script')
 <script>
            layui.use(['layer', 'table', 'form','upload','element'], function () {
                var $ = layui.jquery;
                var layer = layui.layer;
                var form = layui.form;
                var table = layui.table;
                var upload = layui.upload;

                //图片
                $(".uploadPic").each(function (index,elem) {
                    upload.render({
                        elem: $(elem)
                        ,url: '{{ route("uploadImg") }}'
                        ,multiple: false
                        ,data:{"_token":"{{ csrf_token() }}"}
                        ,done: function(res){
                            //如果上传失败
                            if(res.code == 0){
                                layer.msg(res.msg,{icon:1},function () {
                                    $(elem).parent('.layui-upload').find('.layui-upload-box').html('<li><img src="'+res.url+'" /><p>上传成功</p></li>');
                                    $(elem).parent('.layui-upload').find('.layui-upload-input').val(res.url);
                                })
                            }else {
                                layer.msg(res.msg,{icon:2})
                            }
                        }
                    });
                })

                //提交
                form.on('submit(config_group)',function (data) {
                    var parm = data.field;
                    parm['_method'] = 'put';
                    var load = layer.load();
                    $.post("{{route('admin.configuration.update')}}",data.field,function (res) {
                        layer.close(load);
                        if (res.code==0){
                            layer.msg(res.msg,{icon:1})
                        }else {
                            layer.msg(res.msg,{icon:2});
                        }
                    });
                    return false;
                });
            })
        </script>
@endsection