@extends('admin.form')
@section('content')
<form class="layui-form" method="post">
{{csrf_field()}}
  @include('admin.brand._form')
</form>
@endsection
@section('script')
<script>
  layui.use(['form', 'formX'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    form.on('submit(submitData)', function (data) {
      layui.layer.load(2);
      $.ajax({
        type: "POST",
        url: '{{ route("admin.brand.update",['id'=>$brand->id]) }}',
        data: $(this).parents('form').serialize(),
        dataType: 'json',
        success: function (res) {
          layui.layer.closeAll();
          if (res.code == 0) {
            layer.msg(res.msg, {
              icon: 1
            });
            
          } else {
            layer.msg(res.msg, {
              icon: 2
            });
          }
          
        },
        error: function (msg) {
          layui.layer.closeAll();
          if (msg.status == 422) {
            var json = JSON.parse(msg.responseText);
            json = json.errors;
            for (var item in json) {
              for (var i = 0; i < json[item].length; i++) {
                layer.msg(json[item][i], {
                  icon: 2
                });
                return;
              }
            }
          } else {
            layer.msg('服务器连接失败', { icon: 2  });
            return;
          }
        }
      });
      return false;
    });

  });
</script>
@endsection