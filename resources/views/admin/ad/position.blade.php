@extends('admin.base')
@section('content')
<form class="layui-form" id="adForm">
@include('admin.ad.positionform')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','layedit','admin'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    var upload = layui.upload;
    var element = layui.element;
    var layedit = layui.layedit;
    var admin = layui.admin;
    //普通图片上传
    var uploadInst = upload.render({
        elem: '#uploadPic'
        ,url: '{{ route("uploadImg") }}'
        ,multiple: false
        ,data:{"_token":"{{ csrf_token() }}"}
        ,before: function(obj){
            obj.preview(function(index, file, result){
                $('#layui-upload-box').html('<li><img src="'+result+'" width=50/><p>上传中</p></li>')
            });
        }
        ,done: function(res){
            //如果上传失败
            if(res.code == 0){
                $("#thumb").val(res.url);
                $('#layui-upload-box li p').text('上传成功');
                return layer.msg(res.msg);
            }
            return layer.msg(res.msg);
        }
    });

    var editIndex = layedit.build('description', {
      uploadImage: {url: '{{ route("uploadImg") }}', type: 'post'},
      height:100
    }); 




    form.on('submit(SubmitData)', function (data) {
	  $(this).hide();
      var formData=data.field;
        $.ajax({
            type: "POST",
            url: '{{ route("admin.ad.store")}}',
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {
                $(this).show();
                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
    });
  

});
</script>
@endsection