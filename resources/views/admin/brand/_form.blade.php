
    <div class="layui-fluid">
        <div class="layui-form" style="padding-bottom:  70px;">
            <div class="layui-form-item">
                <label class="layui-form-label">品牌名称</label>
                <div class="layui-input-block">
                    <input type="text" name="name" lay-verify="required"  autocomplete="off" value="{{$brand->name??old('name')}}"
                        placeholder="请输入品牌名称" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">封面</label>
                <div class="layui-input-block">
                    <input type="text" name="thumb" lay-verify="url" value="{{$brand->thumb??old('thumb')}}"
                        autocomplete="off" placeholder="请输入标题" class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label">排列顺序</label>
                    <div class="layui-input-inline" style="width: 100px;">
                        <input type="text" name="sort" placeholder="" value="255" autocomplete="off" value="{{$brand->sort??old('sort')}}"
                            class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">栏目内容</label>
                <div class="layui-input-block">
                    <textarea placeholder="请输入栏目描述内容" name="body" class="layui-textarea" 
                        lay-verify="required">{{$brand->body??old('body')}}</textarea>
                </div>
            </div>

            <div class="text-center"
                style="position: fixed;bottom: 0;left: 0;right: 0;background-color: #fff;box-shadow: 0 -1px 5px rgba(0,0,0,.15);padding: 15px;">
                <button class="layui-btn layui-btn-primary" type="reset">重置</button>
				<button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
                <button class="layui-btn" lay-filter="submitData" id="submitData" lay-submit>保存</button>
            </div>

        </div>
    </div>
