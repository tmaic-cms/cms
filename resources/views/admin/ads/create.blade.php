@extends('admin.base')
@section('content')
<form class="layui-form" id="adForm" lay-filter="adsForm">
@include('admin.ads.form')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','layedit','admin'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    var upload = layui.upload;
    var element = layui.element;
    var layedit = layui.layedit;
    var admin = layui.admin;
 
	
	
/*
    var editIndex = layedit.build('description', {
      uploadImage: {url: '{{ route("uploadImg") }}', type: 'post'},
      height:100,
	  tool: ['left', 'center', 'right']
    }); 
*/
 
    form.on('submit(SubmitData)', function (data) {
      $(this).parent().hide();	
      var  editorData = editor.getData();
      var formData=data.field;
      formData.body=editorData;
      //formData.description=layedit.getContent(editIndex);
        $.ajax({
            type: "POST",
            url: '{{ route("admin.ads.store")}}?type=2',
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.code === 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {
				$(this).show();	
                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
    });
 
 
	 
	  
	 // form.val('adsForm', {"title": "","description": 1 ,"link": ""});
	 
	  
	  //保存继续添加
	  layui.$('#yongshi-component-form-getval').on('click', function(){
		var formData = form.val('adsForm');
        $.ajax({
            type: "POST",
            url: '{{ route("admin.ads.store")}}?type=2',
            data: formData,
            dataType: 'json',
            success: function (res) {
                if (res.code === 0) {
					form.val('adsForm', {"title": "","description":"" ,"link": ""});
					editor.setData('');
					layer.msg(res.msg, {icon: 1, time: 1000});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {
				$(this).show();	
                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
	  });
			
 
 
    $('#closeFormBtn').click(function () {
        $(this).parent().parent().parent().remove();
    });
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
 
});
</script>
@endsection