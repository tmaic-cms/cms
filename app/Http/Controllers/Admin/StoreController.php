<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\Goods\Goods;
use App\Models\Skus\Skus;
use App\Models\Store\Store;

class StoreController extends AdminController
{
    /**
     * 列表
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
        return View::make('admin.store.index');
    }

    /**
     * 数据接口
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {
        $pid = $request->pid;
        $status = $request->status;
        $model = Store::query();

      

        switch ($status) {
 
            case '1':
                $model = $model->where('status',1);
                break;
            case '2':
                $model = $model->where('status',0);
                break;
            case '3':
                $model = $model->where('status',3);
                break;
            case '4':
                $model = $model->where('status',4);
                break;
        }


        if (!empty($created_at)) {
            $startime = substr($created_at, 0, 20);
            $endtime = substr($created_at, -20, 20);
            $model = $model->whereBetween('created_at', [$startime, $endtime]);
        }
        $res = $model->orderBy('created_at','desc')->paginate($request->get('limit', 10));
 
        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res->total(),
            'data' => $res->items(),
        ];
        return Response::json($data);
    }
 
    /**
     * 更新 View
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit(Request $request)
    {
        $id = (int) $request->get('id');
        $goodstype=[];
        $goods = Goods::findOrFail($id);
        return View::make('admin.store.edit', compact('goods', 'goodstype'));
    }

    /**
     * 更新 Db
     * @param TagRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function update(Request $request)
    {
        $id = (int)$request->id;
        $data =$request->all();
 
        DB::beginTransaction();
        try {
            $store = Store::findOrFail($id);
            $store = $store->update($data);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '更新成功']);
        } catch (\Excestatusption $exception) {
            DB::rollback();
        }
        return Response::json(['code' => 401, 'msg' => '更新失败']);
    }


    /**
     * 审核
     * @param TagRequest $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse
     */
    public function examine(Request $request)
    {
        $id = (int)$request->id;
        $status =(int)$request->status;
 
       if(empty($status)) Response::json(['code' => 401, 'msg' => '更新失败']);
       if(empty($id)) Response::json(['code' => 401, 'msg' => '更新失败']);


        DB::beginTransaction();
        try {
            $store = Store::findOrFail($id);
            $store = $store->update(['status'=>$status]);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '更新成功']);
        } catch (\Exception $exception) {
            DB::rollback();
        }
        return Response::json(['code' => 401, 'msg' => '更新失败']);
    }









    /**
     * 下线
     * @param Request $request
     */
    public function destroy(Request $request)
    {

        $id = (array)$request->id;
        $status =(int)$request->status;
       if(empty($status)) Response::json(['code' => 401, 'msg' => '操作失败']);
       if(empty($id)) Response::json(['code' => 401, 'msg' => '操作失败']);


        DB::beginTransaction();
       try {
            
            $store = Store::whereIn('id',$id)->update(['status'=>$status]);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '操作成功']);
        } catch (\Exception $exception) {
            DB::rollback();
          
        }
        return Response::json(['code' => 1, 'msg' => '操作失败']);
    }

    /**
     * SKU数据接口
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function dataSku(Request $request)
    {
        $goodsid = $request->id;
        $model = Skus::query();

    
        if (!empty($created_at)) {
            $startime = substr($created_at, 0, 20);
            $endtime = substr($created_at, -20, 20);
            $model = $model->whereBetween('created_at', [$startime, $endtime]);
        }
        $res = $model->with(['attr','goods'])->where('producible_id',$goodsid)->paginate(10);
 
        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res->total(),
            'data' => $res->items(),
        ];
        return Response::json($data);
    }























}
