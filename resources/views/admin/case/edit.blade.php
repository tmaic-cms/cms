@extends('admin.base')
@section('content')
<style>
.layui-form-label.layui-required:after {content: "*"; color: red; position: absolute; top: 10px;left: 15px; }
body{background:#FFF;height:100%;}
</style>
<form class="layui-form" id="caseForm" lay-filter="caseForm">
@include('admin.case.form')
</form>
@endsection
@section('script')
@include('admin.editor')
<script>
layui.use(['form', 'formX','upload','element','admin'], function () {
	var $ = layui.jquery;
	var form = layui.form;
	var formX = layui.formX;
	var upload = layui.upload;
	var element = layui.element;
	var admin = layui.admin;
	form.val('caseForm', { "pid": "{{$case->column_id}}", "build_style": "{{$case->build_style}}"});
	form.on('submit(SubmitData)', function (data) {
	  $(this).parent().hide();
	  var  editorData = editor.getData();
	  var formData=data.field;
	  formData.body=editorData;
		$.ajax({
			type: "POST",
			url: '{{ route("admin.case.update")}}',
			data: formData,
			dataType: 'json',
			success: function (res) {
				if (res.code == 0) {
					layer.msg(res.msg, {icon: 1, time: 1000}, function () {
						admin.closeDialog();
					});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			},
			error: function (msg) {
				$(this).parent().show();
				if (msg.status == 422) {
					var json = JSON.parse(msg.responseText);
					json = json.errors;
					for (var item in json) {
						for (var i = 0; i < json[item].length; i++) {
							layer.msg(json[item][i], {icon: 2});
							return;
						}
					}
				} else {
					layer.msg('服务器连接失败', {icon: 2});
					return;
				}
			}
		});
		return false;
	});
	
 

});
</script>
@endsection