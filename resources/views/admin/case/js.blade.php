@include('admin.case.html')
<script type="text/javascript" src="/assets/js/case.js"></script>
<script>
    var create = "{{ route('admin.case.create') }}";
    var edit = "{{ route('admin.case.edit') }}?id=";
    var update = "{{ route('admin.case.update') }}";
    var updateAjax = "{{ route('admin.case.updateAjax') }}";
    var destroy ="{{ route('admin.case.destroy') }}";
    var tableData = "{{ route('admin.case.data') }}?pid=";


    var AtlasData = "{{ route('admin.case.atlas.data') }}";
    var AtlasDestroy = "{{ route('admin.case.atlas.destroy') }}";
    var AtlasUpload= "{{ route('admin.case.atlas.upload') }}";
    var AtlasStore= "{{ route('admin.case.atlas.store') }}";


</script>