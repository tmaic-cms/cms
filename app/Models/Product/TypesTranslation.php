<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/     
namespace App\Models\Product;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
 
class TypesTranslation extends Model
{
    
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'goods_type_translations';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = true;
   //protected $fillable = ['types_id','sid','pid','title','description','locale','thumb','body','slug','sort','status','hasdelete'];

    protected static function boot()
    {
        parent::boot();
        // 监听模型创建事件，在写入数据库之前触发
        static::creating(function ($model) {
            // 判断slug字段no是否为空，为空的话调用生成方法
            if (!$model->slug) {
                $model->slug = static::findAvailableNo();
                // 如果生成失败，就返回false
                if (!$model->slug) {
                    return false;
                }
            }
        });
    }
      /**
     * Return the sluggable configuration array for this model.
     *
     * @return array
     */
    public function sluggable()
    {
        return [
            'slug' => [
                'source' => self::findAvailableNo()
            ]
        ];
    }

    public static function findAvailableNo()
    {
        for ($i = 0; $i < 11; $i++) {
            $no = str_replace('.','',substr(microtime(true),-9));
            // 判断是否已经存在
            if (!static::query()->where('slug', $no)->exists()) {
                return $no;
            }
        }
        return false;
    }




    //所有子类
    public function children()
    {
        return $this->childs()->with('children');
    }
}
        
 