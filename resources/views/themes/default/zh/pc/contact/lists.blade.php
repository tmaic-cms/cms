@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
<style>
.red{color:#F80000}

</style>
@endsection
@section('body', 'contact')
@section('bodyid', 'top')
@section('main') 
  <div class="box1">
    @channel($contact typeid=88 rows=1)
    <p>CONTACT US</p>
    <h1>{{$contact->name}}</h1>
    <p>{{$contact->description}}</p>
	@endchannel
  </div>
  <div class="box2">
    <ul>
      <li>
        <i class="iconfont">&#xe502;</i>
        <p>@email</p>
      </li>
      <li>
        <i class="iconfont">&#xe619;</i>
        <p>@address</p>
      </li>
      <li>
        <i class="iconfont">&#xe501;</i>
        <p>@phone</p>
      </li>
    </ul>
    <p class="qp3">留下您的联系方式和需求使我们能够响应不同需求</p>
    <form id="form_lx" onsubmit="return Subscribe();">
      <div class="forms" id="jdsj_form">
        <div class="forms_left">
          <input type="text" placeholder="姓名" name="name" id="name">
          <input type="text" placeholder="联系电话" name="phone" id="phone">
          <input type="text" placeholder="所在城市" name="address" id="address">
        </div>
        <div class="forms_right">
          <textarea rows="" cols="" placeholder="请简短叙述您的需求，以便我们设计总监及时回复您哦~" name="comments" id="comments"></textarea>
          <input type="submit" value="提交">
        </div>
      </div>
    </form>
	<div style="width:100%;clear:both;line-height:0"></div>
	<p class="tipBox" style="width:100%;clear:both;line-height:40px;text-align:right"></p>
  </div>



@endsection
@section('footerjs')
<script>
	function Subscribe() {
		var username = $('#name').val();
		var phone = $('#phone').val();
		var address = $('#address').val();
		var comments = $('#comments').val();
	 

		if(!username){
			$(".tipBox").html("<font class='red'>请输入您的称呼!</font>");
			return false;
		}

		if(!phone){
			$(".tipBox").html("<font class='red'>请输入您的手机!</font>");
			return false;
		}
	
		if(!address){
			$(".tipBox").html("<font class='red'>请输入您的联系地址!</font>");
			return false;
		}

		
		
		if(!comments){
			$(".tipBox").html("<font class='red'>请输入咨询内容!</font>");
			return false;
		}
		if(checkPhone(phone)){
			$(".tipBox").html("<font class='red'>手机号码有误，请重填!</font>");
			$('#phone').val('')
			return false;
		}

		$.ajaxSetup({
			headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
		});
		$.ajax({
			type: "POST",

			dataType: "json",
			url: "{{ route('subscribe') }}" ,
			data:{username:username,phone:phone,comments:comments,address:address} ,

			headers: {
				'X-CSRF-TOKEN' : '{{ csrf_token() }}'
			},
			success: function (result) {

				$(".tipBox").html(result.msg);


			},
			error : function() { }
		});
		return false;
	}

	function checkPhone(phone){

		if(!(/^1(3|4|5|6|7|8|9)\d{9}$/.test(phone))){
			alert("手机号码有误，请重填");
			return true;
		}
		return false;
	}
</script>
@endsection








