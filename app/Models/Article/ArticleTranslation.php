<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models\Article;

use App\Models\BaseModel;
use Illuminate\Support\Facades\Auth;
class ArticleTranslation extends BaseModel
{
 
	protected $table = 'article_translations';
    protected $fillable = ['column_id','uid','article_mid','flag','title','seo_title','keywords','description','locale','thumb','body','slug','sort','click','status','isdelete'];
    protected $appends = ['link'];
	
    protected $attributes = ['uid' => 0];
	protected $dateFormat = 'U';

 
	public function setUidAttribute($value)
	{
		$this->uid = Auth::user()->id;
	}
 
 
   
    //过虑文章

    public function scopeCondition($query)
    {
        return $query->lang()->status()->where('isdelete',0);
    }

    public function scopeFlag($query)
    {
        return $query->condition()->where('flag','0');
    }
	
	
    public function scopeFlag1($query)
    {
        return $query->condition()->where('flag','1');
    }
    public function scopeFlag2($query)
    {
        return $query->condition()->where('flag','2');
    }
    public function scopeFlag3($query)
    {
        return $query->condition()->where('flag','3');
    }
    public function scopeFlag4($query)
    {
        return $query->condition()->where('flag','4');
    }
    public function scopeFlag5($query)
    {
        return $query->condition()->where('flag','5');
    }
 

    //生成url
    public function getLinkAttribute($query)
    {
         
        return route('article.info',['slug'=>$this->slug]);
    }

    //关联栏目
    public function column()
    {
        return $this->hasOne('App\Models\ColumnTranslation','column_mid','column_id')->lang();
    }


}
