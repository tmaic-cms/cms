<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers;

use Illuminate\Contracts\View\Factory as View;
use Illuminate\Support\ServiceProvider;
//use App\Models\Themes;

class ThemeServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot(View $view)
    {
		//读取获取DB主题名称
        //$theme = Themes::where('selected', 1)->orderby('updated_at','desc')->value('themename');
		//配置方式读取
        $theme = env('WEB_THEME','default');
		 
        $view->addNameSpace('theme', [
            base_path()."/resources/views/themes/$theme".'/'.lang().'/',
            base_path().'/resources/views/themes/default/zh',
        ]);

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
