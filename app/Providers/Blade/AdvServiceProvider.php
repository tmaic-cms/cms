<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\AdvTagServices;

class AdvServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::directive('adv', function ($expression) {

            $ADVTAGOBJECT = new AdvTagServices($expression);
			//if($ADVTAGOBJECT->object =='$adv_sub')
            //dd($ADVTAGOBJECT);
			
			$ADVOBJECT = (object)$ADVTAGOBJECT->tag;

            $parseStr  = $parseStr   =  '<?php ';
 
            if(isset($ADVTAGOBJECT->useVal) && $ADVTAGOBJECT->useVal)
			$parseStr .= $ADVTAGOBJECT->object .'Reslut = cache()->remember('.$ADVTAGOBJECT->sysTagcachekey.', '.$ADVTAGOBJECT->tagcachetime.', function () use('.$ADVTAGOBJECT->useVal.'){ ';
            else
			$parseStr .= $ADVTAGOBJECT->object .'Reslut = cache()->remember("'.$ADVTAGOBJECT->sysTagcachekey.'", '.$ADVTAGOBJECT->tagcachetime.', function (){ ';
 

			
			
            $parseStr .=$ADVTAGOBJECT->object .'AdveModel = \App\Models\PositionTranslation::query();';
 
			$parseStr .=$ADVTAGOBJECT->object .'AdveModel = '.$ADVTAGOBJECT->object .'AdveModel->lang();';
 
            if(isset($ADVOBJECT->typeid)) $parseStr .=$ADVTAGOBJECT->object .'AdveModel = '.$ADVTAGOBJECT->object .'AdveModel'.$ADVOBJECT->typeid;

            if(isset($ADVOBJECT->group))  $parseStr .=$ADVTAGOBJECT->object .'AdveModel = '.$ADVTAGOBJECT->object .'AdveModel'.$ADVOBJECT->group;

            if(isset($ADVOBJECT->orderby))$parseStr .=$ADVTAGOBJECT->object .'AdveModel = '.$ADVTAGOBJECT->object .'AdveModel'.$ADVOBJECT->orderby;

            if(isset($ADVOBJECT->rows))   $parseStr .=$ADVTAGOBJECT->object .'AdveModel = '.$ADVTAGOBJECT->object .'AdveModel'.$ADVOBJECT->rows;
 
            $parseStr .='return '.$ADVTAGOBJECT->object .'Reslut = '.$ADVTAGOBJECT->object .'AdveModel->get();';
 
		    $parseStr  .=  '}); ';
			$parseStr .=$ADVTAGOBJECT->object .'Count = count('.$ADVTAGOBJECT->object .'Reslut); '; 
			$parseStr .='  $count = count('.$ADVTAGOBJECT->object .'Reslut); ';
 
            $parseStr .= ' foreach('.$ADVTAGOBJECT->object .'Reslut as $key=>$item) { ?>';
  
            $parseStr .= '<?php '. $ADVTAGOBJECT->object .'= $item; ?>';
 
			$parseStr .='<?php  $index = $key; ?>';
			$parseStr .='<?php  $iteration = $key+1; ?>';
			$parseStr .='<?php  $key ==0?$first = true:$first=false; ?>';
			$parseStr .='<?php  $key == ($count-1)?$last = true:$last=false; ?>';
			$parseStr .='<?php  $remaining = $count-($key+1); ?>';
			$parseStr .='<?php  $obj = [
								"index"=>$index,
								"iteration"=>$iteration,
								"first"=>$first,
								"last"=>$last,
								"remaining"=>$remaining		
						];?>';
			$parseStr .='<?php  '. $ADVTAGOBJECT->object .'loop = (object)$obj; ?>';
 
            return $parseStr;
        });

        Blade::directive('endadv', function () {
			
            return " <?php } ?>";
			
        });
 
    }
}
