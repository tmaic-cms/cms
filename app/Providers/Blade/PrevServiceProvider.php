<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\PrevTagServices;




class PrevServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        
        Blade::directive('prev', function ($expression) {

		$prevObject = new PrevTagServices($expression);
	    //dd($prevObject);
	 
		
		$ListTag = (object)$prevObject->tag;
		
		$parseStr   =  '<?php ';
		 
		$parseStr .= $prevObject->object .' = cache()->remember('.$prevObject->sysTagcachekey.','.$prevObject->tagcachetime.', function (){ ';

		 
		$parseStr .= $prevObject->object .'prevModel = '.$ListTag->model;

		$parseStr .= $prevObject->object .'prevModel = '.$prevObject->object .'prevModel->with("column")->lang();';

 
		 
		$parseStr .= $prevObject->object .'prevModel = '.$prevObject->object .'prevModel->where("slug", "<",request()->route("slug"));';
		
		
		$parseStr .= 'return '.$prevObject->object .' = '.$prevObject->object .'prevModel->orderBy("slug","desc")->first(); })?>';
	 
		
		if($prevObject->length > 0) $parseStr .='<?php '.$prevObject->object .'->title=cutstr($item->title,'.$prevObject->length.',"'.$ListTag->adorn.'");?>';
		if($prevObject->desclength > 0) $parseStr .='<?php '.$prevObject->object .'->description=cutstr($item->description,'.$prevObject->desclength.',"'.$ListTag->adorn.'");?>';

		 //echo "<pre>";
		 //print_r($parseStr);
		// die;
 
		return  $parseStr;
        });

        Blade::directive('endprev', function () {
            return "<?php    ?>";
        });
 
    }
}
