
    layui.use(['layer', 'form', 'table', 'util', 'admin', 'xmSelect','dropdown','element'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        var table = layui.table;
        var util = layui.util;
        var admin = layui.admin;
        var xmSelect = layui.xmSelect;
        var dropdown = layui.dropdown;
        /* 渲染表格 */

        var insTb = table.render({
            elem: '#userTable',
            skin: 'line',
            size: 'lg',
            url: userData,
            page: true,
            where: {model: "admins"},
            toolbar: ['<p>',
                '<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
                '<button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
                '</p>'].join(''),
            cellMinWidth: 100,
            cols: [[
                {type: 'checkbox'},
                {type: 'numbers'},
                {field: 'username', title: '账号', sort: true},
                {field: 'nickName', title: '用户名', sort: true},
                {field: 'sex', title: '性别', sort: true},
                {field: 'phone', title: '手机号', sort: true},
                {
                    field: 'display_name', title: '角色', templet: function (d) {
                        return d.roles.map(function (item) {
                            
                            return '<a class="layui-btn layui-btn-sm">' + item.display_name + '</a>';
                        }).join('&nbsp;&nbsp;');
                    }, sort: true, width: 150
                },
                {
                    field: 'created_at', title: '创建时间', templet: function (d) {
                        return util.toDateString(d.createTime);
                    }, sort: true
                },
                {field: 'state', title: '状态', templet: '#userTbState', sort: true, width: 100},
                {title: '操作', toolbar: '#options', align: 'left', fixed: "right"}
            ]]
        });




        /* 表格搜索 */
        form.on('submit(userTbSearch)', function (data) {
            insTb.reload({where: data.field, page: {curr: 1}});
            return false;
        });

        
        /* 表格工具条点击事件 */
        table.on('tool(userTable)', function (obj) {
            if (obj.event === 'edit') { // 修改
                showEditModel(obj.data);
            } else if (obj.event === 'del') { // 删除
                doDel({ids: obj.data.id});
            } else if (obj.event === 'reset') { // 重置密码
                resetPsw(obj);
            }
        });

        /* 表格头工具栏点击事件 */
        table.on('toolbar(userTable)', function (obj) {
            if (obj.event === 'add') { // 添加
                showEditModel();
            } else if (obj.event === 'del') {
                var checkRows = table.checkStatus('userTable');
                if (checkRows.data.length === 0) {
                    layer.msg('请选择要删除的数据', {icon: 2});
                    return;
                }
                var ids = checkRows.data.map(function (d) {
                    return d.id;
                });
                doDel({ids: ids});
            }
        });

        /* 显示表单弹窗 */
        function showEditModel(mData) {
            admin.open({
                type: 1,
                area: ['600px', 'auto'],
                title: (mData ? '修改' : '添加') + '用户',
                content: $('#userEditDialog').html(),
                success: function (layero, dIndex) {
                    // 回显表单数据
                    form.val('userEditForm', mData);
                    // 表单提交事件
                    form.on('submit(userEditSubmit)', function (data) {
                        data.field.roleIds = insRoleSel.getValue('valueStr');
                        var loadIndex = layer.load(2);
                        $.ajax({
                            type: "POST",
                            url: mData ? userupdate : userstore,
                            data: $(this).parents('form').serialize(),
                            dataType: 'json',
                            success: function (res) {
                                layer.close(loadIndex);
                                if (res.code === 0) {
                                    layer.close(dIndex);
                                    layer.msg(res.msg, {icon: 1});
                                    insTb.reload({page: {curr: 1}});
                                } else {
                                    layer.msg(res.msg, {icon: 2});
                                }

                            },
                            error: function (msg) {
                                layer.close(loadIndex);
                                if (msg.status == 422) {
                                    var json = JSON.parse(msg.responseText);
                                    json = json.errors;
                                    for (var item in json) {
                                        for (var i = 0; i < json[item].length; i++) {
                                            layer.msg(json[item][i], {icon: 2});
                                            return;
                                        }
                                    }
                                } else {
                                    layer.msg('服务器连接失败', {icon: 2});
                                    return;
                                }
                            }
                        });
                        return false;
                    });
                    // 渲染多选下拉框
                    var insRoleSel = xmSelect.render({
                        el: '#userRole',
                        name: 'userRole',
                        layVerify: 'required',
                        layVerType: 'tips',
                        /*data: {!! $role->toJSON()  !!}*/
                        data: []
                    });

                    $.get(userData, {
                        model: 'role',
                        list: 1
                    }, function (res) {
                        if (res.code == 0) {
                            insRoleSel.update({data: res.data, autoRow: true})
                        }
                        // 回显选中角色
                        if (mData && mData.roles) {
                            insRoleSel.setValue(mData.roles.map(function (item) {
                                return item.id;
                            }));
                        }
                    }, 'json');
                    // 禁止弹窗出现滚动条
                    $(layero).children('.layui-layer-content').css('overflow', 'visible');
                }
            });
        }

        /* 删除 */
        function doDel(obj) {
            layer.confirm('确定要删除选中数据吗？', {
                skin: 'layui-layer-admin',
                shade: .1
            }, function (i) {
                layer.close(i);
                var loadIndex = layer.load(2);
                $.post(userdestroy, {
                    _method: 'delete',
                    id: obj.data ? obj.data.id : '',
                    ids: obj.ids
                }, function (res) {
                    layer.close(loadIndex);
                    if (res.code === 0) {
                        layer.msg(res.msg, {icon: 1});
                        insTb.reload({page: {curr: 1}});
                    } else {
                        layer.msg(res.msg, {icon: 2});
                    }
                }, 'json');
            });
        }

        /* 修改用户状态 */
        form.on('switch(userTbStateCk)', function (obj) {
            var loadIndex = layer.load(2);
            $.post(userupdateajax, {
                id: obj.elem.value,
                state: obj.elem.checked ? 0 : 1
            }, function (res) {
                layer.close(loadIndex);
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                } else {
                    layer.msg(res.msg, {icon: 2});
                    $(obj.elem).prop('checked', !obj.elem.checked);
                    form.render('checkbox');
                }
            }, 'json');
        });


    });