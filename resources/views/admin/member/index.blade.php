@extends('admin.base')
@section('content')
   <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form toolbar">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label left">账&emsp;号:</label>
                        <div class="layui-input-inline">
                            <input name="username" class="layui-input" placeholder="输入账号"/>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label">手机号:</label>
                        <div class="layui-input-inline">
                            <input name="phone" class="layui-input" placeholder="输入手机号"/>
                        </div>
                    </div>
         
                    <div class="layui-inline">&emsp;
                        <button class="layui-btn icon-btn" lay-filter="userTbSearch" lay-submit>
                            <i class="layui-icon">&#xe615;</i>搜索
                        </button>
                    </div>
              

                </div>
            </form>
        </div>    
     </div>
      <div class="layui-card-body">
       <table id="memberTable" lay-filter="memberTable"></table>
     </div>
 </div>
@endsection
@section('script')
    @can('member.member')
        @include('admin.member.js')
    @endcan
@endsection