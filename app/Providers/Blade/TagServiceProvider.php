<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use DB;

class TagServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
 
        //站点备案号
        Blade::directive('ICP', function ($expression) {
            $parseStr ='<?php echo "<a class=copyright href=https://beian.miit.gov.cn rel=nofollow target=_blank>".webconfig("ICP")."</a>"; ?>';
            return $parseStr;
        });

   

        //站点URL地址
        Blade::directive('indexurl', function ($expression) {
       
            $parseStr ='<?php echo env("APP_URL"); ?>';
            
            return $parseStr;
        });

        //图片资源地址 不加密
        Blade::directive('image_url', function ($expression) {
       
            $parseStr ='<?php echo "http://". env("DOMAINS_DEFAULT")."/"; ?>';
            
            return $parseStr;
        });


        //图片资源地址 加密
        Blade::directive('image_urls', function ($expression) {
       
            $parseStr ='<?php echo "https://". env("DOMAINS_DEFAULT")."/"; ?>';
            
            return $parseStr;
        });

        //样式url
        Blade::directive('style', function ($expression) {
           
            $parseStr ='<?php echo env("APP_URL")."/css/"; ?>';
            
            return $parseStr;
        });


        //js url
        Blade::directive('script', function ($expression) {
           
            $parseStr ='<?php echo env("APP_URL")."/js/"; ?>';
            
            return $parseStr;
        });
 

 
 
 
 
 
 
        //当前url
        Blade::directive('current', function ($expression) {
           
            $parseStr ='<?php echo url()->full(); ?>';
          
            return $parseStr;
        });

        //当前主题
        Blade::directive('theme', function ($expression) {
           
            $parseStr ='<?php echo config("theme.theme"); ?>';
          
            return $parseStr;
        });
 
 
    }
}
