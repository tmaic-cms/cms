<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers;

use App\Models\Permission;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use App\Models\ColumnTranslation;

class AdminController extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


      protected $syslang;
      protected $lang;
      public function __construct(){
         $this->syslang = app()->getLocale();
         
     }

    //栏目列表
    public function getColumnClass($model = ''){
      $lang=request()->get('lang')?? app()->getLocale();

      
      return ColumnTranslation::with(['children'])
            ->when($model,function($q) use($model){return $q->where('model',$model);})
            ->where('locale', $lang)->where('pid',0)->orderBy('sort', 'asc')->get();
    }

   

    /**
     * 处理权限分类
     */
    public function tree($list=[], $pk='id', $pid = 'parent_id', $child = '_child', $root = 0)
    {
      
        if (empty($list)){
             $list = Permission::get()->toArray();
        }
        // 创建Tree
        $tree = array();
        if(is_array($list)) {
            // 创建基于主键的数组引用
            $refer = array();
            foreach ($list as $key => $data) {
                $refer[$data[$pk]] =& $list[$key];
            }
            foreach ($list as $key => $data) {
                // 判断是否存在parent
                $parentId =  $data[$pid];
                if ($root == $parentId) {
                    $tree[] =& $list[$key];
                }else{
                    if (isset($refer[$parentId])) {
                        $parent =& $refer[$parentId];
                        $parent[$child][] =& $list[$key];
                    }
                }
            }
        }
        return $tree;
    }





    public function error($msg,$data = '',$url = '')
    {
        $result['msg'] = $msg;
        $result['status'] = 'error';
        $result['data'] = $data;
        return response()->json($result,401);
    }

    /**
     * 成功是返回json数据
     * @author pangxianfei <421339244@qq.com>
     */
    public function success($status = 0,$msg,$data = '')
    {
        $result['msg'] = $msg;
        $result['status'] = $status;
        $result['data'] = $data;
        return $this->unsetNull($result);

    }

    protected function isApi()
    {
        $getRequest = request()->route()->getAction();
        if($getRequest['middleware'][0] === 'api') {
            return true;
        } else {
            return false;
        }
    }


    /**
     * 把 null转换为空
     * @author pangxianfei <421339244@qq.com>
     */
    protected function unsetNull($data)
    {
        $result = json_decode(str_replace(':null', ':""', json_encode($data)), true);
        if ($result) {
            $data = $result;
        }
        return $data;
    }
	
	
	
	
	
	protected function setServiceProvider($ServiceProvider){
		 
		 
		if(empty($ServiceProvider)) return false;
		
	    $tagData = \App\Models\Defines\DefinesTranslation::where('keys',$ServiceProvider)->first();

        //写入目录
		$path = app_path('Tagserver/');
		//文件名
		$ServiceProvider =ucfirst($tagData->keys)."ServiceProvider";
		$fileName = $path.$ServiceProvider.'.php';
		
		//写入字符串
		$tagcontent="";
		$tagheaderscript = file_get_contents(config('app.tagheaderscript'));
		
		$tagheaderscript = $tagheaderscript;

		$classServiceProviderStr="\n
class {$ServiceProvider} extends ServiceProvider
{
	/**
	 * Register services.
	 *
	 * @return void
	 */
	public function register() {}

	/**
	 * Bootstrap services.
	 *
	 * @return void
	 */
	public function boot()
	{";
    $classServiceProviderStr = sprintf($classServiceProviderStr,$ServiceProvider);
	
	$tagcontent = $tagheaderscript . $classServiceProviderStr;
	
		$tagcontent .=" 
		Blade::directive('".$tagData->keys."', function () { 
			 return '<?php echo siteconfig(\"".$tagData->keys."\"); ?>';
		});";	
		 
       $tagendscript = file_get_contents(config('app.tagendscript'));
	   $tagcontent = $tagcontent . $tagendscript;
		
	   @file_put_contents($fileName,$tagcontent);	
		
		\Artisan::call('view:clear');
	   
		return true;
	}
	
	
	
	
	
    /**
     * 清除全部缓存
     * @return \Illuminate\Contracts\View\View
     */
    public function clearAll()
    {
		    \Artisan::call('cache:clear');
			\Artisan::call('config:clear');
			\Artisan::call('route:clear');
			\Artisan::call('view:clear');
			\Artisan::call('clear-compiled');
			\Cache::flush();
    } 
}
