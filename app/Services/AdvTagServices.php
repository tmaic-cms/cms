<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;


class AdvTagServices
{
    public $expression;
    public $routeName;
    public $musterVal;
    public $sqlstr;
    public $object;
    public $typeid;
    public $group;
    public $rows;
    public $orderby;
 
    public $locale;
    public $tag;
	//缓存
    public $cache;
	//是否缓存
    public $iscache;
	//缓存key
    public $sysTagcachekey;
	//缓存时间
    public $tagcachetime;
	//闭包变量
    public $useVal;
    public $objtag;
    public $tagName;
 
    public function __construct($expression)
    {
	   $this->tagName='adv';
	   $this->typeid=null;
	   $this->group=null;
	   //闭包变量
	   $this->useVal = NULL;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=30;
	   $this->locale = lang();
       $this->decompose($expression);
	   $this->setTagcachekey();

    }

    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		
		$this->musterVal = explode(',',current($this->expression));
		
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
 
		}
		


        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
 
        if(count($this->expression)){
		 
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			
        }
		$this->objtag = (object)$this->tag;
		// dd($this->objtag);
        
        if($this->tag && array_key_exists("typeid",$this->tag)){
          $this->typeid($this->tag['typeid']);
        } 

 

        if($this->tag && array_key_exists("group",$this->tag)){
		  if(substr($this->objtag->group,0,1)=="$"){	
			$this->useVal = substr($this->objtag->group,0,strrpos($this->objtag->group,"->"));
		  }
		  
          $this->group($this->tag['group']);
        } 


        if($this->tag && array_key_exists("orderby",$this->tag)){
          $this->orderby($this->tag['orderby']);
        } 
        if($this->tag && array_key_exists("rows",$this->tag)){
          $this->rows($this->tag['rows']);
        } 
		
	}
    
    public function typeid($typeid)
    {
        $this->typeid = $typeid; 

        $this->tag['typeid']= "->whereIn('sid',[".$this->typeid."]);";   

    }
    public function group($group)
    {
        
        $this->group = $group;
        $this->tag['group']= "->whereIn('pid',[".$this->group."]);";   
        
    }

    public function rows($rows = 1)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows);";   
        
    }

    public function orderby($orderby = 'id')
    {
        $value = null;
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case 'date':
                $value = '->orderBy("created_at","desc");';
                break;

            default: $value = '->orderBy("id","desc");';break;
        }
 
      
        $this->tag['orderby']= $value;
        
    }
    //缓存key
	
	public function setTagcachekey(){
		if($this->useVal){
			$this->sysTagcachekey ="\""; 
		}
		$this->sysTagcachekey .=$this->routeName."_"; 
		$this->sysTagcachekey .=$this->tagName."_"; 
		$this->sysTagcachekey .=substr($this->object,1)."_"; 

		foreach($this->objtag as $k=>$v){ 
		  $this->sysTagcachekey .= (substr($v,0,1)=="$"?substr($v,1):(string)$v).'_';
		} 
		
		foreach(request()->all() as $k=>$v){ 
		  $this->sysTagcachekey .=(string)$v.'_';
		} 
	    

		if($this->useVal){
			$useVal = $this->useVal."->sid";
			$this->sysTagcachekey .= "\".".$useVal;
		}
	}
 
}
