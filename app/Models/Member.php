<?php 
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/    
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;


 
class Member extends Authenticatable
{
    use Notifiable;
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'member';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public $timestamps = true;
    
    
    protected $fillable = ['id','username','nickname','avatar','password','email','moblie','reg_ip','reg_date','login_ip','login_date','usertype','login_hits','salt','address','name_repeat','qqid','status','pwuid','pw_repeat','lock_info','email_status','signature','sinaid','wxid','wxopenid','unionid','wxname','wxbindtime','passtext','source','regcode','did','claim','restname','appeal','appealtime','appealstate','signday','signdays','token','tokentime','appuuid','qqunionid','clientid','deviceToken','applocker','maguid','qfyuid','moblie_status','pid'];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

}
        
 