<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;
use App\Models\ColumnTranslation;
use Cache;
class ChannelTagServices
{
    public $tagName;
    public $expression;
    public $useVal;
    public $routeName;
    public $musterVal;
    public $sqlstr;
    public $object;
    public $objtag;
    public $columnid;
    public $rows;
    public $typeid;
    public $group;
    public $navObj;
    public $orderby;
    public $locale;
    public $tag;
	//缓存
    public $cache;
	//是否缓存
    public $iscache;
	//缓存key
    public $Tagcachekey;
	//缓存时间
    public $tagcachetime;
    public function __construct($expression)
    {
	   $this->tagName ="channel";
       //二级导航对象
	   $this->navObj = NULL;
	   $this->useVal = NULL;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=30;
	   $this->locale = lang();
	   
	   $this->setColumn();
       $this->decompose($expression);
	   $this->setTagcachekey();

    }
    //出始化栏目首次执行 并且缓存，下次有相同的从缓存取数据
    public function setColumn(){
	    
       $this->column = Cache::remember($this->routeName.'_'.$this->locale, webconfig('SYSCACHETIME') , function() {
          return  ColumnTranslation::with(['children','class'])->where('routename',$this->routeName)->lang()->first();
        });
	    
		$this->columnid = $this->column?$this->column->column_mid:null; 
	}

    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
		 
		$this->musterVal = explode(',',current($this->expression));
		
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
			
		}else if(count($this->musterVal) === 2){
			
			// 循环对象
			$this->object = current($this->musterVal);  
			// 二级导航对象
			$this->navObj = next($this->musterVal);  
			
		}
	

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		

        //拆分参数
        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
			$this->objtag = (object)$this->tag;
        }
 
		
		if(isset($this->objtag->typeid) && substr($this->objtag->typeid,0,1)=="$"){
		   $this->useVal = substr($this->objtag->typeid,0,strrpos($this->objtag->typeid,"->"));
		   $this->typeid($this->objtag->typeid);
		}else if(isset($this->objtag->typeid) && is_numeric($this->objtag->typeid)){

			$this->typeid($this->objtag->typeid);
		}elseif(isset($this->objtag->typeid) && strpos($this->objtag->typeid,',') !== false){
           $this->typeid($this->objtag->typeid);  
		}			
 
        //特殊场景
		//只会显示当前栏目下的分类
		if(isset($this->objtag->top) && (int)$this->objtag->top==0){
            $this->useVal='$column';
			$this->group('t($column)');  
		}
		


        if(isset($this->objtag->group) && substr($this->objtag->group,0,1)=="$"){
		   $this->useVal = substr($this->objtag->group,0,strrpos($this->objtag->group,"->"));
           $this->group($this->objtag->group);
        }elseif(isset($this->objtag->group) && is_numeric($this->objtag->group)){
			$this->group($this->objtag->group);
		}	
 
		//设置排序
        if(isset($this->objtag->orderby)){
          $this->orderby($this->objtag->orderby);
		   
        }else{
		  $this->orderby('id');
	 
		}	
		
 
 
        //处理返回行数
        isset($this->tag['rows'])?$this->rows($this->tag['rows']):$this->rows(1);
		
	    //dd($this->object);
    }

	//限制返回字段
	//TODO:
    public function Field(){
		if(empty($this->column)){
			$column = new ColumnTranslation();
		}
	}
	
  

    
    public function typeid($columnid)
    {
        $this->columnid = $columnid; 
        
        $this->tag['typeid']= "->whereIn('column_mid',[".$this->columnid."]);";   

    }
    public function group($group)
    {
        $this->group = $group;
        $this->tag['group']= "->whereIn('pid',[".$this->group."]);";   
    }
	
 
	
	
	
	
	
	
	
	

    public function rows($rows = 10)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows);";   
        
    }

    public function orderby($orderby = 'id')
    {
        $value = null;
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case 'date':
                $value = '->orderBy("created_at","desc");';
                break;

            default: $value = '->orderBy("id","desc");';break;
        }
    
        $this->tag['orderby']= $value;
        
    }
 
    //缓存key
	
	public function setTagcachekey(){
/*
		foreach($this->expression as $k=>$v){ 
		
		  $this->Tagcachekey .=(string)$v.'_';
		} 
		
		 //为变量时
		if(isset($this->objtag->typeid) && substr($this->objtag->typeid,0,1)=="$"){
 
		   $this->Tagcachekey .= $this->routeName.'_channel_typeid_'.$this->locale.'_'.$this->columnid.'_'.$this->group;
		   
		}
		//为数字时
		else if(isset($this->objtag->typeid) && is_numeric($this->objtag->typeid)){

			$this->Tagcachekey .= $this->routeName.'_channel_typeid_'.$this->locale.'_'.$this->columnid.'_'.$this->group;
		}
        //为变量时
        if(isset($this->objtag->group) && substr($this->objtag->group,0,1)=="$"){
			
           $this->Tagcachekey .= $this->routeName.'_channel_group_'.$this->locale.'_';
		   
        }
		//为数字时
		elseif(isset($this->objtag->group) && is_numeric($this->objtag->group)){
			
			$this->Tagcachekey .= $this->routeName.'_channel_group_'.$this->locale.'_'.$this->objtag->group;
		} 
		
*/
		//if($this->Tagcachekey) $this->Tagcachekey=($this->Tagcachekey);
		
		
       // dd($this->Tagcachekey);
	   
	   
	   
		if($this->useVal){
			$this->Tagcachekey ="\""; 
		}
		$this->Tagcachekey .=$this->routeName."_"; 
		$this->Tagcachekey .=$this->tagName."_"; 
		$this->Tagcachekey .=substr($this->object,1)."_"; 

		foreach($this->objtag as $k=>$v){ 
		  $this->Tagcachekey .= (substr($v,0,1)=="$"?substr($v,1):(string)$v).'_';
		} 

		foreach(request()->all() as $k=>$v){ 
		  $this->Tagcachekey .=(string)$v.'_';
		} 

		if($this->useVal){
			$useVal = $this->useVal."->child";
			$this->Tagcachekey .= "\".".$useVal;
		}
 
	   
	}

































}
