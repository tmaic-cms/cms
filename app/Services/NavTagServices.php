<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Services;


class NavTagServices
{
   
    public $expression;
	public $useVal;
    public $routeName;
    public $musterVal;
    public $sqlstr;
    public $object;
    public $columnid;
    public $rows;
    public $group;
    public $navObj;
    public $orderby;
    public $locale;
    public $tag;
	public $navPid;
	//缓存
    public $cache;
	//是否缓存
    public $iscache;
	//缓存key
    public $Tagcachekey;
	//缓存时间
    public $tagcachetime;

    public function __construct($expression)
    {
       //二级导航对象
	   $this->navObj = NULL;
	   $this->navPid = NULL;
	   $this->useVal = NULL;
	   //当前路由
       $this->routeName = request()->route()->getName();
	   //当前缓存时间
	   $this->tagcachetime=webconfig('SYSCACHETIME');
	   //当缓存为空时 设置为0
	   if(empty($this->tagcachetime)) $this->tagcachetime=30;
	   $this->locale = lang();
       $this->decompose($expression);
	   $this->setTagcachekey();
    }


    public function decompose($expression = null){

        $this->expression = explode(' ',$expression);
 
		$this->musterVal = explode(',',current($this->expression));
		
		if(count($this->musterVal) === 1){
			// 循环对象
			$this->object = current($this->musterVal);  
			
		}else if(count($this->musterVal) === 2){
			// 循环对象
			$this->object = current($this->musterVal);  
			// 二级导航对象
			$this->navObj = next($this->musterVal);  
		}

        if(!empty(current($this->expression))){
           array_shift($this->expression);
        }
		


        if($this->expression){
            foreach($this->expression as $item){
              list($tag,$value) = explode('=',$item);
              $this->tag[$tag]=$value;
            }
        }

        if(array_key_exists("typeid",$this->tag)){
          $this->typeid($this->tag['typeid']);
        } 

        if(array_key_exists("group",$this->tag)){
			
		  //闭包变量
		  $this->useVal = substr($this->tag['group'],0,strrpos($this->tag['group'],"->"));
			
          $this->group($this->tag['group']);
        } 

		$this->orderby($this->tag['orderby']??'sort');
		
        if(array_key_exists("rows",$this->tag)){
          $this->rows($this->tag['rows']);
        } 


		$this->navObj = $this->navObj??$this->object."son";
    }

    
    public function typeid($columnid)
    {
        $this->columnid = $columnid; 

        $this->tag['typeid']= "->whereIn('column_mid',[".$this->columnid."]);";   

    }
    public function group($group)
    {
        $this->group = $group;
        $this->tag['group']= "->whereIn('pid',[".$this->group."]);";   
        
    }

    public function rows($rows = 10)
    {
        $this->rows = $rows;
        $this->tag['rows']= "->limit($rows);";   
  
    }

    public function orderby($orderby = 'id')
    {
        $value = null;
        switch ($orderby) {
            case 'id':
                $value = '->orderBy("id","desc");';
                break;
            case 'rand':
                $value = '->inRandomOrder();';
                break;
            case 'date':
                $value = '->orderBy("created_at","desc");';
                break;
            case 'sort':
                $value = '->orderBy("sort","asc");';
                break;
 
            default: $value = '->orderBy("sort","asc");';break;
        }
        

      
        $this->tag['orderby']= $value;
        
    }


 

    //缓存key
	
	public function setTagcachekey(){
 
		   
		if($this->group == "0"){
		   $this->Tagcachekey = 'main_nav_'.$this->locale;
		}else{
			
		    //闭包变量
		    $useVal = substr($this->group,0,strrpos($this->group,"->"));
			$this->navPid = $useVal.'->child'; 
		    $this->Tagcachekey = 'sub_nav_'.$this->locale.'_';
 
		}
		 
	}






}
