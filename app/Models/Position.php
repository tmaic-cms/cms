<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Astrotomic\Translatable\Contracts\Translatable as TranslatableContract;
use Astrotomic\Translatable\Translatable;


class Position extends Model
{
    use Translatable;
    protected $primaryKey = 'mid';
    public $timestamps = true;
    public $translatedAttributes = ['title','sid','pid','firsttitle','secondtitle','thirdtitle','thumb','link','position_mid','keywords','description','sort','status','body'];
    protected $fillable = ['classname','tag','sort','status'];
    //该位置下所有的广告
    public function children()
    {
        return $this->hasMany('App\Models\Advert')->orderBy('updated_at','desc');
    }
    
    public function childs()
    {
        $lang = request()->get('lang')??lang(); 
        


        return $this->hasMany('App\Models\PositionTranslation','position_mid','mid')->where('locale',$lang);
    }
}
