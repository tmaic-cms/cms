@include('admin.product.html')
<script type="text/javascript" src="/assets/js/product.js"></script>
<script>
    var create = "{{ route('admin.product.create') }}";
    var edit = "{{ route('admin.product.edit') }}?id=";
    var update = "{{ route('admin.product.update') }}";
    var updateAjax = "{{ route('admin.product.updateAjax') }}";
    var destroy ="{{ route('admin.product.destroy') }}";
    var tableData = "{{ route('admin.product.data') }}?pid=";


    var AtlasData = "{{ route('admin.Atlas.data') }}";
    var AtlasDestroy = "{{ route('admin.Atlas.destroy') }}";
    var AtlasUpload= "{{ route('admin.Atlas.upload') }}";
    var AtlasStore= "{{ route('admin.Atlas.store') }}";


</script>