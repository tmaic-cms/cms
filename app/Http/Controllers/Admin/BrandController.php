<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;


use App\Http\Controllers\AdminController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Models\brands\Brands;



class BrandController extends AdminController
{
    /**
     * 列表 view
     * @return \Illuminate\Contracts\View\View
     */
    public function index()
    {
		$brand = Brands::orderBy('sort', 'desc')->get();
        return View::make('admin.brand.index',compact('brand'));
    }

    /**
     * 数据接口 DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function data(Request $request)
    {

		$model = Brands::query();
		
        if ($request->get('style')){
            $model = $model->where('style',$request->get('style'));
        }
        if ($request->get('project')){
            $model = $model->where('project',$request->get('project'));
        }
		
        if ($request->get('luxury')){
            $model = $model->where('luxury',$request->get('luxury'));
        }
		
	    $res = $model->orderBy('id', 'desc')->paginate($request->get('limit', 30));
		 
		 
        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res->total(),
            'data' => $res->items(),
        ];
        return Response::json($data);
    }

    /** 创建 view
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function create()
    {
         
        return View::make('admin.brand.create');
    }

    /**创建 DB
     * @param Request $request Request
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function store(Request $request)
    {
        $data = $request->except(['_token']);
       
        if(Brands::where('name',$data['name'])->count()){
            return Response::json(['code' => 1, 'msg' => '品牌已存在!']);
        }
	   
        DB::beginTransaction();
        try {
            Brands::create($data);
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '添加成功']);
           
        } catch (\Exception $exception) {
            DB::rollBack();
        }
        return Response::json(['code' => 1, 'msg' => '添加失败']);
       
    }

    /**
     * 更新 view
     * @param $id
     * @return \Illuminate\Contracts\View\View
     */
    public function edit($id)
    {
        
        $brand       = Brands::findOrFail($id);
        return View::make('admin.brand.edit', compact('brand'));
    }

    /**
     * 更新 DB
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id     = (int)$request->get('id');
        $Brands = Brands::findOrFail($id);
        $data   = $request->all();
     
        
        DB::beginTransaction();
        try {
            $Brands->update($data);
		 
            DB::commit();
         
            return Response::json(['code' => 0, 'msg' => '更新成功']);
            
        } catch (\Exception $exception) {
            DB::rollback();
        }
        return redirect(route('admin.case'))->withErrors(['status' => '系统错误']);
        return Response::json(['code' => 1, 'msg' => '系统错误']);
    }

    /**
     * 删除 DB
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('ids');
        if (!is_array($ids) || empty($ids)) {
            return Response::json(['code' => 1, 'msg' => '请选择删除项']);
        }
        DB::beginTransaction();
        try {
            Brands::whereIn('id', $ids)->delete();
            DB::commit();
            return Response::json(['code' => 0, 'msg' => '删除成功']);
        } catch (\Exception $exception) {
            DB::rollback();
        }
        return Response::json(['code' => 1, 'msg' => '删除失败', 'data' => $exception->getMessage()]);
    }
}
