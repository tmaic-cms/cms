<div class="layui-card-body">
      <input type="hidden" name="id"  value="{{ $position->id??'' }}"> 
   <div class="layui-tab layui-tab-brief" lay-filter="tablist">
      <ul class="layui-tab-title">
        <li class="layui-this" lay-id="base" >基本信息</li>
        <li lay-id="body">详情内容</li>
      </ul>
      <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <div class="layui-form-item">
                <label class="layui-form-label">广告位</label>
                <div class="layui-input-block" style="width:500px">
                    <select name="pid" lay-verify="required" lay-reqtext="产品分类必填项，岂能为空？">
                        <option value="">-选择广告位-</option>
                    @foreach($positions as $key=>$item)
                        <option value="{{ $item->sid }}" @if(isset($positions->pid)&&$positions->pid==$item->sid)selected @endif>{{$key+1}} - {{ $item->title }}-{{$item->sid}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
 
            <div class="layui-form-item">
                <label class="layui-form-label">广告标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{ $position->title ?? old('title') }}" lay-verify="required" placeholder="请输入广告标题" class="layui-input" >
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">描述</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="description" id="description" placeholder="请输入描述">{!! $position->description??'' !!}</textarea>
                </div>
            </div>
		      
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="link" value="{{ $position->link ?? old('link') }}" autocomplete="off"
						   placeholder="请输入跳转地址:http://" class="layui-input">
				</div>
			</div>
        </div>

        <div class="layui-tab-item">
         <div class="layui-form-item layui-form-text">
                <main>
                	<div class="centered">
                		<div class="document-editor">
                			<div class="toolbar-container"></div>
                			<div class="content-container">
                				<div id="editor">
                					{!! $position->body??old('body') !!} 
                				</div>
                			</div>
                		</div>
                	</div>
                </main>
        </div>



      </div>
    </div>
</div>
<div class="form-group-bottom text-center">
    <button class="layui-btn" lay-filter="SubmitData" id="SubmitData" lay-submit>&emsp;提交&emsp;</button>
	@empty($position->id)
    <button type="button" class="layui-btn layui-btn-normal layui-btn-warm" id="yongshi-component-form-getval">&emsp;保存继续添加&emsp;</button>
	@endif
	<button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
   
</div>

