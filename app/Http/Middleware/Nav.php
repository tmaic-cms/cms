<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Middleware;

use Closure;
use Illuminate\View\View;
use App\Models\Column;
use App\Models\ColumnTranslation;
use Cache;	
class Nav
{
    public function handle($request, Closure $next)
    {
        view()->share('routeName',request()->route()->getName());
        view()->composer('*',function($view){	 
            $view->with('isMobile', isMobile());
          
        });
	 
	    $column = Cache::get('column_translations');
	    if(empty($column)){
			$column_translations = cache()->remember('column_translations', webconfig('SYSCACHETIME'), function (){ 
			  return ColumnTranslation::lang()->get();
			});
			Cache::put('column_translations', $column_translations);
				
		}
        return $next($request);
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
