<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Navigation extends Model
{

 
    protected $table = "navigation";
    protected $fillable = [
        'display_name_en',
        'display_name',
		'route',
		'nav_url',
		'spm',
        'icon',
        'parent_id',
        'sort',
		'position',
		'status',
		'seo_title',
		'seo_key',
		'description',
		'param',
    ];




    public function getTypeNameAttribute()
    {
        return $this->attributes['type_name'] = Arr::get([1=>'按钮',2=>'菜单'],$this->type);
    }

    //子权限
    public function childs()
    {
        return $this->hasMany('App\Models\Navigation','parent_id','id');
    }

    //所有子权限递归
    public function allChilds()
    {
        return $this->childs()->with('allChilds');
    }
	
    //前台所有子权限递归
    public function menus()
    {
        return $this->childs()->with('allChilds')->where('status',1);
    }
	

}