@extends('admin.base')
@section('content')
<style>
.layui-card {
    margin-bottom: 0px;
    border-radius: 2px;
    line-height:20px;
 }
.layui-tab-content {
    padding: 0px;
}
.layui-card-body {
    padding: 5px 0px;
}
.layui-form.toolbar .layui-form-item .layui-inline {
    margin-bottom: 1px;
}
/*
.layui-fluid {
    padding: 10px 1px 0 1px;
}
*/
</style>


   <div class="layui-card">
        <div class="layui-card-body">
            <form class="layui-form toolbar" id="order_Search_from">
                <div class="layui-form-item">
                    <div class="layui-inline">
                        <label class="layui-form-label left">订单号 :</label>
                        <div class="layui-input-inline">
                            <input name="order_no" class="layui-input" id="order_no" placeholder="输入订单号查询"/>
                        </div>
                    </div>

                    <div class="layui-inline">
                        <label class="layui-form-label left">退款单号 :</label>
                        <div class="layui-input-inline">
                            <input name="refund_no" class="layui-input" id="refund_no" placeholder="输入用户名查询"/>
                        </div>
                    </div>
                    <div class="layui-inline">
                        <label class="layui-form-label left">手机号 :</label>
                        <div class="layui-input-inline">
                            <input name="phone" class="layui-input" id="phone" placeholder="输入订手机号查询"/>
                        </div>
                    </div>

          
 


                    <div class="layui-inline">&emsp;
                        <button class="layui-btn icon-btn" type="button" lay-filter="btnSearch"  id="btnSearch" lay-submit>
                            <i class="layui-icon">&#xe615;</i>搜索
                        </button>
                        <button class="layui-btn layui-btn-warm"  id="resstSearch" type="reset" lay-filter="resstSearch">
                            <i class="layui-icon">&#x1006;</i>清除
                        </button>
                    </div>
                </div>
            </form>
        </div>    
     </div>
      <div class="layui-card-body">
        <div class="layui-tab layui-tab-brief" lay-filter="orderlist">
          <ul class="layui-tab-title">
            <li class="layui-this" lay-id="orderall" lay-data="">全部</li>
            <li  lay-id="order_pay_done" lay-data="1">审核中</li>
            <li  lay-id="order_pay_done" lay-data="2">退款</li>
            <li  lay-id="order_pay_no" lay-data="3">拒绝</li>
            <li  lay-id="order_pay_no" lay-data="4">已退款</li>
          </ul>
          <div class="layui-tab-content">
            <div class="layui-tab-item layui-show"><table id="orderTree" lay-filter="orderTree"></table></div>
          </div>
        </div> 
       
     </div>
 </div>
@endsection
@section('script')
    @can('affairs.refund')
        @include('admin.refund.js')
    @endcan
@endsection