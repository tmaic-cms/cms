@extends('theme::layouts.h5')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
<style>

.aboutus .box1 .as-bgPic {
	@adv($aboutadv group=31 rows=1)
    background: url("{{$aboutadv->thumb}}") no-repeat;
	@endadv
        background-position-x: 0%;
        background-position-y: 0%;
        background-size: auto;
    background-size: cover;
    background-position: left center;
    height: 6rem;
}

</style>
@endsection
@section('topjs')
@endsection
@section('body', 'aboutus')
@section('bodyid', 'top')
@section('main') 
 
<div class="box1">
    <div class="as-bgPic"></div>
    <div class="ai-cont"></div>
  </div>

@channel($about typeid=83 rows=1)
  <div class="box2">
    <div class="title">
      <h2>{{$about->title}}</h2>
      <p>{{$about->sub_title}}</p>
    </div>
    <div class="img">
      <img class="transition" src="{{$about->thumb}}" title="" alt="">
    </div>
    <div class="text">
      {!! $about->body !!}
    </div>
  </div>
@endchannel 
@adv($partner typeid=14 rows=1)
  <div class="box3">
    <div class="title">
      <h2>{{$partner->title}}</h2>
      <p> {{$partner->firsttitle}}</p>
    </div>
	
    <div>
      <span>{{$partner->title}}</span>
      <div class="logobox">
	    @adv($adv group=14)
        <div class="item-box">
          <img src="{{$adv->thumb}}" title="{{$adv->title}}" alt="{{$adv->title}}">
        </div>
		@endadv
      </div>
    </div>
  </div>
@endadv
@endsection
@section('footerjs')
@endsection
