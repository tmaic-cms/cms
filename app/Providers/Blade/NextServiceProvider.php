<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\NextTagService;




class NextServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Request $request)
    {
        
        Blade::directive('next', function ($expression) {

		$NextObject = new NextTagService($expression);
	    //dd($NextObject);
	 
		
		$ListTag = (object)$NextObject->tag;
		
		$parseStr   =  '<?php ';
		 
		$parseStr .= $NextObject->object .' = cache()->remember('.$NextObject->sysTagcachekey.','.$NextObject->tagcachetime.', function (){ ';

		 
		$parseStr .= $NextObject->object .'prevModel = '.$ListTag->model;

		$parseStr .= $NextObject->object .'prevModel = '.$NextObject->object .'prevModel->with("column")->lang();';

 
		 
		$parseStr .= $NextObject->object .'prevModel = '.$NextObject->object .'prevModel->where("slug", ">",request()->route("slug"));';
		
		
		$parseStr .= 'return '.$NextObject->object .' = '.$NextObject->object .'prevModel->orderBy("id","asc")->first(); ';
		
		$parseStr .= '})?>';
	 
		
		if($NextObject->length > 0) $parseStr .='<?php '.$NextObject->object .'->title=cutstr($item->title,'.$NextObject->length.',"'.$ListTag->adorn.'");?>';
		if($NextObject->desclength > 0) $parseStr .='<?php '.$NextObject->object .'->description=cutstr($item->description,'.$NextObject->desclength.',"'.$ListTag->adorn.'");?>';

		 //echo "<pre>";
		 //print_r($parseStr);
		// die;
 
		return  $parseStr;
        });

        Blade::directive('endnext', function () {
            return "<?php    ?>";
        });
 
    }
}
