<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $date 2020-11-13
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\ProductTagServices;

class ProductServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register() {}

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        
        Blade::directive('product', function ($expression) {

		$PRODUCTTAGOBJECT = new ProductTagServices($expression);
 
		$PRODUCTOBJECT = (object)$PRODUCTTAGOBJECT->tag;
		
		$parseStr   =  '<?php ';
 
		
		if($PRODUCTTAGOBJECT->useVal){
			
			 $parseStr .= $PRODUCTTAGOBJECT->object .'Reslut = cache()->remember('.$PRODUCTTAGOBJECT->sysTagcachekey.', '.$PRODUCTTAGOBJECT->tagcachetime.', function () use('.$PRODUCTTAGOBJECT->useVal.'){ ';

		}else{ 
			 $parseStr .= $PRODUCTTAGOBJECT->object .'Reslut = cache()->remember("'.$PRODUCTTAGOBJECT->sysTagcachekey.'", '.$PRODUCTTAGOBJECT->tagcachetime.', function () use($column) { ';

		}
 
		$parseStr .= $PRODUCTTAGOBJECT->object.'PRODUCTMODEL = \App\Models\Product\ProductTranslation::query();';

		$parseStr .= $PRODUCTTAGOBJECT->object.'PRODUCTMODEL = '.$PRODUCTTAGOBJECT->object.'PRODUCTMODEL->with("column")->lang();';

		if(isset($PRODUCTOBJECT->typeid)){
			$parseStr .=$PRODUCTTAGOBJECT->object.'PRODUCTMODEL = '.$PRODUCTTAGOBJECT->object.'PRODUCTMODEL'.$PRODUCTOBJECT->typeid;
		}

		if(isset($PRODUCTOBJECT->orderby)){
			$parseStr .=$PRODUCTTAGOBJECT->object.'PRODUCTMODEL = '.$PRODUCTTAGOBJECT->object.'PRODUCTMODEL'.$PRODUCTOBJECT->orderby;
		}

		
		if(isset($PRODUCTOBJECT->rows)){
			$parseStr .=$PRODUCTTAGOBJECT->object.'PRODUCTMODEL = '.$PRODUCTTAGOBJECT->object.'PRODUCTMODEL'.$PRODUCTOBJECT->rows;
		}
			

		$parseStr .= 'return '.$PRODUCTTAGOBJECT->object .'Reslut  = '.$PRODUCTTAGOBJECT->object.'PRODUCTMODEL->get();});';

 
		$parseStr .=' $count = count('.$PRODUCTTAGOBJECT->object .'Reslut); ';
		
		$parseStr .=$PRODUCTTAGOBJECT->object .'Count = count('.$PRODUCTTAGOBJECT->object .'Reslut); ';
 
		$parseStr .=' foreach('.$PRODUCTTAGOBJECT->object .'Reslut  as '.$PRODUCTTAGOBJECT->object .'key=>$item) { ?>';
		
		if($PRODUCTTAGOBJECT->length > 0){
			$PRODUCTTAGOBJECT->adorn?
			$parseStr .='<?php $item->title=cutstr($item->title,'.$PRODUCTTAGOBJECT->length.',"'.$PRODUCTTAGOBJECT->adorn.'");?>':
			$parseStr .='<?php $item->title=cutstr($item->title,'.$PRODUCTTAGOBJECT->length.');?>';
		}
		
		if($PRODUCTTAGOBJECT->desclength > 0){
			$PRODUCTTAGOBJECT->adorn?
			$parseStr .='<?php $item->description=cutstr($item->description,'.$PRODUCTTAGOBJECT->desclength.',"'.$PRODUCTTAGOBJECT->adorn.'");?>':
			$parseStr .='<?php $item->description=cutstr($item->description,'.$PRODUCTTAGOBJECT->desclength.');?>';
		}
		$parseStr .='<?php '. $PRODUCTTAGOBJECT->object .'= $item; ?>';

		$parseStr .='<?php  $index = '.$PRODUCTTAGOBJECT->object .'key; ?>';
		$parseStr .='<?php  $iteration = '.$PRODUCTTAGOBJECT->object .'key+1; ?>';
		$parseStr .='<?php  '.$PRODUCTTAGOBJECT->object .'key ==0?$first = true:$first=false; ?>';
		$parseStr .='<?php  '.$PRODUCTTAGOBJECT->object .'key == ('.$PRODUCTTAGOBJECT->object .'Count-1)?$last = true:$last=false; ?>';
		$parseStr .='<?php  $remaining = '.$PRODUCTTAGOBJECT->object .'Count-('.$PRODUCTTAGOBJECT->object .'key+1); ?>';
		$parseStr .='<?php  $obj = [
								"index"=>$index,
								"iteration"=>$iteration,
								"first"=>$first,
								"last"=>$last,
								"remaining"=>$remaining
								]; ?>';
		$parseStr .='<?php  '. $PRODUCTTAGOBJECT->object .'loop = (object)$obj; ?>';			

		return $parseStr;
        });

        Blade::directive('endproduct', function () {
            return " <?php } ?>";
        });
 
    }
}
