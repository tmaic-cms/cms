# Tmaic CMS


# translatedIn(?string $locale = null)
返回所有翻译成英语的帖子
Post::translatedIn('en')->get();

# notTranslatedIn(?string $locale = null)
返回所有未翻译成英语的帖子
Post::notTranslatedIn('en')->get();

# translated()
返回未在任何区域设置中翻译的所有文
Post::translated()->get();



# withTranslation()
Eager只为默认和回退（如果启用）区域设置加载转换关系  返回当前语言下的文章
Post::withTranslation()->get();




# listTranslations(string $translationField)
返回一个数组，其中包含一对post id和翻译后的title属性
Post::listsTranslations('title')->get()->toArray();


# listTranslations(string $translationField)
返回一个数组，其中包含一对post id和翻译后的title属性
Post::listsTranslations('title')->get()->toArray();



# where translation
通过对照给定值检查翻译来筛选帖子
whereTranslation(string $translationField, $value, ?string $locale = null)
Post::whereTranslation('title', 'My first post')->first();

# orWhereTranslation(string $translationField, $value, ?string $locale = null)
Post::whereTranslation('title', 'My first post')
    ->orWhereTranslation('title', 'My second post')
    ->get();

# whereTranslationLike
Post::whereTranslationLike('title', '%first%')->first();



# orWhereTranslationLike(string $translationField, $value, ?string $locale = null)
Post::whereTranslationLike('title', '%first%')
    ->orWhereTranslationLike('title', '%second%')
    ->get();


# orderByTranslation(string $translationField, string $sortMethod = 'asc')
按给定的转换列值对模型排序
Post::orderByTranslation('title')->get()



// If a german translation doesn't exist, it returns
// a new instance of CountryTranslation.
$translation = $post->translateOrNew('tu');
例：
if(empty($posts->hasTranslation('tu'))){

	$posts->translateOrNew('tu')->title = "tu title";
	$posts->translateOrNew('tu')->content = "tu content";
	$posts->save();
}



# The eloquent model relationship. Do what you want with it
return $translation = $posts->getNewTranslation('tu');


# 返回所有语言的文章 
$posts->translations();


# 删除所有语言的文章 
$posts->deleteTranslations();

#  Delete one or multiple translations 删除指定的语言的文章
$posts->deleteTranslations('zh');

# Creates a clone and clones the translations

$clones_posts = $posts->replicateWithTranslations(); 


#translationWith

return Column::where('mid',102)->translationWith(['children'=>function($q){
   return $q->where('status','1');
}])->get();

























