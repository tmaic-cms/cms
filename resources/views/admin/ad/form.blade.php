<div class="layui-card-body">
      <input type="hidden" name="id"  value="{{ $position->id??'' }}"> 
   <div class="layui-tab layui-tab-brief" lay-filter="tablist">
      <ul class="layui-tab-title">
        <li class="layui-this" lay-id="base" >基本信息</li>
        <li lay-id="body">详情内容</li>
      </ul>
      <div class="layui-tab-content">
        <div class="layui-tab-item layui-show">
            <div class="layui-form-item">
                <label class="layui-form-label">广告位</label>
                <div class="layui-input-block" style="width:500px">
                    <select name="pid" lay-verify="required" lay-reqtext="产品分类必填项，岂能为空？">
                        <option value="">-选择广告位-</option>
                    @foreach($positions as $item)
                        <option value="{{ $item->sid }}" @if(isset($positions->pid)&&$positions->pid==$item->sid)selected @endif>{{ $item->title }} - {{ $item->remarks }}</option>  
                    @endforeach
        
                    </select>
                </div>
            </div>
 
            <div class="layui-form-item">
                <label class="layui-form-label">广告标题</label>
                <div class="layui-input-block">
                    <input type="text" name="title" value="{{ $position->title ?? old('title') }}" lay-verify="required" placeholder="请输入广告标题" class="layui-input" >
                </div>
            </div>


            <div class="layui-form-item">
                <label class="layui-form-label">副标题</label>
                <div class="layui-input-block">
                    <input type="text" name="firsttitle" value="{{ $position->firsttitle ?? old('firsttitle') }}" placeholder="请输入副标题" class="layui-input" >
                </div>
            </div>
 
            <div class="layui-form-item">
                <label class="layui-form-label">seo关键字</label>
                <div class="layui-input-block">
                    <input name="keywords" placeholder="请输入seo关键字" value="{{$position->keywords??old('keywords')}}" class="layui-input" />
                </div>
            </div>
			
			<div class="layui-form-item">
				<label class="layui-form-label">跳转地址</label>
				<div class="layui-input-block">
					<input type="text" name="link" value="{{ $position->link ?? old('link') }}" autocomplete="off"
						   placeholder="请输入跳转地址:http://" class="layui-input">
				</div>
			</div>
			
            <div class="layui-form-item">
                <label class="layui-form-label">描述</label>
                <div class="layui-input-block">
                    <textarea class="layui-textarea" name="description" id="description" placeholder="请输入描述">{!! $position->description??'' !!}</textarea>
                </div>
            </div>
			
			
			<div class="layui-form-item">
			<div class="layui-inline">
			  <label class="layui-form-label">网络图片</label>
			  <div class="layui-input-inline"  style="width:600px">
				<input type="url" name="downloadurl" id="downloadurl" autocomplete="off" class="layui-input">
			  </div>
			</div>
			<div class="layui-inline">
			  <button type="button" class="layui-btn downloadImage"><i class="layui-icon">&#xe67c;</i>上传</button>
			</div>
			</div>
			<div class="layui-form-item">
				<label for="" class="layui-form-label">封面(PC端)</label>
				<div class="layui-input-block">
					<div class="layui-upload">
						<button type="button" class="layui-btn layui-btn-sm uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
						</button>
						<div class="layui-upload-list">
							<ul class="layui-upload-box layui-clear" id="layui-upload-box">
								@if(isset($position->thumb))
									<li><img src="{{ $position->thumb }}" width="50"/><p>上传成功</p></li>
									@else
									<li><img src="" width="50"/><p></p></li>	
								@endif
							</ul>
							<input type="hidden" name="thumb" id="thumb" class="layui-upload-input"  value="{{ $position->thumb??'' }}">
						</div>
					</div>
				</div>
			</div>
			
			
			<div class="layui-form-item">
			<div class="layui-inline">
			  <label class="layui-form-label">网络图片</label>
			  <div class="layui-input-inline"  style="width:600px">
				<input type="url" name="downloadurlwap" id="downloadurlwap" autocomplete="off" class="layui-input">
			  </div>
			</div>
			<div class="layui-inline">
			  <button type="button" class="layui-btn downloadImageWap"><i class="layui-icon">&#xe67c;</i>上传</button>
			</div>
			</div>
			<div class="layui-form-item">
				<label for="" class="layui-form-label">封面(移动端)</label>
				<div class="layui-input-block">
					<div class="layui-upload">
						<button type="button" class="layui-btn layui-btn-sm uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
						</button>
						<div class="layui-upload-list" id="wapView">
							<ul class="layui-upload-box layui-clear" id="uploadView-upload-box">
								@if(isset($position->wapthumb))
									<li><img src="{{ $position->wapthumb }}" width="100"/><p>上传成功</p></li>
								@else
								<li><img src="" width="50"/><p></p></li>	
								@endif
							</ul>
							<input type="hidden" name="wapthumb" id="wapthumb" class="layui-upload-input"  value="{{ $position->wapthumb??'' }}">
						</div>
					</div>
				</div>
			</div>	

 
        
        </div>




        <div class="layui-tab-item">
         <div class="layui-form-item layui-form-text">
                <main>
                	<div class="centered">
                		<div class="document-editor">
                			<div class="toolbar-container"></div>
                			<div class="content-container">
                				<div id="editor">
                					{!! $position->body??old('body') !!} 
                				</div>
                			</div>
                		</div>
                	</div>
                </main>
        </div>



      </div>
    </div>
</div>
<div class="form-group-bottom text-center">
    <button class="layui-btn" lay-filter="SubmitData" id="SubmitData" lay-submit>&emsp;提交&emsp;</button>
	<button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
</div>

