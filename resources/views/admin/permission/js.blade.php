@include('admin.permission.html')
<script type="text/javascript" src="/assets/js/permission.js"></script>
<script>
    var create = "{{ route('admin.permission.create') }}";
    var edit = "{{ route('admin.permission.edit') }}?id=";
    var update = "{{ route('admin.permission.update') }}";
    var updateAjax = "{{ route('admin.permission.updateAjax') }}";
    var destroy ="{{ route('admin.permission.destroy') }}";
    var columnsData = "{{ route('admin.permission.data') }}";


</script>