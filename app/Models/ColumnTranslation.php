<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models;

use App\Models\BaseModel;
 

class ColumnTranslation extends BaseModel
{

 

    public $timestamps = false;

    protected $fillable = [ 'column_mid','pid', 'sid','model', 'smalltype', 'locale', 'name','ispart','attribute','domain', 'slug', 'issend', 'seo_title', 'seo_key','description', 'thumb','wapthumb','isdisplay', 'covertpl', 'listtpl', 'infotpl', 'inforule', 'listrule','sort', 'status',  'body', 'urls','ismenu','sub_name','isdelete','routename'];

    protected $appends = ['link','ModelType','langType','son','child'];

 
 

	
    //过虑返回记录
    public function scopeCondition($query)
    {
        return $query->where('ismenu',0)->where('status',1);
    }
	//限制返回字段
    public function scopeField($query){
		return $query->select('id','sid','column_mid as son','pid','sid as child','column_mid','locale','name','sub_name','status','routename','body','description','thumb');
	}
	

    //子分类
    public function childs()
    {

        return $this->hasMany('App\Models\ColumnTranslation','pid','sid')->lang();
    }
    //所有子类
    public function children()
    {
        return $this->childs()->with('children');
    }
	
    //所有子类
    public function menu()
    {
        return $this->childs()->with('menu');
    }
	
 
	
    public function class()
    {
        return $this->hasOne('App\Models\Column','mid','column_id');
    }


    //关联案例
    public function cases()
    {
        $locale = request()->get('lang',lang());
        return $this->hasMany('App\Models\Cases\CasesTranslation','column_id','column_mid')->where('locale',$locale);
    }

    //关联产品
    public function product()
    {
        $locale = request()->get('lang',lang());
        return $this->hasMany('App\Models\Product\ProductTranslation','column_id','column_mid')->where('locale',$locale);
    }

    //关联文章
    public function article()
    {
        $locale = request()->get('lang',lang());
        return $this->hasMany('App\Models\ArticleTranslation','column_id','column_mid')->where('locale',lang());
    }


    //生成url 
    public function getLinkAttribute($query)
    {  
	    $value ='';
	    if(\Route::has($this->routename)){
			
			if(app()->environment() !="production"){
				$value = $this->urls?$this->urls:route($this->routename,['model'=>$this->model]);
			}
			$value = $this->urls?$this->urls:route($this->routename);
			
		}
		return $value;
    }
	
    //son
    public function getSonAttribute($query)
    {  
		return $this->column_mid;
    }
	
    //child
    public function getChildAttribute($query)
    {  
		return $this->sid;
    }
    
    public function getModelTypeAttribute($query)
    {  
        switch ($this->model) {
            case "Article":  $value = "文章模型";  break;
            case "Product":  $value = "产品模型";  break;
            case "Case":  	 $value = "案例模型";  break;
            case "About":    $value = "关于模型";  break;
            case "Contact":  $value = "联系模型";  break;
             default: $value = "其他模型";		   break;
        }
        return $value;
    }

    
 







}
