<script>
 layui.use(['layer','formX','element','form', 'table', 'util','laytpl','element','admin','upload'], function () {
 	
	var $ = layui.jquery;
	var layer = layui.layer;
	var form = layui.form;
	var table = layui.table;
	var util = layui.util;
	var admin = layui.admin;
	var laytpl  = layui.laytpl;
	var element = layui.element;
	var formX = layui.formX;
	var upload = layui.upload;

	
	var insTb = table.render({
		elem: '#memberTable',
		skin: 'line',
		size: 'lg',
		url: '{{ route("admin.member.data") }}',
		page: true,
		toolbar: ['<p>',
			'<button lay-event="add" class="layui-btn layui-btn-sm icon-btn"><i class="layui-icon">&#xe654;</i>添加</button>&nbsp;',
			'<button lay-event="del" class="layui-btn layui-btn-sm layui-btn-danger icon-btn"><i class="layui-icon">&#xe640;</i>删除</button>',
			'</p>'].join(''),
		cellMinWidth: 100,
		cols: [[
			{type: 'checkbox'},
			{type: 'numbers'},
			{field: 'username', title: '账号', sort: true},
			{field: 'nickname', title: '用户名', sort: true},
			{field: 'moblie', title: '手机号', sort: true},
			{field: 'address', title: '地址', sort: true},
			{
				field: 'created_at', title: '创建时间', templet: function (d) {
					return util.toDateString(d.created_at);
				}, sort: true
			},
			{field: 'state', title: '状态', templet: '#userTbState', sort: true, width: 100},
			{title: '操作', toolbar: '#memberBar', align: 'left', minWidth: 120}
		]]
	});
	
	/* 表格搜索 */
	form.on('submit(userTbSearch)', function (data) {
		insTb.reload({where: data.field, page: {curr: 1}});
		return false;
	});

	
	/* 表格工具条点击事件 */
	table.on('tool(memberTable)', function (obj) {
		if (obj.event === 'edit') { // 修改
			showEditModel(obj.data);
		} else if (obj.event === 'del') { // 删除
			doDel({ids: obj.data.id});
		} else if (obj.event === 'reset') { // 重置密码
			resetPsw(obj);
		}
	});
	/* 表格头工具栏点击事件 */
	table.on('toolbar(memberTable)', function (obj) {
		if (obj.event === 'add') { // 添加
			showEditModel('');
		} else if (obj.event === 'del') {
			var checkRows = table.checkStatus('memberTable');
			if (checkRows.data.length === 0) {
				var ids = checkRows.data.map(function (d) {
					return d.id;
				});
				doDel({ids: ids});
				return;
			}
			var ids = checkRows.data.map(function (d) {
				return d.id;
			});
			doDel({ids: ids});
		}
	});

    

	/* 删除 */
	function doDel(obj) {
		layer.confirm('确定要删除选中数据吗？', {
			skin: 'layui-layer-admin',
			shade: .1
		}, function (i) {
			layer.close(i);
			var loadIndex = layer.load(2);
			$.post('{{ route("admin.member.destroy") }}', {
				_method: 'delete',
				id: obj.data ? obj.data.id : '',
				ids: obj.ids
			}, function (res) {
				layer.close(loadIndex);
				if (res.code === 0) {
					layer.msg(res.msg, {icon: 1});
					insTb.reload({page: {curr: 1}});
				} else {
					layer.msg(res.msg, {icon: 2});
				}
			}, 'json');
		});
	}

	/* 修改用户状态 */
	form.on('switch(userTbStateCk)', function (obj) {
		var loadIndex = layer.load(2);
		$.get("{{ route('admin.member.updateajax') }}", {
			id: obj.elem.value,
			status: obj.elem.checked ? 1 : 0
		}, function (res) {
			layer.close(loadIndex);
			if (res.code == 0) {
				layer.msg(res.msg, {icon: 1});
			} else {
				layer.msg(res.msg, {icon: 2});
				$(obj.elem).prop('checked', !obj.elem.checked);
				form.render('checkbox');
			}
		}, 'json');
	});
 
 


	/* 显示表单弹窗 */
	function showEditModel(mData) {
		admin.open({
			type: 1,
			area: ['100%', '100%'],
			offset: '0px',
			title: (mData ? '修改' : '添加') + '会员',
			content: $('#EditDialog').html(),
			success: function (layero, dIndex) {
				// 回显表单数据
				form.val('EditForm', mData);
				
				if(mData.avatar){
					layui.$('#uploadView').removeClass('layui-hide').find('img').attr('src', mData.avatar);
					layui.$('#avatar').val(mData.avatar);
				}else{
					 
					layui.$('#uploadViewUpload').html('<i class="layui-icon"></i>');
				}
				
				
				
				//layui.$('#uploadView').attr('src', mData.avatar);
				// 表单提交事件
				form.on('submit(EditSubmit)', function (data) {
					var loadIndex = layer.load(2);
					$.ajax({
						type: "POST",
						url: mData ? "{{ route('admin.member.update') }}" : "{{ route('admin.member.store') }}",
						data: $(this).parents('form').serialize(),
						dataType: 'json',
						success: function (res) {
							layer.close(loadIndex);
							if (res.code === 0) {
								layer.close(dIndex);
								layer.msg(res.msg, {icon: 1});
								insTb.reload({page: {curr: 1}});
							} else {
								layer.msg(res.msg, {icon: 2});
							}

						},
						error: function (msg) {
							layer.close(loadIndex);
							if (msg.status == 422) {
								var json = JSON.parse(msg.responseText);
								json = json.errors;
								for (var item in json) {
									for (var i = 0; i < json[item].length; i++) {
										layer.msg(json[item][i], {icon: 2});
										return;
									}
								}
							} else {
								layer.msg('服务器连接失败', {icon: 2});
								return;
							}
						}
					});
					return false;
				});
				  upload.render({
					elem: '#uploadViewUpload'
					,url: '{{ route("uploadImg") }}'
					,done: function(res){
					  
					  layui.$('#uploadView').removeClass('layui-hide').find('img').attr('src', res.url);
					  layui.$('#avatar').val(res.url);
					  layer.msg('上传成功');
					  console.log(res)
					}
				  });
 
 
 
				// 禁止弹窗出现滚动条
				$(layero).children('.layui-layer-content').css('overflow', 'visible');
			}
		});
	};




   
});
</script>
<!-- 表格状态列 -->
<script type="text/html" id="userTbState">
    <input type="checkbox" lay-filter="userTbStateCk" value="@{{d.id}}" lay-skin="switch"
           lay-text="正常|锁定" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'正常':'锁定'}}</p>
</script>
<script type="text/html" id="memberBar">
    <div class="layui-btn-group">
        @can('member.member.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('member.member.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>
  
  
<!-- 表单弹窗 -->
<script type="text/html" id="EditDialog">
    <form id="EditForm" lay-filter="EditForm" class="layui-form model-form">
        <input name="id" type="hidden" value=""/>
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">账号:</label>
            <div class="layui-input-block">
                <input name="username" placeholder="请输入账号" class="layui-input" lay-verType="tips" lay-verify="required"
                       value=""/>
            </div>
        </div>
 
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">昵称:</label>
            <div class="layui-input-block">
                <input name="nickname" placeholder="请输入昵称" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>
		
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">邮箱:</label>
            <div class="layui-input-block">
                <input name="email" placeholder="请输入email" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>	
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">手机号:</label>
            <div class="layui-input-block">
                <input name="moblie" placeholder="请输入moblie" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>	
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required" lay-verType="tips">地址:</label>
            <div class="layui-input-block">
                <input name="address" placeholder="请输入地址" class="layui-input" lay-verType="tips" lay-verify="required"   required value=""/>
            </div>
        </div>	
 
		<div class="layui-form-item">
			<label for="" class="layui-form-label">头像</label>
			 <div class="layui-upload-drag uploadPic" id="uploadViewUpload">
   
			  <div class="" id="uploadView">
				<img src="" alt="上传成功后渲染" style="max-width: 50px">
			  </div>
 
			  <input type="hidden" name="avatar" id="avatar" class="layui-upload-input"  value="">
			</div>
		</div>
		
  
        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">密码:</label>
            <div class="layui-input-block">
                <input name="password" placeholder="请输入密码" class="layui-input" lay-verType="tips" value=""/>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label layui-form-required">重复密码:</label>
            <div class="layui-input-block">
                <input name="password_confirmation" placeholder="请输入密码" class="layui-input" lay-verType="tips" value=""/>
            </div>
        </div>
 
         
        <div class="layui-form-item text-center">
            <button class="layui-btn" lay-filter="EditSubmit" lay-submit>保存</button>
            <button class="layui-btn layui-btn-primary" type="button" ew-event="closeDialog">取消</button>
        </div>
    </form>
</script>
  

  
