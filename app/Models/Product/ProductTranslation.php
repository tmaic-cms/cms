<?php 
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/    
namespace App\Models\Product;

use App\Models\BaseModel;
 
class ProductTranslation extends BaseModel
{
 
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'product_translations';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
 
    protected $fillable = ['id','column_id','seo_title','product_mid','title','keywords','description','locale','thumb','body','slug','sort','status','hasdelete','click'];
    protected $appends = ['link','langType'];
 
 
 

    public function scopeCondition($query)
    {
        return $query->status()->where('isdelete',0);
    }

 

    //生成url
    public function getLinkAttribute($query)
    {
        return route('product.info',['slug'=>$this->slug]);
    }
    //子分类
    public function childs()
    {
        return $this->hasMany('App\Models\Product\TypesTranslation','types_id','sid');
    }

    //所有子类
    public function children()
    {
        return $this->childs()->with('children');
    }

    //关联栏目
    public function column()
    {
        return $this->hasOne('App\Models\ColumnTranslation','column_mid','column_id')->lang();
    }

    //关联栏目
    public function image()
    {
        return $this->hasMany('App\Models\Product\Atlas','product_id','id');
    }


 



}
  
 