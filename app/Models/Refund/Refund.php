<?php     
namespace App\Models\Refund;

use App\Models\Order\Order;
use Illuminate\Database\Eloquent\Model;
 
class Refund extends Model
{
    
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'refund';

    protected $primaryKey = "rid";
    
    protected $fillable = ['rid','uid','store_id','refund_no','order_no','out_trade_no','refund_status','refund_detail','refund_type','refund_fee','total_money'];
    protected $appends = ['refund_status_name'];
    //关联member
    public function user()
    {
        return $this->hasOne('App\Models\Member','id','uid');
    }
    //关联order
    public function order()
    {
        return $this->hasOne(Order::class,'order_no','order_no');
    }
    //关联order
    public function goods()
    {
        return $this->hasMany("App\Models\Refund\Log",'refundid','rid');
    }

    //关联refund_goods
    public function LogData()
    {
        return $this->hasMany('App\Models\Refund\Log','refundid','rid');
    }

 
    //支付方式式化
    public function getRefundStatusNameAttribute($query)
    {
        
        switch ($this->refund_status) {
            case 1:   $value = "商家审核";  break;
            case 2:   $value = "平台审核";  break;
            case 3:   $value = "拒绝退款";  break;            
            case 4:   $value = "退款完成";  break;
        }
        
        
        return $value;
    }


}
        
 