<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;

use App\Models\Tag;
use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\DB;
class TestController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.test.index');
    }

    public function index2()
    {
        return view('admin.test2.index');
    }

    public function index3()
    {
         
        return view('admin.test3.index');
    }


    public function data(Request $request)
    {
        $res = Tag::orderBy('id','desc')->orderBy('sort','desc')->paginate($request->get('limit',30))->toArray();
        $data = [
            'code' => 0,
            'msg'   => '正在请求中...',
            'count' => $res['total'],
            'data'  => $res['data']
        ];
        return response()->json($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.test.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'name'  => 'required|string',
            'sort'  => 'required|numeric'
        ]);
        if (Tag::create($request->all())){
            return redirect(route('admin.test'))->with(['status'=>'添加完成']);
        }
        return redirect(route('admin.test'))->with(['status'=>'系统错误']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tag = Tag::findOrFail($id);
        return view('admin.test.edit',compact('tag'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,[
            'name'  => 'required|string',
            'sort'  => 'required|numeric'
        ]);
        $tag = Tag::findOrFail($id);
        if ($tag->update($request->only(['name','sort']))){
            return redirect(route('admin.test'))->with(['status'=>'更新成功']);
        }
        return redirect(route('admin.test'))->withErrors(['status'=>'系统错误']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $ids = $request->get('ids');
        if (empty($ids)){
            return response()->json(['code'=>1,'msg'=>'请选择删除项']);
        }
		
	    $goods_tag = DB::table('goods_tag')->whereIn('tag_id',$ids)->count();	 
	    $case_tag = DB::table('case_tag')->whereIn('tag_id',$ids)->count();	 
	    $article_tag = DB::table('article_tag')->whereIn('tag_id',$ids)->count();	 
		
		if(empty($goods_tag) && empty($case_tag) && empty($article_tag)){
			
			if (Tag::destroy($ids)){
				return response()->json(['code'=>0,'msg'=>'删除成功']);
			}
		}
        return response()->json(['code'=>1,'msg'=>'删除失败,标签正在使用中']);
    }





    public function model(Request $request)
    {
        $tables[] = $request->get('t');

        if(!$tables){
            $tables = Schema::getConnection()->getDoctrineSchemaManager()->listTableNames();
        }
        if (!$tables) return 'no';
        foreach ($tables as $table) {
            $columns = Schema::getColumnListing($table);
            $columns = implode("','", $columns);
            $columns = "['$columns']";
            $class = $table;
            $tables = $table;
            $class = explode('_', $class);
            $className = '';
            if (count($class) == 1) {
                $className = $class[0];
            } else {
                for ($i = 1; $i < count($class); $i++) {
                    $className .= ucwords($class[$i]);
                }
            }
            $path = 'Models/' . ucwords($class[0]);

            $path = app_path($path) . "/";
            if (!is_dir($path)) {
                mkdir($path, 0777, true);
            } else {
                chmod($path, 0777);
            }
            $file = ucwords($className) . '.php';
            if (file_exists($path . $file)) {
                echo $tables;
                return;
            }
            $r = fopen($path . $file, 'w');

            $dir = ucwords($class[0]);
            $className = ucwords($className);

            $text = "<?php     
namespace App\Models\\$dir;

use Illuminate\Database\Eloquent\Model;
 
class $className extends Model
{
    
    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected \$table = '$tables';
    
     /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    public \$timestamps = false;
    
    
    protected \$fillable = $columns;
    
    

}
        
 ";
            fwrite($r, $text);
            fclose($r);

        }

        return 'test';
    }






  }

