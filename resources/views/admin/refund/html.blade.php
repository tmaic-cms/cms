<!-- 表格操作列 -->
<script type="text/html" id="userTbBar">
    <a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="edit">修改</a>
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
    <a class="layui-btn layui-btn-warm layui-btn-xs" lay-event="reset">重置密码</a>
</script>



<script type="text/html" id="options">
    <div class="layui-btn-group">
<!--
        @can('affairs.refund.edit')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
-->
        @can('affairs.refund.execute')
            @{{# if(d.refund_status==2){ }}
                <a class="layui-btn layui-btn-sm" lay-event="refund">退款</a>
            @{{# } }}
        @endcan
        @can('affairs.refund.destroy')
            @{{# if(d.refund_status==0){ }}
                 <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
            @{{# } }}
        @endcan
    </div>
</script>



 
 

<!-- 表格操作列 -->
<script type="text/html" id="eDialogTbBar">
    <!-- <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="view"><i class="layui-icon">&#xe6b2;</i>查看</a> -->
    <a class="layui-btn layui-btn-primary layui-btn-sm" lay-event="comments"><i class="layui-icon">&#xe6b2;</i></i>查看</a>
</script>

<!-- 查看评论弹窗 -->
<script type="text/html" id="eDialogCommentDialog">
    <table id="eDialogCommentTable" lay-filter="eDialogCommentTable" class="layui-hide"></table>
    <div class="btn-circle" id="eDialogCommentBtnAdd" style="position: absolute; bottom: 60px;" title="发表评论">
        <i class="layui-icon layui-icon-edit"></i>
    </div>
</script>



 <!-- 查看表格操作列 -->
<script type="text/html" id="eDialogCommentTbBar">
    <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>





 

<!-- 查看支付状态 -->
<script id="eDialogCheckDialog" type="text/html">
    <div style="padding: 25px 0 0 30px;">
        <ul class="layui-timeline">
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">曲丽丽</h3>&emsp;2019-06-03 13:29
                    </div>
                    <p>提交了外出申请</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            林东东&nbsp;<span class="layui-badge layui-bg-green">组长</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请（速去速回！）</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            周星星&nbsp;<span class="layui-badge layui-bg-blue">经理</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请</p>
                </div>
            </li>
            <li class="layui-timeline-item">
                <i class="layui-icon layui-timeline-axis">&#xe63f;</i>
                <div class="layui-timeline-content layui-text">
                    <div class="layui-timeline-title">
                        <h3 class="inline-block">
                            付小小&nbsp;<span class="layui-badge layui-bg-orange">人事</span>
                        </h3>&emsp;2019-06-03 13:48
                    </div>
                    <p>同意了申请</p>
                </div>
            </li>
        </ul>
    </div>
</script>


 

<!-- 表格状态列 -->
<script type="text/html" id="refund_status">
    <input type="checkbox"  value="@{{d.rid}}" lay-skin="switch" lay-text="正常|锁定" @{{d.refund_status==0?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.refund_status==0?'正常':'已锁定'}}</p>
</script>