@if($isMobile)
{{--
  <div class="foot-sidebar__plug">
    <div class="fsp-row fsp-row1">
      <a href="###">
        <span>在线<br>咨询</span>
      </a>
    </div>
    <div class="fsp-row fsp-row2">
      <a href="###">
        <span class="iconfont">&#xe6ee;</span>
      </a>
      <div class="fsp-inner">
        <a href="@phome" id="telBtn">
          +86 @phome
        </a>
      </div>
    </div>
    <div class="fsp-row fsp-row3">
      <a href="###">
        <span class="iconfont">&#xe615;</span>
      </a>
      <div class="fsp-inner">
        <img src="/picture/wx-code.jpg" alt="">
      </div>
    </div>
    <div class="fsp-row fsp-row4">
      <a href="#top" class="back-top">
        <span class="iconfont">&#xe601;</span>
      </a>
    </div>
  </div>
  --}}
 
 <footer class="footer">
    <div class="footer-main">
      <div class="foot-top__cont">
        <div class="foot-share">
          <a href="###" class="icon-linkedin"><i class="iconfont">&#xe61d;</i></a>
          <a href="###" class="icon-wb"><i class="iconfont">&#xe83f;</i></a>
          <a href="###" class="icon-wx">
            <i class="iconfont">&#xe503;</i>
            <img src="{{webconfig('server_wx')}}" title="二维码" alt="二维码">
          </a>
        </div>
      </div>
      <div class="foot-middle__cont">
        <div class="foot-nav">
		 @channel($footerNav typeid=102,84,85,87,88 rows=6)
          <a href="{{$footerNav->link}}">{{$footerNav->name}}</a>
		  @endchannel
        </div>
        @ICP
      </div>
    </div>
  </footer>
  
@else
<div class="foot-sidebar__plug">
    <div class="fsp-row fsp-row1">
      <a href="###">
        <span>在线<br>咨询</span>
      </a>
    </div>
    <div class="fsp-row fsp-row2">
      <a href="###">
        <span class="iconfont">&#xe6ee;</span>
      </a>
      <div class="fsp-inner">
        <a href="###" id="telBtn">
          +86 @phone
        </a>
      </div>
    </div>
    <div class="fsp-row fsp-row3">
      <a href="###">
        <span class="iconfont">&#xe615;</span>
      </a>
      <div class="fsp-inner">
        <img src="{{webconfig('server_wx')}}" alt="官网微信">
      </div>
    </div>
    <div class="fsp-row fsp-row4">
      <a href="#top" class="back-top">
        <span class="iconfont">&#xe601;</span>
      </a>
    </div>
  </div>
 
 <footer>
    <div class="footer-main">
      <div class="foot-top__cont">
        <a href="{{ route('home')}}" class="foot-logo">
          <img src="@logo" title="logo" alt="logo">
        </a>
        <div class="foot-share">
          <a href="###" class="icon-linkedin">
            <span class="iconfont">&#xe61d;</span>
          </a>
          <a href="###" class="icon-wb">
            <span class="iconfont">&#xe83f;</span>
          </a>
          <a href="###" class="icon-wx">
            <span class="iconfont">&#xe501;</span>
            <img src="{{webconfig('server_wx')}}" title="官网微信" alt="官网微信">
          </a>
        </div>
      </div>
      <div class="foot-middle__cont">
        <div class="fm-left__area">
          <ul class="fm-left__list">
            <li>
              <div class="fm-item">
                <dl>
				  @adv($advCase typeid=57)
                  <dt>{{$advCase->title}}</dt>
				  @endadv
				  @adv($advCaseList group=57 rows=10)
                  <dd>
                    <a href="{{$advCaseList->link}}">
						{{$advCaseList->title}}
                    </a>
                  </dd>
                  @endadv 
                </dl>
              </div>
            </li>
            <li>
              <div class="fm-item">
                <dl>
				  @adv($advDesign typeid=40)
                  <dt>{{$advDesign->title}}</dt>
				  @endadv
				  @adv($advDesignList group=40 rows=10)
                  <dd>
                    <a href="{{$advDesignList->link}}">
						{{$advDesignList->title}}
                    </a>
                  </dd>
				  @endadv
                </dl>
              </div>
            </li>
            <li>
              <div class="fm-item">
                <dl>
				  @adv($investment typeid=60)
                  <dt>{{$investment->title}}</dt>
				  @endadv
				  @adv($investmentList group=60 rows=10)
                  <dd>
                    <a href="{{$advDesignList->link}}">
						{{$investmentList->title}}
                    </a>
                  </dd>
				  @endadv
                </dl>
              </div>
            </li>
			 
            <li>
              <div class="fm-item">
                <dl>
				  @adv($investment typeid=63)
                  <dt>{{$investment->title}}</dt>
				  @endadv
				  @adv($investmentList group=63 rows=10)
                  <dd>
                    <a href="{{$advDesignList->link}}">
						{{$investmentList->title}}
                    </a>
                  </dd>
				  @endadv
                </dl>
              </div>
            </li>
 
            <li>
              <div class="fm-item">
                <dl>
                  <dt>联系方式</dt>
                  <dd>
                    联系电话：+86 @phone
                  </dd>
                  <dd>
                    商务直线：+86 @phone
                  </dd>
                  <dd>
                    联系邮箱：<a href="mailto:@email">@email</a></a>
                  </dd>
                  
                </dl>
              </div>
            </li>
          </ul>
        </div>
        <div class="fm-right__area">
          <div class="fr-item">
            <span>@ds_address</span>
            <p>@address</p>
          </div>
         @ICP
        </div>
      </div>
	  
		@if($routename != "home")
			  <div class="foot-bottom__cont">
				<span>友情链接： </span>
				<div>
				@links($link)
				  <a href="{{ $link->url }}" target="_blank">{{$link->name}}</a>
				@endlinks
				</div>
			  </div>
			
		@endif
	  
	  
	  

    </div>
  </footer>


@endif

