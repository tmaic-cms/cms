@extends('admin.login_register.base')
@section('content')
<div class="login-wrapper layui-anim layui-anim-scale layui-hide">
    <form class="layui-form">
     {{csrf_field()}}
        <h2>{{ trans('app.login') }}</h2>
        <div class="layui-form-item layui-input-icon-group">
            <i class="layui-icon layui-icon-username"></i>
            <input class="layui-input" name="username" placeholder="请输入登录账号" autocomplete="off"  lay-verify="required" required/>
        </div>
        <div class="layui-form-item layui-input-icon-group">
            <i class="layui-icon layui-icon-password"></i>
            <input class="layui-input" name="password" placeholder="请输入登录密码" type="password"  lay-verType="tips" lay-verify="required" required/>
        </div>
{{--
        <div class="layui-form-item layui-input-icon-group login-captcha-group">
            <i class="layui-icon layui-icon-auz"></i>
            <input class="layui-input" name="code" placeholder="请输入验证码" autocomplete="off"
                   lay-verType="tips" lay-verify="required" required/>
            <img class="login-captcha" alt=""/>
        </div>

        <div class="layui-form-item">
            <input type="checkbox" name="remember" title="记住密码" lay-skin="primary" checked>
            <a href="reg.html" class="layui-link pull-right">{{ trans('app.registered') }}</a>
        </div>
--}}
        <div class="layui-form-item">
            <button class="layui-btn layui-btn-fluid" lay-filter="loginSubmit" lay-submit>{{ trans('app.login') }}</button>
        </div>
		{{--
        <div class="layui-form-item login-oauth-group text-center">
            <a href="javascript:;"><i class="layui-icon layui-icon-login-qq" style="color:#3492ed;"></i></a>&emsp;
            <a href="javascript:;"><i class="layui-icon layui-icon-login-wechat" style="color:#4daf29;"></i></a>&emsp;
            <a href="javascript:;"><i class="layui-icon layui-icon-login-weibo" style="color:#CF1900;"></i></a>
        </div>
		--}}
    </form>
</div>
<div class="login-copyright">@copyright</div>
@endsection
@section('script')
<script>
    layui.use(['layer', 'form'], function () {
        var $ = layui.jquery;
        var layer = layui.layer;
        var form = layui.form;
        $('.login-wrapper').removeClass('layui-hide');
        form.on('submit(loginSubmit)', function (obj) {
            console.log(obj.field);

            var loadIndex = layer.load(2);
            $.post("{{route('admin.authenticate')}}", obj.field, function (res) {
                layer.close(loadIndex);
                if (res.code == 0) {
                    layer.msg(res.msg, {icon: 1, time: 1000}, function () {
                        location.replace(res.url);
                    });
                } else {
                    layer.msg(res.msg, {icon: 2, anim: 6});
                }
            }, 'json');
            return false;
        });
        {{-- 图形验证码 
        var captchaUrl = 'http://shiro.easyweb.vip/assets/captcha';
        $('img.login-captcha').click(function () {
            this.src = captchaUrl + '?t=' + (new Date).getTime();
        }).trigger('click');--}}
    });
</script>
@endsection