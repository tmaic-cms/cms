@extends('theme::layouts.h5')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
@endsection
@section('topjs')
@endsection
@section('body', 'case')
@section('bodyid', 'top')
@section('main') 
<div class="box1">
    <ul>
	 @foreach($topColum as $casecolumn)
      <li class="transition @if($casecolumn->column_mid ==$column->column_mid) active @endif">
        <a href="{{$casecolumn->link}}"><h1>{{$casecolumn->name}}</h1></a>
      </li>
	 @endforeach
    </ul>
    <div class="caselist">
		<div class="casenr transition active">
			<ul>
			@cases($cases typeid=$column->column_mid orderby=date page=15)
			  <li>
			   <a href="{{$cases->link}}">
				  <img src="{{$cases->thumb}}" title="{{$cases->title}}" alt="{{$cases->seo_title}}" />
				  <p class="transition">{{$cases->title}}</p>
				</a>
			  </li>
			@endcases
			</ul>
        {{$casespage->links()}}
		</div>	  
    </div>
</div>
@endsection
@section('footerjs')
@endsection










