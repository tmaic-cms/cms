@extends('admin.base')
@section('content')
<!-- 正文开始 -->
@include('admin.goodstype.form')
@endsection
@section('script')
<script>
layui.use(['form', 'formX','element','upload'], function () {
    var $ = layui.jquery;
    var form = layui.form;
    var formX = layui.formX;
    var upload = layui.upload;
    
 
    
    
    //自定义验证规则
    form.verify({
    otherReq: function(value,item){
      var verifyName=$(item).attr('name')
        ,verifyType=$(item).attr('type')
        ,formElem=$(item).parents('.layui-form')
        ,verifyElem=formElem.find('input[name='+verifyName+']')
        ,isTrue= verifyElem.is(':checked')//是否命中校验
        ,focusElem = verifyElem.next().find('i.layui-icon');
        if(!isTrue || !value){
        //定位焦点
        focusElem.css(verifyType=='radio'?{"color":"#FF5722"}:{"border-color":"#FF5722"});
        //对非输入框设置焦点
        focusElem.first().attr("tabIndex","1").css("outline","0").blur(function() {
            focusElem.css(verifyType=='radio'?{"color":""}:{"border-color":""});
         }).focus();
          return '栏目属性必填项,且不能为空';
        }
    }
    });
    
    
    form.on('submit(SubmitForm)', function (data) {
 
        $.ajax({
            type: "POST",
            url: '{{ route("admin.goodstype.store") }}',
            data: $(this).parents('form').serialize(),
            dataType: 'json',
            success: function (res) {
                if (res.code === 0) {
                    layer.msg(res.msg, {icon: 1});
                    insTb.reload({page: {curr: 1}});
                } else {
                    layer.msg(res.msg, {icon: 2});
                }
            },
            error: function (msg) {
    
                if (msg.status == 422) {
                    var json = JSON.parse(msg.responseText);
                    json = json.errors;
                    for (var item in json) {
                        for (var i = 0; i < json[item].length; i++) {
                            layer.msg(json[item][i], {icon: 2});
                            return;
                        }
                    }
                } else {
                    layer.msg('服务器连接失败', {icon: 2});
                    return;
                }
            }
        });
        return false;
    });
    $('#closeFormBtn').click(function () {
        $(this).parent().parent().parent().remove();
    });
    
    
    //普通图片上传
    $(".uploadPic").each(function (index,elem) {
        upload.render({
            elem: $(elem)
            ,url: '{{ route("uploadImg") }}'
            ,multiple: false
            ,data:{"_token":"{{ csrf_token() }}"}
            ,done: function(res){
                //如果上传失败
                if(res.code == 0){
                    $(elem).parent('.layui-upload').find('.layui-upload-box').html('<li><img src="'+res.url+'"  width="100"/><p>上传成功</p></li>');
                    $(elem).parent('.layui-upload').find('.layui-upload-input').val(res.url);
                    layer.msg(res.msg,{icon:1})
                }else {
                    layer.msg(res.msg,{icon:2})
                }
            }
        });
    });
    


});
</script>
@endsection