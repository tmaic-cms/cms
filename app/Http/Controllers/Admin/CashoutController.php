<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Http\Controllers\Admin;


use Illuminate\Http\Request;
use App\Http\Controllers\AdminController;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;
use App\Models\Order\Order;
use App\Models\Order\Goods;
use App\Models\Order\Bill;
use DB;

class CashoutController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
         
        return view('admin.cashout.index');
    }

 
    public function data(Request $request)
    {

        $username = $request->get('username')??null;
        $phone = $request->get('phone')??null;
        $created_at = $request->get('created_at')??null;     
        $model = Bill::query();
        if ($request->get('order_no')) {
            $model = $model->where('order_bill.order_no', 'like', '%' . $request->get('order_no') . '%');
        }

        if ($request->get('status')) {
            $status = $request->get('status');
            switch ($request->get('status'))
            {
                case "1":  $model = $model->where('status',1);  break; //提现申请
                case "2":  $model = $model->where('status',2);  break; //提现审核
                case "3":  $model = $model->where('status',3);  break; //已结算
                case "4":  $model = $model->where('status',4);  break; //退回
        
            }
        }       
        if(!empty($username)){
           $model = $model->where('member.username', 'like', "%{$username}%");
        } 
        
        if(!empty($phone)){
           $model = $model->where('member.moblie', 'like', "%{$phone}%");
        }    

        if(!empty($created_at)){
            $startime = substr($created_at,0,20);
            $endtime =  substr($created_at,-20,20);
            $model = $model->whereBetween('order_bill.created_at',[$startime,$endtime]);
        } 

        $res = $model->orderBy('order_bill.order_no','desc')->paginate(8);//$request->get('limit', 8)

        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res->total(),
            'data' => $res->items()
        ];
        return response()->json($data);
    }


    public function goodsData(Request $request)
    {
        $model = Goods::query();
        if ($request->get('id')) {
            $model = $model->where('order_id',$request->get('id'));
        }
        $res = $model->orderBy('id','asc')->paginate($request->get('limit', 10));
        $data = [
            'code' => 0,
            'msg' => '正在请求中...',
            'count' => $res['total'],
            'data' => $res->items()
        ];
        return response()->json($data);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {

        $data = $request->except(['userRole','password_confirmation','_token']);
        $data['uuid'] = \Faker\Provider\Uuid::uuid();
        $data['password'] = bcrypt($data['password']);

        if ($user = User::create($data)) {
            if($request->get('userRole')){
                $roles = explode(',',$request->get('userRole'));
                if ($roles)   $user->syncRoles($roles);
            }
            return response()->json(['code' => 0, 'msg' => '添加用户成功']);
        }
        return response()->json(['code' => 1, 'msg' => '系统错误']);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $id = (int)$request->id;
        $Bill = Bill::findOrFail($id);
        return view('admin.cashout.edit', compact('Bill'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $id = (int)$request->get('id');
        $data = $request->all();

        DB::beginTransaction();

        try{
          $Bill = Bill::findOrFail($id);
          $data['status']=4;
          $data['end_date']=now();
          $data['is_delete']=time();


          $Bill->update($data);
           DB::commit();
            return response()->json(['code' => 0, 'msg' => '结算成功']);
        }catch (\Exception $e) {
            DB::rollBack();
            return response()->json(['code' => 1, 'msg' => '更新失败']);
        }
    }






    /**
     * 审核通过
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update_a(Request $request)
    {
        $ids = (int)$request->get('id');

        if (empty($ids)) {
            return response()->json(['code' => 1, 'msg' => '请选择项']);
        }
        if (Bill::where('id',$ids)->update(['status'=>2])) {
            return response()->json(['code' => 0, 'msg' => '审核成功']);
        }
        return response()->json(['code' => 1, 'msg' => '审核失败']);
    }



    /**
     * 退回
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update_b(Request $request)
    {
        $ids = (int)$request->get('ids');

        if (empty($ids)) {
            return response()->json(['code' => 1, 'msg' => '请选择项']);
        }
        $Bill = Bill::find($ids);
        $order = \App\Models\Order\Order::where('out_trade_no',$Bill->trade_no)->first();
        
        DB::beginTransaction();
        try{
            $orderStatus = $order->where('order_id',$order->order_id)->update(['is_settlement'=>0]);
            if ($orderStatus && Bill::where('id',$ids)->delete()) {
                DB::commit();
                return response()->json(['code' => 0, 'msg' => '退回成功']);
            }
        }catch (\Exception $e) {
            DB::rollBack();
            
        }



        return response()->json(['code' => 1, 'msg' => '退回失败']);
    }



    /**
     * Update ajax.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function updateAjax(Request $request)
    {
        $id = $request->get('id');
        $user = User::findOrFail($id);

        $data = $request->except('state');

        if ($user->update($data)) {
            return response()->json(['code' => 0, 'msg' => '状态更新成功']);
        }

        return response()->json(['code' => 1, 'msg' => '状态更新失败']);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       return $ids = $request->get('ids');

        if (empty($ids)) {
            return response()->json(['code' => 1, 'msg' => '请选择删除项']);
        }
        if (User::destroy($ids)) {
            return response()->json(['code' => 0, 'msg' => '删除成功']);
        }
        return response()->json(['code' => 1, 'msg' => '删除失败']);
    }

 
 

 
 

}
