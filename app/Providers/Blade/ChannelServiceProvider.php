<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
*/
namespace App\Providers\Blade;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Blade;
use Illuminate\Http\Request;
use App\Services\ChannelTagServices;
 
class ChannelServiceProvider extends ServiceProvider
{
/**
 * Register services.
 *
 * @return void
 */
public function register() {}

/**
 * Bootstrap services.
 *
 * @return void
 */
public function boot()
{
        

	Blade::directive('channel', function ($expression) {

		$CHANNELTAG = new ChannelTagServices($expression);
		 //dd($CHANNELTAG);
		$CHANNELTAGOBJECT = (object)$CHANNELTAG->tag;
		
		$parseStr  = $parseStr   ='<?php ';
 
        if(isset($CHANNELTAG->useVal)){
			
			$parseStr .= $CHANNELTAG->object .'COLUMN = cache()->remember('.$CHANNELTAG->Tagcachekey.', '.$CHANNELTAG->tagcachetime.', function () use('.$CHANNELTAG->useVal.'){ ';

		}else{
			$parseStr .= $CHANNELTAG->object .'COLUMN = cache()->remember("'.$CHANNELTAG->Tagcachekey.'", '.$CHANNELTAG->tagcachetime.', function ()use($column) { ';

		}
 
		$parseStr .= $CHANNELTAG->object.'ChannelModel = \App\Models\ColumnTranslation::query();';
		
		$parseStr .=$CHANNELTAG->object.'ChannelModel  = '.$CHANNELTAG->object.'ChannelModel->field()->lang()->condition();';
		
		$parseStr .=  $CHANNELTAG->object.'ChannelModel= '.$CHANNELTAG->object.'ChannelModel->with(["children"=>function($q){$q->field();}]);';

		if(isset($CHANNELTAGOBJECT->typeid))  $parseStr .=$CHANNELTAG->object.'ChannelModel = '.$CHANNELTAG->object.'ChannelModel'.$CHANNELTAGOBJECT->typeid;
		
		if(isset($CHANNELTAGOBJECT->group))   $parseStr .=$CHANNELTAG->object.'ChannelModel = '.$CHANNELTAG->object.'ChannelModel'.$CHANNELTAGOBJECT->group;
		
		if(isset($CHANNELTAGOBJECT->rows))    $parseStr .=$CHANNELTAG->object.'ChannelModel = '.$CHANNELTAG->object.'ChannelModel'.$CHANNELTAGOBJECT->rows;

		if(isset($CHANNELTAGOBJECT->orderby)) $parseStr .=$CHANNELTAG->object.'ChannelModel = '.$CHANNELTAG->object.'ChannelModel'.$CHANNELTAGOBJECT->orderby;


		$parseStr .='return '.$CHANNELTAG->object .'COLUMN = '.$CHANNELTAG->object .'ChannelModel->get();});';

 
 
		 
        //统计结果个数
		$parseStr .=$CHANNELTAG->object .'Count = count('.$CHANNELTAG->object .'COLUMN); ';
 
		$parseStr .= ' foreach('.$CHANNELTAG->object .'COLUMN as '. $CHANNELTAG->object .'key=>$item) { ?>';
		
		$parseStr .= '<?php '. $CHANNELTAG->object .'= $item; ?>';
		
		if(!empty($CHANNELTAG->navObj)) $parseStr .= '<?php '. $CHANNELTAG->navObj .'= $item->children; ?>';
		
			$parseStr .='<?php  '. $CHANNELTAG->object .'index =    '. $CHANNELTAG->object .'key; ?>';
			$parseStr .='<?php  '. $CHANNELTAG->object .'iteration ='. $CHANNELTAG->object .'key+1; ?>';
			$parseStr .='<?php  '. $CHANNELTAG->object .'key ==0?'.    $CHANNELTAG->object .'first = true:'. $CHANNELTAG->object .'first=false; ?>';
			$parseStr .='<?php  '. $CHANNELTAG->object .'key == ('.    $CHANNELTAG->object .'Count-1)?'. $CHANNELTAG->object .'last = true:'. $CHANNELTAG->object .'last=false; ?>';
			$parseStr .='<?php  '. $CHANNELTAG->object .'remaining = '.$CHANNELTAG->object .'Count-('. $CHANNELTAG->object .'key+1); ?>';
			$parseStr .='<?php  
								$obj = [
									"index"=>		'. $CHANNELTAG->object .'index,
									"iteration"=>	'. $CHANNELTAG->object .'iteration,
									"first"=>		'. $CHANNELTAG->object .'first,
									"last"=>		'. $CHANNELTAG->object .'last,
									"remaining"=>	'. $CHANNELTAG->object .'remaining
								];?>';
			$parseStr .='<?php  '. $CHANNELTAG->object .'loop = (object)$obj; ?>'; 	

		return $parseStr;
		
	});

	
	Blade::directive('endchannel', function () {
		return " <?php } ?>";
	});

}
 

}//end class
