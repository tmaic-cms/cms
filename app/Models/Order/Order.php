<?php
/**
 * tmaic.com 
 * ============================================================================
 * * 版权所有 2020-2030 tmaic，并保留所有权利。
 * 网站地址: http://www.tmaic.com；
 * ----------------------------------------------------------------------------
 * 这不是一个自由软件！您只能在不用于商业目的的前提下对程序代码进行修改和
 * 使用；不允许对程序代码以任何形式任何目的的再发布。
 * ============================================================================
 * $Author: pangxianfei
 * $email:421339244@qq.com
 * $phone:18929142226
 * $date 2020-11-13
*/
namespace App\Models\Order;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{

    /**
     * 关联到模型的数据表
     *
     * @var string
     */
    protected $table = 'order';
    protected $primaryKey="order_id";
    /**
     * 表明模型是否应该被打上时间戳
     *
     * @var bool
     */
    //public $timestamps = false;


    protected $fillable = ['order_id', 'order_no', 'store_id', 'store_name', 'order_name', 'order_from', 'order_type', 'out_trade_no', 'order_status', 'order_status_action', 'pay_status', 'delivery_status', 'refund_status', 'pay_type', 'delivery_type', 'member_id', 'address_id', 'buyer_ip', 'buyer_ask_delivery_time', 'buyer_message', 'coupon_id', 'coupon_money', 'goods_money', 'price', 'order_money', 'pay_time', 'delivery_time', 'sign_time', 'finish_time', 'close_time', 'is_lock', 'is_delete', 'is_enable_refund', 'remark', 'goods_num', 'delivery_status_name', 'is_settlement', 'refund_money'];

    protected $appends = ['pay_status_name','pay_type_name','id','pay_time_format'];


    public function scopeCondition($query, $uid)
    {
        return $query->where('member_id', $uid);
    }

    public function scopePay($query, $status)
    {
        return $query->where('pay_status', $status);
    }

    public function scopeStatus($query, $status)
    {
        return $query->where('order_status', $status);
    }

    
    public function getIdAttribute($query)
    {   
        return $this->order_id;
    }

    public function getPayTimeFormatAttribute($query)
    {   
        return $this->pay_time?date("Y-m-d H:i:s",$this->pay_time):'';
    }


    //支付状态格式化
    public function getPayStatusNameAttribute($query)
    {
        switch ($this->pay_status) {
            case 1:
                $value = "已支付";
                break;
            default: $value = "未付款";
        }
        return $value;
    }

    //支付方式式化
    public function getPayTypeNameAttribute($query)
    {
        switch ($this->pay_type) {
            case 1:   $value = "微信";  break;
            case 2:   $value = "支付宝";  break;
            default: $value = "未付款";
        }
        return $value;
    }


    //关联order_goods
    public function children()
    {
        return $this->hasMany('App\Models\Order\Goods','order_id','order_id');
    }

    //关联member
    public function user()
    {
        return $this->hasOne('App\Models\Member','id','member_id');
    }


}
        
 