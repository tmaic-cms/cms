@extends('admin.base')

@section('content')
    <div class="layui-card">
        <div class="layui-card-header layuiadmin-card-header-auto">
            <h2>添加友情链接</h2>
        </div>
        <div class="layui-card-body">
            <form class="layui-form" action="{{route('admin.link.store')}}" method="post">
                @include('admin.link._form')
            </form>
        </div>
    </div>
@endsection

@section('script')
<script>
layui.use(['element','form','upload'],function () {
    var $ = layui.jquery;
    var form = layui.form;
    var upload = layui.upload;
    var element = layui.element;


    //普通图片上传
    $(".uploadPic").each(function (index,elem) {
        upload.render({
            elem: $(elem)
            ,url: '{{ route("uploadImg") }}'
            ,multiple: false
            ,data:{"_token":"{{ csrf_token() }}"}
            ,done: function(res){
                //如果上传失败
                if(res.code == 0){
                    $(elem).parent('.layui-upload').find('.layui-upload-box').html('<li><img src="'+res.url+'"  width="100"/><p>上传成功</p></li>');
                    $(elem).parent('.layui-upload').find('.layui-upload-input').val(res.url);
                    layer.msg(res.msg,{icon:1})
                }else {
                    layer.msg(res.msg,{icon:2})
                }
            }
        });
    });
	












});
</script>
@endsection