
<script type="text/html" id="options">
    <div class="layui-btn-group">
        @can('system.permission.create')
            <a class="layui-btn layui-btn-sm" lay-event="edit">编辑</a>
        @endcan
        @can('system.permission.destroy')
            <a class="layui-btn layui-btn-danger layui-btn-sm " lay-event="del">删除</a>
        @endcan
    </div>
</script>
<!-- 表格状态列 -->
<script type="text/html" id="tbState">
    <input type="checkbox" lay-filter="tbState" value="@{{d.id}}" lay-skin="switch" lay-text="正常|禁用" @{{d.status==1?'checked':''}} style="display: none;"/>
    <p style="display: none;">@{{d.status==1?'正常':'锁定'}}</p>
</script>

 
 


<script type="text/html" id="icon">
      <i class="layui-icon @{{ d.icon.class }}"></i>
</script>