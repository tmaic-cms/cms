{{csrf_field()}}



<div class="layui-form-item">
    <label for="" class="layui-form-label">名称</label>
    <div class="layui-input-inline">
        <input type="text" name="name" value="{{ $link->name ?? old('name') }}" lay-verify="required" placeholder="请输入名称" class="layui-input" >
    </div>
</div>





<div class="layui-form-item">
	<label for="" class="layui-form-label">封面</label>
	<div class="layui-input-block">
		<div class="layui-upload">
			<button type="button" class="layui-btn layui-btn-sm uploadPic"><i class="layui-icon">&#xe67c;</i>图片上传
			</button>
			<div class="layui-upload-list">
				<ul class="layui-upload-box layui-clear">
					@if(isset($link->thumb))
						<li><img src="{{ $link->thumb }}" width="100"/>
							<p>上传成功</p></li>
					@endif
				</ul>
				<input type="hidden" name="thumb" class="layui-upload-input"  value="{{ $link->thumb??'' }}">
			</div>
		</div>
	</div>
</div>






<div class="layui-form-item">
    <label for="" class="layui-form-label">跳转地址</label>
    <div class="layui-input-inline">
        <input type="text" name="url" value="{{ $link->url ?? old('url') }}" lay-verify="required" placeholder="请输入URL跳转地址" class="layui-input" >
    </div>
</div>


<div class="layui-form-item">
    <label for="" class="layui-form-label">排序</label>
    <div class="layui-input-inline">
        <input type="number" name="sort" value="{{ $link->sort ?? 10 }}" lay-verify="required|number" placeholder="请输入数字" class="layui-input" >
    </div>
</div>
  <div class="layui-form-item">
    <label class="layui-form-label">是否启用</label>
	
	
	@if(isset($link)) 
    <div class="layui-input-block">
      <input type="radio" name="status" value="1" @if($link->status == 1) checked  @endif title="启用">
      <input type="radio" name="status" value="0" @if($link->status == 0) checked  @endif title="禁用">
    </div>
	@else
    <div class="layui-input-block">
      <input type="radio" name="status" value="1"  title="启用">
      <input type="radio" name="status" value="0"  title="禁用">
    </div>
	@endif
	
	
	
  </div> 
<div class="layui-form-item">
    <div class="layui-input-block">
        <button type="submit" class="layui-btn" lay-submit="" lay-filter="formDemo">确 认</button>
        <a  class="layui-btn" href="{{route('admin.link')}}" >返 回</a>
    </div>
</div>