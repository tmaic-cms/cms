@extends('theme::layouts.app')
@section('title'){{$seo_title}}@endsection
@section('keyword'){{$keyword}}@endsection
@section('description'){{$description}}@endsection
@section('css')
<link href="/css/tinpage-1.0.css" rel="stylesheet" type="text/css">
<link href="/css/fancybox.css" rel="stylesheet" type="text/css" />
<link href="/css/inpage.css" rel="stylesheet" type="text/css">
 
<style>
       
   /*    
    #laymod_313392{
		width:600px;float:right;
	}   
      */ 
 #laymod_313392 #container .content-window-card {
            position: relative;
            box-shadow: none;
            bottom: 0;
            left: 0;
            width: auto;
            padding: 0;
        }

        #laymod_313392 #container .content-window-card p {
            height: 2rem;
        }

        #laymod_313392 .custom-info {
            border: solid 1px silver;
        }

       #laymod_313392  .map #container div.info-top {
            position: relative;
            background: none repeat scroll 0 0 #F9F9F9;
            border-bottom: 1px solid #CCC;
            border-radius: 5px 5px 0 0;
        }

       #laymod_313392 #container div.info-top div {
            display: inline-block;
            color: #333333;
            font-size: 14px;
            font-weight: bold;
            line-height: 31px;
            padding: 0 10px;
        }

       #laymod_313392 #container div.info-top img {
            position: absolute;
            top: 10px;
            right: 10px;
            transition-duration: 0.25s;
        }

        #laymod_313392 #container div.info-top img:hover {
            box-shadow: 0px 0px 5px #000;
        }

       #laymod_313392 #container div.info-middle {
            font-size: 12px;
            padding: 10px 6px;
            line-height: 20px;
        }

        #laymod_313392 #container div.info-bottom {
            height: 0px;
            width: 100%;
            clear: both;
            text-align: center;
        }

       #laymod_313392 #container div.info-bottom img {
            position: relative;
            z-index: 104;
        }

        #laymod_313392 #container .span {
            margin-left: 5px;
            font-size: 11px;
        }

        .info-middle img {
            float: left;
            margin-right: 6px;
        }
        
</style>
@endsection
@section('topjs')
 
 
<script>
function SendLeaveword() {
    var sContact = $("#txtContact").val();
    var sMobile = $("#txtMobile").val();
    var sShortDesc = $("#txtShortDesc").val();
  
    var err = "";
    if (sContact == "" || sContact == "您的姓名") {
        err += "<p>请输入您的姓名</p>";
    }
    if (sMobile == "" || sMobile == "您的电话") {
        err += "<p>请输入您的电话</p>";
    }
    else if (sMobile.length > 0 && !PTN_Tel.test(sMobile)) {
        err += "<p>您的电话格式错误</p>";
    }

    if (err.length > 0) {
        $a(err);
        return;
    }
    
	$.ajaxSetup({
		headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' }
	});
    $.post("{{ route('subscribe') }}?random" + Math.random(), {
        username: sContact,
        phone: sMobile,
        comments: sShortDesc
    }, function (msg) {
         $a(msg.msg); 
    });
}
</script>
@endsection
@section('body', '')
@section('bodyid', '')
@section('main') 

@adv($adv group=57 row=1)
  <div class="inpage-banner" style="background:url({{ $adv->thumb}});" title="{{ $adv->title }}"></div>
@endadv
@include('theme::layouts.crumb')
   
  <div class="y-wrapper bgcolor clearfix">
 
 
    <div class="contact-us1200">
      <!-- linkUsMap -->
      <div class="linkUsMap">
        <div class="container">
          <div class="mainCon clearfix">

            <p style="text-align: center; margin-top: 5px;"><span style="font-size: 36px;"></span><br /></p>
            <h1 style="font-size: 26px;margin-top: 5px; text-align: center;">联系{{ webconfig('abbreviation') }}</h1>
            <p style="text-align: center; line-height: 1.75em;">——</p>
			@channel($about typeid=42 rows=1)		 
			{!! $about->body !!}
			@endchannel
            <div style="text-align: center; position: relative;">
              <table data-sort="sortDisabled" style="width: 435px; position: absolute;z-index: 9;  bottom: 0px;left: 0px;background: #fff; border-radius: 5px; box-shadow: 0px 12px 14px -13px rgb(0, 0, 0);z-index:9999;">
                <tbody>
                  <tr class="firstRow">
                    <td valign="middle" rowspan="1" colspan="2"
                      style="word-break: break-all;padding-left: 50px;padding-bottom: 20px;padding-top: 24px;"
                      width="364" align="left"><span style="font-size: 20px;">{{ webconfig('corporateName') }}</span></td>
                  </tr>
                  <tr style="border-bottom: 1px solid #f4f4f4;">
                    <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                      align="right">
                      <p><img src="/picture/712539bf0743d324.png" title="热线" alt="热线" /></p>
                    </td>
                    <td valign="middle" align="left" style="word-break: break-all;" width="313">服务热线：@400</td>
                  </tr>
                  <tr style="border-bottom: 1px solid #f4f4f4;">
                    <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                      align="right">
                      <p><img src="/picture/6089785caa1eb592.png" title="电话" alt="电话" /></p>
                    </td>
                    <td valign="middle" align="left" style="word-break: break-all;" width="313">联系电话：@phone</td>
                  </tr>
                  <tr style="border-bottom: 1px solid #f4f4f4;">
                    <td valign="middle" style="word-break: break-all;padding: 10px 10px 10px 10px;" width="30"
                      align="right"><img src="/picture/51058606ffc8f1ea.png" title="邮箱" alt="邮箱" /></td>
                    <td valign="middle" align="left" style="word-break: break-all;" width="313">电子邮箱：@email
                    </td>
                  </tr>
			 
                  <tr>
                    <td valign="middle" style="word-break: break-all;padding-bottom: 10px;padding: 10px 10px 10px 10px;"
                      width="30" align="right">
                      <p style="text-align:center"><img src="/picture/6087dbb8da9bcce8.png" title="地址" alt="地址" /></p>
                    </td>
                    <td valign="middle" align="left" style="word-break: break-all;padding-bottom: 10px;" width="313">
                      <p>公司地址： @address</p>
                    </td>
                  </tr>
                </tbody>
              </table>
			  
			    <div class="map" id="laymod_313392">
                  <div id="container" style="height:546px"></div>
                </div>
            </div>
          </div>
        </div>
      </div>
      <!-- linkUsLy-->
      <div class="linkUsLy">
        <div class="container">
		  @adv($linkUsLy group=73 rows=1)
          <div class="mainCon" style="background:url({{$linkUsLy->thumb}})">
            <div class="title">
              <h3>{{$linkUsLy->title}}</h3>
              <p>{{$linkUsLy->firsttitle}}</p>
            </div>
            <div class="box">
              <input type="text" value="您的姓名" class="input1" onfocus="if(this.value=='您的姓名'){this.value=''}"
                onblur="if(this.value==''){this.value='您的姓名'}" id="txtContact" />
              <input type="text" value="您的电话" class="input2" onfocus="if(this.value=='您的电话'){this.value=''}"
                onblur="if(this.value==''){this.value='您的电话'}" id="txtMobile" />
              <textarea onfocus="if(this.value=='留言内容'){this.value=''}" onblur="if(this.value==''){this.value='留言内容'}"
                id="txtShortDesc">留言内容</textarea>
              <a title="提交留言" onclick="SendLeaveword();" class="tj">提交留言</a>
            </div>
          </div>
		  @endadv
        </div>
      </div>
    </div>
     
  </div>

@endsection
@section('footerjs')
<script type="text/javascript" src="https://webapi.amap.com/maps?v=1.4.15&key=8325164e247e15eea68b59e89200988b"></script>
<script type="text/javascript">    
var map = new AMap.Map("container", {
    resizeEnable: true,
    center: [113.669498, 22.943401],
    zoom: 16
});
addMarker();
var title = '{{ webconfig("corporateName") }}',  content = [];
content.push("<img src='http://image.zhuomulao.com/1597126240_cc723e4a-c6fc-4614-9c5e-4bd4983a089f.jpg' width='100'>地址：{{ webconfig('address')}}");
content.push("联系人：@Contacts");
content.push("电话：@phone");
content.push("<a href='http://{{ webconfig("abbreviation") }}'>详细信息</a>");
var infoWindow = new AMap.InfoWindow({
    isCustom: true,
    content: createInfoWindow(title, content.join("<br/>")),
    offset: new AMap.Pixel(16, -45)
});



function createInfoWindow(title, content) {
    var info = document.createElement("div");
    info.className = "custom-info input-card content-window-card";
    var top = document.createElement("div");
    var titleD = document.createElement("div");
    var closeX = document.createElement("img");
    top.className = "info-top";
    titleD.innerHTML = title;
    closeX.src = "https://webapi.amap.com/images/close2.gif";
    closeX.onclick = closeInfoWindow;

    top.appendChild(titleD);
    top.appendChild(closeX);
    info.appendChild(top);



    var middle = document.createElement("div");
    middle.className = "info-middle";
    middle.style.backgroundColor = 'white';
    middle.innerHTML = content;
    info.appendChild(middle);


    var bottom = document.createElement("div");
    bottom.className = "info-bottom";
    bottom.style.position = 'relative';
    bottom.style.top = '0px';
    bottom.style.margin = '0 auto';
    var sharp = document.createElement("img");
    sharp.src = "https://webapi.amap.com/images/sharp.png";
    bottom.appendChild(sharp);
    info.appendChild(bottom);
    return info;
}


function closeInfoWindow() {
    map.clearInfoWindow();
}
 
function addMarker() {
    map.clearMap();
    var marker = new AMap.Marker({
        map: map,
        position: [113.669498, 22.943401]
    });
     
    AMap.event.addListener(marker, 'click', function () {
        infoWindow.open(map, marker.getPosition());
    });
}


</script>
@endsection